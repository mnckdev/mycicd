package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.modelsis.analyseselection.service.entity.TrancheAge;

import java.util.Optional;

public interface TrancheAgeRepository extends JpaRepository<TrancheAge,Integer> {

   Optional<TrancheAge>  findTrancheAgeByCode(String code );
}
