package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.Statut;
import sn.modelsis.analyseselection.service.repository.StatutRepository;

import java.util.List;
import java.util.Optional;

@Service
public class StatutService {

    private StatutRepository statutRepository;

    public StatutService(StatutRepository statutRepository){
        this.statutRepository=statutRepository;
    }

    public Statut newStatut(Statut statut){
        return  statutRepository.save(statut);
    }

    public Optional<Statut> findById(Integer id){
        return statutRepository.findById(id);
    }
    public Optional<Statut> findByCode(String code){
        return Optional.of(statutRepository.findStatutByCode(code));
    }
    public List<Statut> findAll(){
        return statutRepository.findAll();
    }
     public void deleteStatut(Integer id){
        statutRepository.deleteById(id);
     }


}
