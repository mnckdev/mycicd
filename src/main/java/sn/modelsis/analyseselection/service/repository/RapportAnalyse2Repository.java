package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.modelsis.analyseselection.service.entity.RapportAnalyse2;

import java.util.UUID;

public interface RapportAnalyse2Repository extends JpaRepository<RapportAnalyse2, UUID> {
}
