package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.FormeJuridique;
import sn.modelsis.analyseselection.service.repository.FormeJuridiqueRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class FormeJuridiqueService {

    private FormeJuridiqueRepository formeJuridiqueRepository;

    public FormeJuridiqueService(FormeJuridiqueRepository formeJuridique) {
        this.formeJuridiqueRepository = formeJuridique;
    }

    public FormeJuridique newFormeJuridique(FormeJuridique statut) {
        return formeJuridiqueRepository.save(statut);
    }

    public Optional<FormeJuridique> findById(UUID id) {
        return formeJuridiqueRepository.findById(id);
    }

    public Optional<FormeJuridique> findByCode(String code) {
        return Optional.of(formeJuridiqueRepository.findFormeJuridiqueByCode(code));
    }

    public List<FormeJuridique> findAll() {
        return formeJuridiqueRepository.findAll();
    }

    public void deleteFormeJuridique(UUID id) {
        formeJuridiqueRepository.deleteById(id);
    }


}
