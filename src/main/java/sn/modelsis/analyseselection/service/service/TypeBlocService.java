package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.TypeBloc;
import sn.modelsis.analyseselection.service.repository.TypeBlocRepository;

import java.util.List;
import java.util.Optional;
@Service
public class TypeBlocService {

    private TypeBlocRepository typeBlocRepository;

    public TypeBlocService(TypeBlocRepository typeBlocRepository){
        this.typeBlocRepository=typeBlocRepository;
    }

    public TypeBloc newTypeBloc(TypeBloc typeBloc){
        return  typeBlocRepository.save(typeBloc);
    }

    public Optional<TypeBloc> findById(Integer id){
        return typeBlocRepository.findById(id);
    }
    public List<TypeBloc> findAll(){
        return typeBlocRepository.findAll();
    }
    public void deleteTypeBloc(Integer id){
        typeBlocRepository.deleteById(id);
    }

}
