package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.CollectifsDTO;
import sn.modelsis.analyseselection.service.entity.Collectif;

@Mapper(componentModel = "spring")
public interface CollectifMapper extends GenericMapper<Collectif, CollectifsDTO>{
}
