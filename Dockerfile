FROM maven:3.8-jdk-11 as builder
WORKDIR /opt/app
COPY pom.xml .
RUN mvn dependency:go-offline
COPY ./src ./src
RUN mvn clean install -Dmaven.test.skip=true
ENV JAVA_HEAD = -Xms2048m -Xmx4096m

FROM openjdk:11
EXPOSE 8081
WORKDIR /app
COPY --from=builder /opt/app/target/analyse-et-selection-1.0.0-SNAPSHOT.jar /app/analyse.jar
ENTRYPOINT ["java","-Dspring.profiles.active=container","-jar","analyse.jar"]
