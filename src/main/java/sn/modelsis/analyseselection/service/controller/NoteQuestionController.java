package sn.modelsis.analyseselection.service.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.NoteQuestion;
import sn.modelsis.analyseselection.service.service.NoteQuestionService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/note-question")
@Api(
        value = "NoteQuestion REST endpoint",
        tags = "NoteQuestion controller(v1)"
)

public class NoteQuestionController {

    private final NoteQuestionService noteQuestionService;

    public NoteQuestionController(NoteQuestionService noteQuestionService) {
        this.noteQuestionService = noteQuestionService;
    }

    @GetMapping
    @ApiOperation(value = "Get all notes questions", notes = "Get all notes questions")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    public ResponseEntity<List<NoteQuestion>> findAll(){
        List<NoteQuestion> noteQuestions = noteQuestionService.read();
        if(noteQuestions == null)
            return ResponseEntity.badRequest().body(noteQuestions);
        return ResponseEntity.ok().body(noteQuestions);
    }

    @ApiOperation(value = "Get one note question", notes = "Get one note question")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<NoteQuestion>> get(@PathVariable UUID id){
        Optional<NoteQuestion> noteQuestion = noteQuestionService.read(id);
        if(noteQuestion== null)
            return  ResponseEntity.badRequest().body(noteQuestion);
        return ResponseEntity.ok().body(noteQuestion);
    }

    @PostMapping
    @ApiResponses(
            {
                    @ApiResponse(code=200, message = "created"),
            }
    )
    @ApiOperation(value = "post a note", notes = "post a note")
    // Devrait changer le retour de ce controller
    public ResponseEntity<NoteQuestion> create(@RequestBody NoteQuestion noteQuestion){
        noteQuestion = noteQuestionService.create(noteQuestion);
        return ResponseEntity.ok().body(noteQuestion);
    }

    @DeleteMapping("/{id}")
    @ApiResponses(
            {
                    @ApiResponse(code=200, message = "deleted"),
            }
    )
    @ApiOperation(value = "delete a note d'une question", notes = "delete a note d'une question")
    public ResponseEntity<String> delete(@PathVariable UUID id){
        noteQuestionService.delete(id);
        return ResponseEntity.ok().body("deleted");
    }
}
