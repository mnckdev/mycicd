package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.ZoneDTO;
import sn.modelsis.analyseselection.service.entity.Zone;

@Mapper(componentModel = "spring")
public interface ZoneMapper extends GenericMapper<Zone, ZoneDTO>{
    
}
