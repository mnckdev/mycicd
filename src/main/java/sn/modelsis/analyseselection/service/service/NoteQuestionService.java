package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.NoteQuestion;
import sn.modelsis.analyseselection.service.repository.NoteQuestionRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class NoteQuestionService {

    private final NoteQuestionRepository noteQuestionRepository;

    public NoteQuestionService(NoteQuestionRepository noteQuestionRepository) {
        this.noteQuestionRepository = noteQuestionRepository;
    }

    public NoteQuestion create(NoteQuestion noteQuestion){
        if (noteQuestion == null)
            return null;
        return noteQuestionRepository.save(noteQuestion);
    }

    public Optional<NoteQuestion> read(UUID uuid){
        if(uuid == null)
            return null;
        return  noteQuestionRepository.findById(uuid);
    }

    public List<NoteQuestion> read(){
        return noteQuestionRepository.findAll();
    }

    public void delete(UUID uuid){
        Optional<NoteQuestion> noteQuestion = noteQuestionRepository.findById(uuid);
        if(noteQuestion.isPresent())
            noteQuestionRepository.delete(noteQuestion.get());
    }
}
