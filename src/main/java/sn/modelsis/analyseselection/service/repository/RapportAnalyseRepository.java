package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.RapportAnalyse;

@Repository
public interface RapportAnalyseRepository extends JpaRepository<RapportAnalyse,Integer> {
}
