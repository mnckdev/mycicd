package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.ModelEconomique;
import sn.modelsis.analyseselection.service.repository.ModelEconomiqueRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ModelEconomiqueService {


    private ModelEconomiqueRepository modelEconomiqueRepository;

    public ModelEconomiqueService(ModelEconomiqueRepository modelEconomiqueRepository){
        this.modelEconomiqueRepository=modelEconomiqueRepository;
    }

    public ModelEconomique newModelEconomique(ModelEconomique modelEconomique){
        return  modelEconomiqueRepository.save(modelEconomique);
    }

    public Optional<ModelEconomique> findById(Integer id){
        return modelEconomiqueRepository.findById(id);
    }
    public List<ModelEconomique> findAll(){
        return modelEconomiqueRepository.findAll();
    }
    public void deleteModelEconomique(Integer id){
        modelEconomiqueRepository.deleteById(id);
    }

}
