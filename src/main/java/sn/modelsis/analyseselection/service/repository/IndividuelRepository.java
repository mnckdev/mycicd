package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.Cohorte;
import sn.modelsis.analyseselection.service.entity.Commune;
import sn.modelsis.analyseselection.service.entity.Individuel;
import sn.modelsis.analyseselection.service.entity.Statut;

import java.util.List;
import java.util.UUID;

@Repository
public interface IndividuelRepository extends JpaRepository<Individuel, UUID> {

    List<Individuel> findAllByTypeCategorieIgnoreCase(String typeCategorie);
    List<Individuel> findIndividuelByTypeCategorie(String typeCategorie);

    List<Individuel> findAllByCommuneRattachAndTypeCategorie(
            Commune commmune, String typeCategorie);
    List<Individuel> findAllByCommune(
            Commune commmune);

    List<Individuel> findAllByCommuneAndTypeCategorieIgnoreCase(
            Commune commune, String typeCategorie);

    List<Individuel> findAllByTypeCategorieIgnoreCaseAndStatut(String typeCategorie, Statut statut);

    List<Individuel> findAllByCohorteAndTypeCategorieIgnoreCase(Cohorte cohorte, String typeCategories);

    List<Individuel> findAllByCohorteAndTypeCategorieIgnoreCaseAndAndCommune(Cohorte cohorte, String typeCategories, Commune commune);

    List<Individuel> findAllByStatut(Statut statut);
}
