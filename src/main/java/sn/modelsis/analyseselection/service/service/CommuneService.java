package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.DTO.CommuneDTO;
import sn.modelsis.analyseselection.service.entity.Commune;
import sn.modelsis.analyseselection.service.mapper.CommuneMapper;
import sn.modelsis.analyseselection.service.repository.CommuneRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CommuneService {

    private final CommuneRepository communeRepository;
    private final CommuneMapper communeMapper;

    public CommuneService(CommuneRepository communeRepository, CommuneMapper communeMapper) {
        this.communeRepository = communeRepository;
        this.communeMapper = communeMapper;
    }

    public Optional<Commune> getCommune(Integer id){
        if(id == null)
            return null;
        return communeRepository.findById(id);
    }

    public List<CommuneDTO> getCommunes(){
        return communeMapper.asDTOList(communeRepository.findAll());
    }
}
