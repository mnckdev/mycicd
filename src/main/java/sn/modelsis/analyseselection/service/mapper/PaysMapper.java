package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.PaysDTO;
import sn.modelsis.analyseselection.service.entity.Pays;

@Mapper(componentModel = "spring")
public interface PaysMapper extends GenericMapper<Pays, PaysDTO>{
}
