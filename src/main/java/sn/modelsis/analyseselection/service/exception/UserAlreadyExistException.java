package sn.modelsis.analyseselection.service.exception;

public class UserAlreadyExistException extends RuntimeException{
    public UserAlreadyExistException(String message){
        super(message);
    } public UserAlreadyExistException(){
        super("the user already exist ");
    }
}
