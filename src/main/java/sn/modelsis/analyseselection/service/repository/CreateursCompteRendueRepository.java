package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.modelsis.analyseselection.service.entity.CreateursCompteRendue;

public interface CreateursCompteRendueRepository extends JpaRepository<CreateursCompteRendue,Integer> {
}
