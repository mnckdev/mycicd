package sn.modelsis.analyseselection.service.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

//import java.util.Date;
import java.sql.Time;
import java.util.UUID;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Rencontre {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private UUID idRencontre;

    @Temporal(TemporalType.DATE)
    private Date date;

//    @Temporal(TemporalType.TIME)
    private Time heure;

    private String lieu;

    private String commentaire;

//    @ManyToOne(cascade = CascadeType.ALL)
//    @JsonIgnore
//    private PIE pie;

}
