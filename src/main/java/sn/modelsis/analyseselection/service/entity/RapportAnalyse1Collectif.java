package sn.modelsis.analyseselection.service.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RapportAnalyse1Collectif {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private UUID id;

    @Column(length = 2000)
    private String identification;

    @Column(length = 2000)
    private String gouvernanceLeadership;

    @Column(length = 2000)
    private String membership;

    @Column(length = 2000)
    private String fonctionnement;

    @Column(length = 2000)
    private String patrimoineGestion;

    @Column(length = 2000)
    private String partenariat;

    @Column(length = 2000)
    private String commentaire;

    @Column(length = 2000)
    private String faiblesse;
}
