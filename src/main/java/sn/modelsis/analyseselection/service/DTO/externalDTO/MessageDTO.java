package sn.modelsis.analyseselection.service.DTO.externalDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MessageDTO {
    private String codeApp;

    private String objet;

    private String contenu;

    private String destinataire;

    private String expediteur;

    private String priorite;
}
