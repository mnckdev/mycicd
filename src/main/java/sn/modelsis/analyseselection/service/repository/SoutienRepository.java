package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.Soutien;

import java.util.UUID;

@Repository
public interface SoutienRepository extends JpaRepository<Soutien, UUID> {
}
