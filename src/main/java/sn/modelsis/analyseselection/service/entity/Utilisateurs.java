package sn.modelsis.analyseselection.service.entity;



import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Utilisateurs {
    @Id
    private Integer idUtilisateur;
    private  String prenom;
    private String nom;
    private String sexe;
    private String login;
    private  String pass;
    @OneToMany(mappedBy = "utilisateurs")
    private List<Profil> profils;
    @OneToMany(mappedBy = "utilisateurs")
    private List<Action> actions;
    @OneToOne
    private PIE pie;
}
