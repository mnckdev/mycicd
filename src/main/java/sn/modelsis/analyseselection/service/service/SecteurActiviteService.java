package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.SecteurActivite;
import sn.modelsis.analyseselection.service.repository.SecteurActiviteRepository;

import java.util.List;
import java.util.Optional;

@Service
public class SecteurActiviteService {

    private SecteurActiviteRepository secteurActiviteRepository;

    public SecteurActiviteService(SecteurActiviteRepository secteurActiviteRepository){
        this.secteurActiviteRepository=secteurActiviteRepository;
    }

    public SecteurActivite newSecteurActivite(SecteurActivite secteurActivite){
        return  secteurActiviteRepository.save(secteurActivite);
    }

    public Optional<SecteurActivite> findById(Integer id){
        return secteurActiviteRepository.findById(id);
    }
    public List<SecteurActivite> findAll(){
        return secteurActiviteRepository.findAll();
    }
    public void deleteSecteurActivite(Integer id){
        secteurActiviteRepository.deleteById(id);
    }
}
