--
-- PostgreSQL database dump
--

-- Dumped from database version 14.8
-- Dumped by pg_dump version 15.3

-- Started on 2023-11-02 10:54:41

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6 (class 2615 OID 189465)
-- Name: public; Type: SCHEMA; Schema: -; Owner: adel
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO adel;

--
-- TOC entry 3746 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: adel
--

COMMENT ON SCHEMA public IS '';


--
-- TOC entry 2 (class 3079 OID 189514)
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- TOC entry 3748 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- TOC entry 282 (class 1255 OID 189525)
-- Name: select_commune(); Type: PROCEDURE; Schema: public; Owner: adel
--

CREATE PROCEDURE public.select_commune()
    LANGUAGE plpgsql
    AS $$
		declare
		v_type_pie_developpeur varchar(255);
		v_type_pie_createur varchar(255);
begin
	   select i.idpie, c.idpie, commune_id, type_categorie
	   from individuel i, collectif c
	   where type_categorie is not null limit 9;
end;
$$;


ALTER PROCEDURE public.select_commune() OWNER TO adel;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 213 (class 1259 OID 190219)
-- Name: action; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.action (
    id_action uuid NOT NULL,
    description character varying(255),
    m_projet character varying(255),
    nature_action character varying(255),
    projet_id_projet uuid,
    utilisateurs_id_utilisateur integer
);


ALTER TABLE public.action OWNER TO adel;

--
-- TOC entry 211 (class 1259 OID 189531)
-- Name: aer; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.aer (
    id integer NOT NULL,
    prenom character varying(1000) NOT NULL,
    nom character varying(1000) NOT NULL,
    sexe character varying(500) NOT NULL,
    tel1 character varying(1000) NOT NULL,
    tel2 character varying(1000) NOT NULL,
    email character varying(1000) NOT NULL,
    commune character varying(1000) NOT NULL,
    role integer NOT NULL,
    structure_partenaire character varying(1000)
);


ALTER TABLE public.aer OWNER TO adel;

--
-- TOC entry 214 (class 1259 OID 190226)
-- Name: block_model_economique; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.block_model_economique (
    id_blocme uuid NOT NULL,
    code character varying(255),
    libelle character varying(255)
);


ALTER TABLE public.block_model_economique OWNER TO adel;

--
-- TOC entry 215 (class 1259 OID 190233)
-- Name: block_model_economique_sous_blocs; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.block_model_economique_sous_blocs (
    block_model_economique_id_blocme uuid NOT NULL,
    sous_blocs_id_sous_bloc uuid NOT NULL
);


ALTER TABLE public.block_model_economique_sous_blocs OWNER TO adel;

--
-- TOC entry 216 (class 1259 OID 190236)
-- Name: categories; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.categories (
    id integer NOT NULL,
    libelle character varying(255)
);


ALTER TABLE public.categories OWNER TO adel;

--
-- TOC entry 218 (class 1259 OID 190242)
-- Name: cohorte; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.cohorte (
    id_cohorte integer NOT NULL,
    date_debut date,
    date_fin date,
    process_instance_id character varying(255),
    statut character varying(255)
);


ALTER TABLE public.cohorte OWNER TO adel;

--
-- TOC entry 217 (class 1259 OID 190241)
-- Name: cohorte_id_cohorte_seq; Type: SEQUENCE; Schema: public; Owner: adel
--

CREATE SEQUENCE public.cohorte_id_cohorte_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cohorte_id_cohorte_seq OWNER TO adel;

--
-- TOC entry 3749 (class 0 OID 0)
-- Dependencies: 217
-- Name: cohorte_id_cohorte_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: adel
--

ALTER SEQUENCE public.cohorte_id_cohorte_seq OWNED BY public.cohorte.id_cohorte;


--
-- TOC entry 219 (class 1259 OID 190250)
-- Name: collectif; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.collectif (
    commune_activite_id integer,
    contact1_representant character varying(255),
    contact2_representant character varying(255),
    date_creation date,
    denomination character varying(255),
    lieu_siege character varying(255),
    montant_capital_social double precision  ,
    montant_endettement double precision default 0.0,
    montant_epargne_mobilisee double precision  ,
    montant_subvension_recu double precision  ,
    nbr_employe_permanent integer  ,
    nbr_employe_temporaire integer  ,
    quartier_village_interieur character varying(255),
    reconnaissance_juridique character varying(255),
    situation_economique character varying(255),
    titre character varying(255),
    categories character varying(255),
    total_membre integer  ,
    total_femmes integer  ,
    total_homme integer  ,
    idpie uuid NOT NULL,
    commune_id integer,
    couverture_geographique_id uuid,
    forme_juridique_id integer,
    profil_membre_id uuid,
    rapport_analyse1collectif_id uuid,
    rapport_analyse2collectif_id uuid,
    tranche_age_id integer
);


ALTER TABLE public.collectif OWNER TO adel;

--
-- TOC entry 220 (class 1259 OID 190257)
-- Name: commune; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.commune (
    id integer NOT NULL,
    code character varying(255),
    libelle character varying(255),
    departement_id integer
);


ALTER TABLE public.commune OWNER TO adel;

--
-- TOC entry 222 (class 1259 OID 190265)
-- Name: compte_rendue; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.compte_rendue (
    id_compte_rendue integer NOT NULL,
    cohorte character varying(255),
    commune character varying(255),
    id_document character varying(255),
    interventions_echanges character varying(2000),
    lieu character varying(255),
    nombre_idees integer NOT NULL,
    participants character varying(2000),
    periode date
);


ALTER TABLE public.compte_rendue OWNER TO adel;

--
-- TOC entry 223 (class 1259 OID 190273)
-- Name: compte_rendue_createurs_content; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.compte_rendue_createurs_content (
    compte_rendue_id_compte_rendue integer NOT NULL,
    createurs_content_id integer NOT NULL
);


ALTER TABLE public.compte_rendue_createurs_content OWNER TO adel;

--
-- TOC entry 221 (class 1259 OID 190264)
-- Name: compte_rendue_id_compte_rendue_seq; Type: SEQUENCE; Schema: public; Owner: adel
--

CREATE SEQUENCE public.compte_rendue_id_compte_rendue_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.compte_rendue_id_compte_rendue_seq OWNER TO adel;

--
-- TOC entry 3750 (class 0 OID 0)
-- Dependencies: 221
-- Name: compte_rendue_id_compte_rendue_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: adel
--

ALTER SEQUENCE public.compte_rendue_id_compte_rendue_seq OWNED BY public.compte_rendue.id_compte_rendue;


--
-- TOC entry 224 (class 1259 OID 190276)
-- Name: couverture_geographique; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.couverture_geographique (
    id uuid NOT NULL,
    code character varying(255),
    libelle character varying(255)
);


ALTER TABLE public.couverture_geographique OWNER TO adel;

--
-- TOC entry 226 (class 1259 OID 190284)
-- Name: createurs_compte_rendue; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.createurs_compte_rendue (
    id integer NOT NULL,
    commentaires character varying(2000),
    idees_priorite_commune character varying(2000),
    idees_prioritepie character varying(2000),
    nom character varying(255),
    prenom character varying(255)
);


ALTER TABLE public.createurs_compte_rendue OWNER TO adel;

--
-- TOC entry 225 (class 1259 OID 190283)
-- Name: createurs_compte_rendue_id_seq; Type: SEQUENCE; Schema: public; Owner: adel
--

CREATE SEQUENCE public.createurs_compte_rendue_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.createurs_compte_rendue_id_seq OWNER TO adel;

--
-- TOC entry 3751 (class 0 OID 0)
-- Dependencies: 225
-- Name: createurs_compte_rendue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: adel
--

ALTER SEQUENCE public.createurs_compte_rendue_id_seq OWNED BY public.createurs_compte_rendue.id;


--
-- TOC entry 227 (class 1259 OID 190292)
-- Name: departement; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.departement (
    id integer NOT NULL,
    code character varying(255),
    libelle character varying(255),
    region_id integer
);


ALTER TABLE public.departement OWNER TO adel;

--
-- TOC entry 228 (class 1259 OID 190299)
-- Name: documents; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.documents (
    id_document uuid NOT NULL,
    chemin character varying(255),
    date_creation timestamp without time zone,
    date_modification timestamp without time zone,
    etat_document boolean,
    nom_document integer,
    compte_rendue_id_compte_rendue integer,
    projet_id_projet uuid,
    type_document_id integer
);


ALTER TABLE public.documents OWNER TO adel;


--
-- TOC entry 229 (class 1259 OID 190304)
-- Name: forme_juridique; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.forme_juridique (
    id integer NOT NULL,
    code character varying(255),
    libelle character varying(255)
);


ALTER TABLE public.forme_juridique OWNER TO adel;

--
-- TOC entry 212 (class 1259 OID 190218)
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: adel
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO adel;

--
-- TOC entry 230 (class 1259 OID 190311)
-- Name: idee_projet; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.idee_projet (
    id_projet uuid NOT NULL,
    description_projet character varying(255)
);


ALTER TABLE public.idee_projet OWNER TO adel;

--
-- TOC entry 231 (class 1259 OID 190316)
-- Name: individuel; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.individuel (
    autre_region character varying(255),
    banque_ousfd character varying(255),
    beneficier_soutien boolean,
    experience_prof character varying(255),
    formation_prof character varying(255),
    hors_senegal character varying(255),
    ninea character varying(255),
    niveau_etude character varying(255),
    categories character varying(255),
    numero_rc character varying(255),
    ref_professionnel character varying(255),
    situation_actuel character varying(255),
    type_categorie character varying(255),
    idpie uuid NOT NULL,
    categories_id integer,
    commune_id integer,
    commune_rattach_id integer,
    rapport_analyse1_id_rapport uuid,
    rapport_analyse2_id uuid,
    tranche_age_id integer
);


ALTER TABLE public.individuel OWNER TO adel;

--
-- TOC entry 232 (class 1259 OID 190323)
-- Name: individuel_idee_projets; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.individuel_idee_projets (
    individuel_idpie uuid NOT NULL,
    idee_projets_id_projet uuid NOT NULL
);


ALTER TABLE public.individuel_idee_projets OWNER TO adel;

--
-- TOC entry 233 (class 1259 OID 190326)
-- Name: marqueur; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.marqueur (
    id integer NOT NULL,
    libelle character varying(255),
    parcours_id integer
);


ALTER TABLE public.marqueur OWNER TO adel;

--
-- TOC entry 234 (class 1259 OID 190331)
-- Name: model_economique; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.model_economique (
    id uuid NOT NULL,
    status character varying(255)
);


ALTER TABLE public.model_economique OWNER TO adel;

--
-- TOC entry 235 (class 1259 OID 190336)
-- Name: model_economique_block_model_economique; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.model_economique_block_model_economique (
    model_economique_id uuid NOT NULL,
    block_model_economique_id_blocme uuid NOT NULL
);


ALTER TABLE public.model_economique_block_model_economique OWNER TO adel;

--
-- TOC entry 236 (class 1259 OID 190339)
-- Name: note_question; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.note_question (
    id_note uuid NOT NULL,
    note integer NOT NULL,
    reponse character varying(255),
    question_id_question integer
);


ALTER TABLE public.note_question OWNER TO adel;

--
-- TOC entry 238 (class 1259 OID 190345)
-- Name: observation; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.observation (
    id_retour integer NOT NULL,
    auteur character varying(255),
    date_retour date,
    observations character varying(255),
    username_expediteur character varying(255),
    statut_id integer
);


ALTER TABLE public.observation OWNER TO adel;

--
-- TOC entry 237 (class 1259 OID 190344)
-- Name: observation_id_retour_seq; Type: SEQUENCE; Schema: public; Owner: adel
--

CREATE SEQUENCE public.observation_id_retour_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.observation_id_retour_seq OWNER TO adel;

--
-- TOC entry 3752 (class 0 OID 0)
-- Dependencies: 237
-- Name: observation_id_retour_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: adel
--

ALTER SEQUENCE public.observation_id_retour_seq OWNED BY public.observation.id_retour;


--
-- TOC entry 239 (class 1259 OID 190353)
-- Name: parcours; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.parcours (
    id integer NOT NULL,
    libelle character varying(255),
    sous_categories_id integer
);


ALTER TABLE public.parcours OWNER TO adel;

--
-- TOC entry 240 (class 1259 OID 190358)
-- Name: pays; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.pays (
    id integer NOT NULL,
    code character varying(255),
    libelle character varying(255)
);


ALTER TABLE public.pays OWNER TO adel;

--
-- TOC entry 241 (class 1259 OID 190365)
-- Name: phase; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.phase (
    id_phase integer NOT NULL,
    libelle character varying(255)
);


ALTER TABLE public.phase OWNER TO adel;

--
-- TOC entry 242 (class 1259 OID 190370)
-- Name: pie; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.pie (
    idpie uuid NOT NULL,
    date timestamp without time zone,
    domain_activite character varying(255),
    id_contrat_accompagnement character varying(255),
    inscritpar character varying(255),
    nom character varying(255),
    parcours character varying(255),
    prenom character varying(255),
    process_instance_id character varying(255),
    quartier character varying(255),
    secteur_activite character varying(255),
    sexe character varying(255),
    tel1 character varying(255),
    tel2 character varying(255),
    typepie character varying(255),
    variable_id character varying(255),
    cohorte_id_cohorte integer,
    projet_id_projet uuid,
    soutiens_immediat_id uuid,
    statut_id integer
);


ALTER TABLE public.pie OWNER TO adel;

--
-- TOC entry 243 (class 1259 OID 190377)
-- Name: pie_list_soutiens; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.pie_list_soutiens (
    pie_idpie uuid NOT NULL,
    list_soutiens_id uuid NOT NULL
);


ALTER TABLE public.pie_list_soutiens OWNER TO adel;

--
-- TOC entry 244 (class 1259 OID 190380)
-- Name: pie_note_questions; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.pie_note_questions (
    pie_idpie uuid NOT NULL,
    note_questions_id_note uuid NOT NULL
);


ALTER TABLE public.pie_note_questions OWNER TO adel;

--
-- TOC entry 245 (class 1259 OID 190383)
-- Name: pie_observations; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.pie_observations (
    pie_idpie uuid NOT NULL,
    observations_id_retour integer NOT NULL
);


ALTER TABLE public.pie_observations OWNER TO adel;

--
-- TOC entry 246 (class 1259 OID 190386)
-- Name: pie_rencontres; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.pie_rencontres (
    pie_idpie uuid NOT NULL,
    rencontres_id_rencontre uuid NOT NULL
);


ALTER TABLE public.pie_rencontres OWNER TO adel;

--
-- TOC entry 247 (class 1259 OID 190389)
-- Name: profil; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.profil (
    id_profil integer NOT NULL,
    libelle character varying(255),
    utilisateurs_id_utilisateur integer
);


ALTER TABLE public.profil OWNER TO adel;

--
-- TOC entry 248 (class 1259 OID 190394)
-- Name: profil_membre; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.profil_membre (
    id uuid NOT NULL,
    code character varying(255),
    libelle character varying(255)
);


ALTER TABLE public.profil_membre OWNER TO adel;

--
-- TOC entry 249 (class 1259 OID 190401)
-- Name: projet; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.projet (
    id_projet uuid NOT NULL,
    description character varying(255),
    information_supplementaire character varying(255),
    intitule_projet character varying(255),
    motif_retour character varying(255),
    objet integer,
    model_economique_id uuid,
    secteur_activite_id integer,
    statut_id integer,
    type_projet_id integer
);


ALTER TABLE public.projet OWNER TO adel;

--
-- TOC entry 250 (class 1259 OID 190408)
-- Name: questionnaire; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.questionnaire (
    id_question integer NOT NULL,
    bloc character varying(255),
    categorie character varying(255),
    code integer NOT NULL,
    libelle character varying(255),
    phase_id_phase integer,
    id_synthese_notation integer
);


ALTER TABLE public.questionnaire OWNER TO adel;

--
-- TOC entry 251 (class 1259 OID 190415)
-- Name: rapport_analyse; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.rapport_analyse (
    id_rapport uuid NOT NULL,
    activite character varying(2000),
    environnement character varying(2000),
    faiblesse character varying(2000),
    organisation character varying(2000),
    porteur character varying(2000)
);


ALTER TABLE public.rapport_analyse OWNER TO adel;

--
-- TOC entry 252 (class 1259 OID 190422)
-- Name: rapport_analyse1collectif; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.rapport_analyse1collectif (
    id uuid NOT NULL,
    commentaire character varying(2000),
    faiblesse character varying(2000),
    fonctionnement character varying(2000),
    gouvernance_leadership character varying(2000),
    identification character varying(2000),
    membership character varying(2000),
    partenariat character varying(2000),
    patrimoine_gestion character varying(2000)
);


ALTER TABLE public.rapport_analyse1collectif OWNER TO adel;

--
-- TOC entry 253 (class 1259 OID 190429)
-- Name: rapport_analyse2; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.rapport_analyse2 (
    id uuid NOT NULL,
    faiblesse character varying(2000),
    fonctionnement character varying(2000),
    gestion character varying(2000),
    pratiques_manageriales character varying(2000),
    ressources_financieres character varying(2000)
);


ALTER TABLE public.rapport_analyse2 OWNER TO adel;

--
-- TOC entry 254 (class 1259 OID 190436)
-- Name: rapport_analyse2collectif; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.rapport_analyse2collectif (
    id uuid NOT NULL,
    faiblesse character varying(2000),
    finances_depenses character varying(2000),
    finances_ressources character varying(2000),
    fonctionnement character varying(2000),
    modele_economique character varying(2000),
    partenariat character varying(2000)
);


ALTER TABLE public.rapport_analyse2collectif OWNER TO adel;

--
-- TOC entry 255 (class 1259 OID 190443)
-- Name: reference_note; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.reference_note (
    id_question integer NOT NULL,
    note integer NOT NULL,
    reponse character varying(255),
    question_id_question integer
);


ALTER TABLE public.reference_note OWNER TO adel;

--
-- TOC entry 256 (class 1259 OID 190448)
-- Name: region; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.region (
    id integer NOT NULL,
    code character varying(255),
    libelle character varying(255),
    pays_id integer,
    zone_id_zone integer
);


ALTER TABLE public.region OWNER TO adel;

--
-- TOC entry 257 (class 1259 OID 190455)
-- Name: rencontre; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.rencontre (
    id_rencontre uuid NOT NULL,
    commentaire character varying(255),
    date date,
    heure time without time zone,
    lieu character varying(255)
);


ALTER TABLE public.rencontre OWNER TO adel;

--
-- TOC entry 258 (class 1259 OID 190462)
-- Name: secteur_activite; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.secteur_activite (
    id integer NOT NULL,
    code character varying(255),
    libelle character varying(255)
);


ALTER TABLE public.secteur_activite OWNER TO adel;

--
-- TOC entry 259 (class 1259 OID 190469)
-- Name: sous_bloc; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.sous_bloc (
    id_sous_bloc uuid NOT NULL,
    valeur character varying(255)
);


ALTER TABLE public.sous_bloc OWNER TO adel;

--
-- TOC entry 260 (class 1259 OID 190474)
-- Name: sous_categories; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.sous_categories (
    id integer NOT NULL,
    libelle character varying(255),
    categories_id integer
);


ALTER TABLE public.sous_categories OWNER TO adel;

--
-- TOC entry 261 (class 1259 OID 190479)
-- Name: soutien; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.soutien (
    id integer NOT NULL,
    code character varying(255),
    libelle character varying(255)
);


ALTER TABLE public.soutien OWNER TO adel;

--
-- TOC entry 262 (class 1259 OID 190486)
-- Name: statut; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.statut (
    id integer NOT NULL,
    code character varying(255),
    libelle character varying(255)
);


ALTER TABLE public.statut OWNER TO adel;

--
-- TOC entry 263 (class 1259 OID 190493)
-- Name: synthese_notation; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.synthese_notation (
    id_synthese_notation integer NOT NULL,
    categorie character varying(255),
    libelle character varying(255),
    note integer NOT NULL
);


ALTER TABLE public.synthese_notation OWNER TO adel;

--
-- TOC entry 264 (class 1259 OID 190500)
-- Name: tranche_age; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.tranche_age (
    id integer NOT NULL,
    code character varying(255),
    description character varying(255),
    libelle character varying(255)
);


ALTER TABLE public.tranche_age OWNER TO adel;

--
-- TOC entry 265 (class 1259 OID 190507)
-- Name: type_bloc; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.type_bloc (
    id integer NOT NULL,
    libelle character varying(255),
    block_model_economique_id_blocme uuid
);


ALTER TABLE public.type_bloc OWNER TO adel;

--
-- TOC entry 266 (class 1259 OID 190512)
-- Name: type_document; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.type_document (
    id integer NOT NULL,
    code character varying(255),
    nom character varying(255)
);


ALTER TABLE public.type_document OWNER TO adel;

--
-- TOC entry 267 (class 1259 OID 190519)
-- Name: type_projet; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.type_projet (
    id integer NOT NULL,
    code character varying(255),
    libelle character varying(255)
);


ALTER TABLE public.type_projet OWNER TO adel;

--
-- TOC entry 268 (class 1259 OID 190526)
-- Name: type_soutien; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.type_soutien (
    id uuid NOT NULL,
    code character varying(255),
    description character varying(255),
    libelle character varying(255)
);


ALTER TABLE public.type_soutien OWNER TO adel;

--
-- TOC entry 269 (class 1259 OID 190533)
-- Name: utilisateurs; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.utilisateurs (
    id_utilisateur integer NOT NULL,
    login character varying(255),
    nom character varying(255),
    pass character varying(255),
    prenom character varying(255),
    sexe character varying(255),
    pie_idpie uuid
);


ALTER TABLE public.utilisateurs OWNER TO adel;

--
-- TOC entry 271 (class 1259 OID 190541)
-- Name: zone; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE public.zone (
    id_zone integer NOT NULL,
    actif boolean,
    libelle character varying(255)
);


ALTER TABLE public.zone OWNER TO adel;

--
-- TOC entry 270 (class 1259 OID 190540)
-- Name: zone_id_zone_seq; Type: SEQUENCE; Schema: public; Owner: adel
--

CREATE SEQUENCE public.zone_id_zone_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.zone_id_zone_seq OWNER TO adel;

--
-- TOC entry 3753 (class 0 OID 0)
-- Dependencies: 270
-- Name: zone_id_zone_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: adel
--

ALTER SEQUENCE public.zone_id_zone_seq OWNED BY public.zone.id_zone;


--
-- TOC entry 3425 (class 2604 OID 190245)
-- Name: cohorte id_cohorte; Type: DEFAULT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.cohorte ALTER COLUMN id_cohorte SET DEFAULT nextval('public.cohorte_id_cohorte_seq'::regclass);


--
-- TOC entry 3426 (class 2604 OID 190268)
-- Name: compte_rendue id_compte_rendue; Type: DEFAULT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.compte_rendue ALTER COLUMN id_compte_rendue SET DEFAULT nextval('public.compte_rendue_id_compte_rendue_seq'::regclass);


--
-- TOC entry 3427 (class 2604 OID 190287)
-- Name: createurs_compte_rendue id; Type: DEFAULT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.createurs_compte_rendue ALTER COLUMN id SET DEFAULT nextval('public.createurs_compte_rendue_id_seq'::regclass);


--
-- TOC entry 3428 (class 2604 OID 190348)
-- Name: observation id_retour; Type: DEFAULT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.observation ALTER COLUMN id_retour SET DEFAULT nextval('public.observation_id_retour_seq'::regclass);


--
-- TOC entry 3429 (class 2604 OID 190544)
-- Name: zone id_zone; Type: DEFAULT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.zone ALTER COLUMN id_zone SET DEFAULT nextval('public.zone_id_zone_seq'::regclass);


--
-- TOC entry 3436 (class 2606 OID 190225)
-- Name: action action_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.action
    ADD CONSTRAINT action_pkey PRIMARY KEY (id_action);


--
-- TOC entry 3434 (class 2606 OID 189776)
-- Name: aer aer_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.aer
    ADD CONSTRAINT aer_pkey PRIMARY KEY (id);


--
-- TOC entry 3438 (class 2606 OID 190232)
-- Name: block_model_economique block_model_economique_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.block_model_economique
    ADD CONSTRAINT block_model_economique_pkey PRIMARY KEY (id_blocme);


--
-- TOC entry 3442 (class 2606 OID 190240)
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- TOC entry 3444 (class 2606 OID 190249)
-- Name: cohorte cohorte_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.cohorte
    ADD CONSTRAINT cohorte_pkey PRIMARY KEY (id_cohorte);


--
-- TOC entry 3446 (class 2606 OID 190256)
-- Name: collectif collectif_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.collectif
    ADD CONSTRAINT collectif_pkey PRIMARY KEY (idpie);


--
-- TOC entry 3448 (class 2606 OID 190263)
-- Name: commune commune_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.commune
    ADD CONSTRAINT commune_pkey PRIMARY KEY (id);


--
-- TOC entry 3450 (class 2606 OID 190272)
-- Name: compte_rendue compte_rendue_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.compte_rendue
    ADD CONSTRAINT compte_rendue_pkey PRIMARY KEY (id_compte_rendue);


--
-- TOC entry 3454 (class 2606 OID 190282)
-- Name: couverture_geographique couverture_geographique_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.couverture_geographique
    ADD CONSTRAINT couverture_geographique_pkey PRIMARY KEY (id);


--
-- TOC entry 3456 (class 2606 OID 190291)
-- Name: createurs_compte_rendue createurs_compte_rendue_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.createurs_compte_rendue
    ADD CONSTRAINT createurs_compte_rendue_pkey PRIMARY KEY (id);


--
-- TOC entry 3458 (class 2606 OID 190298)
-- Name: departement departement_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.departement
    ADD CONSTRAINT departement_pkey PRIMARY KEY (id);


--
-- TOC entry 3460 (class 2606 OID 190303)
-- Name: documents documents_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.documents
    ADD CONSTRAINT documents_pkey PRIMARY KEY (id_document);




--
-- TOC entry 3462 (class 2606 OID 190310)
-- Name: forme_juridique forme_juridique_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.forme_juridique
    ADD CONSTRAINT forme_juridique_pkey PRIMARY KEY (id);


--
-- TOC entry 3464 (class 2606 OID 190315)
-- Name: idee_projet idee_projet_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.idee_projet
    ADD CONSTRAINT idee_projet_pkey PRIMARY KEY (id_projet);


--
-- TOC entry 3466 (class 2606 OID 190322)
-- Name: individuel individuel_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.individuel
    ADD CONSTRAINT individuel_pkey PRIMARY KEY (idpie);


--
-- TOC entry 3470 (class 2606 OID 190330)
-- Name: marqueur marqueur_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.marqueur
    ADD CONSTRAINT marqueur_pkey PRIMARY KEY (id);


--
-- TOC entry 3472 (class 2606 OID 190335)
-- Name: model_economique model_economique_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.model_economique
    ADD CONSTRAINT model_economique_pkey PRIMARY KEY (id);


--
-- TOC entry 3476 (class 2606 OID 190343)
-- Name: note_question note_question_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.note_question
    ADD CONSTRAINT note_question_pkey PRIMARY KEY (id_note);


--
-- TOC entry 3478 (class 2606 OID 190352)
-- Name: observation observation_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.observation
    ADD CONSTRAINT observation_pkey PRIMARY KEY (id_retour);


--
-- TOC entry 3480 (class 2606 OID 190357)
-- Name: parcours parcours_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.parcours
    ADD CONSTRAINT parcours_pkey PRIMARY KEY (id);


--
-- TOC entry 3482 (class 2606 OID 190364)
-- Name: pays pays_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.pays
    ADD CONSTRAINT pays_pkey PRIMARY KEY (id);


--
-- TOC entry 3484 (class 2606 OID 190369)
-- Name: phase phase_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.phase
    ADD CONSTRAINT phase_pkey PRIMARY KEY (id_phase);


--
-- TOC entry 3486 (class 2606 OID 190376)
-- Name: pie pie_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.pie
    ADD CONSTRAINT pie_pkey PRIMARY KEY (idpie);


--
-- TOC entry 3498 (class 2606 OID 190400)
-- Name: profil_membre profil_membre_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.profil_membre
    ADD CONSTRAINT profil_membre_pkey PRIMARY KEY (id);


--
-- TOC entry 3496 (class 2606 OID 190393)
-- Name: profil profil_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.profil
    ADD CONSTRAINT profil_pkey PRIMARY KEY (id_profil);


--
-- TOC entry 3500 (class 2606 OID 190407)
-- Name: projet projet_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.projet
    ADD CONSTRAINT projet_pkey PRIMARY KEY (id_projet);


--
-- TOC entry 3502 (class 2606 OID 190414)
-- Name: questionnaire questionnaire_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.questionnaire
    ADD CONSTRAINT questionnaire_pkey PRIMARY KEY (id_question);


--
-- TOC entry 3506 (class 2606 OID 190428)
-- Name: rapport_analyse1collectif rapport_analyse1collectif_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.rapport_analyse1collectif
    ADD CONSTRAINT rapport_analyse1collectif_pkey PRIMARY KEY (id);


--
-- TOC entry 3508 (class 2606 OID 190435)
-- Name: rapport_analyse2 rapport_analyse2_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.rapport_analyse2
    ADD CONSTRAINT rapport_analyse2_pkey PRIMARY KEY (id);


--
-- TOC entry 3510 (class 2606 OID 190442)
-- Name: rapport_analyse2collectif rapport_analyse2collectif_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.rapport_analyse2collectif
    ADD CONSTRAINT rapport_analyse2collectif_pkey PRIMARY KEY (id);


--
-- TOC entry 3504 (class 2606 OID 190421)
-- Name: rapport_analyse rapport_analyse_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.rapport_analyse
    ADD CONSTRAINT rapport_analyse_pkey PRIMARY KEY (id_rapport);


--
-- TOC entry 3512 (class 2606 OID 190447)
-- Name: reference_note reference_note_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.reference_note
    ADD CONSTRAINT reference_note_pkey PRIMARY KEY (id_question);


--
-- TOC entry 3514 (class 2606 OID 190454)
-- Name: region region_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.region
    ADD CONSTRAINT region_pkey PRIMARY KEY (id);


--
-- TOC entry 3516 (class 2606 OID 190461)
-- Name: rencontre rencontre_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.rencontre
    ADD CONSTRAINT rencontre_pkey PRIMARY KEY (id_rencontre);


--
-- TOC entry 3518 (class 2606 OID 190468)
-- Name: secteur_activite secteur_activite_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.secteur_activite
    ADD CONSTRAINT secteur_activite_pkey PRIMARY KEY (id);


--
-- TOC entry 3520 (class 2606 OID 190473)
-- Name: sous_bloc sous_bloc_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.sous_bloc
    ADD CONSTRAINT sous_bloc_pkey PRIMARY KEY (id_sous_bloc);


--
-- TOC entry 3522 (class 2606 OID 190478)
-- Name: sous_categories sous_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.sous_categories
    ADD CONSTRAINT sous_categories_pkey PRIMARY KEY (id);


--
-- TOC entry 3524 (class 2606 OID 190485)
-- Name: soutien soutien_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.soutien
    ADD CONSTRAINT soutien_pkey PRIMARY KEY (id);


--
-- TOC entry 3526 (class 2606 OID 190492)
-- Name: statut statut_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.statut
    ADD CONSTRAINT statut_pkey PRIMARY KEY (id);


--
-- TOC entry 3528 (class 2606 OID 190499)
-- Name: synthese_notation synthese_notation_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.synthese_notation
    ADD CONSTRAINT synthese_notation_pkey PRIMARY KEY (id_synthese_notation);


--
-- TOC entry 3530 (class 2606 OID 190506)
-- Name: tranche_age tranche_age_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.tranche_age
    ADD CONSTRAINT tranche_age_pkey PRIMARY KEY (id);


--
-- TOC entry 3532 (class 2606 OID 190511)
-- Name: type_bloc type_bloc_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.type_bloc
    ADD CONSTRAINT type_bloc_pkey PRIMARY KEY (id);


--
-- TOC entry 3534 (class 2606 OID 190518)
-- Name: type_document type_document_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.type_document
    ADD CONSTRAINT type_document_pkey PRIMARY KEY (id);


--
-- TOC entry 3536 (class 2606 OID 190525)
-- Name: type_projet type_projet_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.type_projet
    ADD CONSTRAINT type_projet_pkey PRIMARY KEY (id);


--
-- TOC entry 3538 (class 2606 OID 190532)
-- Name: type_soutien type_soutien_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.type_soutien
    ADD CONSTRAINT type_soutien_pkey PRIMARY KEY (id);


--
-- TOC entry 3490 (class 2606 OID 190558)
-- Name: pie_note_questions uk_4giuyd652hh1ywjl6xhdscqxr; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.pie_note_questions
    ADD CONSTRAINT uk_4giuyd652hh1ywjl6xhdscqxr UNIQUE (note_questions_id_note);


--
-- TOC entry 3452 (class 2606 OID 190550)
-- Name: compte_rendue_createurs_content uk_4t7yya8mc1an2c4dg5dh0n64s; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.compte_rendue_createurs_content
    ADD CONSTRAINT uk_4t7yya8mc1an2c4dg5dh0n64s UNIQUE (createurs_content_id);


--
-- TOC entry 3494 (class 2606 OID 190562)
-- Name: pie_rencontres uk_7beqcqbsfma9nr5wxnl9wvpfq; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.pie_rencontres
    ADD CONSTRAINT uk_7beqcqbsfma9nr5wxnl9wvpfq UNIQUE (rencontres_id_rencontre);


--
-- TOC entry 3488 (class 2606 OID 190556)
-- Name: pie_list_soutiens uk_eiq7vam6vdblh4xcrtm4lqtjf; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.pie_list_soutiens
    ADD CONSTRAINT uk_eiq7vam6vdblh4xcrtm4lqtjf UNIQUE (list_soutiens_id);


--
-- TOC entry 3440 (class 2606 OID 190548)
-- Name: block_model_economique_sous_blocs uk_f6a9kw4o8tlyiwtysrja5g0u7; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.block_model_economique_sous_blocs
    ADD CONSTRAINT uk_f6a9kw4o8tlyiwtysrja5g0u7 UNIQUE (sous_blocs_id_sous_bloc);


--
-- TOC entry 3492 (class 2606 OID 190560)
-- Name: pie_observations uk_lbd5fjh2klpd2b6ggtdk1efwx; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.pie_observations
    ADD CONSTRAINT uk_lbd5fjh2klpd2b6ggtdk1efwx UNIQUE (observations_id_retour);


--
-- TOC entry 3474 (class 2606 OID 190554)
-- Name: model_economique_block_model_economique uk_mqixvu2ducn1xxxxs7mfm7dl2; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.model_economique_block_model_economique
    ADD CONSTRAINT uk_mqixvu2ducn1xxxxs7mfm7dl2 UNIQUE (block_model_economique_id_blocme);


--
-- TOC entry 3468 (class 2606 OID 190552)
-- Name: individuel_idee_projets uk_pymv6x6u2f1aaur1w3dbpbevv; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.individuel_idee_projets
    ADD CONSTRAINT uk_pymv6x6u2f1aaur1w3dbpbevv UNIQUE (idee_projets_id_projet);


--
-- TOC entry 3540 (class 2606 OID 190539)
-- Name: utilisateurs utilisateurs_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.utilisateurs
    ADD CONSTRAINT utilisateurs_pkey PRIMARY KEY (id_utilisateur);


--
-- TOC entry 3542 (class 2606 OID 190546)
-- Name: zone zone_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.zone
    ADD CONSTRAINT zone_pkey PRIMARY KEY (id_zone);




--
-- TOC entry 3547 (class 2606 OID 190608)
-- Name: collectif fk154a5b8dhepf61asp8uw1wpl1; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.collectif
    ADD CONSTRAINT fk154a5b8dhepf61asp8uw1wpl1 FOREIGN KEY (rapport_analyse2collectif_id) REFERENCES public.rapport_analyse2collectif(id);


--
-- TOC entry 3548 (class 2606 OID 190583)
-- Name: collectif fk18esq2tsj3cgmb2ai0kyufgaw; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.collectif
    ADD CONSTRAINT fk18esq2tsj3cgmb2ai0kyufgaw FOREIGN KEY (commune_id) REFERENCES public.commune(id);


--
-- TOC entry 3562 (class 2606 OID 190673)
-- Name: individuel fk1m3ynfk4kw9me8ay2jqneubxv; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.individuel
    ADD CONSTRAINT fk1m3ynfk4kw9me8ay2jqneubxv FOREIGN KEY (rapport_analyse1_id_rapport) REFERENCES public.rapport_analyse(id_rapport);


--
-- TOC entry 3549 (class 2606 OID 190613)
-- Name: collectif fk1tqm3mbqdljj0xvdyfstqqw1o; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.collectif
    ADD CONSTRAINT fk1tqm3mbqdljj0xvdyfstqqw1o FOREIGN KEY (tranche_age_id) REFERENCES public.tranche_age(id);


--
-- TOC entry 3572 (class 2606 OID 190713)
-- Name: model_economique_block_model_economique fk24371laycw78dfxr8wgobytxr; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.model_economique_block_model_economique
    ADD CONSTRAINT fk24371laycw78dfxr8wgobytxr FOREIGN KEY (model_economique_id) REFERENCES public.model_economique(id);


--
-- TOC entry 3563 (class 2606 OID 190688)
-- Name: individuel fk296j21280pmo0pxvn7ehiy10w; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.individuel
    ADD CONSTRAINT fk296j21280pmo0pxvn7ehiy10w FOREIGN KEY (idpie) REFERENCES public.pie(idpie);


--
-- TOC entry 3594 (class 2606 OID 190823)
-- Name: questionnaire fk2wecsvrj7ab3r6jjm1rtidrad; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.questionnaire
    ADD CONSTRAINT fk2wecsvrj7ab3r6jjm1rtidrad FOREIGN KEY (id_synthese_notation) REFERENCES public.synthese_notation(id_synthese_notation);


--
-- TOC entry 3577 (class 2606 OID 190743)
-- Name: pie fk3fg0mqhppsda0ko4mvlguxaae; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.pie
    ADD CONSTRAINT fk3fg0mqhppsda0ko4mvlguxaae FOREIGN KEY (soutiens_immediat_id) REFERENCES public.type_soutien(id);


--
-- TOC entry 3550 (class 2606 OID 190598)
-- Name: collectif fk3gct97g2an09drjas2vk8tg4g; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.collectif
    ADD CONSTRAINT fk3gct97g2an09drjas2vk8tg4g FOREIGN KEY (profil_membre_id) REFERENCES public.profil_membre(id);


--
-- TOC entry 3578 (class 2606 OID 190738)
-- Name: pie fk3xl3x1glfopq7uf9hyt81i7vv; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.pie
    ADD CONSTRAINT fk3xl3x1glfopq7uf9hyt81i7vv FOREIGN KEY (projet_id_projet) REFERENCES public.projet(id_projet);


--
-- TOC entry 3597 (class 2606 OID 190833)
-- Name: region fk3ypxrvo06oogq5orufss0bxdt; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.region
    ADD CONSTRAINT fk3ypxrvo06oogq5orufss0bxdt FOREIGN KEY (pays_id) REFERENCES public.pays(id);


--
-- TOC entry 3590 (class 2606 OID 190798)
-- Name: projet fk45exqutagxt17mbr4sb95iqjt; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.projet
    ADD CONSTRAINT fk45exqutagxt17mbr4sb95iqjt FOREIGN KEY (model_economique_id) REFERENCES public.model_economique(id);


--
-- TOC entry 3573 (class 2606 OID 190708)
-- Name: model_economique_block_model_economique fk45udhbsvah83117nr87yhqsax; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.model_economique_block_model_economique
    ADD CONSTRAINT fk45udhbsvah83117nr87yhqsax FOREIGN KEY (block_model_economique_id_blocme) REFERENCES public.block_model_economique(id_blocme);


--
-- TOC entry 3581 (class 2606 OID 190753)
-- Name: pie_list_soutiens fk48kinkwl3ps8jf4pwhyp57w8w; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.pie_list_soutiens
    ADD CONSTRAINT fk48kinkwl3ps8jf4pwhyp57w8w FOREIGN KEY (list_soutiens_id) REFERENCES public.type_soutien(id);


--
-- TOC entry 3551 (class 2606 OID 190618)
-- Name: collectif fk58cx9h94ykgjfwkhkmwhemknk; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.collectif
    ADD CONSTRAINT fk58cx9h94ykgjfwkhkmwhemknk FOREIGN KEY (idpie) REFERENCES public.pie(idpie);


--
-- TOC entry 3583 (class 2606 OID 190768)
-- Name: pie_note_questions fk5l1ll3ewbpfy5ka9a3v9pqc3u; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.pie_note_questions
    ADD CONSTRAINT fk5l1ll3ewbpfy5ka9a3v9pqc3u FOREIGN KEY (pie_idpie) REFERENCES public.pie(idpie);


--
-- TOC entry 3571 (class 2606 OID 190703)
-- Name: marqueur fk7di9unsmeio1eqacp2l3puchj; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.marqueur
    ADD CONSTRAINT fk7di9unsmeio1eqacp2l3puchj FOREIGN KEY (parcours_id) REFERENCES public.parcours(id);


--
-- TOC entry 3543 (class 2606 OID 190563)
-- Name: action fk82s3vjnanvf91bxuggim7k47x; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.action
    ADD CONSTRAINT fk82s3vjnanvf91bxuggim7k47x FOREIGN KEY (projet_id_projet) REFERENCES public.projet(id_projet);


--
-- TOC entry 3559 (class 2606 OID 190653)
-- Name: documents fk9x6j51mtaj0ot3hhtpprq7phy; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.documents
    ADD CONSTRAINT fk9x6j51mtaj0ot3hhtpprq7phy FOREIGN KEY (type_document_id) REFERENCES public.type_document(id);


--
-- TOC entry 3552 (class 2606 OID 190593)
-- Name: collectif fka8gx0hw3oj4ufajv39x9ycd6w; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.collectif
    ADD CONSTRAINT fka8gx0hw3oj4ufajv39x9ycd6w FOREIGN KEY (forme_juridique_id) REFERENCES public.forme_juridique(id);


--
-- TOC entry 3545 (class 2606 OID 190578)
-- Name: block_model_economique_sous_blocs fka8u49x09xakvf7uusy5pu8xtg; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.block_model_economique_sous_blocs
    ADD CONSTRAINT fka8u49x09xakvf7uusy5pu8xtg FOREIGN KEY (block_model_economique_id_blocme) REFERENCES public.block_model_economique(id_blocme);


--
-- TOC entry 3569 (class 2606 OID 190698)
-- Name: individuel_idee_projets fkbctvu3rwyarj9ag27se4xe4ta; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.individuel_idee_projets
    ADD CONSTRAINT fkbctvu3rwyarj9ag27se4xe4ta FOREIGN KEY (individuel_idpie) REFERENCES public.individuel(idpie);


--
-- TOC entry 3564 (class 2606 OID 190668)
-- Name: individuel fkbhssgyxh7nbiq2guwhw134s3j; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.individuel
    ADD CONSTRAINT fkbhssgyxh7nbiq2guwhw134s3j FOREIGN KEY (commune_rattach_id) REFERENCES public.commune(id);


--
-- TOC entry 3565 (class 2606 OID 190663)
-- Name: individuel fkbn6dnu915x18b0uteoulsxjn3; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.individuel
    ADD CONSTRAINT fkbn6dnu915x18b0uteoulsxjn3 FOREIGN KEY (commune_id) REFERENCES public.commune(id);


--
-- TOC entry 3566 (class 2606 OID 190678)
-- Name: individuel fkc624xuf8xm8ccswvp4w9fr7j7; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.individuel
    ADD CONSTRAINT fkc624xuf8xm8ccswvp4w9fr7j7 FOREIGN KEY (rapport_analyse2_id) REFERENCES public.rapport_analyse2(id);


--
-- TOC entry 3567 (class 2606 OID 190658)
-- Name: individuel fkcr2bxinevp4qc6tyw5905nm99; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.individuel
    ADD CONSTRAINT fkcr2bxinevp4qc6tyw5905nm99 FOREIGN KEY (categories_id) REFERENCES public.categories(id);


--
-- TOC entry 3585 (class 2606 OID 190778)
-- Name: pie_observations fkdpt6m96tguvqxra99blrcq1ma; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.pie_observations
    ADD CONSTRAINT fkdpt6m96tguvqxra99blrcq1ma FOREIGN KEY (pie_idpie) REFERENCES public.pie(idpie);


--
-- TOC entry 3544 (class 2606 OID 190568)
-- Name: action fke1kjovkjb96pmevbft98yolv8; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.action
    ADD CONSTRAINT fke1kjovkjb96pmevbft98yolv8 FOREIGN KEY (utilisateurs_id_utilisateur) REFERENCES public.utilisateurs(id_utilisateur);


--
-- TOC entry 3579 (class 2606 OID 190748)
-- Name: pie fke3sruj3twgc5dmae4fhyvki14; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.pie
    ADD CONSTRAINT fke3sruj3twgc5dmae4fhyvki14 FOREIGN KEY (statut_id) REFERENCES public.statut(id);


--
-- TOC entry 3596 (class 2606 OID 190828)
-- Name: reference_note fke87pruyr23f4bgckir1astwx8; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.reference_note
    ADD CONSTRAINT fke87pruyr23f4bgckir1astwx8 FOREIGN KEY (question_id_question) REFERENCES public.questionnaire(id_question);


--
-- TOC entry 3587 (class 2606 OID 190788)
-- Name: pie_rencontres fkekdaomdqcor5qgcdsj79q31gk; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.pie_rencontres
    ADD CONSTRAINT fkekdaomdqcor5qgcdsj79q31gk FOREIGN KEY (pie_idpie) REFERENCES public.pie(idpie);


--
-- TOC entry 3588 (class 2606 OID 190783)
-- Name: pie_rencontres fkf3gl1g1orxub1ubtvwgc09scj; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.pie_rencontres
    ADD CONSTRAINT fkf3gl1g1orxub1ubtvwgc09scj FOREIGN KEY (rencontres_id_rencontre) REFERENCES public.rencontre(id_rencontre);


--
-- TOC entry 3582 (class 2606 OID 190758)
-- Name: pie_list_soutiens fkf7hi6ykrfnam8hmlngtxwgto9; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.pie_list_soutiens
    ADD CONSTRAINT fkf7hi6ykrfnam8hmlngtxwgto9 FOREIGN KEY (pie_idpie) REFERENCES public.pie(idpie);


--
-- TOC entry 3598 (class 2606 OID 190838)
-- Name: region fkfemnybtq2eheo8nji1xqukpqt; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.region
    ADD CONSTRAINT fkfemnybtq2eheo8nji1xqukpqt FOREIGN KEY (zone_id_zone) REFERENCES public.zone(id_zone);


--
-- TOC entry 3560 (class 2606 OID 190648)
-- Name: documents fkfydy7aum55k7mdsfdfag6qr95; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.documents
    ADD CONSTRAINT fkfydy7aum55k7mdsfdfag6qr95 FOREIGN KEY (projet_id_projet) REFERENCES public.projet(id_projet);


--
-- TOC entry 3575 (class 2606 OID 190723)
-- Name: observation fkgv9rkpjiwlb21i5pnf6i4parm; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.observation
    ADD CONSTRAINT fkgv9rkpjiwlb21i5pnf6i4parm FOREIGN KEY (statut_id) REFERENCES public.statut(id);


--
-- TOC entry 3556 (class 2606 OID 190628)
-- Name: compte_rendue_createurs_content fkgxi1c6a6usk7c55smjgquu24l; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.compte_rendue_createurs_content
    ADD CONSTRAINT fkgxi1c6a6usk7c55smjgquu24l FOREIGN KEY (createurs_content_id) REFERENCES public.createurs_compte_rendue(id);


--
-- TOC entry 3558 (class 2606 OID 190638)
-- Name: departement fkh8gowqmuphi5bw438h1ikoal8; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.departement
    ADD CONSTRAINT fkh8gowqmuphi5bw438h1ikoal8 FOREIGN KEY (region_id) REFERENCES public.region(id);


--
-- TOC entry 3568 (class 2606 OID 190683)
-- Name: individuel fki0p81xff4fsk73n22pqjp3on5; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.individuel
    ADD CONSTRAINT fki0p81xff4fsk73n22pqjp3on5 FOREIGN KEY (tranche_age_id) REFERENCES public.tranche_age(id);


--
-- TOC entry 3586 (class 2606 OID 190773)
-- Name: pie_observations fki2sp2q2w2v13pshyq300l7ukc; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.pie_observations
    ADD CONSTRAINT fki2sp2q2w2v13pshyq300l7ukc FOREIGN KEY (observations_id_retour) REFERENCES public.observation(id_retour);


--
-- TOC entry 3574 (class 2606 OID 190718)
-- Name: note_question fkim35n2bgx1d1lgkl7lw6ety9r; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.note_question
    ADD CONSTRAINT fkim35n2bgx1d1lgkl7lw6ety9r FOREIGN KEY (question_id_question) REFERENCES public.questionnaire(id_question);


--
-- TOC entry 3553 (class 2606 OID 190603)
-- Name: collectif fkkm3artk8k7ahu4fit4iqfu7wa; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.collectif
    ADD CONSTRAINT fkkm3artk8k7ahu4fit4iqfu7wa FOREIGN KEY (rapport_analyse1collectif_id) REFERENCES public.rapport_analyse1collectif(id);


--
-- TOC entry 3600 (class 2606 OID 190848)
-- Name: type_bloc fklh84qule2qsi0m2aab2abyxg2; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.type_bloc
    ADD CONSTRAINT fklh84qule2qsi0m2aab2abyxg2 FOREIGN KEY (block_model_economique_id_blocme) REFERENCES public.block_model_economique(id_blocme);


--
-- TOC entry 3589 (class 2606 OID 190793)
-- Name: profil fklo6mc52ff8b7p5jmtweenr1te; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.profil
    ADD CONSTRAINT fklo6mc52ff8b7p5jmtweenr1te FOREIGN KEY (utilisateurs_id_utilisateur) REFERENCES public.utilisateurs(id_utilisateur);


--
-- TOC entry 3601 (class 2606 OID 190853)
-- Name: utilisateurs fkn563suu0ohh83ua4w3hk4jdao; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.utilisateurs
    ADD CONSTRAINT fkn563suu0ohh83ua4w3hk4jdao FOREIGN KEY (pie_idpie) REFERENCES public.pie(idpie);


--
-- TOC entry 3591 (class 2606 OID 190813)
-- Name: projet fkn647w4oc8ic00gutsebel7d3s; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.projet
    ADD CONSTRAINT fkn647w4oc8ic00gutsebel7d3s FOREIGN KEY (type_projet_id) REFERENCES public.type_projet(id);


--
-- TOC entry 3555 (class 2606 OID 190623)
-- Name: commune fknpb4g8pcu98cr0xm6xlrsnyqs; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.commune
    ADD CONSTRAINT fknpb4g8pcu98cr0xm6xlrsnyqs FOREIGN KEY (departement_id) REFERENCES public.departement(id);


--
-- TOC entry 3584 (class 2606 OID 190763)
-- Name: pie_note_questions fkoqjq8oo2m0grnanqkfyffaven; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.pie_note_questions
    ADD CONSTRAINT fkoqjq8oo2m0grnanqkfyffaven FOREIGN KEY (note_questions_id_note) REFERENCES public.note_question(id_note);


--
-- TOC entry 3554 (class 2606 OID 190588)
-- Name: collectif fkp0msvuy2ygeh1urmpf8511sl8; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.collectif
    ADD CONSTRAINT fkp0msvuy2ygeh1urmpf8511sl8 FOREIGN KEY (couverture_geographique_id) REFERENCES public.couverture_geographique(id);


--
-- TOC entry 3592 (class 2606 OID 190808)
-- Name: projet fkp3lvo2iqceniqn9hxlm85lis9; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.projet
    ADD CONSTRAINT fkp3lvo2iqceniqn9hxlm85lis9 FOREIGN KEY (statut_id) REFERENCES public.statut(id);


--
-- TOC entry 3570 (class 2606 OID 190693)
-- Name: individuel_idee_projets fkp7yp1mivcv8ecfrdi086s4o1a; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.individuel_idee_projets
    ADD CONSTRAINT fkp7yp1mivcv8ecfrdi086s4o1a FOREIGN KEY (idee_projets_id_projet) REFERENCES public.idee_projet(id_projet);


--
-- TOC entry 3546 (class 2606 OID 190573)
-- Name: block_model_economique_sous_blocs fkppa7bm2ft59qkexkqnyp87p1j; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.block_model_economique_sous_blocs
    ADD CONSTRAINT fkppa7bm2ft59qkexkqnyp87p1j FOREIGN KEY (sous_blocs_id_sous_bloc) REFERENCES public.sous_bloc(id_sous_bloc);


--
-- TOC entry 3595 (class 2606 OID 190818)
-- Name: questionnaire fkppxs983qx3roj50v5iytmefbb; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.questionnaire
    ADD CONSTRAINT fkppxs983qx3roj50v5iytmefbb FOREIGN KEY (phase_id_phase) REFERENCES public.phase(id_phase);


--
-- TOC entry 3580 (class 2606 OID 190733)
-- Name: pie fkpw5xie6r9feajnw5w7s7sxyxw; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.pie
    ADD CONSTRAINT fkpw5xie6r9feajnw5w7s7sxyxw FOREIGN KEY (cohorte_id_cohorte) REFERENCES public.cohorte(id_cohorte);


--
-- TOC entry 3557 (class 2606 OID 190633)
-- Name: compte_rendue_createurs_content fkqb6scneg9ooh9phx3vi8bq6d; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.compte_rendue_createurs_content
    ADD CONSTRAINT fkqb6scneg9ooh9phx3vi8bq6d FOREIGN KEY (compte_rendue_id_compte_rendue) REFERENCES public.compte_rendue(id_compte_rendue);


--
-- TOC entry 3576 (class 2606 OID 190728)
-- Name: parcours fkqudwj4s9e8wp639s63xiisvhl; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.parcours
    ADD CONSTRAINT fkqudwj4s9e8wp639s63xiisvhl FOREIGN KEY (sous_categories_id) REFERENCES public.sous_categories(id);


--
-- TOC entry 3599 (class 2606 OID 190843)
-- Name: sous_categories fkrkd1mssr6o6c82p33qsullg2m; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.sous_categories
    ADD CONSTRAINT fkrkd1mssr6o6c82p33qsullg2m FOREIGN KEY (categories_id) REFERENCES public.categories(id);


--
-- TOC entry 3561 (class 2606 OID 190643)
-- Name: documents fkss6knax061uyr9vhrlvfubir7; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.documents
    ADD CONSTRAINT fkss6knax061uyr9vhrlvfubir7 FOREIGN KEY (compte_rendue_id_compte_rendue) REFERENCES public.compte_rendue(id_compte_rendue);


--
-- TOC entry 3593 (class 2606 OID 190803)
-- Name: projet fktq8nytrbobcpwte3b0518gr0; Type: FK CONSTRAINT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.projet
    ADD CONSTRAINT fktq8nytrbobcpwte3b0518gr0 FOREIGN KEY (secteur_activite_id) REFERENCES public.secteur_activite(id);


--
-- TOC entry 3747 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: adel
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;


-- Completed on 2023-11-02 10:54:41

--
-- PostgreSQL database dump complete
--

