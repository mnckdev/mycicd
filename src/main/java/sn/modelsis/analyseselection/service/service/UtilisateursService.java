package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.Utilisateurs;
import sn.modelsis.analyseselection.service.repository.UtilisateursRepository;

import java.util.List;
import java.util.Optional;
@Service
public class UtilisateursService {


    private UtilisateursRepository profilRepository;

    public UtilisateursService(UtilisateursRepository profilRepository){
        this.profilRepository=profilRepository;
    }

    public Utilisateurs newUtilisateurs(Utilisateurs profil){
        return  profilRepository.save(profil);
    }

    public Optional<Utilisateurs> findById(Integer id){
        return profilRepository.findById(id);
    }
    public List<Utilisateurs> findAll(){
        return profilRepository.findAll();
    }
    public void deleteUtilisateurs(Integer id){
        profilRepository.deleteById(id);
    }

}
