package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.ModelEconomique;

@Repository
public interface ModelEconomiqueRepository extends JpaRepository<ModelEconomique,Integer> {
}
