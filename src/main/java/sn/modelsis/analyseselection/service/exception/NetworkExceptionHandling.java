package sn.modelsis.analyseselection.service.exception;

public class NetworkExceptionHandling extends RuntimeException{

    public NetworkExceptionHandling(){
        super("Error Connection Exception The Host is Unreachable");
    }
    public NetworkExceptionHandling(String message){
        super(message);
    }
}
