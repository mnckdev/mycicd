package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.Profil;
import sn.modelsis.analyseselection.service.service.ProfilService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/profil")
@Api(
        value = "Profil REST Endpoint",
        tags = {"Profil Controller (v1)"}
)

public class ProfilController {


    private final ProfilService profilService;

    public ProfilController(ProfilService profilService) {
        this.profilService = profilService;
    }

    @ApiOperation(value = "find All Profil", notes = "find all  Profil")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<Profil>> findAll(){
        List<Profil> profil = profilService.findAll();
        return new ResponseEntity<List<Profil>>(profil, HttpStatus.OK);
    }
    @ApiOperation(value = "find By ID Profil", notes = "find By ID  Profil")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Profil>> findById(@PathVariable UUID uuid){
        return new ResponseEntity<Optional<Profil>>(profilService.findById(uuid), HttpStatus.OK);
    }

    @ApiOperation(value = "create  new  Profils", notes = "create a new Profils")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @PostMapping
    public ResponseEntity<Profil> newProfil(@RequestBody Profil Profil) {
        return new ResponseEntity<Profil>(profilService.newProfil(Profil), HttpStatus.OK);
    }

    @ApiOperation(value = "delete  Profils", notes = "delete  Profils")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable UUID uuid) {
        profilService.deleteProfil(uuid);
        return new ResponseEntity<>("deleted successfully ...",HttpStatus.OK);
    }

}
