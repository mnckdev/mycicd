package sn.modelsis.analyseselection.service.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import sn.modelsis.analyseselection.service.entity.SyntheseNotation;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class QuestionnaireDTO {

    private Integer idQuestion;

    private int code;

    private String libelle;

    private String categorie;

    private String bloc;

    private SyntheseNotation syntheseNotation;

    private PhaseDTO phase;

    private List<ReferenceNoteDTO> referenceNotes;
}
