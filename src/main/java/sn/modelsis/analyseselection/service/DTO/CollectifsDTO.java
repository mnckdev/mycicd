package sn.modelsis.analyseselection.service.DTO;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.*;
import lombok.experimental.SuperBuilder;
import sn.modelsis.analyseselection.service.entity.CouvertureGeographique;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@SuperBuilder
@EqualsAndHashCode(callSuper=true)
@JsonTypeName("COLLECTIF")
public class CollectifsDTO extends PIEDTO {

    private String contact1Representant;

    private String contact2Representant;

    private String titre;

    private String denomination;

    private String reconnaissanceJuridique;

    private CommuneDTO communeActivite;

    private String quartierVillageInterieur;

    private Date dateCreation;

    private Integer totalMembre;

    private Integer total_homme;

    private Integer total_femmes;

    private FormeJuridiqueDTO formeJuridique;

    private CommuneDTO commune;

    private String lieuSiege;

    private CouvertureGeographique couvertureGeographique;

    private ProfilMembreDTO profilMembre;

    private TrancheAgeDTO trancheAge;

    private String situationEconomique;

    private Double montantCapitalSocial;

    private Integer nbrEmployePermanent;

    private Integer nbrEmployeTemporaire;

    private Double montantEpargneMobilisee;

    private Double montantEndettement;

    private Double montantSubvensionRecu;

    private StatutDTO statut;
}
