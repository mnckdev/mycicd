package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.RencontreDTO;
import sn.modelsis.analyseselection.service.entity.Rencontre;

@Mapper(componentModel = "spring")
public interface RencontreMapper extends GenericMapper<Rencontre, RencontreDTO>{
}
