package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.RapportAnalyse;
import sn.modelsis.analyseselection.service.service.RapportAnalyseService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/rapportAanlyse")
@Api(
        value = "RapportAanlyse  REST Endpoint",
        tags = {"RapportAanlyse  Controller (v1)"}
)

public class RapportAnalyseController {


    private final RapportAnalyseService rapportAnalyseService;

    public RapportAnalyseController(RapportAnalyseService rapportAnalyseService) {
        this.rapportAnalyseService = rapportAnalyseService;
    }

    @ApiOperation(value = "find All Rapport Analyse", notes = "find all  Rapport Analyse")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<RapportAnalyse>> findAll(){
        List<RapportAnalyse> rapportAnalyse= rapportAnalyseService.findAll();
        return new ResponseEntity<List<RapportAnalyse>>(rapportAnalyse, HttpStatus.OK);
    }


    @ApiOperation(value = "find By ID Rapport Analyse", notes = "find By ID  Rapport Analyse")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping("/{id}")
    public ResponseEntity<Optional<RapportAnalyse>> findById(@PathVariable Integer id){
        return new ResponseEntity<Optional<RapportAnalyse>>(rapportAnalyseService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "create  new  Rapport Analyses", notes = "create a new Rapport Analyses")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @PostMapping
    public ResponseEntity<RapportAnalyse> newRapportAnalyse(@RequestBody RapportAnalyse rapportAnalyse) {
        return new ResponseEntity<RapportAnalyse>(rapportAnalyseService.newRapportAnalyse(rapportAnalyse), HttpStatus.OK);
    }

    @ApiOperation(value = "delete  Rapport Analyses", notes = "delete  Rapport Analyses")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id) {
        rapportAnalyseService.deleteRapportAnalyse(id);
        return new ResponseEntity<>("deleted successfully ...",HttpStatus.OK);
    }

}
