package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.RapportAnalyseDTO;
import sn.modelsis.analyseselection.service.entity.RapportAnalyse;

@Mapper(componentModel = "spring")
public interface RapportAnalyseMapper extends GenericMapper<RapportAnalyse, RapportAnalyseDTO>{
}
