package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.Profil;

import java.util.UUID;

@Repository
public interface ProfilRepository extends JpaRepository<Profil, UUID> {
}
