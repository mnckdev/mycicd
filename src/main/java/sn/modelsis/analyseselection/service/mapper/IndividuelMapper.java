package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.IndividuelDTO;
import sn.modelsis.analyseselection.service.entity.Individuel;

@Mapper(componentModel = "spring")
public interface IndividuelMapper extends GenericMapper<Individuel, IndividuelDTO>{
}
