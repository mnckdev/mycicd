package sn.modelsis.analyseselection.service.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ModeleEconomiqueDTO {

    private UUID id;

    private String status;

    private List<BlockModelEconomiqueDTO> blockModelEconomique;
}
