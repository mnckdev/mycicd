package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.ModelEconomique;
import sn.modelsis.analyseselection.service.service.ModelEconomiqueService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/modelEconomique")
@Api(
        value = "ModelEconomique  REST Endpoint",
        tags = {"ModelEconomique  Controller (v1)"}
)

public class ModelEconomiqueController {

    private final ModelEconomiqueService modelEconomiqueService;

    public ModelEconomiqueController(ModelEconomiqueService modelEconomiqueService) {
        this.modelEconomiqueService = modelEconomiqueService;
    }

    @ApiOperation(value = "find All Model Economique", notes = "find all  Model Economique")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<ModelEconomique>> findAll(){
        List<ModelEconomique> modelEconomique = modelEconomiqueService.findAll();
        return new ResponseEntity<List<ModelEconomique>>(modelEconomique, HttpStatus.OK);
    }


    @ApiOperation(value = "find By ID Model Economique", notes = "find By ID  Model Economique")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping("/{id}")
    public ResponseEntity<Optional<ModelEconomique>> findById(@PathVariable Integer id){
        return new ResponseEntity<Optional<ModelEconomique>>(modelEconomiqueService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "create  new  Model Economiques", notes = "create a new Model Economiques")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })
    @PostMapping
    public ResponseEntity<ModelEconomique> newModelEconomique(@RequestBody ModelEconomique modelEconomique) {
        return new ResponseEntity<ModelEconomique>(modelEconomiqueService.newModelEconomique(modelEconomique), HttpStatus.OK);
    }

    @ApiOperation(value = "delete  Model Economiques", notes = "delete  Model Economiques")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id) {
        modelEconomiqueService.deleteModelEconomique(id);
        return new ResponseEntity<>("deleted successfully ...",HttpStatus.OK);
    }

}
