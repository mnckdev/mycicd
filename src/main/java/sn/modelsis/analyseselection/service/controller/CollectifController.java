package sn.modelsis.analyseselection.service.controller;

import feign.Param;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.DTO.CohorteDTO;
import sn.modelsis.analyseselection.service.DTO.CollectifsDTO;
import sn.modelsis.analyseselection.service.DTO.IndividuelDTO;
import sn.modelsis.analyseselection.service.entity.Cohorte;
import sn.modelsis.analyseselection.service.entity.Collectif;
import sn.modelsis.analyseselection.service.entity.Commune;
import sn.modelsis.analyseselection.service.mapper.CohorteMapper;
import sn.modelsis.analyseselection.service.mapper.CollectifMapper;
import sn.modelsis.analyseselection.service.mapper.CommuneMapper;
import sn.modelsis.analyseselection.service.mapper.StatutMapper;
import sn.modelsis.analyseselection.service.service.CollectifService;

import javax.ws.rs.NotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/collectif")
@Api(
        value = "Collectif REST endpoint",
        tags = {"Collectif controller(v1)"}
)

public class CollectifController {

    private final CollectifService collectifService;

    private final CollectifMapper mapper;
    private final CohorteMapper mapperCohorte;
    private final CommuneMapper mapperCommune;
    private final StatutMapper statutMapper;

    public CollectifController(CollectifService collectifService,
                               CollectifMapper mapper,
                               CohorteMapper mapperCohorte,
                               CommuneMapper mapperCommune, StatutMapper statutMapper) {
        this.collectifService = collectifService;
        this.mapper = mapper;
        this.mapperCohorte = mapperCohorte;
        this.mapperCommune = mapperCommune;
        this.statutMapper = statutMapper;
    }


    @GetMapping
    @ApiOperation(value = "Get all collectifs", notes = "Get all  collectifs")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    public ResponseEntity<List<CollectifsDTO>> findAll(){
        List<Collectif> collectifs = collectifService.read();
        List<CollectifsDTO> collectifsDTO = mapper.asDTOList(collectifs);
        if(collectifsDTO == null)
            return ResponseEntity.badRequest().body(collectifsDTO);
        return ResponseEntity.ok().body(collectifsDTO);
    }

    @ApiOperation(value = "Get one collectif", notes = "Get one  collectif")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<CollectifsDTO>> get(@PathVariable UUID id){
        Optional<Collectif> collectif = collectifService.read(id);
        if(collectif.isEmpty())
            return  ResponseEntity.badRequest().body(Optional.empty());
        return ResponseEntity.ok().body(Optional.of(mapper.asDTO(collectif.get())));
    }

    @PostMapping
    @ApiResponses(
            {
                    @ApiResponse(code=200, message = "created"),
            }
    )
    @ApiOperation(value = "post a collectif", notes = "post a  collectif")
    // Devrait changer le retour de ce controller
    public ResponseEntity<CollectifsDTO> create(@RequestBody CollectifsDTO collectifsDTO){
        Collectif col= mapper.asEntity(collectifsDTO);
        Collectif collectif = collectifService.create(col);
        return ResponseEntity.ok().body(mapper.asDTO(collectif));
    }

    @DeleteMapping("/{id}")
    @ApiResponses(
            {
                    @ApiResponse(code=200, message = "deleted"),
            }
    )
    @ApiOperation(value = "delete a collectif", notes = "delete a  collectif")
    public ResponseEntity<String> delete(@PathVariable UUID id){
        collectifService.delete(id);
        return ResponseEntity.ok().body("deleted");
    }

    @GetMapping("/cohorte")
    public ResponseEntity<List<CollectifsDTO>> findByCohorte(
            @RequestBody CohorteDTO cohorteDTO
    ){
        Cohorte cohorte = mapperCohorte.asEntity(cohorteDTO);
        List<CollectifsDTO> collectifs = collectifService.findByCohorte(cohorte);
        return ResponseEntity.ok().body(collectifs);
    }

    @GetMapping("/cohorte-commune")
    public ResponseEntity<List<CollectifsDTO>> findByCohorteAndCommune(
            @RequestBody CollectifsDTO collectifsDTO
    ){
        Cohorte cohorte = mapperCohorte.asEntity(collectifsDTO.getCohorte());
        Commune commune = mapperCommune.asEntity(collectifsDTO.getCommune());
        return ResponseEntity.ok().body(collectifService.findByCohorteAndCommune(cohorte,commune));
    }

    @GetMapping("/cohorte-commune-statut")
    public ResponseEntity<List<CollectifsDTO>> findByCohorteAndCommuneAndStatut(
            @RequestBody CollectifsDTO collectifsDTO

    ){
        return ResponseEntity.ok().body(
                collectifService.findAllByCohorteAndCommuneAndStatus(
                       mapperCohorte.asEntity(collectifsDTO.getCohorte()),
                        mapperCommune.asEntity(collectifsDTO.getCommune()),
                        statutMapper.asEntity(collectifsDTO.getStatut())
                ));
    }

    @PutMapping
    @ApiOperation(value = "UPDATE a collectif", notes = "UPDATE a collectif")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Updated"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    public ResponseEntity<Optional<?>> update(@Param Boolean check, @RequestBody CollectifsDTO collectifsDTO){
       Optional<CollectifsDTO>  collectifsDTOUpdated;
        Optional<String> verifyresult =  collectifService.verifyBeforeUpdate(collectifsDTO);
        if(collectifService.findById(collectifsDTO.getIdPIE()).isEmpty())
            throw new NotFoundException("user not found make sure that the userId is provided");

        if(verifyresult.isPresent()){
            return ResponseEntity.badRequest().body(Optional.of(verifyresult));
        }
        collectifsDTOUpdated = collectifService.update(collectifsDTO);

        return ResponseEntity.ok().body(collectifsDTOUpdated);
    }

}
