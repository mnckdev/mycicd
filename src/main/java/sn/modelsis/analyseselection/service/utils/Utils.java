package sn.modelsis.analyseselection.service.utils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class Utils {

    public static Map<String, Object> convertObjectToMap(Object object) {
        Map<String, Object> properties = new HashMap<>();

        Class<?> clazz = object.getClass();
        Field[] fields = clazz.getDeclaredFields();

        for (Field field : fields) {
            try {
                field.setAccessible(true);
                Object value = field.get(object);
                properties.put(field.getName(), value);
            } catch (IllegalAccessException e) {
                e.printStackTrace(); // Handle the exception according to your needs
            }
        }

        return properties;
    }

    // Your other methods or code go here
}


