package sn.modelsis.analyseselection.service.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.Parcours;
import sn.modelsis.analyseselection.service.service.ParcoursService;


import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/parcours")
@Api(
        value = "Parcours REST Endpoint",
        tags = {"Parcours Controller (v1)"}
)

public class ParcoursControlleur {


    private final ParcoursService ParcoursService;

    public ParcoursControlleur(sn.modelsis.analyseselection.service.service.ParcoursService parcoursService) {
        ParcoursService = parcoursService;
    }

    @ApiOperation(value = "find All Parcours", notes = "find all  Parcours")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<Parcours>> findAll(){
        List<Parcours> Parcours = ParcoursService.findAll();
        return new ResponseEntity<List<Parcours>>(Parcours, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID Parcours", notes = "find By ID  Parcours")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<Parcours>> findById(@PathVariable Integer id){
        return new ResponseEntity<Optional<Parcours>>(ParcoursService.findById(id), HttpStatus.OK);
    }



}
