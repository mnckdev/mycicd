package sn.modelsis.analyseselection.service.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Individuel extends PIE{

    private String niveauEtude;

    private String formationProf;

    private String experienceProf;

    @ManyToOne(cascade = CascadeType.ALL)
    private TrancheAge trancheAge;

    @ManyToOne
    private Commune communeRattach;

    @ManyToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "commune_id")
    private Commune commune;

    @ManyToOne
    private Categories categories;

    private String typeCategorie;

    @OneToOne(cascade = CascadeType.ALL)
    private RapportAnalyse rapportAnalyse1;

    @OneToOne(cascade = CascadeType.ALL)
    private RapportAnalyse2 rapportAnalyse2;

    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<IdeeProjet> ideeProjets;
    // some added fields
    private String horsSenegal;

    private String autreRegion;

    private String situationActuel;

    private String  ninea;

    private String numeroRc;

    private String refProfessionnel;

    private String banqueOuSFD;

    private Boolean beneficierSoutien;
    @ManyToOne
    private SousCategories sousCategorieInd;
    @ManyToOne
    private SousCategories sousCategorieMig;
}
