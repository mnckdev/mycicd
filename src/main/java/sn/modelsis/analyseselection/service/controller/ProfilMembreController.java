package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.ProfilMembre;
import sn.modelsis.analyseselection.service.service.ProfilMembreService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/profil_membre")
@Api(
        value = "Status  REST Endpoint",
        tags = {"Status  Controller (v1)"}
)

public class ProfilMembreController {


    private final ProfilMembreService profilMembreService;

    public ProfilMembreController(ProfilMembreService profilMembreService) {
        this.profilMembreService = profilMembreService;
    }

    @ApiOperation(value = "find All ProfilMembre", notes = "find all  ProfilMembre")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<ProfilMembre>> findAll(){
        List<ProfilMembre> profilMembre = profilMembreService.findAll();
        return new ResponseEntity<List<ProfilMembre>>(profilMembre, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID ProfilMembre", notes = "find By ID  ProfilMembre")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping("/{id}")
    public ResponseEntity<Optional<ProfilMembre>> findById(@PathVariable UUID id){
        return new ResponseEntity<Optional<ProfilMembre>>(profilMembreService.findById(id), HttpStatus.OK);
    }
    @ApiOperation(value = "create  new  ProfilMembres", notes = "create a new ProfilMembres")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @PostMapping
    public ResponseEntity<ProfilMembre> newProfilMembre(@RequestBody ProfilMembre profilMembre) {
        return new ResponseEntity<ProfilMembre>(profilMembreService.newProfilMembre(profilMembre), HttpStatus.OK);
    }

    @ApiOperation(value = "delete  ProfilMembres", notes = "delete  ProfilMembres")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable UUID id) {
        profilMembreService.deleteProfilMembre(id);
        return new ResponseEntity<>("deleted successfully ...",HttpStatus.OK);
    }


}
