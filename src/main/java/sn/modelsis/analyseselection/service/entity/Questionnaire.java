package sn.modelsis.analyseselection.service.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Questionnaire {

    @Id
    private Integer idQuestion;

    private int code;

    private String libelle;

    private String categorie;

    private String bloc;

    @ManyToOne
    @JoinColumn(name = "id_synthese_notation")
    private SyntheseNotation syntheseNotation;

    @OneToMany(mappedBy = "question", fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ReferenceNote> referenceNotes;

    @ManyToOne
    private Phase phase;
}
