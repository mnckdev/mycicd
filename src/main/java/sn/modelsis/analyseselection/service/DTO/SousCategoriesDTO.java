package sn.modelsis.analyseselection.service.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SousCategoriesDTO {

    private Integer id;

    private String libelle;

    private Integer marqueur;

    private String code;

    private List<ParcoursDTO> parcours;
}
