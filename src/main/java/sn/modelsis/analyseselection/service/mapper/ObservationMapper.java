package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.ObservationDTO;
import sn.modelsis.analyseselection.service.entity.Observation;

@Mapper(componentModel = "spring")
public interface ObservationMapper extends GenericMapper<Observation, ObservationDTO> {
}
