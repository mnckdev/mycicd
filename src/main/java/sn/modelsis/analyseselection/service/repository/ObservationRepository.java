package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.Observation;

@Repository
public interface ObservationRepository extends JpaRepository<Observation, Integer> {

//    List<Retour> findAllByPie(PIE pie);
}
