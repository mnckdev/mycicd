package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.CategoriesDTO;
import sn.modelsis.analyseselection.service.entity.Categories;

@Mapper(componentModel = "spring")
public interface CategoriesMapper extends GenericMapper<Categories, CategoriesDTO>{
}
