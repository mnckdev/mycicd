--
-- TOC entry 3742 (class 0 OID 188052)
-- Dependencies: 211
-- Data for Name: aer; Type: TABLE DATA; Schema: public; Owner: -
--

--
-- TOC entry 3743 (class 0 OID 188057)
-- Dependencies: 212
-- Data for Name: block_model_economique; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('fdfaadeb-10c0-4ffa-8a6e-88c83e6313ad', NULL, 'Segments de client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('30278aa9-1267-4f6f-9c9d-5a96803a5ca5', NULL, 'Relation avec le client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('84352fbe-3fdc-45d1-9921-cbfb49fe8677', NULL, 'Canaux de communications');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('ea1ac5d4-e725-4c57-9d51-4eb2307b275b', NULL, 'Proposition de valeurs');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('322b0fe5-bb99-4919-8074-cae829ef13ca', NULL, 'Activités clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('03791dcb-66d7-49d9-af06-3753678fd87a', NULL, 'Ressources clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('f2856ed1-a3ac-403f-99ff-90c721a2cbce', NULL, 'Partenaires clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('137f0306-be93-4229-b5c2-38637c037a63', NULL, 'Segments de client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('04cd2a9f-d12d-4ead-9081-8d25c46f6053', NULL, 'Relation avec le client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('45136eda-6f73-45df-bfdb-eaf99e317dd7', NULL, 'Canaux de communications');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('c7839033-a293-4b9a-b703-471cc363eba1', NULL, 'Proposition de valeurs');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('e5d7eb1d-8398-4825-870f-6127d9fcdcb2', NULL, 'Activités clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('f983d44d-0aae-4f55-b8ff-c157f7e9af57', NULL, 'Ressources clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('99bcdcf1-6aa1-4f1f-9914-a347ce8f0620', NULL, 'Partenaires clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('f97a6437-3a6d-4874-8c23-16eaed4b3800', NULL, 'Segments de client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('189886a1-1470-4e07-bbb3-f4a4f9f79969', NULL, 'Relation avec le client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('8af70b7d-5b93-4952-951d-4623fa802e8b', NULL, 'Canaux de communications');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('181a08d6-f8d8-4481-bb25-8334b179c245', NULL, 'Proposition de valeurs');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('e151e074-8bd6-441c-b8fa-b9cc6346d3f7', NULL, 'Activités clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('e9cb095e-ad7f-422e-8b24-75c5e69d5123', NULL, 'Ressources clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('34b8967d-6c41-4f35-bfb7-e65eadab100d', NULL, 'Partenaires clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('2bc41196-69ff-4ff7-bad5-d3b4192985cd', NULL, 'Segments de client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('993c9742-88f6-44df-bf36-b8b1265a87dd', NULL, 'Relation avec le client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('214e6a28-9f1d-4aea-985d-7a4c11ce3594', NULL, 'Canaux de communications');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('182085d5-90d3-4a1b-8296-6fc75acc3f58', NULL, 'Proposition de valeurs');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('ab08b74f-0184-461b-b160-c9f8469ea80f', NULL, 'Activités clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('3c2b68df-582b-4d4c-a7a2-8b99491fe8fb', NULL, 'Ressources clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('25d8edb5-0597-4a01-b4d3-44f6b3c60a2e', NULL, 'Partenaires clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('f05eefbe-eb4b-4268-9569-ae906d626ffd', NULL, 'Segments de client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('e01ea9d8-8a37-445c-a8f5-606b9309ce6e', NULL, 'Relation avec le client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('d0158c97-eeb1-4d81-ba9b-88bc2975abe7', NULL, 'Canaux de communications');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('05ff7095-3fb5-40bf-ac20-49aa1449f500', NULL, 'Proposition de valeurs');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('6aacc09d-584c-4e8d-81aa-cc1b8f425fee', NULL, 'Activités clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('22daca05-7fa6-4925-bccd-3d8a354d43cf', NULL, 'Ressources clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('5303ae62-d015-47d1-bcd1-fec3594ddf3c', NULL, 'Partenaires clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('0018b52b-bf5f-4ba7-9e87-fa026320654f', NULL, 'Segments de client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('c25cd97b-51ba-4345-9ac5-b3a8fbd3e44f', NULL, 'Relation avec le client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('06938c10-3334-42c1-a20f-f36f92aae643', NULL, 'Canaux de communications');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('2efe284e-e8fe-4971-96ff-4baeacc8bdce', NULL, 'Proposition de valeurs');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('281e61f9-1737-4252-a7fe-803a82ea3e5c', NULL, 'Activités clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('64685f94-63f0-49b3-b4a0-d0431e4bc140', NULL, 'Ressources clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('3b0f5b27-5c13-4d2c-b0f2-ad13c85599cc', NULL, 'Partenaires clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('1b1004c3-becb-49fc-bd0e-0d7fde6eaf85', NULL, 'Segments de client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('caf8d845-00e1-4edb-b121-e7089038b230', NULL, 'Relation avec le client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('456d10e4-65cb-43a2-8ea0-8e20d9871b71', NULL, 'Canaux de communications');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('b1b1460f-05c6-4203-b966-b9ac1d65d32c', NULL, 'Proposition de valeurs');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('e3055b5e-93de-4e93-a209-a8c3fe4a78b6', NULL, 'Activités clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('387beb97-a5b8-4f6e-8084-890cce850fdc', NULL, 'Ressources clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('387288b7-66b9-4bb4-ba98-5185f9d44fc2', NULL, 'Partenaires clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('2944f009-46a8-41b5-8574-a63a4c4e1365', NULL, 'Segments de client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('39af7775-90f7-4ead-b911-3259517e6335', NULL, 'Relation avec le client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('194ed703-b8ec-4a8f-80f0-30f57a6ab0f2', NULL, 'Canaux de communications');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('166cc51a-766d-4e38-9263-566fd447670b', NULL, 'Proposition de valeurs');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('e261b8f5-7b4d-4ffc-bbef-d4b62fb4096f', NULL, 'Activités clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('507c3473-09c5-4aaa-b8f8-5c060feefb1d', NULL, 'Ressources clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('73c09e49-5b37-467c-b41c-4ca058dc7cf9', NULL, 'Partenaires clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('452d112b-5d42-47e3-b8be-5d3d10d62cb5', NULL, 'Segments de client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('a533815d-a4a8-408f-924c-0d4c5440aa51', NULL, 'Relation avec le client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('d88d6d94-4cab-4cb1-8b48-6d187c743aec', NULL, 'Canaux de communications');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('457695f9-89bb-41f3-b86f-51c585a8c7ed', NULL, 'Proposition de valeurs');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('b6a34cb2-c8d6-4577-92be-89193aa085ad', NULL, 'Activités clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('6e7e8209-aefd-4780-9782-532ab929df75', NULL, 'Ressources clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('24bb7d7b-c7fb-4d5e-a52f-e8f1b0365c4e', NULL, 'Partenaires clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('2b110ad4-600a-41cb-8200-98369f3e1ad9', NULL, 'Segments de client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('a98e9f9d-c9eb-45fb-9dcd-a4079b0c4007', NULL, 'Relation avec le client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('2acd0e5d-8ea4-4d5c-8ee3-c45d224be713', NULL, 'Canaux de communications');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('d6021cae-ad4a-4374-ab15-f8020d0c3a59', NULL, 'Proposition de valeurs');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('656dee8c-9ab5-4a0d-a897-78e6e79da69e', NULL, 'Activités clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('62f69f1e-48cc-476b-b143-aeb788186b5a', NULL, 'Ressources clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('c973d9e1-8862-4dd5-ad93-27f3fc73f8c9', NULL, 'Partenaires clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('dac8e124-7c77-44f2-b6fa-89171b1ab798', NULL, 'Segments de client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('2149ced9-0ff7-484e-ab21-91fcc8931621', NULL, 'Relation avec le client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('63a91269-1ab1-4c52-b39e-06aca311b02d', NULL, 'Canaux de communications');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('989ffc38-6a60-453e-b2ae-fbf9d318d4f4', NULL, 'Proposition de valeurs');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('0db4dd64-6e89-404f-811f-751251632507', NULL, 'Activités clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('3865a7fc-238c-43e2-b5c7-fda645ccfb48', NULL, 'Ressources clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('f9932ea1-fb6c-492f-9b84-59e2917d84c6', NULL, 'Partenaires clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('c9bbe03f-e283-428f-ae2f-a5fd2cea2cd4', NULL, 'Segments de client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('489a525b-a61e-4a89-8025-925f3cc7d3c5', NULL, 'Relation avec le client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('68cfe0cd-95ab-4346-98ae-09e0b6700baa', NULL, 'Canaux de communications');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('1a271709-6c77-4454-835f-532158dc62cc', NULL, 'Proposition de valeurs');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('6a6c7db2-0a04-4fc2-9407-8d4e24be95d8', NULL, 'Activités clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('aceead3f-0e4f-4036-a465-73c136a74ba4', NULL, 'Ressources clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('6bfaf794-accf-49b9-bd73-e731197cc341', NULL, 'Partenaires clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('975ed0ea-71f5-450e-b300-6aa9f47e4d7c', NULL, 'Segments de client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('b95a4ec4-d949-4193-9fa7-936e1c22ce2e', NULL, 'Relation avec le client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('e830a071-2f46-4dc9-ae48-56e519301f65', NULL, 'Canaux de communications');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('33dc565f-1ea2-43bd-9a24-f9f34b584a1e', NULL, 'Proposition de valeurs');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('be112b29-0797-4065-bebf-7a9ded3409d0', NULL, 'Activités clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('0e33f4f4-06a6-408b-a008-05b1b09fc63b', NULL, 'Ressources clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('c0997376-4a0a-4bd9-86c6-2441da215beb', NULL, 'Partenaires clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('c5031ce0-bed2-454f-9ddd-cce3f66071c6', NULL, 'Segments de client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('4459fc1f-ee1c-4d0a-b8e3-5bbdeb661565', NULL, 'Relation avec le client');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('ae668c4d-ea8c-4e1c-b48f-3b2686083dc9', NULL, 'Canaux de communications');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('9916b93a-8a69-48bb-b4f3-b4125ecad5eb', NULL, 'Proposition de valeurs');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('e15e5cfd-7abe-4d56-a54d-f3a83d154bf2', NULL, 'Activités clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('a332d1ba-c11e-42ad-95c8-6d8504bcb2c3', NULL, 'Ressources clés');
INSERT INTO public.block_model_economique (id_blocme, code, libelle) VALUES ('f2511bd5-c89b-4e4e-907d-94570c7a0568', NULL, 'Partenaires clés');






--
-- TOC entry 3785 (class 0 OID 188207)
-- Dependencies: 254
-- Data for Name: sous_bloc; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('a77eae47-cd06-47db-b02d-ab05fd484550', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('a450b8b6-dcd6-4deb-bb00-57bcada51e55', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('4293d938-c4e3-4586-9326-7c0d49f65248', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('c7607eeb-2d1b-4fd6-840d-ad521196df07', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('fff242a8-781f-4acf-b66a-6d3bd7beb263', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('d5b85009-3fb6-42c9-a662-a8731390881b', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('8385bb7d-80cd-4034-ad61-02375a991f10', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('a54c325e-12e6-4cd6-b83b-66a585cc106e', 'asja');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('42c5b60c-6279-44db-b8cc-96ce81c843b8', 'lkdjze');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('99d126b6-589e-4eba-bb54-8f9e2ac70a70', 'zoajlda');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('d0fdf4aa-4a0d-4047-b178-f808171fda65', 'zkda');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('b66d68d5-1eac-4487-9a48-e6cd3f461f52', 'zoidazje');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('8b48d335-d74f-4ea5-8a25-579fc8195458', 'qidjqs');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('30b4fea7-9415-4864-986d-a052e609e970', 'zeijaz');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('6d6de817-4d58-462b-a08c-050dbd506aa8', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('891c97e6-a352-4dc1-824e-446ef412313b', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('2f3f987a-7ace-4174-be2b-f2047e7e4763', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('f96bbed0-31a6-4633-941a-c5a8b3d5dbb6', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('12f340bd-ad7a-4040-a248-7d8c209a78b6', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('1d9871f6-d9bd-48fb-bae1-7ac810484f61', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('8985ba98-9db4-4e99-ae03-527388eefb67', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('489ddbf1-75d4-4467-a93c-ba1f1fb6c376', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('25c52a90-8a85-422b-91a0-b2c34b6f1b48', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('24cdcb24-464f-421c-b05f-8f4b7799bc16', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('513c96e6-4824-47cd-926f-7ae1462e3d3f', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('da177093-2e41-460c-b24e-ef9028de3de9', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('5e210310-7552-41d3-8d11-09630d583570', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('80855d34-e1c5-47ae-a413-4d275c0680d8', 'idea');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('8084de65-5131-4b34-9633-972daa3246b2', 'test value');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('e3372c98-18ea-4d5b-af5c-229a18fd5421', 'test value');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('b1e89e54-a88b-4f61-86b8-4ca881f58c6d', 'pieSelected');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('1aae1290-ea96-46d1-8d0b-5be527311293', 'tes');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('3fa36b53-dd13-4ce0-b55c-985d31243c34', 'pieSelected');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('00c9984b-af3d-45b2-b2a1-f702c1db4635', 'pieSelected');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('c53d8250-c3ba-48b2-9de0-92980864905e', 'pieSelected');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('b5b6fca0-de2f-4713-8bce-b02f9212cb5d', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('57b986d7-d387-4081-90fe-4ad05e28bb5e', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('46ec8144-6cf8-43e1-b292-41b21d6feca9', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('0258007d-6837-40b5-a657-fca437a70551', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('2b69cb01-8a12-4b50-ae02-4673040be838', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('2bcf8907-f0c8-4b3e-a645-c2a6830667e0', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('4c59716b-291b-4cc4-9bd0-75ea7bd55f18', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('f0aba85f-2302-4af9-b539-75307002e5ed', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('00b0e1ed-21de-4486-9fae-0aff2e2c8d31', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('db915356-b586-49b7-84f3-3017acf2e4bf', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('627d87b2-724a-4ddf-b7ca-899c590baccc', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('c2714f81-ffb8-45ba-861c-80d9a2049329', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('2b284bce-db93-4473-8d9b-1fc5584c5e7d', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('691d02c6-7cf1-4697-b8ee-00f258ad2f31', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('2c2afb15-29e9-4a84-83d4-5c6a12f52b45', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('4d6566dc-d875-4ede-8056-95cabcae424e', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('283666cf-3c9e-4a65-a773-42812e7d6e68', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('f13978f4-417f-493b-b8fa-1a117aca9d94', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('adbc38d9-c25a-4a67-938d-7910448c5c50', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('39b7aa5e-c62b-4f57-99ff-ba6962195a9e', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('fed5c30f-5fe7-44e8-92b2-390a5c6547c6', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('b646b8e5-71ff-48dc-a0c4-79f12c757326', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('3d13f786-c32a-4cd0-af19-daeb4ea10731', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('531cf039-fcfb-46f9-8b8e-bfc4d4af1449', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('c54ed9e1-a452-4a9a-a3bb-fa9de4d4acc4', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('67968852-1d73-4ea6-b3c4-2fb8055b5e64', 'testtest');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('5ed7c5b5-4569-4a38-80fa-17b1e18d1cbb', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('186940ce-cda4-4a3e-b6c0-db35abf58395', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('948b9be2-fca6-4b6a-b70c-70174cbdc211', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('b6cc9357-e626-4ea4-a029-061de7758aff', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('df751866-c7bb-4fa8-a4fa-9b788ca6e49c', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('b72507c4-f019-4d05-b0bb-2b09f4727765', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('e6e4c2fb-d7d3-4b78-8048-54a9adc3e028', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('15c1223d-c202-42fe-8bf3-b21c0d130d6c', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('b0d4c4b8-c3b7-4856-ad04-4951219a2183', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('a2ae86dc-865a-494c-89a3-6edc890eb0d8', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('84eb0756-3583-4b10-9509-77a9f5d17f3b', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('45afd35a-3de4-496e-8027-055d13cc3c72', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('36bb7007-9569-4b28-96aa-7d9e82997515', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('7e7370a6-7f5e-4d84-8a42-e32aa359b706', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('ffe1ca6f-d40b-44b9-8cab-416073106bed', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('e2ee49e6-b22e-4a96-afc1-6c08a5c51d29', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('a9073b59-f237-4d76-a12a-012ffbde2256', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('082317dc-a002-4a53-bec5-c15dd8bb2b24', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('727daa51-9b32-4e14-99f6-fa29938bdafc', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('338079a5-2ff0-4084-aca0-60033a43b259', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('07b9343f-cd0d-4275-97cd-2e8fbf7d64f8', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('bc8d7e36-f989-49fe-8541-3bc5cd4d392b', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('0c160ee9-2e16-48ad-944c-90d076644bec', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('a17f972d-1034-44f1-b628-7a32376326a1', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('8f99cc22-8ea1-430f-8520-c65a2ff8f8af', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('a2c2b2ba-f005-44f8-84d7-9320dd530b87', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('cac556ab-4578-4b2f-8da7-a4ac82228388', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('fc1247e9-b1fe-463b-add2-0016b9cb0353', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('2e0fd9c8-4433-4471-8caf-b3fd3f1e018f', '	Les ménages');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('1612dd80-e9e0-445a-8b83-04f3ee047c7f', '	Les revendeurs');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('92efb323-9d41-45e4-9ac6-71fe8c37ac5f', '	Les grossistes');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('7e2a7731-3f64-498a-bee2-6d1f26dba234', '	Les particuliers');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('55221034-341b-45b7-98f3-5564685c2769', '	Relations de confiance, de fidélité et de reconnaissance');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('b77cd334-a397-480b-b5af-7908192ef7cb', '	Les réseaux sociaux (Facebook Watsa pp)');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('0ffa0ab9-6411-4830-9ac1-88a5c48b51b3', '	Des appels téléphoniques');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('e600fee5-c3ef-4d3b-821f-7f95fbca69d5', '	Portes à portes');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('8a9fdb05-41ef-4dbb-9fe1-5d3898b30709', '	Prix fixé en fonction du niveau de vie du client');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('6d7da7ad-d7c5-47b6-a320-3b515ba14c26', '	Non utilisation des produits chimiques (surtout jouer sur la qualité des produits)');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('b5363e63-a872-4053-abc0-e998508bda47', '	La production et la vente des produits horticoles');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('e030cc35-dd15-49e2-bfec-e4a149ba88e9', '	Un technicien agronome');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('1f04781c-f1a6-4eb3-be24-0f3f80b46914', '	Un ouvrier agricole');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('4e09a701-9873-4c6b-83c4-524521ed26a3', '	Un technicien agronome');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('f410ee7a-aefe-4413-b696-3ee37a982760', '	Outils agricoles (arrosoir pulvérisateur brouette, râteaux, etc.)');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('e902e2c9-d74e-4609-9f86-e5367dfc8e9b', '	Fonds de roulement');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('8a0c7a16-6700-4c68-bc41-e644cdd72fba', 'La famille');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('107b0e98-f981-49e1-92cc-55eb65b1f33e', '	Les banques');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('d01cba85-9831-43d4-9250-c3849a4fdfb3', '	Les ONG');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('c8283649-8c78-4062-bc3f-ba0d58282ad6', '	Le BEL');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('b04739d6-4a84-431c-98b7-fee41a6b9cde', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('41e8bd90-efde-443a-a19a-169b86c46dc8', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('eec3f846-e8fe-4a49-99cf-900da5fb6154', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('2218e3f3-cbb6-457e-9c54-aef678cb8b37', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('f6352f5a-cd56-48a5-8c10-60b5a4d9dcdf', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('9ebae96e-e42b-4050-b5f6-16c84d46567e', 'test');
INSERT INTO public.sous_bloc (id_sous_bloc, valeur) VALUES ('12acae08-b5c3-4f9d-a28a-2baf10ed1b4f', 'test');


--
-- TOC entry 3744 (class 0 OID 188062)
-- Dependencies: 213
-- Data for Name: block_model_economique_sous_blocs; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('fdfaadeb-10c0-4ffa-8a6e-88c83e6313ad', 'a77eae47-cd06-47db-b02d-ab05fd484550');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('30278aa9-1267-4f6f-9c9d-5a96803a5ca5', 'a450b8b6-dcd6-4deb-bb00-57bcada51e55');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('84352fbe-3fdc-45d1-9921-cbfb49fe8677', '4293d938-c4e3-4586-9326-7c0d49f65248');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('ea1ac5d4-e725-4c57-9d51-4eb2307b275b', 'c7607eeb-2d1b-4fd6-840d-ad521196df07');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('322b0fe5-bb99-4919-8074-cae829ef13ca', 'fff242a8-781f-4acf-b66a-6d3bd7beb263');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('03791dcb-66d7-49d9-af06-3753678fd87a', 'd5b85009-3fb6-42c9-a662-a8731390881b');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('f2856ed1-a3ac-403f-99ff-90c721a2cbce', '8385bb7d-80cd-4034-ad61-02375a991f10');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('137f0306-be93-4229-b5c2-38637c037a63', 'a54c325e-12e6-4cd6-b83b-66a585cc106e');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('04cd2a9f-d12d-4ead-9081-8d25c46f6053', '42c5b60c-6279-44db-b8cc-96ce81c843b8');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('45136eda-6f73-45df-bfdb-eaf99e317dd7', '99d126b6-589e-4eba-bb54-8f9e2ac70a70');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('c7839033-a293-4b9a-b703-471cc363eba1', 'd0fdf4aa-4a0d-4047-b178-f808171fda65');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('e5d7eb1d-8398-4825-870f-6127d9fcdcb2', 'b66d68d5-1eac-4487-9a48-e6cd3f461f52');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('f983d44d-0aae-4f55-b8ff-c157f7e9af57', '8b48d335-d74f-4ea5-8a25-579fc8195458');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('99bcdcf1-6aa1-4f1f-9914-a347ce8f0620', '30b4fea7-9415-4864-986d-a052e609e970');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('f97a6437-3a6d-4874-8c23-16eaed4b3800', '6d6de817-4d58-462b-a08c-050dbd506aa8');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('189886a1-1470-4e07-bbb3-f4a4f9f79969', '891c97e6-a352-4dc1-824e-446ef412313b');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('8af70b7d-5b93-4952-951d-4623fa802e8b', '2f3f987a-7ace-4174-be2b-f2047e7e4763');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('181a08d6-f8d8-4481-bb25-8334b179c245', 'f96bbed0-31a6-4633-941a-c5a8b3d5dbb6');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('e151e074-8bd6-441c-b8fa-b9cc6346d3f7', '12f340bd-ad7a-4040-a248-7d8c209a78b6');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('e9cb095e-ad7f-422e-8b24-75c5e69d5123', '1d9871f6-d9bd-48fb-bae1-7ac810484f61');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('34b8967d-6c41-4f35-bfb7-e65eadab100d', '8985ba98-9db4-4e99-ae03-527388eefb67');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('2bc41196-69ff-4ff7-bad5-d3b4192985cd', '489ddbf1-75d4-4467-a93c-ba1f1fb6c376');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('993c9742-88f6-44df-bf36-b8b1265a87dd', '25c52a90-8a85-422b-91a0-b2c34b6f1b48');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('214e6a28-9f1d-4aea-985d-7a4c11ce3594', '24cdcb24-464f-421c-b05f-8f4b7799bc16');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('182085d5-90d3-4a1b-8296-6fc75acc3f58', '513c96e6-4824-47cd-926f-7ae1462e3d3f');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('ab08b74f-0184-461b-b160-c9f8469ea80f', 'da177093-2e41-460c-b24e-ef9028de3de9');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('3c2b68df-582b-4d4c-a7a2-8b99491fe8fb', '5e210310-7552-41d3-8d11-09630d583570');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('25d8edb5-0597-4a01-b4d3-44f6b3c60a2e', '80855d34-e1c5-47ae-a413-4d275c0680d8');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('f05eefbe-eb4b-4268-9569-ae906d626ffd', '8084de65-5131-4b34-9633-972daa3246b2');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('e01ea9d8-8a37-445c-a8f5-606b9309ce6e', 'e3372c98-18ea-4d5b-af5c-229a18fd5421');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('d0158c97-eeb1-4d81-ba9b-88bc2975abe7', 'b1e89e54-a88b-4f61-86b8-4ca881f58c6d');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('05ff7095-3fb5-40bf-ac20-49aa1449f500', '1aae1290-ea96-46d1-8d0b-5be527311293');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('6aacc09d-584c-4e8d-81aa-cc1b8f425fee', '3fa36b53-dd13-4ce0-b55c-985d31243c34');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('22daca05-7fa6-4925-bccd-3d8a354d43cf', '00c9984b-af3d-45b2-b2a1-f702c1db4635');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('5303ae62-d015-47d1-bcd1-fec3594ddf3c', 'c53d8250-c3ba-48b2-9de0-92980864905e');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('0018b52b-bf5f-4ba7-9e87-fa026320654f', 'b5b6fca0-de2f-4713-8bce-b02f9212cb5d');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('c25cd97b-51ba-4345-9ac5-b3a8fbd3e44f', '57b986d7-d387-4081-90fe-4ad05e28bb5e');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('06938c10-3334-42c1-a20f-f36f92aae643', '46ec8144-6cf8-43e1-b292-41b21d6feca9');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('2efe284e-e8fe-4971-96ff-4baeacc8bdce', '0258007d-6837-40b5-a657-fca437a70551');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('281e61f9-1737-4252-a7fe-803a82ea3e5c', '2b69cb01-8a12-4b50-ae02-4673040be838');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('64685f94-63f0-49b3-b4a0-d0431e4bc140', '2bcf8907-f0c8-4b3e-a645-c2a6830667e0');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('3b0f5b27-5c13-4d2c-b0f2-ad13c85599cc', '4c59716b-291b-4cc4-9bd0-75ea7bd55f18');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('1b1004c3-becb-49fc-bd0e-0d7fde6eaf85', 'f0aba85f-2302-4af9-b539-75307002e5ed');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('caf8d845-00e1-4edb-b121-e7089038b230', '00b0e1ed-21de-4486-9fae-0aff2e2c8d31');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('456d10e4-65cb-43a2-8ea0-8e20d9871b71', 'db915356-b586-49b7-84f3-3017acf2e4bf');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('b1b1460f-05c6-4203-b966-b9ac1d65d32c', '627d87b2-724a-4ddf-b7ca-899c590baccc');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('e3055b5e-93de-4e93-a209-a8c3fe4a78b6', 'c2714f81-ffb8-45ba-861c-80d9a2049329');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('387beb97-a5b8-4f6e-8084-890cce850fdc', '2b284bce-db93-4473-8d9b-1fc5584c5e7d');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('387288b7-66b9-4bb4-ba98-5185f9d44fc2', '691d02c6-7cf1-4697-b8ee-00f258ad2f31');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('2944f009-46a8-41b5-8574-a63a4c4e1365', '2c2afb15-29e9-4a84-83d4-5c6a12f52b45');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('39af7775-90f7-4ead-b911-3259517e6335', '4d6566dc-d875-4ede-8056-95cabcae424e');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('194ed703-b8ec-4a8f-80f0-30f57a6ab0f2', '283666cf-3c9e-4a65-a773-42812e7d6e68');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('166cc51a-766d-4e38-9263-566fd447670b', 'f13978f4-417f-493b-b8fa-1a117aca9d94');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('e261b8f5-7b4d-4ffc-bbef-d4b62fb4096f', 'adbc38d9-c25a-4a67-938d-7910448c5c50');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('507c3473-09c5-4aaa-b8f8-5c060feefb1d', '39b7aa5e-c62b-4f57-99ff-ba6962195a9e');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('73c09e49-5b37-467c-b41c-4ca058dc7cf9', 'fed5c30f-5fe7-44e8-92b2-390a5c6547c6');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('452d112b-5d42-47e3-b8be-5d3d10d62cb5', 'b646b8e5-71ff-48dc-a0c4-79f12c757326');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('a533815d-a4a8-408f-924c-0d4c5440aa51', '3d13f786-c32a-4cd0-af19-daeb4ea10731');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('d88d6d94-4cab-4cb1-8b48-6d187c743aec', '531cf039-fcfb-46f9-8b8e-bfc4d4af1449');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('457695f9-89bb-41f3-b86f-51c585a8c7ed', 'c54ed9e1-a452-4a9a-a3bb-fa9de4d4acc4');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('b6a34cb2-c8d6-4577-92be-89193aa085ad', '67968852-1d73-4ea6-b3c4-2fb8055b5e64');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('6e7e8209-aefd-4780-9782-532ab929df75', '5ed7c5b5-4569-4a38-80fa-17b1e18d1cbb');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('24bb7d7b-c7fb-4d5e-a52f-e8f1b0365c4e', '186940ce-cda4-4a3e-b6c0-db35abf58395');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('2b110ad4-600a-41cb-8200-98369f3e1ad9', '948b9be2-fca6-4b6a-b70c-70174cbdc211');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('a98e9f9d-c9eb-45fb-9dcd-a4079b0c4007', 'b6cc9357-e626-4ea4-a029-061de7758aff');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('2acd0e5d-8ea4-4d5c-8ee3-c45d224be713', 'df751866-c7bb-4fa8-a4fa-9b788ca6e49c');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('d6021cae-ad4a-4374-ab15-f8020d0c3a59', 'b72507c4-f019-4d05-b0bb-2b09f4727765');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('656dee8c-9ab5-4a0d-a897-78e6e79da69e', 'e6e4c2fb-d7d3-4b78-8048-54a9adc3e028');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('62f69f1e-48cc-476b-b143-aeb788186b5a', '15c1223d-c202-42fe-8bf3-b21c0d130d6c');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('c973d9e1-8862-4dd5-ad93-27f3fc73f8c9', 'b0d4c4b8-c3b7-4856-ad04-4951219a2183');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('dac8e124-7c77-44f2-b6fa-89171b1ab798', 'a2ae86dc-865a-494c-89a3-6edc890eb0d8');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('2149ced9-0ff7-484e-ab21-91fcc8931621', '84eb0756-3583-4b10-9509-77a9f5d17f3b');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('63a91269-1ab1-4c52-b39e-06aca311b02d', '45afd35a-3de4-496e-8027-055d13cc3c72');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('989ffc38-6a60-453e-b2ae-fbf9d318d4f4', '36bb7007-9569-4b28-96aa-7d9e82997515');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('0db4dd64-6e89-404f-811f-751251632507', '7e7370a6-7f5e-4d84-8a42-e32aa359b706');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('3865a7fc-238c-43e2-b5c7-fda645ccfb48', 'ffe1ca6f-d40b-44b9-8cab-416073106bed');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('f9932ea1-fb6c-492f-9b84-59e2917d84c6', 'e2ee49e6-b22e-4a96-afc1-6c08a5c51d29');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('c9bbe03f-e283-428f-ae2f-a5fd2cea2cd4', 'a9073b59-f237-4d76-a12a-012ffbde2256');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('489a525b-a61e-4a89-8025-925f3cc7d3c5', '082317dc-a002-4a53-bec5-c15dd8bb2b24');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('68cfe0cd-95ab-4346-98ae-09e0b6700baa', '727daa51-9b32-4e14-99f6-fa29938bdafc');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('1a271709-6c77-4454-835f-532158dc62cc', '338079a5-2ff0-4084-aca0-60033a43b259');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('6a6c7db2-0a04-4fc2-9407-8d4e24be95d8', '07b9343f-cd0d-4275-97cd-2e8fbf7d64f8');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('aceead3f-0e4f-4036-a465-73c136a74ba4', 'bc8d7e36-f989-49fe-8541-3bc5cd4d392b');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('aceead3f-0e4f-4036-a465-73c136a74ba4', '0c160ee9-2e16-48ad-944c-90d076644bec');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('aceead3f-0e4f-4036-a465-73c136a74ba4', 'a17f972d-1034-44f1-b628-7a32376326a1');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('aceead3f-0e4f-4036-a465-73c136a74ba4', '8f99cc22-8ea1-430f-8520-c65a2ff8f8af');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('aceead3f-0e4f-4036-a465-73c136a74ba4', 'a2c2b2ba-f005-44f8-84d7-9320dd530b87');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('aceead3f-0e4f-4036-a465-73c136a74ba4', 'cac556ab-4578-4b2f-8da7-a4ac82228388');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('6bfaf794-accf-49b9-bd73-e731197cc341', 'fc1247e9-b1fe-463b-add2-0016b9cb0353');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('975ed0ea-71f5-450e-b300-6aa9f47e4d7c', '2e0fd9c8-4433-4471-8caf-b3fd3f1e018f');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('975ed0ea-71f5-450e-b300-6aa9f47e4d7c', '1612dd80-e9e0-445a-8b83-04f3ee047c7f');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('975ed0ea-71f5-450e-b300-6aa9f47e4d7c', '92efb323-9d41-45e4-9ac6-71fe8c37ac5f');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('975ed0ea-71f5-450e-b300-6aa9f47e4d7c', '7e2a7731-3f64-498a-bee2-6d1f26dba234');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('b95a4ec4-d949-4193-9fa7-936e1c22ce2e', '55221034-341b-45b7-98f3-5564685c2769');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('e830a071-2f46-4dc9-ae48-56e519301f65', 'b77cd334-a397-480b-b5af-7908192ef7cb');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('e830a071-2f46-4dc9-ae48-56e519301f65', '0ffa0ab9-6411-4830-9ac1-88a5c48b51b3');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('e830a071-2f46-4dc9-ae48-56e519301f65', 'e600fee5-c3ef-4d3b-821f-7f95fbca69d5');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('33dc565f-1ea2-43bd-9a24-f9f34b584a1e', '8a9fdb05-41ef-4dbb-9fe1-5d3898b30709');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('33dc565f-1ea2-43bd-9a24-f9f34b584a1e', '6d7da7ad-d7c5-47b6-a320-3b515ba14c26');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('be112b29-0797-4065-bebf-7a9ded3409d0', 'b5363e63-a872-4053-abc0-e998508bda47');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('be112b29-0797-4065-bebf-7a9ded3409d0', 'e030cc35-dd15-49e2-bfec-e4a149ba88e9');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('0e33f4f4-06a6-408b-a008-05b1b09fc63b', '1f04781c-f1a6-4eb3-be24-0f3f80b46914');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('0e33f4f4-06a6-408b-a008-05b1b09fc63b', '4e09a701-9873-4c6b-83c4-524521ed26a3');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('0e33f4f4-06a6-408b-a008-05b1b09fc63b', 'f410ee7a-aefe-4413-b696-3ee37a982760');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('0e33f4f4-06a6-408b-a008-05b1b09fc63b', 'e902e2c9-d74e-4609-9f86-e5367dfc8e9b');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('c0997376-4a0a-4bd9-86c6-2441da215beb', '8a0c7a16-6700-4c68-bc41-e644cdd72fba');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('c0997376-4a0a-4bd9-86c6-2441da215beb', '107b0e98-f981-49e1-92cc-55eb65b1f33e');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('c0997376-4a0a-4bd9-86c6-2441da215beb', 'd01cba85-9831-43d4-9250-c3849a4fdfb3');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('c0997376-4a0a-4bd9-86c6-2441da215beb', 'c8283649-8c78-4062-bc3f-ba0d58282ad6');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('c5031ce0-bed2-454f-9ddd-cce3f66071c6', 'b04739d6-4a84-431c-98b7-fee41a6b9cde');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('4459fc1f-ee1c-4d0a-b8e3-5bbdeb661565', '41e8bd90-efde-443a-a19a-169b86c46dc8');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('ae668c4d-ea8c-4e1c-b48f-3b2686083dc9', 'eec3f846-e8fe-4a49-99cf-900da5fb6154');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('9916b93a-8a69-48bb-b4f3-b4125ecad5eb', '2218e3f3-cbb6-457e-9c54-aef678cb8b37');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('e15e5cfd-7abe-4d56-a54d-f3a83d154bf2', 'f6352f5a-cd56-48a5-8c10-60b5a4d9dcdf');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('a332d1ba-c11e-42ad-95c8-6d8504bcb2c3', '9ebae96e-e42b-4050-b5f6-16c84d46567e');
INSERT INTO public.block_model_economique_sous_blocs (block_model_economique_id_blocme, sous_blocs_id_sous_bloc) VALUES ('f2511bd5-c89b-4e4e-907d-94570c7a0568', '12acae08-b5c3-4f9d-a28a-2baf10ed1b4f');


--
-- TOC entry 3745 (class 0 OID 188065)
-- Dependencies: 214
-- Data for Name: categories; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.categories (id, libelle) VALUES (1, 'Insertion à l''emploi');
INSERT INTO public.categories (id, libelle) VALUES (2, 'Insertion ou Reconversion par l''auto-emploi');
INSERT INTO public.categories (id, libelle) VALUES (3, 'Renforcement ou extension activité');
INSERT INTO public.categories (id, libelle) VALUES (4, 'Migrants');


--
-- TOC entry 3746 (class 0 OID 188068)
-- Dependencies: 215
-- Data for Name: cohorte; Type: TABLE DATA; Schema: public; Owner: -
--


--
-- TOC entry 3748 (class 0 OID 188074)
-- Dependencies: 217id
-- Data for Name: collectif; Type: TABLE DATA; Schema: public; Owner: -
--

--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('GUINAW RAILS SUD', '77 675 44 21', '', '2014-05-02', 'ande ligueyale Seydina mouhamet PSL', NULL, 'non', 'responsable_morale', 30, 0, 30, 34, 1, 'b47eedc2-468d-4779-8128-b684cd1affc7',  NULL, NULL);
--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('Gandiaye', '76 887 89 00', '', '2021-12-01', 'DIAM BOUGOUME', NULL, 'non', 'responsable_morale', 30, 25, 5, 189, 2, '9d5e3952-3968-4887-b59c-4146106fea1c', NULL, NULL, NULL, NULL);
--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('Gandiaye', '78 334 67 98', '', '2021-01-01', 'Takkou Ligueye Ngaraf', NULL, 'non', 'responsable_morale', 20, 10, 10, 189, 2, 'e04c20fd-be62-4f8a-8996-8bef1842ccd1',   NULL, NULL);
--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('Gandiaye', '77 656 77 88', '', '2021-01-01', 'Takkou Ligueye Thioupan', NULL, 'non', 'responsable_morale', 20, 20, 0, 189, 2, 'de954c55-9220-49fd-99ea-b73d4414c8a2',   NULL, NULL);
--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('Gandiaye', '75 020 26 78', '', '2021-01-01', 'Takkou Ligueye Ngaraf', NULL, 'non', 'responsable_morale', 30, 30, 0, 189, 2, '77b771cf-8e7d-4b8e-a9ed-f8dbfed53c11',   NULL, NULL);
--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('GUINAW RAILS SUD', '76 565 77 00', '', '0219-10-10', 'Centre socio-culturel et educatif de Niaguis', NULL, 'oui', 'responsable_delegue', 11, 3, 8, 34, 3, '16848287-b22d-44f1-bd39-1f6eaa4ce559',   NULL, NULL);
--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('GUINAW RAILS SUD', '676 767 118', '', '2014-02-11', 'GIE DES DELEGUES DE QUARTIER ', NULL, 'non', 'responsable_delegue', 67, 0, 67, 34, 2, '0ad0488e-b8a2-4541-9035-fe5232b24028',   NULL, NULL);
--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('GUINAW RAILS SUD', '77 634 22 22', '', '2021-01-24', 'GIE LES MAITRES GUINAW RAILS SUD (GIE/MTGRS)', NULL, 'non', 'responsable_morale', 19, 7, 11, 34, 2, '1b3464c9-a025-4ac2-ae1d-db76bae599ba',   NULL, NULL);
--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('Gandiaye', '78 667 88 76', '', '2021-01-01', 'MOUVEMENT DES TAILLEURS DE GANDIAYE ( MTGG)', NULL, 'oui', 'responsable_morale', 47, 2, 45, 189, 2, '798c65aa-392d-482e-abd3-c73a7fb8e902',   NULL, NULL);
--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('Gandiaye', '76 554 12 34', '', '2021-01-01', 'GROUPEMENT ROSE', NULL, 'non', 'responsable_morale', 20, 20, 0, 189, 2, '92144f1a-2062-4824-bb19-fb011087e56b',   NULL, NULL);
--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('Gandiaye', '78 990 88 77', '', '2021-01-01', 'NDOK JAM Khout', NULL, 'non', 'responsable_morale', 30, 30, 0, 189, 2, 'acbe34af-fe1a-4728-be7a-a0f87d47a0ce',   NULL, NULL);
--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('Gandiaye', '33 876 99 89', '', '2021-01-02', 'BOOK GUEUM', NULL, 'non', 'responsable_morale', 20, 20, 0, 189, 2, '3892bff0-0f72-4080-b5a9-79f39164f663',   NULL, NULL);
--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('Gandiaye', '77 890 00 00', '', '2021-01-01', 'Dame de Coeurs', NULL, 'non', 'responsable_morale', 50, 25, 25, 189, 2, '0697d88d-9a41-4542-a8cf-c07222d3a342',   NULL, NULL);
--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('Gandiaye', '88778 998', '', '2021-01-01', 'Takkou Ligueye BAMTARA', NULL, 'non', 'responsable_morale', 29, 19, 10, 189, 2, '9589a6db-730c-4f83-926c-a4be14586691',   NULL, NULL);
--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('Gandiaye', '79 889 00 00', '', '2021-02-02', 'MOUVEMENT DES NEIGNONS', NULL, 'non', 'responsable_morale', 73, 73, 0, 189, 2, '564a4d05-c87b-4a21-86a1-66908c18a250',   NULL, NULL);
--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('Gandiaye', '77 676 88 00', '', '2021-01-01', 'SANT YALLA SWT', NULL, 'non', 'responsable_morale', 30, 30, 0, 189, 2, '0efd4faf-f800-4b14-be42-93d0c310df44',   NULL, NULL);
--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('GUINAW RAILS SUD', '77 876 88 99', '', '2017-03-06', 'hIP HOP ART ( Art Développement National)', NULL, 'oui', 'responsable_morale', 17, 4, 13, 34, 1, '6b1e0f7b-1203-4f1d-aa00-c236a10af7af',   NULL, NULL);
--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('Gandiaye', '78 980 00 99', '', '2021-01-01', 'SOP SEN DIEUKEUR', NULL, 'non', 'responsable_morale', 29, 29, 0, 189, 2, '761693f8-dcf4-4c20-82be-96f00176a75a',   NULL, NULL);
--INSERT INTO public.collectif (commune_interieur, contact1_representant, contact2_representant, date_creation, denomination, quartier_village_interieur, reconnaissance_juridique, titre, total_membre, total_femmes, total_homme, commune_id, forme_juridique_id, idpie,   rapport_analyse1collectif_id, rapport_analyse2collectif_id) VALUES ('GUINAW RAILS SUD', '77 865 77 99', '', '2014-09-02', 'KHELCOM TYPE SERVICES ( GKS)', NULL, 'oui', 'responsable_morale', 11, 6, 5, 34, 4, '9feb64e3-6867-4f47-a07a-fd4cc6e6c357',   NULL, NULL);
--
--


INSERT INTO public.zone (id_zone, libelle, actif) OVERRIDING SYSTEM VALUE VALUES (1, 'Zone Ouest', true);
INSERT INTO public.zone (id_zone, libelle, actif) OVERRIDING SYSTEM VALUE VALUES (2, 'Zone Centre', true);
INSERT INTO public.zone (id_zone, libelle, actif) OVERRIDING SYSTEM VALUE VALUES (3, 'Zone Sud', true);
INSERT INTO public.zone (id_zone, libelle, actif) OVERRIDING SYSTEM VALUE VALUES (4, 'Zone Oriental', true);
INSERT INTO public.zone (id_zone, libelle, actif) OVERRIDING SYSTEM VALUE VALUES (5, 'Zone Nord', true);
INSERT INTO public.zone (id_zone, libelle, actif) OVERRIDING SYSTEM VALUE VALUES (33, 'test zone 47485', false);

--
-- TOC entry 3763 (class 0 OID 148087)
-- Dependencies: 237
-- Data for Name: pays; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.pays (id, libelle) VALUES (1, 'Afghanistan');
INSERT INTO public.pays (id, libelle) VALUES (2, 'Afrique du Sud');
INSERT INTO public.pays (id, libelle) VALUES (3, 'Albanie');
INSERT INTO public.pays (id, libelle) VALUES (4, 'Algérie');
INSERT INTO public.pays (id, libelle) VALUES (5, 'Allemagne');
INSERT INTO public.pays (id, libelle) VALUES (6, 'Andorre');
INSERT INTO public.pays (id, libelle) VALUES (7, 'Angola');
INSERT INTO public.pays (id, libelle) VALUES (8, 'Anguilla');
INSERT INTO public.pays (id, libelle) VALUES (9, 'Antarctique');
INSERT INTO public.pays (id, libelle) VALUES (10, 'Antigua-et-Barbuda');
INSERT INTO public.pays (id, libelle) VALUES (11, 'Antilles néerlandaises');
INSERT INTO public.pays (id, libelle) VALUES (12, 'Arabie saoudite');
INSERT INTO public.pays (id, libelle) VALUES (13, 'Argentine');
INSERT INTO public.pays (id, libelle) VALUES (14, 'Arménie');
INSERT INTO public.pays (id, libelle) VALUES (15, 'Aruba');
INSERT INTO public.pays (id, libelle) VALUES (16, 'Australie');
INSERT INTO public.pays (id, libelle) VALUES (17, 'Autriche');
INSERT INTO public.pays (id, libelle) VALUES (18, 'Azerbaïdjan');
INSERT INTO public.pays (id, libelle) VALUES (19, 'Bahamas');
INSERT INTO public.pays (id, libelle) VALUES (20, 'Bahreïn');
INSERT INTO public.pays (id, libelle) VALUES (21, 'Bangladesh');
INSERT INTO public.pays (id, libelle) VALUES (22, 'Barbade');
INSERT INTO public.pays (id, libelle) VALUES (23, 'Belau');
INSERT INTO public.pays (id, libelle) VALUES (24, 'Belgique');
INSERT INTO public.pays (id, libelle) VALUES (25, 'Belize');
INSERT INTO public.pays (id, libelle) VALUES (26, 'Bénin');
INSERT INTO public.pays (id, libelle) VALUES (27, 'Bermudes');
INSERT INTO public.pays (id, libelle) VALUES (28, 'Bhoutan');
INSERT INTO public.pays (id, libelle) VALUES (29, 'Biélorussie');
INSERT INTO public.pays (id, libelle) VALUES (30, 'Birmanie');
INSERT INTO public.pays (id, libelle) VALUES (31, 'Bolivie');
INSERT INTO public.pays (id, libelle) VALUES (32, 'Bosnie-Herzégovine');
INSERT INTO public.pays (id, libelle) VALUES (33, 'Botswana');
INSERT INTO public.pays (id, libelle) VALUES (34, 'Brésil');
INSERT INTO public.pays (id, libelle) VALUES (35, 'Brunei');
INSERT INTO public.pays (id, libelle) VALUES (36, 'Bulgarie');
INSERT INTO public.pays (id, libelle) VALUES (37, 'Burkina Faso');
INSERT INTO public.pays (id, libelle) VALUES (38, 'Burundi');
INSERT INTO public.pays (id, libelle) VALUES (39, 'Cambodge');
INSERT INTO public.pays (id, libelle) VALUES (40, 'Cameroun');
INSERT INTO public.pays (id, libelle) VALUES (41, 'Canada');
INSERT INTO public.pays (id, libelle) VALUES (42, 'Cap-Vert');
INSERT INTO public.pays (id, libelle) VALUES (43, 'Chili');
INSERT INTO public.pays (id, libelle) VALUES (44, 'Chine');
INSERT INTO public.pays (id, libelle) VALUES (45, 'Chypre');
INSERT INTO public.pays (id, libelle) VALUES (46, 'Colombie');
INSERT INTO public.pays (id, libelle) VALUES (47, 'Comores');
INSERT INTO public.pays (id, libelle) VALUES (48, 'Congo');
INSERT INTO public.pays (id, libelle) VALUES (49, 'Corée du Nord');
INSERT INTO public.pays (id, libelle) VALUES (50, 'Corée du Sud');
INSERT INTO public.pays (id, libelle) VALUES (51, 'Costa Rica');
INSERT INTO public.pays (id, libelle) VALUES (52, 'Côte d''Ivoire');
INSERT INTO public.pays (id, libelle) VALUES (53, 'Croatie');
INSERT INTO public.pays (id, libelle) VALUES (54, 'Cuba');
INSERT INTO public.pays (id, libelle) VALUES (55, 'Danemark');
INSERT INTO public.pays (id, libelle) VALUES (56, 'Djibouti');
INSERT INTO public.pays (id, libelle) VALUES (57, 'Dominique');
INSERT INTO public.pays (id, libelle) VALUES (58, 'Égypte');
INSERT INTO public.pays (id, libelle) VALUES (59, 'Émirats arabes unis');
INSERT INTO public.pays (id, libelle) VALUES (60, 'Équateur');
INSERT INTO public.pays (id, libelle) VALUES (61, 'Érythrée');
INSERT INTO public.pays (id, libelle) VALUES (62, 'Espagne');
INSERT INTO public.pays (id, libelle) VALUES (63, 'Estonie');
INSERT INTO public.pays (id, libelle) VALUES (64, 'États-Unis');
INSERT INTO public.pays (id, libelle) VALUES (65, 'Éthiopie');
INSERT INTO public.pays (id, libelle) VALUES (66, 'ex-République yougoslave de Macédoine');
INSERT INTO public.pays (id, libelle) VALUES (67, 'Finlande');
INSERT INTO public.pays (id, libelle) VALUES (68, 'France');
INSERT INTO public.pays (id, libelle) VALUES (69, 'Gabon');
INSERT INTO public.pays (id, libelle) VALUES (70, 'Gambie');
INSERT INTO public.pays (id, libelle) VALUES (71, 'Géorgie');
INSERT INTO public.pays (id, libelle) VALUES (72, 'Ghana');
INSERT INTO public.pays (id, libelle) VALUES (73, 'Gibraltar');
INSERT INTO public.pays (id, libelle) VALUES (74, 'Grèce');
INSERT INTO public.pays (id, libelle) VALUES (75, 'Grenade');
INSERT INTO public.pays (id, libelle) VALUES (76, 'Groenland');
INSERT INTO public.pays (id, libelle) VALUES (77, 'Guadeloupe');
INSERT INTO public.pays (id, libelle) VALUES (78, 'Guam');
INSERT INTO public.pays (id, libelle) VALUES (79, 'Guatemala');
INSERT INTO public.pays (id, libelle) VALUES (80, 'Guinée');
INSERT INTO public.pays (id, libelle) VALUES (81, 'Guinée équatoriale');
INSERT INTO public.pays (id, libelle) VALUES (82, 'Guinée-Bissao');
INSERT INTO public.pays (id, libelle) VALUES (83, 'Guyana');
INSERT INTO public.pays (id, libelle) VALUES (84, 'Guyane française');
INSERT INTO public.pays (id, libelle) VALUES (85, 'Haïti');
INSERT INTO public.pays (id, libelle) VALUES (86, 'Honduras');
INSERT INTO public.pays (id, libelle) VALUES (87, 'Hong Kong');
INSERT INTO public.pays (id, libelle) VALUES (88, 'Hongrie');
INSERT INTO public.pays (id, libelle) VALUES (89, 'Ile Bouvet');
INSERT INTO public.pays (id, libelle) VALUES (90, 'Ile Christmas');
INSERT INTO public.pays (id, libelle) VALUES (91, 'Ile Norfolk');
INSERT INTO public.pays (id, libelle) VALUES (92, 'Iles Cayman');
INSERT INTO public.pays (id, libelle) VALUES (93, 'Iles Cook');
INSERT INTO public.pays (id, libelle) VALUES (94, 'Iles des Cocos ,Keeling)');
INSERT INTO public.pays (id, libelle) VALUES (95, 'Iles Falkland');
INSERT INTO public.pays (id, libelle) VALUES (96, 'Iles Féroé');
INSERT INTO public.pays (id, libelle) VALUES (97, 'Iles Fidji');
INSERT INTO public.pays (id, libelle) VALUES (98, 'Iles Géorgie du Sud et Sandwich du Sud');
INSERT INTO public.pays (id, libelle) VALUES (99, 'Iles Heard et McDonald');
INSERT INTO public.pays (id, libelle) VALUES (100, 'Iles Marshall');
INSERT INTO public.pays (id, libelle) VALUES (101, 'Iles mineures éloignées des États-Unis');
INSERT INTO public.pays (id, libelle) VALUES (102, 'Iles Pitcairn');
INSERT INTO public.pays (id, libelle) VALUES (103, 'Iles Salomon');
INSERT INTO public.pays (id, libelle) VALUES (104, 'Iles Svalbard et Jan Mayen');
INSERT INTO public.pays (id, libelle) VALUES (105, 'Iles Turks-et-Caicos');
INSERT INTO public.pays (id, libelle) VALUES (106, 'Iles Vierges américaines');
INSERT INTO public.pays (id, libelle) VALUES (107, 'Iles Vierges britanniques');
INSERT INTO public.pays (id, libelle) VALUES (108, 'Inde');
INSERT INTO public.pays (id, libelle) VALUES (109, 'Indonésie');
INSERT INTO public.pays (id, libelle) VALUES (110, 'Iran');
INSERT INTO public.pays (id, libelle) VALUES (111, 'Iraq');
INSERT INTO public.pays (id, libelle) VALUES (112, 'Irlande');
INSERT INTO public.pays (id, libelle) VALUES (113, 'Islande');
INSERT INTO public.pays (id, libelle) VALUES (114, 'Israël');
INSERT INTO public.pays (id, libelle) VALUES (115, 'Italie');
INSERT INTO public.pays (id, libelle) VALUES (116, 'Jamaïque');
INSERT INTO public.pays (id, libelle) VALUES (117, 'Japon');
INSERT INTO public.pays (id, libelle) VALUES (118, 'Jordanie');
INSERT INTO public.pays (id, libelle) VALUES (119, 'Kazakhstan');
INSERT INTO public.pays (id, libelle) VALUES (120, 'Kenya');
INSERT INTO public.pays (id, libelle) VALUES (121, 'Kirghizistan');
INSERT INTO public.pays (id, libelle) VALUES (122, 'Kiribati');
INSERT INTO public.pays (id, libelle) VALUES (123, 'Koweït');
INSERT INTO public.pays (id, libelle) VALUES (124, 'Laos');
INSERT INTO public.pays (id, libelle) VALUES (125, 'Lesotho');
INSERT INTO public.pays (id, libelle) VALUES (126, 'Lettonie');
INSERT INTO public.pays (id, libelle) VALUES (127, 'Liban');
INSERT INTO public.pays (id, libelle) VALUES (128, 'Liberia');
INSERT INTO public.pays (id, libelle) VALUES (129, 'Libye');
INSERT INTO public.pays (id, libelle) VALUES (130, 'Liechtenstein');
INSERT INTO public.pays (id, libelle) VALUES (131, 'Lituanie');
INSERT INTO public.pays (id, libelle) VALUES (132, 'Luxembourg');
INSERT INTO public.pays (id, libelle) VALUES (133, 'Macao');
INSERT INTO public.pays (id, libelle) VALUES (134, 'Madagascar');
INSERT INTO public.pays (id, libelle) VALUES (135, 'Malaisie');
INSERT INTO public.pays (id, libelle) VALUES (136, 'Malawi');
INSERT INTO public.pays (id, libelle) VALUES (137, 'Maldives');
INSERT INTO public.pays (id, libelle) VALUES (138, 'Mali');
INSERT INTO public.pays (id, libelle) VALUES (139, 'Malte');
INSERT INTO public.pays (id, libelle) VALUES (140, 'Mariannes du Nord');
INSERT INTO public.pays (id, libelle) VALUES (141, 'Maroc');
INSERT INTO public.pays (id, libelle) VALUES (142, 'Martinique');
INSERT INTO public.pays (id, libelle) VALUES (143, 'Maurice');
INSERT INTO public.pays (id, libelle) VALUES (144, 'Mauritanie');
INSERT INTO public.pays (id, libelle) VALUES (145, 'Mayotte');
INSERT INTO public.pays (id, libelle) VALUES (146, 'Mexique');
INSERT INTO public.pays (id, libelle) VALUES (147, 'Micronésie');
INSERT INTO public.pays (id, libelle) VALUES (148, 'Moldavie');
INSERT INTO public.pays (id, libelle) VALUES (149, 'Monaco');
INSERT INTO public.pays (id, libelle) VALUES (150, 'Mongolie');
INSERT INTO public.pays (id, libelle) VALUES (151, 'Montserrat');
INSERT INTO public.pays (id, libelle) VALUES (152, 'Mozambique');
INSERT INTO public.pays (id, libelle) VALUES (153, 'Namibie');
INSERT INTO public.pays (id, libelle) VALUES (154, 'Nauru');
INSERT INTO public.pays (id, libelle) VALUES (155, 'Népal');
INSERT INTO public.pays (id, libelle) VALUES (156, 'Nicaragua');
INSERT INTO public.pays (id, libelle) VALUES (157, 'Niger');
INSERT INTO public.pays (id, libelle) VALUES (158, 'Nigeria');
INSERT INTO public.pays (id, libelle) VALUES (159, 'Nioué');
INSERT INTO public.pays (id, libelle) VALUES (160, 'Norvège');
INSERT INTO public.pays (id, libelle) VALUES (161, 'Nouvelle-Calédonie');
INSERT INTO public.pays (id, libelle) VALUES (162, 'Nouvelle-Zélande');
INSERT INTO public.pays (id, libelle) VALUES (163, 'Oman');
INSERT INTO public.pays (id, libelle) VALUES (164, 'Ouganda');
INSERT INTO public.pays (id, libelle) VALUES (165, 'Ouzbékistan');
INSERT INTO public.pays (id, libelle) VALUES (166, 'Pakistan');
INSERT INTO public.pays (id, libelle) VALUES (167, 'Palestinien occupé, territoire');
INSERT INTO public.pays (id, libelle) VALUES (168, 'Panama');
INSERT INTO public.pays (id, libelle) VALUES (169, 'Papouasie-Nouvelle-Guinée');
INSERT INTO public.pays (id, libelle) VALUES (170, 'Paraguay');
INSERT INTO public.pays (id, libelle) VALUES (171, 'Pays-Bas');
INSERT INTO public.pays (id, libelle) VALUES (172, 'Pérou');
INSERT INTO public.pays (id, libelle) VALUES (173, 'Philippines');
INSERT INTO public.pays (id, libelle) VALUES (174, 'Pologne');
INSERT INTO public.pays (id, libelle) VALUES (175, 'Polynésie française');
INSERT INTO public.pays (id, libelle) VALUES (176, 'Porto Rico');
INSERT INTO public.pays (id, libelle) VALUES (177, 'Portugal');
INSERT INTO public.pays (id, libelle) VALUES (178, 'Qatar');
INSERT INTO public.pays (id, libelle) VALUES (179, 'République centrafricaine');
INSERT INTO public.pays (id, libelle) VALUES (180, 'République démocratique du Congo');
INSERT INTO public.pays (id, libelle) VALUES (181, 'République dominicaine');
INSERT INTO public.pays (id, libelle) VALUES (182, 'République tchèque');
INSERT INTO public.pays (id, libelle) VALUES (183, 'Réunion');
INSERT INTO public.pays (id, libelle) VALUES (184, 'Roumanie');
INSERT INTO public.pays (id, libelle) VALUES (185, 'Royaume-Uni');
INSERT INTO public.pays (id, libelle) VALUES (186, 'Russie');
INSERT INTO public.pays (id, libelle) VALUES (187, 'Rwanda');
INSERT INTO public.pays (id, libelle) VALUES (188, 'Sahara occidental');
INSERT INTO public.pays (id, libelle) VALUES (189, 'Saint-Christophe-et-Niévès');
INSERT INTO public.pays (id, libelle) VALUES (190, 'Sainte-Hélène');
INSERT INTO public.pays (id, libelle) VALUES (191, 'Sainte-Lucie');
INSERT INTO public.pays (id, libelle) VALUES (192, 'Saint-Marin');
INSERT INTO public.pays (id, libelle) VALUES (193, 'Saint-Pierre-et-Miquelon');
INSERT INTO public.pays (id, libelle) VALUES (194, 'Saint-Siège');
INSERT INTO public.pays (id, libelle) VALUES (195, 'Saint-Vincent-et-les-Grenadines');
INSERT INTO public.pays (id, libelle) VALUES (196, 'Salvador');
INSERT INTO public.pays (id, libelle) VALUES (197, 'Samoa');
INSERT INTO public.pays (id, libelle) VALUES (198, 'Samoa américaines');
INSERT INTO public.pays (id, libelle) VALUES (199, 'Sao Tomé-et-Principe');
INSERT INTO public.pays (id, libelle) VALUES (200, 'Sénégal');
INSERT INTO public.pays (id, libelle) VALUES (201, 'Seychelles');
INSERT INTO public.pays (id, libelle) VALUES (202, 'Sierra Leone');
INSERT INTO public.pays (id, libelle) VALUES (203, 'Singapour');
INSERT INTO public.pays (id, libelle) VALUES (204, 'Slovaquie');
INSERT INTO public.pays (id, libelle) VALUES (205, 'Slovénie');
INSERT INTO public.pays (id, libelle) VALUES (206, 'Somalie');
INSERT INTO public.pays (id, libelle) VALUES (207, 'Soudan');
INSERT INTO public.pays (id, libelle) VALUES (208, 'Sri Lanka');
INSERT INTO public.pays (id, libelle) VALUES (209, 'Suède');
INSERT INTO public.pays (id, libelle) VALUES (210, 'Suisse');
INSERT INTO public.pays (id, libelle) VALUES (211, 'Suriname');
INSERT INTO public.pays (id, libelle) VALUES (212, 'Swaziland');
INSERT INTO public.pays (id, libelle) VALUES (213, 'Syrie');
INSERT INTO public.pays (id, libelle) VALUES (214, 'Tadjikistan');
INSERT INTO public.pays (id, libelle) VALUES (215, 'Taïwan');
INSERT INTO public.pays (id, libelle) VALUES (216, 'Tanzanie');
INSERT INTO public.pays (id, libelle) VALUES (217, 'Tchad');
INSERT INTO public.pays (id, libelle) VALUES (218, 'Terres australes françaises');
INSERT INTO public.pays (id, libelle) VALUES (219, 'Territoire britannique de l''Océan Indien');
INSERT INTO public.pays (id, libelle) VALUES (220, 'Thaïlande');
INSERT INTO public.pays (id, libelle) VALUES (221, 'Timor Oriental');
INSERT INTO public.pays (id, libelle) VALUES (222, 'Togo');
INSERT INTO public.pays (id, libelle) VALUES (223, 'Tokélaou');
INSERT INTO public.pays (id, libelle) VALUES (224, 'Tonga');
INSERT INTO public.pays (id, libelle) VALUES (225, 'Trinité-et-Tobago');
INSERT INTO public.pays (id, libelle) VALUES (226, 'Tunisie');
INSERT INTO public.pays (id, libelle) VALUES (227, 'Turkménistan');
INSERT INTO public.pays (id, libelle) VALUES (228, 'Turquie');
INSERT INTO public.pays (id, libelle) VALUES (229, 'Tuvalu');
INSERT INTO public.pays (id, libelle) VALUES (230, 'Ukraine');
INSERT INTO public.pays (id, libelle) VALUES (231, 'Uruguay');
INSERT INTO public.pays (id, libelle) VALUES (232, 'Vanuatu');
INSERT INTO public.pays (id, libelle) VALUES (233, 'Venezuela');
INSERT INTO public.pays (id, libelle) VALUES (234, 'Viêt Nam');
INSERT INTO public.pays (id, libelle) VALUES (235, 'Wallis-et-Futuna');
INSERT INTO public.pays (id, libelle) VALUES (236, 'Yémen');
INSERT INTO public.pays (id, libelle) VALUES (237, 'Yougoslavie');
INSERT INTO public.pays (id, libelle) VALUES (238, 'Zambie');
INSERT INTO public.pays (id, libelle) VALUES (239, 'Zimbabwe');

--
-- TOC entry 3777 (class 0 OID 148143)
-- Dependencies: 251
-- Data for Name: region; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.region (id, libelle, pays_id, zone_id_zone) VALUES (4, 'KAFFRINE', 200, 2);
INSERT INTO public.region (id, libelle, pays_id, zone_id_zone) VALUES (1, 'DAKAR', 200, 1);
INSERT INTO public.region (id, libelle, pays_id, zone_id_zone) VALUES (13, 'THIES', 200, 1);
INSERT INTO public.region (id, libelle, pays_id, zone_id_zone) VALUES (2, 'DIOURBEL', 200, 1);
INSERT INTO public.region (id, libelle, pays_id, zone_id_zone) VALUES (3, 'FATICK', 200, 2);
INSERT INTO public.region (id, libelle, pays_id, zone_id_zone) VALUES (7, 'KOLDA', 200, 33);
INSERT INTO public.region (id, libelle, pays_id, zone_id_zone) VALUES (12, 'TAMBACOUNDA', 200, 4);
INSERT INTO public.region (id, libelle, pays_id, zone_id_zone) VALUES (6, 'KEDOUGOU', 200, 4);
INSERT INTO public.region (id, libelle, pays_id, zone_id_zone) VALUES (10, 'SAINT LOUIS', 200, 5);
INSERT INTO public.region (id, libelle, pays_id, zone_id_zone) VALUES (9, 'MATAM', 200, 5);
INSERT INTO public.region (id, libelle, pays_id, zone_id_zone) VALUES (8, 'LOUGA', 200, 5);
INSERT INTO public.region (id, libelle, pays_id, zone_id_zone) VALUES (14, 'ZIGUINCHOR', 200, 3);
INSERT INTO public.region (id, libelle, pays_id, zone_id_zone) VALUES (11, 'SEDHIOU', 200, 3);
INSERT INTO public.region (id, libelle, pays_id, zone_id_zone) VALUES (5, 'KAOLACK', 200, 2);




--
-- TOC entry 3750 (class 0 OID 148046)
-- Dependencies: 224
-- Data for Name: departement; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.departement (id, libelle, region_id) VALUES (1, 'DAKAR', 1);
INSERT INTO public.departement (id, libelle, region_id) VALUES (2, 'GUEDIAWAYE', 1);
INSERT INTO public.departement (id, libelle, region_id) VALUES (3, 'KEUR MASSAR', 1);
INSERT INTO public.departement (id, libelle, region_id) VALUES (4, 'PIKINE', 1);
INSERT INTO public.departement (id, libelle, region_id) VALUES (5, 'RUFISQUE', 1);
INSERT INTO public.departement (id, libelle, region_id) VALUES (6, 'BAMBEY', 2);
INSERT INTO public.departement (id, libelle, region_id) VALUES (7, 'DIOURBEL', 2);
INSERT INTO public.departement (id, libelle, region_id) VALUES (8, 'MBACKE', 2);
INSERT INTO public.departement (id, libelle, region_id) VALUES (9, 'FATICK', 3);
INSERT INTO public.departement (id, libelle, region_id) VALUES (10, 'FOUNDIOUGNE', 3);
INSERT INTO public.departement (id, libelle, region_id) VALUES (11, 'GOSSAS', 3);
INSERT INTO public.departement (id, libelle, region_id) VALUES (12, 'BIRKELANE', 4);
INSERT INTO public.departement (id, libelle, region_id) VALUES (13, 'KAFFRINE', 4);
INSERT INTO public.departement (id, libelle, region_id) VALUES (14, 'KOUNGHEUL', 4);
INSERT INTO public.departement (id, libelle, region_id) VALUES (15, 'MALEM HODDAR', 4);
INSERT INTO public.departement (id, libelle, region_id) VALUES (16, 'GUINGUINEO', 5);
INSERT INTO public.departement (id, libelle, region_id) VALUES (17, 'KAOLACK', 5);
INSERT INTO public.departement (id, libelle, region_id) VALUES (18, 'NIORO', 5);
INSERT INTO public.departement (id, libelle, region_id) VALUES (19, 'KEDOUGOU', 6);
INSERT INTO public.departement (id, libelle, region_id) VALUES (20, 'SALEMATA', 6);
INSERT INTO public.departement (id, libelle, region_id) VALUES (21, 'SARAYA', 6);
INSERT INTO public.departement (id, libelle, region_id) VALUES (22, 'KOLDA', 7);
INSERT INTO public.departement (id, libelle, region_id) VALUES (23, 'MEDINA YORO FOULAH', 7);
INSERT INTO public.departement (id, libelle, region_id) VALUES (24, 'VELINGARA', 7);
INSERT INTO public.departement (id, libelle, region_id) VALUES (25, 'KEBEMER', 8);
INSERT INTO public.departement (id, libelle, region_id) VALUES (26, 'LINGUERE', 8);
INSERT INTO public.departement (id, libelle, region_id) VALUES (27, 'LOUGA', 8);
INSERT INTO public.departement (id, libelle, region_id) VALUES (28, 'KANEL', 9);
INSERT INTO public.departement (id, libelle, region_id) VALUES (29, 'MATAM', 9);
INSERT INTO public.departement (id, libelle, region_id) VALUES (30, 'RANEROU FERLO', 9);
INSERT INTO public.departement (id, libelle, region_id) VALUES (31, 'DAGANA', 10);
INSERT INTO public.departement (id, libelle, region_id) VALUES (32, 'PODOR', 10);
INSERT INTO public.departement (id, libelle, region_id) VALUES (33, 'SAINT LOUIS', 10);
INSERT INTO public.departement (id, libelle, region_id) VALUES (34, 'BOUNKILING', 11);
INSERT INTO public.departement (id, libelle, region_id) VALUES (35, 'GOUDOMP', 11);
INSERT INTO public.departement (id, libelle, region_id) VALUES (36, 'SEDHIOU', 11);
INSERT INTO public.departement (id, libelle, region_id) VALUES (37, 'BAKEL', 12);
INSERT INTO public.departement (id, libelle, region_id) VALUES (38, 'GOUDIRY', 12);
INSERT INTO public.departement (id, libelle, region_id) VALUES (39, 'KOUMPENTOUM', 12);
INSERT INTO public.departement (id, libelle, region_id) VALUES (40, 'TAMBACOUNDA', 12);
INSERT INTO public.departement (id, libelle, region_id) VALUES (41, 'MBOUR', 13);
INSERT INTO public.departement (id, libelle, region_id) VALUES (42, 'THIES', 13);
INSERT INTO public.departement (id, libelle, region_id) VALUES (43, 'TIVAOUANE', 13);
INSERT INTO public.departement (id, libelle, region_id) VALUES (44, 'BIGNONA', 14);
INSERT INTO public.departement (id, libelle, region_id) VALUES (45, 'OUSSOUYE', 14);
INSERT INTO public.departement (id, libelle, region_id) VALUES (46, 'ZIGUINCHOR', 14);



--
-- TOC entry 3744 (class 0 OID 148028)
-- Dependencies: 218
-- Data for Name: commune; Type: TABLE DATA; Schema: public; Owner: -
--


INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (1, 'GOREE', 1, '031202020001');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (2, 'Plateau', 1, '031202020002');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (3, 'GUEULE TAPEE-FASS-COLOBANE', 1, '031202020003');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (4, 'FANN - POINT E - AMITIE', 1, '031202020004');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (5, 'MEDINA', 1, '031202020005');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (6, 'BISCUITERIE', 1, '031202020006');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (7, 'GRAND DAKAR', 1, '031202020007');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (9, 'HANN - BEL AIR', 1, '031202020009');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (10, 'SICAP LIBERTE', 1, '031202020010');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (11, 'H.L.M', 1, '031202020011');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (12, 'MERMOZ - SACRE COEUR', 1, '031202020012');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (13, 'OUAKAM', 1, '031202020013');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (14, 'NGOR', 1, '031202020014');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (15, 'YOFF', 1, '031202020015');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (16, 'GRANDYOFF', 1, '031202020016');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (17, 'PATTE D''OIE', 1, '031202020017');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (18, 'CAMBERENE', 1, '031202020018');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (19, 'PARCELLES ASSAINIES', 1, '031202020019');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (20, 'GOLF SUD', 2, '031202020020');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (21, 'SAM NOTAIRE', 2, '031202020021');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (22, 'NDIAREME LIMAMOULAYE', 2, '031202020022');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (23, 'MEDINA GOUNASS', 2, '031202020023');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (24, 'WAKHINANE NIMZATH', 2, '031202020024');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (25, 'MALIKA', 3, '031202020025');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (26, 'KEUR MASSAR NORD', 3, '031202020026');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (27, 'JAXAAY - PARCELLES', 3, '031202020027');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (28, 'KEUR MASSAR SUD', 3, '031202020028');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (29, 'YEUMBEUL NORD', 3, '031202020029');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (30, 'YEUMBEUL SUD', 3, '031202020030');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (31, 'DALIFORT', 4, '031202020031');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (32, 'DJIDAH THIAROYE KAO', 4, '031202020032');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (33, 'GUINAW RAIL NORD', 4, '031202020033');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (34, 'GUINAW RAIL SUD', 4, '031202020034');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (35, 'PIKINE EST', 4, '031202020035');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (36, 'PIKINE QUEST', 4, '031202020036');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (37, 'PIKINE NORD', 4, '031202020037');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (38, 'DIAMAGUEUNE SICAP BAO', 4, '031202020038');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (39, 'MBAO', 4, '031202020039');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (40, 'THIAROYE GARE', 4, '031202020040');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (41, 'THIAROYE SUR MER', 4, '031202020041');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (42, 'TIVAOUANE DIACKSAO', 4, '031202020042');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (43, 'BARGNY', 5, '031202020043');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (44, 'SENDOU', 5, '031202020044');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (45, 'RUFISOUE EST', 5, '031202020045');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (46, 'RUFISQUE NORD', 5, '031202020046');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (47, 'RUFISQUE QUEST', 5, '031202020047');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (48, 'DIAMNIADIO', 5, '031202020048');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (49, 'SEBIKOTANE', 5, '031202020049');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (50, 'YENNE', 5, '031202020050');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (51, 'BAMBILOR', 5, '031202020051');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (52, 'TIVAOUANE PEUL - NIAGA', 5, '031202020052');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (53, 'SANGALKAM', 5, '031202020053');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (54, 'BAMBEY', 6, '031202020054');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (55, 'DINGUIRAYE', 6, '031202020055');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (56, 'BABA GARAGE', 6, '031202020056');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (57, 'KEUR SAMBA KANE', 6, '031202020057');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (58, 'NGOYE', 6, '031202020058');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (59, 'NDANGALMA', 6, '031202020059');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (60, 'NDONDOL', 6, '031202020060');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (61, 'THIAKHAR', 6, '031202020061');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (62, 'LAMBAYE', 6, '031202020062');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (63, 'REFANE', 6, '031202020063');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (64, 'GAWANE', 6, '031202020064');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (65, 'NGOGOM', 6, '031202020065');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (66, 'DIOURBEL', 7, '031202020066');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (67, 'GADE ESCALE', 7, '031202020067');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (68, 'KEUR NGALGOU', 7, '031202020068');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (69, 'NDANKH SENE', 7, '031202020069');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (70, 'NDINDY', 7, '031202020070');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (71, 'TAIBA MOUTOUPHA', 7, '031202020071');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (72, 'TOUBA LAPPE', 7, '031202020072');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (73, 'NDOULO', 7, '031202020073');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (74, 'NGOHE', 7, '031202020074');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (75, 'PATTAR', 7, '031202020075');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (76, 'TOCKY GARE', 7, '031202020076');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (77, 'TOURE MBONDE', 7, '031202020077');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (78, 'MBACKE', 8, '031202020078');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (79, 'DALLA NGABOU', 8, '031202020079');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (80, 'MISSIRAH', 8, '031202020080');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (81, 'NGAYE', 8, '031202020081');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (82, 'TOUBA FALL', 8, '031202020082');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (83, 'TOUBA MOSQUEE', 8, '031202020083');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (84, 'DAROU SALAM TYP', 8, '031202020084');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (85, 'DAROU NAHIM', 8, '031202020085');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (86, 'KAEL', 8, '031202020086');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (87, 'MADINA', 8, '031202020087');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (88, 'TOUBA MBOUL', 8, '031202020088');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (89, 'TAIBA THIEKENE', 8, '031202020089');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (90, 'DENDEYE GOUYE GUI', 8, '031202020090');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (91, 'NDIOUMANE', 8, '031202020091');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (92, 'SADIO', 8, '031202020092');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (93, 'TAIF', 8, '031202020093');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (94, 'FATICK', 9, '031202020094');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (95, 'DIOFFIOR', 9, '031202020095');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (96, 'DIAKHAO', 9, '031202020096');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (97, 'DIAOULE', 9, '031202020097');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (98, 'MBELLACADIAO', 9, '031202020098');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (99, 'NDIOB', 9, '031202020099');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (100, 'THIARE NDIALGUI', 9, '031202020100');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (101, 'DJILASSE', 9, '031202020101');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (102, 'FIMELA', 9, '031202020102');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (103, 'LOUL SESSENE', 9, '031202020103');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (104, 'PALMARIN FACAO', 9, '031202020104');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (105, 'NIAKHAR', 9, '031202020105');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (106, 'NGAYOKHEME', 9, '031202020106');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (107, 'PATAR', 9, '031202020107');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (108, 'DIARRERE', 9, '031202020108');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (109, 'DIOUROUP', 9, '031202020109');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (110, 'TATTAGUINE', 9, '031202020110');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (111, 'FOUNDIOUGNE', 10, '031202020111');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (112, 'SOKONE', 10, '031202020112');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (113, 'DIAGANE BARKA', 10, '031202020113');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (114, 'DIOSSIONG', 10, '031202020114');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (115, 'DJILOR', 10, '031202020115');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (116, 'MBAM', 10, '031202020116');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (117, 'NIASSENE', 10, '031202020117');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (118, 'PASSY', 10, '031202020118');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (119, 'SOUM', 10, '031202020119');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (120, 'BASSOUL', 10, '031202020120');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (121, 'DIONEWAR', 10, '031202020121');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (122, 'DJIRNDA', 10, '031202020122');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (123, 'KEUR SALOUM DIANE', 10, '031202020123');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (124, 'KEUR SAMBA GUEYE', 10, '031202020124');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (125, 'KARANG POSTE', 10, '031202020125');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (126, 'NIORO ALASSANE TALL', 10, '031202020126');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (127, 'TOUBACOUTA', 10, '031202020127');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (128, 'GOSSAS', 11, '031202020128');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (129, 'COLOBANE', 11, '031202020129');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (130, 'MBAR', 11, '031202020130');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (131, 'NDIENE LAGANE', 11, '031202020131');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (132, 'OUADIOUR', 11, '031202020132');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (133, 'PATAR LIA', 11, '031202020133');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (134, 'BIRKELANE', 12, '031202020134');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (135, 'DIAMAL', 12, '031202020135');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (136, 'KEUR MBOUCKI', 12, '031202020136');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (137, 'TOUBA MBELLA', 12, '031202020137');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (138, 'MABO', 12, '031202020138');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (139, 'MBEULEUP', 12, '031202020139');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (140, 'NDIOGNICK', 12, '031202020140');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (141, 'SEGRE GATTA', 12, '031202020141');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (142, 'KAFFRINE', 13, '031202020142');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (143, 'NGANDA', 13, '031202020143');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (144, 'BOULEL', 13, '031202020144');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (145, 'GNIBY', 13, '031202020145');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (146, 'KAHI', 13, '031202020146');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (147, 'DIAMAGADIO', 13, '031202020147');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (148, 'DIOKOUL MBELBOUCK', 13, '031202020148');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (149, 'KATHIOTTE', 13, '031202020149');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (150, 'MEDINATOUL SALAM 2', 13, '031202020150');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (151, 'KOUNGHEUL', 14, '031202020151');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (152, 'FASS THIEKENE', 14, '031202020152');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (153, 'IDA MOURIDE', 14, '031202020153');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (154, 'SALY ESCALE', 14, '031202020154');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (155, 'LOUR ESCALE', 14, '031202020155');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (156, 'RIBOT ESCALE', 14, '031202020156');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (157, 'MAKA YOP', 14, '031202020157');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (158, 'MISSIRAH WADENE', 14, '031202020158');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (159, 'NGAINTHE PATHE', 14, '031202020159');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (160, 'MALEM HODDAR', 15, '031202020160');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (161, 'DAROU MINAME', 15, '031202020161');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (162, 'KHELCOM', 15, '031202020162');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (163, 'NDIOBENE SAMBA LAMO', 15, '031202020163');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (164, 'NDIOUM NGAINTHE', 15, '031202020164');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (165, 'DIANKE SOUF', 15, '031202020165');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (166, 'SAGNA', 15, '031202020166');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (167, 'GUINGUINEO', 16, '031202020167');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (168, 'FASS', 16, '031202020168');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (169, 'KHELCOM BIRANE', 16, '031202020169');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (170, 'MBADAKHOUNE', 16, '031202020170');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (171, 'NDIAGO', 16, '031202020171');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (172, 'NGATHIE NAOUDE', 16, '031202020172');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (173, 'DARA MBOSS', 16, '031202020173');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (174, 'GAGNICK', 16, '031202020174');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (175, 'MBOSS', 16, '031202020175');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (176, 'NGELOU', 16, '031202020176');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (177, 'OUROUR', 16, '031202020177');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (178, 'PANAL OUOLOF', 16, '031202020178');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (179, 'KAOLACK', 17, '031202020179');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (180, 'KAHONE', 17, '031202020180');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (181, 'KEUR BAKA', 17, '031202020181');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (182, 'LATMINGUE', 17, '031202020182');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (183, 'NDOFFANE', 17, '031202020183');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (184, 'THIARE', 17, '031202020184');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (185, 'KEUR SOCE', 17, '031202020185');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (186, 'NDIAFFATE', 17, '031202020186');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (187, 'NDIEDIENG', 17, '031202020187');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (188, 'OYA', 17, '031202020188');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (189, 'GANDIAYE', 17, '031202020189');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (190, 'NDIEBEL', 17, '031202020190');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (191, 'SIBASSOR', 17, '031202020191');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (192, 'THIOMBY', 17, '031202020192');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (193, 'NIORO', 18, '031202020193');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (194, 'KAYEMOR', 18, '031202020194');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (195, 'MEDINA SABAKH', 18, '031202020195');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (196, 'NGAYENE', 18, '031202020196');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (197, 'DABALY', 18, '031202020197');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (198, 'DAROU SALAM', 18, '031202020198');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (199, 'GAINTHE KAYE', 18, '031202020199');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (200, 'PAOS KOTO', 18, '031202020200');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (201, 'POROKHANE', 18, '031202020201');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (202, 'TAIBA NIASSENE', 18, '031202020202');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (203, 'KEUR MABA DIAKHOU', 18, '031202020203');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (204, 'KEUR MADIABEL', 18, '031202020204');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (205, 'KEUR MANDONGO', 18, '031202020205');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (206, 'NDRAME ESCALE', 18, '031202020206');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (207, 'WACK NGOUNA', 18, '031202020207');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (208, 'KEDOUGOU', 19, '031202020208');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (209, 'BANDAFASSI', 19, '031202020209');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (210, 'DINDIFELO', 19, '031202020210');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (211, 'NINEFECHA', 19, '031202020211');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (212, 'TOMBORONCOTO', 19, '031202020212');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (213, 'DIMBOU', 19, '031202020213');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (214, 'FONGOLIMBI', 19, '031202020214');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (215, 'SALEMATA', 20, '031202020215');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (216, 'DAKATELI', 20, '031202020216');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (217, 'KEVOYE', 20, '031202020217');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (218, 'DAR SALAM', 20, '031202020218');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (219, 'ETHIOLO', 20, '031202020219');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (220, 'OUBADJI', 20, '031202020220');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (221, 'SARAYA', 21, '031202020221');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (222, 'BEMBOU', 21, '031202020222');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (223, 'MEDINA BAFFE', 21, '031202020223');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (224, 'KHOSSANTO', 21, '031202020224');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (225, 'MISSIRA SIRIMANA', 21, '031202020225');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (226, 'SABODALA', 21, '031202020226');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (227, 'KOLDA', 22, '031202020227');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (228, 'DIOULACOLON', 22, '031202020228');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (229, 'GUIRO YERO BOCAR', 22, '031202020229');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (230, 'MEDINA El HADJI', 22, '031202020230');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (231, 'SALIKEGNE', 22, '031202020231');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (232, 'SARE YOBA DIEGA', 22, '031202020232');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (233, 'TANKANTO ESCALE', 22, '031202020233');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (234, 'BAGADADJI', 22, '031202020234');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (235, 'DABO', 22, '031202020235');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (236, 'DIALAMBERE', 22, '031202020236');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (237, 'COUMBACARA', 22, '031202020237');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (238, 'MAMPATIM', 22, '031202020238');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (239, 'MEDINA CHERIF', 22, '031202020239');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (240, 'SARE BIDJI', 22, '031202020240');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (241, 'THIETY', 22, '031202020241');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (242, 'MEDINA YORO FOULAH', 23, '031202020242');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (243, 'BADION', 23, '031202020243');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (244, 'FAFACOUROU', 23, '031202020244');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (245, 'BOUROUCO', 23, '031202020245');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (246, 'BIGNIRABE', 23, '031202020246');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (247, 'KOULINTO', 23, '031202020247');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (248, 'NDORNA', 23, '031202020248');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (249, 'DINGUIRAYE', 23, '031202020249');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (250, 'KEREWANE', 23, '031202020250');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (251, 'NIAMING', 23, '031202020251');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (252, 'PATA', 23, '031202020252');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (253, 'VELINGARA', 24, '031202020253');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (254, 'BONCONTO', 24, '031202020254');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (255, 'LINKERING', 24, '031202020255');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (256, 'MEDINA GOUNASS', 24, '031202020256');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (257, 'SINTHIANG KOUNDARA', 24, '031202020257');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (258, 'OUASSADOU', 24, '031202020258');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (259, 'PAKOUR', 24, '031202020259');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (260, 'PAROUMBA', 24, '031202020260');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (261, 'DIAOBE - KABENDOU', 24, '031202020261');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (262, 'KANDIAYE', 24, '031202020262');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (263, 'KANDIA', 24, '031202020263');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (264, 'KOUNKANE', 24, '031202020264');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (265, 'NEMATABA', 24, '031202020265');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (266, 'SARE COLY SALLE', 24, '031202020266');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (267, 'KEBEMER', 25, '031202020267');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (268, 'DAROU MARNANE', 25, '031202020268');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (269, 'DAROU MOUSTY', 25, '031202020269');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (270, 'MBACKE CADIOR', 25, '031202020270');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (271, 'MBADIANE', 25, '031202020271');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (272, 'NDOYENNE', 25, '031202020272');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (273, 'SAM YABAL', 25, '031202020273');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (274, 'TOUBA MERINA', 25, '031202020274');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (275, 'BANDEGNE OUOLOF', 25, '031202020275');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (276, 'DIOKOUL DIAWRIGNE', 25, '031202020276');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (277, 'GUEOUL', 25, '031202020277');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (278, 'KAB GAYE', 25, '031202020278');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (279, 'NDANDE', 25, '031202020279');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (280, 'THIEPPE', 25, '031202020280');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (281, 'KANENE NDIOB', 25, '031202020281');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (282, 'NGOURANE OUOLOF', 25, '031202020282');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (283, 'LORO', 25, '031202020283');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (284, 'SAGATTA GUETH', 25, '031202020284');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (285, 'THIOLOM FALL', 25, '031202020285');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (286, 'DAHRA', 26, '031202020286');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (287, 'LINGUERE', 26, '031202020287');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (288, 'BARKEDJI', 26, '031202020288');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (289, 'GASSANE', 26, '031202020289');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (290, 'THIARGNY', 26, '031202020290');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (291, 'THIEL', 26, '031202020291');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (292, 'DODJI', 26, '031202020292');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (293, 'LABGAR', 26, '031202020293');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (294, 'OUARKHOKH', 26, '031202020294');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (295, 'BOULAL', 26, '031202020295');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (296, 'DEALY', 26, '031202020296');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (297, 'AFFE DJOLOF', 26, '031202020297');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (298, 'SAGATTA DJOLOF', 26, '031202020298');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (299, 'THIAMENE PASS', 26, '031202020299');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (300, 'KAMB', 26, '031202020300');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (301, 'MBEULEUKHE', 26, '031202020301');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (302, 'MBOULA', 26, '031202020302');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (303, 'TESSEKERE FORAGE ', 26, '031202020303');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (304, 'YANG YANG', 26, '031202020304');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (305, 'LOUGA', 27, '031202020305');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (306, 'COKI', 27, '031202020306');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (307, 'GUETH ARDO', 27, '031202020307');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (308, 'NDIAGNE', 27, '031202020308');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (309, 'PETE OUARACK', 27, '031202020309');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (310, 'THIAMENE CAYOR', 27, '031202020310');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (311, 'KEUR MOMAR SARR', 27, '031202020311');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (312, 'NGUER MALAL', 27, '031202020312');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (313, 'NGANDE', 27, '031202020313');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (314, 'SYER', 27, '031202020314');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (315, 'KELLE GUEYE', 27, '031202020315');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (316, 'MBEDIENE', 27, '031202020316');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (317, 'NGUIDILIE', 27, '031202020317');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (318, 'NIOMRE', 27, '031202020318');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (319, 'LEONA', 27, '031202020319');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (320, 'NGUEUNE SARR', 27, '031202020320');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (321, 'SAKAL', 27, '031202020321');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (322, 'KANEL', 28, '031202020322');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (323, 'AOURE', 28, '031202020323');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (324, 'BOKILADJI', 28, '031202020324');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (325, 'OUAOUNDE', 28, '031202020325');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (326, 'DEMBACANE', 28, '031202020326');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (327, 'ORKADIERE', 28, '031202020327');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (328, 'SEMME', 28, '031202020328');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (329, 'HAMADY HOUNARE', 28, '031202020329');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (330, 'NDENDORY', 28, '031202020330');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (331, 'ODOBERE', 28, '031202020331');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (332, 'SINTHIOU BAMAMBEBANADJI ', 28, '031202020332');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (333, 'WOURO SIDY', 28, '031202020333');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (334, 'MATAM', 29, '031202020334');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (335, 'OUROSSOGUI', 29, '031202020335');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (336, 'AGNAM CIVOL', 29, '031202020336');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (337, 'DABIA', 29, '031202020337');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (338, 'OREFONDE', 29, '031202020338');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (339, 'THILOGNE', 29, '031202020339');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (340, 'BOKIDIAWE', 29, '031202020340');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (341, 'NABADJI CIVOL', 29, '031202020341');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (342, 'NGUIDJILONE', 29, '031202020342');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (343, 'OGO', 29, '031202020343');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (344, 'RANEROU', 30, '031202020344');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (345, 'LOUGRE THIOLY', 30, '031202020345');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (346, 'OUDALAYE', 30, '031202020346');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (347, 'DAGANA', 31, '031202020347');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (348, 'RICHARD TOLL', 31, '031202020348');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (349, 'DIAMA', 31, '031202020349');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (350, 'NGNITH', 31, '031202020350');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (351, 'RONKH', 31, '031202020351');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (352, 'ROSS BETHIO', 31, '031202020352');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (353, 'ROSSO SENEGAL', 31, '031202020353');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (354, 'BOKHOL', 31, '031202020354');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (355, 'GAE', 31, '031202020355');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (356, 'MBANE', 31, '031202020356');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (357, 'NDOMBO SANDJIRY', 31, '031202020357');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (358, 'NDIOUM', 32, '031202020358');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (359, 'PODOR', 32, '031202020359');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (360, 'AERE LAO', 32, '031202020360');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (361, 'DOUMGALAO', 32, '031202020361');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (362, 'GOLLERE', 32, '031202020362');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (363, 'MADINA DIATHBE', 32, '031202020363');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (364, 'MBOUMBA', 32, '031202020364');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (365, 'MERV', 32, '031202020365');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (366, 'WALALDE', 32, '031202020366');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (367, 'BODE LAO', 32, '031202020367');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (368, 'DEMETTE', 32, '031202020368');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (369, 'DODEL', 32, '031202020369');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (370, 'GAMADJI SARE', 32, '031202020370');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (371, 'GUEDE CHANTIER', 32, '031202020371');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (372, 'GUEDE VILLAGE', 32, '031202020372');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (373, 'FANAYE', 32, '031202020373');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (374, 'NDIAYENE PEINDAO', 32, '031202020374');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (375, 'NIANDANE', 32, '031202020375');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (376, 'BOKE DIALOIJBE

', 32, '031202020376');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (377, 'GALOYA TOUCOULEUR', 32, '031202020377');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (378, 'MBOLO BIRANE', 32, '031202020378');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (379, 'PETE', 32, '031202020379');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (380, 'SAINT LOUIS', 33, '031202020380');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (381, 'FASS NGOM', 33, '031202020381');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (382, 'GANDON', 33, '031202020382');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (383, 'NDIEBENE GANDIOL ', 33, '031202020383');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (384, 'MPAL', 33, '031202020384');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (385, 'BOUNKILING', 34, '031202020385');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (386, 'BOGHAL', 34, '031202020386');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (387, 'DJINANY', 34, '031202020387');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (388, 'NDIAMACOUTA', 34, '031202020388');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (389, 'NDIAMALATHIEL', 34, '031202020389');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (390, 'TANKON', 34, '031202020390');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (391, 'BONA', 34, '031202020391');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (392, 'DIACOUNDA', 34, '031202020392');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (393, 'INOR', 34, '031202020393');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (394, 'KANDION MANGANA', 34, '031202020394');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (395, 'DIAMBATI', 34, '031202020395');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (396, 'DIAROUME', 34, '031202020396');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (397, 'FAOUNE', 34, '031202020397');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (398, 'MADINA WANDIFA', 34, '031202020398');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (399, 'GOUDOMP', 35, '031202020399');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (400, 'DIATTACOUNDA', 35, '031202020400');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (401, 'DJIBANAR', 35, '031202020401');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (402, 'KAOUR', 35, '031202020402');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (403, 'MANGAROUNGOU SANTO', 35, '031202020403');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (404, 'SAMINE', 35, '031202020404');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (405, 'SIMBANDI BALANTE', 35, '031202020405');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (406, 'YARANG BALANTE', 35, '031202020406');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (407, 'KARANTABA', 35, '031202020407');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (408, 'KOLIBANTANG', 35, '031202020408');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (409, 'BAGHERE ', 35, '031202020409');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (410, 'DIOUDOUBOU ', 35, '031202020410');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (411, 'NIAGHA ', 35, '031202020411');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (412, 'SIMBANDI BRASSOU', 35, '031202020412');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (413, 'TANAFF', 35, '031202020413');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (414, 'MARSASSOUM', 36, '031202020414');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (415, 'SEDHIOU', 36, '031202020415');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (416, 'DIANNAH BA', 36, '031202020416');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (417, 'DIANAH MALARY', 36, '031202020417');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (418, 'DIENDE', 36, '031202020418');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (419, 'KOUSSY', 36, '031202020419');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (420, 'OUDOUCAR', 36, '031202020420');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (421, 'SAKAR', 36, '031202020421');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (422, 'SAMA KANTA PEULH', 36, '031202020422');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (423, 'DJIBABOUYA', 36, '031202020423');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (424, 'BEMETBIDJINI', 36, '031202020424');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (425, 'SAN SAMBA', 36, '031202020425');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (426, 'BAMBALY', 36, '031202020426');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (427, 'DJIREDJI', 36, '031202020427');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (428, 'BAKEL', 37, '031202020428');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (429, 'BELE', 37, '031202020429');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (430, 'KIDIRA', 37, '031202020430');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (431, 'SINTHIOU FISSA', 37, '031202020431');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (432, 'GATHIARY', 37, '031202020432');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (433, 'MADINA FOULBE', 37, '031202020433');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (434, 'SADATOU', 37, '031202020434');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (435, 'TOUMBOURA', 37, '031202020435');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (436, 'BALLOU', 37, '031202020436');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (437, 'DIAWARA', 37, '031202020437');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (438, 'GABOU', 37, '031202020438');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (439, 'MOUDERY', 37, '031202020439');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (440, 'GOUDIRY', 38, '031202020440');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (441, 'BALA', 38, '031202020441');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (442, 'KOAR', 38, '031202020442');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (443, 'GOUMBAYEL', 38, '031202020443');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (444, 'BOYNGUEL BAMBA', 38, '031202020444');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (445, 'DOUGUE', 38, '031202020445');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (446, 'KOUSSAN', 38, '031202020446');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (447, 'SINTHIOU MAMADOU BOUBOU', 38, '031202020447');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (448, 'BANI ISRAEL', 38, '031202020448');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (449, 'BOUTOUCOUFARA', 38, '031202020449');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (450, 'DIANKE MAKHA', 38, '031202020450');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (451, 'KOMOTI', 38, '031202020451');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (452, 'KOULOR', 38, '031202020452');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (453, 'KOTHIARY', 38, '031202020453');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (454, 'SINTHIOU BOCAR ALY', 38, '031202020454');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (455, 'KOUMPENTOUM', 39, '031202020455');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (456, 'BAMBA THIALENE', 39, '031202020456');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (457, 'KAHENE', 39, '031202020457');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (458, 'MERETO', 39, '031202020458');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (459, 'NDAM', 39, '031202020459');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (460, 'MALEM NIANI', 39, '031202020460');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (461, 'KOUTHIABA OUOLOF ', 39, '031202020461');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (462, 'KOUTHIA GAYDI', 39, '031202020462');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (463, 'PASS KOTO', 39, '031202020463');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (464, 'PAYAR', 39, '031202020464');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (465, 'TAMBACOUNDA', 40, '031202020465');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (466, 'KOUSSANAR', 40, '031202020466');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (467, 'SINTHIOU MALEME', 40, '031202020467');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (468, 'MAKACOLIBAITTANG', 40, '031202020468');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (469, 'NDOGA BABACAR', 40, '031202020469');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (470, 'NIANI TOUCOULEUR ', 40, '031202020470');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (471, 'DIALACOTO', 40, '031202020471');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (472, 'MISSIRAH', 40, '031202020472');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (473, 'NETEBOULOU', 40, '031202020473');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (474, 'MBOUR', 41, '031202020474');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (475, 'JOAL FADHIOUT', 41, '031202020475');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (476, 'FISSEL', 41, '031202020476');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (477, 'NDIAGANIAO', 41, '031202020477');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (478, 'NGUENIENE', 41, '031202020478');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (479, 'SANDIARA', 41, '031202020479');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (480, 'SESSENE', 41, '031202020480');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (481, 'THIADIAYE', 41, '031202020481');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (482, 'DIASS', 41, '031202020482');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (483, 'MALICOUNDA', 41, '031202020483');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (484, 'NGAPAROU', 41, '031202020484');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (485, 'NGUEKHOKH', 41, '031202020485');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (486, 'POPENGUINE', 41, '031202020486');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (487, 'SALY PORTUDAL', 41, '031202020487');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (488, 'SINDIA', 41, '031202020488');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (489, 'SOMONE', 41, '031202020489');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (490, 'KHOMBOLE', 42, '031202020490');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (491, 'POUT', 42, '031202020491');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (492, 'THIES NORD', 42, '031202020492');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (493, 'THIES EST', 42, '031202020493');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (494, 'THIES OUEST', 42, '031202020494');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (495, 'DIENDER GUEDJ', 42, '031202020495');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (496, 'KAYAR', 42, '031202020496');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (497, 'KEUR MOUSSA', 42, '031202020497');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (498, 'FANDENE', 42, '031202020498');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (499, 'NOTTO', 42, '031202020499');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (500, 'TASSETE', 42, '031202020500');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (501, 'NDIEYENE SIRAKH', 42, '031202020501');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (502, 'NGOUDIANE', 42, '031202020502');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (503, 'THIENABA', 42, '031202020503');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (504, 'TOUBATOUL', 42, '031202020504');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (505, 'TIVAOUANE', 43, '031202020505');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (506, 'MEKHE', 43, '031202020506');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (507, 'DAROU KHOUDOSS', 43, '031202020507');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (508, 'MBORO', 43, '031202020508');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (509, 'MEOUANE', 43, '031202020509');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (510, 'TAIBA NDIAYE', 43, '031202020510');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (511, 'KOUL', 43, '031202020511');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (512, 'MERINA DAKHAR', 43, '031202020512');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (513, 'PEKESSE', 43, '031202020513');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (514, 'MBAYENE', 43, '031202020514');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (515, 'NIAKHENE', 43, '031202020515');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (516, 'NGANDIOUF', 43, '031202020516');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (517, 'THILMAKHA ', 43, '031202020517');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (518, 'MONT ROLLAND ', 43, '031202020518');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (519, 'NOTTO GOUYE DIAMA', 43, '031202020519');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (520, 'CHERIF LO', 43, '031202020520');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (521, 'PAMBAL', 43, '031202020521');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (522, 'PIRE GOU REYE', 43, '031202020522');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (523, 'BIGNONA', 44, '031202020523');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (524, 'THIONCK ESSYL', 44, '031202020524');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (525, 'DIOULOULOU', 44, '031202020525');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (526, 'DJINAKY', 44, '031202020526');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (527, 'KAFOUNTINE', 44, '031202020527');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (528, 'KATABA 1', 44, '031202020528');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (529, 'BALINGHORE', 44, '031202020529');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (530, 'DIEGOUNE', 44, '031202020530');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (531, 'KARTHIACK', 44, '031202020531');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (532, 'MANGAGOULACK', 44, '031202020532');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (533, 'MLOMP', 44, '031202020533');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (534, 'COUBALAN', 44, '031202020534');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (535, 'NIAMONE', 44, '031202020535');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (536, 'OUONCK', 44, '031202020536');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (537, 'TENGHORI', 44, '031202020537');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (538, 'DJIBIDIONE', 44, '031202020538');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (539, 'OULAMPANE', 44, '031202020539');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (540, 'SINDIAN', 44, '031202020540');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (541, 'SUEL', 44, '031202020541');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (542, 'OUSSOUYE', 45, '031202020542');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (543, 'DIEMBERING', 45, '031202020543');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (544, 'SANTHIABA MANJACK', 45, '031202020544');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (545, 'MLOMP', 45, '031202020545');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (546, 'OUKOUT', 45, '031202020546');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (547, 'ZIGUINCHOR', 46, '031202020547');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (548, 'ADEANE', 46, '031202020548');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (549, 'BOUTOUPA CAMARACOUNDA', 46, '031202020549');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (550, 'NIAGUIS', 46, '031202020550');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (551, 'ENAMPORE', 46, '031202020551');
INSERT INTO public.commune (id, libelle, departement_id, code) VALUES (552, 'NYASSIA', 46, '031202020552');

--
-- TOC entry 3750 (class 0 OID 188082)
-- Dependencies: 219
-- Data for Name: compte_rendue; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3751 (class 0 OID 188087)
-- Dependencies: 220
-- Data for Name: compte_rendue_createurs_content; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3797 (class 0 OID 188642)
-- Dependencies: 266
-- Data for Name: couverture_geographique; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.couverture_geographique (id, code, libelle) VALUES ('706539a1-bed2-4948-9ffe-fc3c1fd8d48d', 'national', 'Niveau National');
INSERT INTO public.couverture_geographique (id, code, libelle) VALUES ('29c3a845-856d-4f6b-8e24-6b2ff1d845bf', 'depatemental', 'Niveau Dépatemental');
INSERT INTO public.couverture_geographique (id, code, libelle) VALUES ('6710f525-eb05-42a8-9bb6-6b8534d932a8', 'communal', 'Niveau Communal');
INSERT INTO public.couverture_geographique (id, code, libelle) VALUES ('b3e9fb14-b4ef-4980-8973-1dc0a6a4de17', 'regional', 'Niveau Regional');
INSERT INTO public.couverture_geographique (id, code, libelle) VALUES ('9e5f47c6-d82d-48c7-b175-3d6a6c98f62d', 'horssenegal', 'En dehors de Sénégal');


--
-- TOC entry 3753 (class 0 OID 188091)
-- Dependencies: 222
-- Data for Name: createurs_compte_rendue; Type: TABLE DATA; Schema: public; Owner: -
--




--
-- TOC entry 3756 (class 0 OID 188100)
-- Dependencies: 225
-- Data for Name: documents; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3757 (class 0 OID 188103)
-- Dependencies: 226
-- Data for Name: forme_juridique; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.forme_juridique (id, libelle, code) VALUES (1, 'association', 'association');
INSERT INTO public.forme_juridique (id, libelle, code) VALUES (2, 'GIE', 'gie');
INSERT INTO public.forme_juridique (id, libelle, code) VALUES (3, 'Partenaire', 'partenaire');
INSERT INTO public.forme_juridique (id, libelle, code) VALUES (4, 'autres', 'autres');
INSERT INTO public.forme_juridique (id, libelle, code) VALUES (5, 'Organisation', 'organisation');
INSERT INTO public.forme_juridique (id, libelle, code) VALUES (6, 'Professionnelle', 'professionnelle');
INSERT INTO public.forme_juridique (id, libelle, code) VALUES (7, 'GPF', 'gpf');


--
-- TOC entry 3758 (class 0 OID 188106)
-- Dependencies: 227
-- Data for Name: idee_projet; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3759 (class 0 OID 188109)
-- Dependencies: 228
-- Data for Name: individuel; Type: TABLE DATA; Schema: public; Owner: -
--

--INSERT INTO public.individuel (experience_prof, formation_prof, niveau_etude, idpie, categories_id,  commune_id, commune_rattach_id, soutien_immediat_id_soutien, tranche_age_id, type_categorie, soutien_immediat_soutien, soutien_immediat_id, rapport_analyse1_id_rapport, rapport_analyse2_id, autre_region, banque_ousfd, hors_senegal, ninea, numero_rc, ref_professionnel, situation_actuel, commune_rattach_id) VALUES ('NON', 'NON', '', 'f3d91b8c-090c-4330-a73e-e3a9d5a5a511', 2, 34, NULL, 4, 4, 'createur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
--INSERT INTO public.individuel (experience_prof, formation_prof, niveau_etude, idpie, categories_id,  commune_id, commune_rattach_id, soutien_immediat_id_soutien, tranche_age_id, type_categorie, soutien_immediat_soutien, soutien_immediat_id, rapport_analyse1_id_rapport, rapport_analyse2_id, autre_region, banque_ousfd, hors_senegal, ninea, numero_rc, ref_professionnel, situation_actuel, commune_rattach_id) VALUES ('OUI', 'NON', 'Alphabetisation', '8419a4e4-cf87-435f-a79f-80558741dafe', 2, 34, NULL, 4, 3, 'createur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
--INSERT INTO public.individuel (experience_prof, formation_prof, niveau_etude, idpie, categories_id,  commune_id, commune_rattach_id, soutien_immediat_id_soutien, tranche_age_id, type_categorie, soutien_immediat_soutien, soutien_immediat_id, rapport_analyse1_id_rapport, rapport_analyse2_id, autre_region, banque_ousfd, hors_senegal, ninea, numero_rc, ref_professionnel, situation_actuel, commune_rattach_id) VALUES ('OUI', 'NON', 'Francais Universitaire', '92fef55d-23e7-49a4-acb6-a8cf917fafd8', 3, 40, NULL, NULL, 2, 'developpeur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
--INSERT INTO public.individuel (experience_prof, formation_prof, niveau_etude, idpie, categories_id,  commune_id, commune_rattach_id, soutien_immediat_id_soutien, tranche_age_id, type_categorie, soutien_immediat_soutien, soutien_immediat_id, rapport_analyse1_id_rapport, rapport_analyse2_id, autre_region, banque_ousfd, hors_senegal, ninea, numero_rc, ref_professionnel, situation_actuel, commune_rattach_id) VALUES ('OUI', 'NON', 'Francais Universitaire', '84d4012b-583e-40f5-9ab9-2207914d41c9', 2, 34, NULL, 4, 2, 'createur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
--INSERT INTO public.individuel (experience_prof, formation_prof, niveau_etude, idpie, categories_id,  commune_id, commune_rattach_id, soutien_immediat_id_soutien, tranche_age_id, type_categorie, soutien_immediat_soutien, soutien_immediat_id, rapport_analyse1_id_rapport, rapport_analyse2_id, autre_region, banque_ousfd, hors_senegal, ninea, numero_rc, ref_professionnel, situation_actuel, commune_rattach_id) VALUES ('OUI', 'OUI', 'Francais Universitaire', '453fc453-bab0-439a-a6d0-2fcc8f60f0fd', 2, 34, NULL, 5, 5, 'createur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
--INSERT INTO public.individuel (experience_prof, formation_prof, niveau_etude, idpie, categories_id,  commune_id, commune_rattach_id, soutien_immediat_id_soutien, tranche_age_id, type_categorie, soutien_immediat_soutien, soutien_immediat_id, rapport_analyse1_id_rapport, rapport_analyse2_id, autre_region, banque_ousfd, hors_senegal, ninea, numero_rc, ref_professionnel, situation_actuel, commune_rattach_id) VALUES ('NON', 'NON', 'Francais Secondaire', '509bdfb7-e651-4eb0-ba82-d0d843d2cdd6', 2, 34, NULL, 4, 1, 'createur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
--INSERT INTO public.individuel (experience_prof, formation_prof, niveau_etude, idpie, categories_id,  commune_id, commune_rattach_id, soutien_immediat_id_soutien, tranche_age_id, type_categorie, soutien_immediat_soutien, soutien_immediat_id, rapport_analyse1_id_rapport, rapport_analyse2_id, autre_region, banque_ousfd, hors_senegal, ninea, numero_rc, ref_professionnel, situation_actuel, commune_rattach_id) VALUES ('NON', 'OUI', 'Francais Universitaire', '1e9df31d-7c8b-401a-bd9e-176609e6517e', 3, 34, NULL, 4, 2, 'developpeur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
--INSERT INTO public.individuel (experience_prof, formation_prof, niveau_etude, idpie, categories_id,  commune_id, commune_rattach_id, soutien_immediat_id_soutien, tranche_age_id, type_categorie, soutien_immediat_soutien, soutien_immediat_id, rapport_analyse1_id_rapport, rapport_analyse2_id, autre_region, banque_ousfd, hors_senegal, ninea, numero_rc, ref_professionnel, situation_actuel, commune_rattach_id) VALUES ('OUI', 'OUI', 'Arabe General', '82d64dfd-042c-4539-b26b-74908c80ae39', 3, 34, NULL, 5, 2, 'developpeur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
--INSERT INTO public.individuel (experience_prof, formation_prof, niveau_etude, idpie, categories_id,  commune_id, commune_rattach_id, soutien_immediat_id_soutien, tranche_age_id, type_categorie, soutien_immediat_soutien, soutien_immediat_id, rapport_analyse1_id_rapport, rapport_analyse2_id, autre_region, banque_ousfd, hors_senegal, ninea, numero_rc, ref_professionnel, situation_actuel, commune_rattach_id) VALUES ('OUI', 'OUI', 'Francais Universitaire', '97ad2b03-8ad5-4cec-af09-f2594ee59b86', 2, 34, NULL, 3, 2, 'createur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
--INSERT INTO public.individuel (experience_prof, formation_prof, niveau_etude, idpie, categories_id,  commune_id, commune_rattach_id, soutien_immediat_id_soutien, tranche_age_id, type_categorie, soutien_immediat_soutien, soutien_immediat_id, rapport_analyse1_id_rapport, rapport_analyse2_id, autre_region, banque_ousfd, hors_senegal, ninea, numero_rc, ref_professionnel, situation_actuel, commune_rattach_id) VALUES ('OUI', 'OUI', 'Francais Secondaire', 'adaa5542-2496-43ae-be0b-aa8a548308fc', 2, 34, NULL, 4, 3, 'createur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
--INSERT INTO public.individuel (experience_prof, formation_prof, niveau_etude, idpie, categories_id,  commune_id, commune_rattach_id, soutien_immediat_id_soutien, tranche_age_id, type_categorie, soutien_immediat_soutien, soutien_immediat_id, rapport_analyse1_id_rapport, rapport_analyse2_id, autre_region, banque_ousfd, hors_senegal, ninea, numero_rc, ref_professionnel, situation_actuel, commune_rattach_id) VALUES ('OUI', 'NON', 'Francais Secondaire', '33f096cd-65f8-4898-adb1-8740a44aeaca', 2, 34, NULL, 4, 3, 'createur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
--INSERT INTO public.individuel (experience_prof, formation_prof, niveau_etude, idpie, categories_id,  commune_id, commune_rattach_id, soutien_immediat_id_soutien, tranche_age_id, type_categorie, soutien_immediat_soutien, soutien_immediat_id, rapport_analyse1_id_rapport, rapport_analyse2_id, autre_region, banque_ousfd, hors_senegal, ninea, numero_rc, ref_professionnel, situation_actuel, commune_rattach_id) VALUES ('OUI', 'OUI', 'Arabe General', '1ad925a8-6463-488a-8a1d-e8c981bf1668', 3, 34, NULL, 3, 3, 'developpeur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
--INSERT INTO public.individuel (experience_prof, formation_prof, niveau_etude, idpie, categories_id,  commune_id, commune_rattach_id, soutien_immediat_id_soutien, tranche_age_id, type_categorie, soutien_immediat_soutien, soutien_immediat_id, rapport_analyse1_id_rapport, rapport_analyse2_id, autre_region, banque_ousfd, hors_senegal, ninea, numero_rc, ref_professionnel, situation_actuel, commune_rattach_id) VALUES ('NON', 'NON', 'Francais Secondaire', '28958dff-cd16-4614-96a8-001099dffddd', 3, 189, NULL, 5, 2, 'developpeur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
--INSERT INTO public.individuel (experience_prof, formation_prof, niveau_etude, idpie, categories_id,  commune_id, commune_rattach_id, soutien_immediat_id_soutien, tranche_age_id, type_categorie, soutien_immediat_soutien, soutien_immediat_id, rapport_analyse1_id_rapport, rapport_analyse2_id, autre_region, banque_ousfd, hors_senegal, ninea, numero_rc, ref_professionnel, situation_actuel, commune_rattach_id) VALUES ('NON', 'OUI', 'Francais Universitaire', '2c577ef4-9b2a-4451-9d88-8746e4635e26', 2, 189, NULL, 5, 2, 'createur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
--INSERT INTO public.individuel (experience_prof, formation_prof, niveau_etude, idpie, categories_id,  commune_id, commune_rattach_id, soutien_immediat_id_soutien, tranche_age_id, type_categorie, soutien_immediat_soutien, soutien_immediat_id, rapport_analyse1_id_rapport, rapport_analyse2_id, autre_region, banque_ousfd, hors_senegal, ninea, numero_rc, ref_professionnel, situation_actuel, commune_rattach_id) VALUES ('NON', 'NON', 'Francais Secondaire', '594bc7c4-b713-423d-88f8-f02f5aebc312', 2, 189, NULL, 3, 3, 'createur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
--INSERT INTO public.individuel (experience_prof, formation_prof, niveau_etude, idpie, categories_id,  commune_id, commune_rattach_id, soutien_immediat_id_soutien, tranche_age_id, type_categorie, soutien_immediat_soutien, soutien_immediat_id, rapport_analyse1_id_rapport, rapport_analyse2_id, autre_region, banque_ousfd, hors_senegal, ninea, numero_rc, ref_professionnel, situation_actuel, commune_rattach_id) VALUES ('NON', 'NON', 'Francais Universitaire', '602a0fee-c2cf-41b2-aeab-c8e5575658fa', 3, 78, NULL, 4, 2, 'developpeur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
--INSERT INTO public.individuel (experience_prof, formation_prof, niveau_etude, idpie, categories_id,  commune_id, commune_rattach_id, soutien_immediat_id_soutien, tranche_age_id, type_categorie, soutien_immediat_soutien, soutien_immediat_id, rapport_analyse1_id_rapport, rapport_analyse2_id, autre_region, banque_ousfd, hors_senegal, ninea, numero_rc, ref_professionnel, situation_actuel, commune_rattach_id) VALUES ('NON', 'NON', 'Francais Secondaire', '7fc83b52-4327-4a5a-bc25-5cdda5b112dc', 2, 34, NULL, 3, 1, 'createur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);


--
-- TOC entry 3760 (class 0 OID 188114)
-- Dependencies: 229
-- Data for Name: individuel_idee_projets; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3761 (class 0 OID 188117)
-- Dependencies: 230
-- Data for Name: marqueur; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.marqueur (id, libelle, parcours_id) VALUES (1, 'Définition et accompagnement du projet professionnel', NULL);
INSERT INTO public.marqueur (id, libelle, parcours_id) VALUES (2, 'Accompagnement la recherche d’emploi/stage', NULL);
INSERT INTO public.marqueur (id, libelle, parcours_id) VALUES (3, 'Accompagnement de groupe', NULL);
INSERT INTO public.marqueur (id, libelle, parcours_id) VALUES (4, 'Accompagnement individualisé', NULL);
INSERT INTO public.marqueur (id, libelle, parcours_id) VALUES (5, 'Définition etaccompagnement du projetprofessionnel', NULL);
INSERT INTO public.marqueur (id, libelle, parcours_id) VALUES (6, 'Accompagnement individualisé ', NULL);
INSERT INTO public.marqueur (id, libelle, parcours_id) VALUES (7, 'Accompagnement individualisé ', NULL);
INSERT INTO public.marqueur (id, libelle, parcours_id) VALUES (8, 'Accompagnement de groupe - Accompagnement individualisé', NULL);
INSERT INTO public.marqueur (id, libelle, parcours_id) VALUES (9, 'Accompagnement individualisé', NULL);




--
-- TOC entry 3762 (class 0 OID 188120)
-- Dependencies: 231
-- Data for Name: model_economique; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.model_economique (id, status) VALUES ('eeff1f3c-c7b6-45bb-9894-72048f70f600', NULL);
INSERT INTO public.model_economique (id, status) VALUES ('8d590540-fa9f-4793-ba1f-e3b97139dd43', NULL);
INSERT INTO public.model_economique (id, status) VALUES ('5d0e71b9-a638-4f1f-a445-bf28721ce4cd', NULL);
INSERT INTO public.model_economique (id, status) VALUES ('cdcbe722-c7b6-455f-8942-e7f4dd693a7b', NULL);
INSERT INTO public.model_economique (id, status) VALUES ('b1678a91-9479-419c-a31b-cb3572bf27db', NULL);
INSERT INTO public.model_economique (id, status) VALUES ('05ed93d7-c40c-4068-bcea-50f04b505ae0', NULL);
INSERT INTO public.model_economique (id, status) VALUES ('8047b83d-acba-40a0-a20b-7d7a26a8c6ba', NULL);
INSERT INTO public.model_economique (id, status) VALUES ('dab51a86-578c-4e63-841f-ddb0eeba1d17', NULL);
INSERT INTO public.model_economique (id, status) VALUES ('b145aacd-cf31-437d-9d87-93645fe90b12', NULL);
INSERT INTO public.model_economique (id, status) VALUES ('52c1ba8a-82b5-4830-b63e-5a3f21a11aa7', NULL);
INSERT INTO public.model_economique (id, status) VALUES ('1b0c651f-ca16-4ca3-96f2-b29353cbee33', NULL);
INSERT INTO public.model_economique (id, status) VALUES ('e771f858-1c25-4f3b-bc6b-ca7e8822bf50', NULL);
INSERT INTO public.model_economique (id, status) VALUES ('47a68426-09ec-49ca-a21a-3ad1e920fc27', NULL);
INSERT INTO public.model_economique (id, status) VALUES ('08c1cbfd-6509-4dd5-b8d6-74ff35f16cd4', NULL);


--
-- TOC entry 3763 (class 0 OID 188123)
-- Dependencies: 232
-- Data for Name: model_economique_block_model_economique; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('eeff1f3c-c7b6-45bb-9894-72048f70f600', 'fdfaadeb-10c0-4ffa-8a6e-88c83e6313ad');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('eeff1f3c-c7b6-45bb-9894-72048f70f600', '30278aa9-1267-4f6f-9c9d-5a96803a5ca5');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('eeff1f3c-c7b6-45bb-9894-72048f70f600', '84352fbe-3fdc-45d1-9921-cbfb49fe8677');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('eeff1f3c-c7b6-45bb-9894-72048f70f600', 'ea1ac5d4-e725-4c57-9d51-4eb2307b275b');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('eeff1f3c-c7b6-45bb-9894-72048f70f600', '322b0fe5-bb99-4919-8074-cae829ef13ca');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('eeff1f3c-c7b6-45bb-9894-72048f70f600', '03791dcb-66d7-49d9-af06-3753678fd87a');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('eeff1f3c-c7b6-45bb-9894-72048f70f600', 'f2856ed1-a3ac-403f-99ff-90c721a2cbce');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('8d590540-fa9f-4793-ba1f-e3b97139dd43', '137f0306-be93-4229-b5c2-38637c037a63');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('8d590540-fa9f-4793-ba1f-e3b97139dd43', '04cd2a9f-d12d-4ead-9081-8d25c46f6053');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('8d590540-fa9f-4793-ba1f-e3b97139dd43', '45136eda-6f73-45df-bfdb-eaf99e317dd7');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('8d590540-fa9f-4793-ba1f-e3b97139dd43', 'c7839033-a293-4b9a-b703-471cc363eba1');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('8d590540-fa9f-4793-ba1f-e3b97139dd43', 'e5d7eb1d-8398-4825-870f-6127d9fcdcb2');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('8d590540-fa9f-4793-ba1f-e3b97139dd43', 'f983d44d-0aae-4f55-b8ff-c157f7e9af57');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('8d590540-fa9f-4793-ba1f-e3b97139dd43', '99bcdcf1-6aa1-4f1f-9914-a347ce8f0620');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('5d0e71b9-a638-4f1f-a445-bf28721ce4cd', 'f97a6437-3a6d-4874-8c23-16eaed4b3800');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('5d0e71b9-a638-4f1f-a445-bf28721ce4cd', '189886a1-1470-4e07-bbb3-f4a4f9f79969');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('5d0e71b9-a638-4f1f-a445-bf28721ce4cd', '8af70b7d-5b93-4952-951d-4623fa802e8b');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('5d0e71b9-a638-4f1f-a445-bf28721ce4cd', '181a08d6-f8d8-4481-bb25-8334b179c245');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('5d0e71b9-a638-4f1f-a445-bf28721ce4cd', 'e151e074-8bd6-441c-b8fa-b9cc6346d3f7');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('5d0e71b9-a638-4f1f-a445-bf28721ce4cd', 'e9cb095e-ad7f-422e-8b24-75c5e69d5123');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('5d0e71b9-a638-4f1f-a445-bf28721ce4cd', '34b8967d-6c41-4f35-bfb7-e65eadab100d');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('cdcbe722-c7b6-455f-8942-e7f4dd693a7b', '2bc41196-69ff-4ff7-bad5-d3b4192985cd');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('cdcbe722-c7b6-455f-8942-e7f4dd693a7b', '993c9742-88f6-44df-bf36-b8b1265a87dd');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('cdcbe722-c7b6-455f-8942-e7f4dd693a7b', '214e6a28-9f1d-4aea-985d-7a4c11ce3594');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('cdcbe722-c7b6-455f-8942-e7f4dd693a7b', '182085d5-90d3-4a1b-8296-6fc75acc3f58');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('cdcbe722-c7b6-455f-8942-e7f4dd693a7b', 'ab08b74f-0184-461b-b160-c9f8469ea80f');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('cdcbe722-c7b6-455f-8942-e7f4dd693a7b', '3c2b68df-582b-4d4c-a7a2-8b99491fe8fb');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('cdcbe722-c7b6-455f-8942-e7f4dd693a7b', '25d8edb5-0597-4a01-b4d3-44f6b3c60a2e');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('b1678a91-9479-419c-a31b-cb3572bf27db', 'f05eefbe-eb4b-4268-9569-ae906d626ffd');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('b1678a91-9479-419c-a31b-cb3572bf27db', 'e01ea9d8-8a37-445c-a8f5-606b9309ce6e');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('b1678a91-9479-419c-a31b-cb3572bf27db', 'd0158c97-eeb1-4d81-ba9b-88bc2975abe7');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('b1678a91-9479-419c-a31b-cb3572bf27db', '05ff7095-3fb5-40bf-ac20-49aa1449f500');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('b1678a91-9479-419c-a31b-cb3572bf27db', '6aacc09d-584c-4e8d-81aa-cc1b8f425fee');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('b1678a91-9479-419c-a31b-cb3572bf27db', '22daca05-7fa6-4925-bccd-3d8a354d43cf');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('b1678a91-9479-419c-a31b-cb3572bf27db', '5303ae62-d015-47d1-bcd1-fec3594ddf3c');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('05ed93d7-c40c-4068-bcea-50f04b505ae0', '0018b52b-bf5f-4ba7-9e87-fa026320654f');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('05ed93d7-c40c-4068-bcea-50f04b505ae0', 'c25cd97b-51ba-4345-9ac5-b3a8fbd3e44f');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('05ed93d7-c40c-4068-bcea-50f04b505ae0', '06938c10-3334-42c1-a20f-f36f92aae643');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('05ed93d7-c40c-4068-bcea-50f04b505ae0', '2efe284e-e8fe-4971-96ff-4baeacc8bdce');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('05ed93d7-c40c-4068-bcea-50f04b505ae0', '281e61f9-1737-4252-a7fe-803a82ea3e5c');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('05ed93d7-c40c-4068-bcea-50f04b505ae0', '64685f94-63f0-49b3-b4a0-d0431e4bc140');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('05ed93d7-c40c-4068-bcea-50f04b505ae0', '3b0f5b27-5c13-4d2c-b0f2-ad13c85599cc');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('8047b83d-acba-40a0-a20b-7d7a26a8c6ba', '1b1004c3-becb-49fc-bd0e-0d7fde6eaf85');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('8047b83d-acba-40a0-a20b-7d7a26a8c6ba', 'caf8d845-00e1-4edb-b121-e7089038b230');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('8047b83d-acba-40a0-a20b-7d7a26a8c6ba', '456d10e4-65cb-43a2-8ea0-8e20d9871b71');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('8047b83d-acba-40a0-a20b-7d7a26a8c6ba', 'b1b1460f-05c6-4203-b966-b9ac1d65d32c');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('8047b83d-acba-40a0-a20b-7d7a26a8c6ba', 'e3055b5e-93de-4e93-a209-a8c3fe4a78b6');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('8047b83d-acba-40a0-a20b-7d7a26a8c6ba', '387beb97-a5b8-4f6e-8084-890cce850fdc');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('8047b83d-acba-40a0-a20b-7d7a26a8c6ba', '387288b7-66b9-4bb4-ba98-5185f9d44fc2');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('dab51a86-578c-4e63-841f-ddb0eeba1d17', '2944f009-46a8-41b5-8574-a63a4c4e1365');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('dab51a86-578c-4e63-841f-ddb0eeba1d17', '39af7775-90f7-4ead-b911-3259517e6335');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('dab51a86-578c-4e63-841f-ddb0eeba1d17', '194ed703-b8ec-4a8f-80f0-30f57a6ab0f2');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('dab51a86-578c-4e63-841f-ddb0eeba1d17', '166cc51a-766d-4e38-9263-566fd447670b');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('dab51a86-578c-4e63-841f-ddb0eeba1d17', 'e261b8f5-7b4d-4ffc-bbef-d4b62fb4096f');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('dab51a86-578c-4e63-841f-ddb0eeba1d17', '507c3473-09c5-4aaa-b8f8-5c060feefb1d');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('dab51a86-578c-4e63-841f-ddb0eeba1d17', '73c09e49-5b37-467c-b41c-4ca058dc7cf9');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('b145aacd-cf31-437d-9d87-93645fe90b12', '452d112b-5d42-47e3-b8be-5d3d10d62cb5');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('b145aacd-cf31-437d-9d87-93645fe90b12', 'a533815d-a4a8-408f-924c-0d4c5440aa51');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('b145aacd-cf31-437d-9d87-93645fe90b12', 'd88d6d94-4cab-4cb1-8b48-6d187c743aec');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('b145aacd-cf31-437d-9d87-93645fe90b12', '457695f9-89bb-41f3-b86f-51c585a8c7ed');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('b145aacd-cf31-437d-9d87-93645fe90b12', 'b6a34cb2-c8d6-4577-92be-89193aa085ad');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('b145aacd-cf31-437d-9d87-93645fe90b12', '6e7e8209-aefd-4780-9782-532ab929df75');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('b145aacd-cf31-437d-9d87-93645fe90b12', '24bb7d7b-c7fb-4d5e-a52f-e8f1b0365c4e');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('52c1ba8a-82b5-4830-b63e-5a3f21a11aa7', '2b110ad4-600a-41cb-8200-98369f3e1ad9');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('52c1ba8a-82b5-4830-b63e-5a3f21a11aa7', 'a98e9f9d-c9eb-45fb-9dcd-a4079b0c4007');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('52c1ba8a-82b5-4830-b63e-5a3f21a11aa7', '2acd0e5d-8ea4-4d5c-8ee3-c45d224be713');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('52c1ba8a-82b5-4830-b63e-5a3f21a11aa7', 'd6021cae-ad4a-4374-ab15-f8020d0c3a59');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('52c1ba8a-82b5-4830-b63e-5a3f21a11aa7', '656dee8c-9ab5-4a0d-a897-78e6e79da69e');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('52c1ba8a-82b5-4830-b63e-5a3f21a11aa7', '62f69f1e-48cc-476b-b143-aeb788186b5a');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('52c1ba8a-82b5-4830-b63e-5a3f21a11aa7', 'c973d9e1-8862-4dd5-ad93-27f3fc73f8c9');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('1b0c651f-ca16-4ca3-96f2-b29353cbee33', 'dac8e124-7c77-44f2-b6fa-89171b1ab798');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('1b0c651f-ca16-4ca3-96f2-b29353cbee33', '2149ced9-0ff7-484e-ab21-91fcc8931621');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('1b0c651f-ca16-4ca3-96f2-b29353cbee33', '63a91269-1ab1-4c52-b39e-06aca311b02d');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('1b0c651f-ca16-4ca3-96f2-b29353cbee33', '989ffc38-6a60-453e-b2ae-fbf9d318d4f4');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('1b0c651f-ca16-4ca3-96f2-b29353cbee33', '0db4dd64-6e89-404f-811f-751251632507');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('1b0c651f-ca16-4ca3-96f2-b29353cbee33', '3865a7fc-238c-43e2-b5c7-fda645ccfb48');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('1b0c651f-ca16-4ca3-96f2-b29353cbee33', 'f9932ea1-fb6c-492f-9b84-59e2917d84c6');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('e771f858-1c25-4f3b-bc6b-ca7e8822bf50', 'c9bbe03f-e283-428f-ae2f-a5fd2cea2cd4');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('e771f858-1c25-4f3b-bc6b-ca7e8822bf50', '489a525b-a61e-4a89-8025-925f3cc7d3c5');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('e771f858-1c25-4f3b-bc6b-ca7e8822bf50', '68cfe0cd-95ab-4346-98ae-09e0b6700baa');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('e771f858-1c25-4f3b-bc6b-ca7e8822bf50', '1a271709-6c77-4454-835f-532158dc62cc');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('e771f858-1c25-4f3b-bc6b-ca7e8822bf50', '6a6c7db2-0a04-4fc2-9407-8d4e24be95d8');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('e771f858-1c25-4f3b-bc6b-ca7e8822bf50', 'aceead3f-0e4f-4036-a465-73c136a74ba4');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('e771f858-1c25-4f3b-bc6b-ca7e8822bf50', '6bfaf794-accf-49b9-bd73-e731197cc341');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('47a68426-09ec-49ca-a21a-3ad1e920fc27', '975ed0ea-71f5-450e-b300-6aa9f47e4d7c');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('47a68426-09ec-49ca-a21a-3ad1e920fc27', 'b95a4ec4-d949-4193-9fa7-936e1c22ce2e');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('47a68426-09ec-49ca-a21a-3ad1e920fc27', 'e830a071-2f46-4dc9-ae48-56e519301f65');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('47a68426-09ec-49ca-a21a-3ad1e920fc27', '33dc565f-1ea2-43bd-9a24-f9f34b584a1e');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('47a68426-09ec-49ca-a21a-3ad1e920fc27', 'be112b29-0797-4065-bebf-7a9ded3409d0');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('47a68426-09ec-49ca-a21a-3ad1e920fc27', '0e33f4f4-06a6-408b-a008-05b1b09fc63b');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('47a68426-09ec-49ca-a21a-3ad1e920fc27', 'c0997376-4a0a-4bd9-86c6-2441da215beb');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('08c1cbfd-6509-4dd5-b8d6-74ff35f16cd4', 'c5031ce0-bed2-454f-9ddd-cce3f66071c6');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('08c1cbfd-6509-4dd5-b8d6-74ff35f16cd4', '4459fc1f-ee1c-4d0a-b8e3-5bbdeb661565');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('08c1cbfd-6509-4dd5-b8d6-74ff35f16cd4', 'ae668c4d-ea8c-4e1c-b48f-3b2686083dc9');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('08c1cbfd-6509-4dd5-b8d6-74ff35f16cd4', '9916b93a-8a69-48bb-b4f3-b4125ecad5eb');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('08c1cbfd-6509-4dd5-b8d6-74ff35f16cd4', 'e15e5cfd-7abe-4d56-a54d-f3a83d154bf2');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('08c1cbfd-6509-4dd5-b8d6-74ff35f16cd4', 'a332d1ba-c11e-42ad-95c8-6d8504bcb2c3');
INSERT INTO public.model_economique_block_model_economique (model_economique_id, block_model_economique_id_blocme) VALUES ('08c1cbfd-6509-4dd5-b8d6-74ff35f16cd4', 'f2511bd5-c89b-4e4e-907d-94570c7a0568');



--
-- TOC entry 3789 (class 0 OID 188221)
-- Dependencies: 258
-- Data for Name: synthese_notation; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (1, 'PORTEUR INDIVIDUEL', 3, 'Niveau d''instruction du porteur');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (2, 'PORTEUR INDIVIDUEL', 3, 'Motivations entrepreneuriales');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (3, 'PORTEUR INDIVIDUEL', 3, 'Profil de l’équipe dirigeante');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (4, 'PORTEUR INDIVIDUEL', 3, 'Compétences et connaissances');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (5, 'PORTEUR INDIVIDUEL', 3, 'Expérience de gestion de projet');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (6, 'PORTEUR INDIVIDUEL', 3, 'Capacité à renouveler le produit');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (7, 'PORTEUR INDIVIDUEL', 3, 'Qualité des relations dans l’équipe');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (8, 'PORTEUR INDIVIDUEL', 3, 'Complémentarité équipe');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (9, 'PORTEUR INDIVIDUEL', 3, 'Environnement familial ');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (10, 'PORTEUR INDIVIDUEL', 3, 'Objectifs professionnels');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (11, 'PORTEUR COLLECTIF', 6, 'Niveau de formalisation');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (12, 'PORTEUR COLLECTIF', 9, 'Niveau d''organisation et de prise de décision');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (13, 'PORTEUR COLLECTIF', 9, 'Profil et compétences des dirigeants clés');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (14, 'PORTEUR COLLECTIF', 6, 'Culture de l''organisation - style de management');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (15, 'PROJET', 10, 'LE MARCHE');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (16, 'PROJET', 10, 'PERTINENCE DES CHOIX STRATEGIQUES');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (17, 'PROJET', 10, 'EFFICACITE DU PROJET');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (18, 'PROJET', 10, 'IMPACT DU PROJET');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (19, 'PROJET', 10, 'VIABILITE ECONOMIQUE');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (20, 'PROJET', 10, 'ORIGINALITE ET FAISABILITE DU PROJET');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (21, 'PROJET', 10, 'ANCRAGE TERRITORIAL');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (22, 'PORTEUR DEVELOPEUR', 3, 'Profil entrepreneurial');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (23, 'PORTEUR DEVELOPEUR', 3, 'Capacités entrepreneuriales');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (24, 'PORTEUR DEVELOPEUR', 3, 'Formalisation');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (25, 'PORTEUR DEVELOPEUR', 3, 'Profil de l''entreprise');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (26, 'PORTEUR DEVELOPEUR', 3, 'performance');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (27, 'PORTEUR DEVELOPEUR', 3, 'plan de marketing');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (28, 'PORTEUR DEVELOPEUR', 3, 'Segment client');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (30, 'PORTEUR DEVELOPEUR', 3, 'Canaux de distribution (canaux de communication)');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (31, 'PORTEUR DEVELOPEUR', 3, 'Relation client');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (32, 'PORTEUR DEVELOPEUR', 3, 'Activités clés');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (33, 'PORTEUR DEVELOPEUR', 3, 'Partenaires clés');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (34, 'PORTEUR DEVELOPEUR', 3, 'Cohésion sociale');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (35, 'PORTEUR DEVELOPEUR', 3, 'Participation');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (36, 'PORTEUR DEVELOPEUR', 3, 'Relation externe');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (37, 'PORTEUR DEVELOPEUR', 3, 'Cohésion sociale');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (38, 'PORTEUR DEVELOPEUR', 3, 'Participation');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (39, 'PORTEUR DEVELOPEUR', 3, 'Relation externe');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (40, 'Niveau de formalisation', 6, 'PORTEUR COLLECTIF');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (41, 'Niveau d''organisation et de prise de décision', 9, 'PORTEUR COLLECTIF');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (42, 'Profil et compétences des dirigeants clés', 9, 'PORTEUR COLLECTIF');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (50, 'Culture de l''organisation - style de management', 6, 'PORTEUR COLLECTIF');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (43, 'LE MARCHE', 10, 'PROJET');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (44, 'PERTINENCE DES CHOIX STRATEGIQUES', 10, 'PROJET');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (45, 'EFFICACITE DU PROJET', 10, 'PROJET');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (46, 'IMPACT DU PROJET', 10, 'PROJET');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (47, 'VIABILITE ECONOMIQUE', 10, 'PROJET');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (48, 'ORIGINALITE ET FAISABILITE DU PROJET', 10, 'PROJET');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (49, 'ANCRAGE TERRITORIAL', 10, 'PROJET');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (29, 'PORTEUR DEVELOPEUR', 3, 'Proposition de valeur');
INSERT INTO public.synthese_notation (id_synthese_notation, libelle, note, categorie) VALUES (51, 'PORTEUR DEVELOPEUR', 3, 'Ressources clés');




--
-- TOC entry 3769 (class 0 OID 188141)
-- Dependencies: 238
-- Data for Name: phase; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.phase (id_phase, libelle) VALUES (1, 'Porteur');
INSERT INTO public.phase (id_phase, libelle) VALUES (2, 'Organisation');
INSERT INTO public.phase (id_phase, libelle) VALUES (4, 'Environnement');
INSERT INTO public.phase (id_phase, libelle) VALUES (3, 'Projet');
INSERT INTO public.phase (id_phase, libelle) VALUES (5, 'Modèle Economique');
INSERT INTO public.phase (id_phase, libelle) VALUES (9, 'Ressources Financières ');
INSERT INTO public.phase (id_phase, libelle) VALUES (8, 'Gestion');
INSERT INTO public.phase (id_phase, libelle) VALUES (7, 'Pratiques manageriales');
INSERT INTO public.phase (id_phase, libelle) VALUES (6, 'Fonctionnement');
INSERT INTO public.phase (id_phase, libelle) VALUES (10, 'Gouvernance Leadership');
INSERT INTO public.phase (id_phase, libelle) VALUES (11, 'Membership');
INSERT INTO public.phase (id_phase, libelle) VALUES (12, 'FonctionnementC');
INSERT INTO public.phase (id_phase, libelle) VALUES (13, 'Patrimoine');
INSERT INTO public.phase (id_phase, libelle) VALUES (14, 'Partenariat');
INSERT INTO public.phase (id_phase, libelle) VALUES (15, 'FonctionnementC');
INSERT INTO public.phase (id_phase, libelle) VALUES (16, 'Finances-Resources');
INSERT INTO public.phase (id_phase, libelle) VALUES (17, 'Finances-Dépenses');
INSERT INTO public.phase (id_phase, libelle) VALUES (18, 'Modéle EconomiqueC');
INSERT INTO public.phase (id_phase, libelle) VALUES (19, 'Partenariat');

--
-- TOC entry 3776 (class 0 OID 188166)
-- Dependencies: 245
-- Data for Name: questionnaire; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (21, 'PROJET INDIVIDUEL/COLLECTIF', 21, 'Quelle est la potentialité du marché ?', 'LE MARCHE', 3, 43);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (22, 'PROJET INDIVIDUEL/COLLECTIF', 22, 'Quel  le niveau de concurrence ?', 'LE MARCHE', 3, 43);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (23, 'PROJET INDIVIDUEL/COLLECTIF', 23, 'Quel est la qualité de l’étude de marché ?', 'LE MARCHE', 3, 43);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (24, 'PROJET INDIVIDUEL/COLLECTIF', 24, 'Quel est le positionnement de l’offre ?', 'LE MARCHE', 3, 44);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (25, 'PROJET INDIVIDUEL/COLLECTIF', 25, 'Les objectifs envisagés dans le projet répondent-ils aux problèmes identifiés ou aux besoins réels du territoire ?', 'PERTINENCE DES CHOIX STRATEGIQUES', 3, 44);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (26, 'PROJET INDIVIDUEL/COLLECTIF', 26, 'Le projet est-il pertinent pour développement économique et social du territoire ?', 'PERTINENCE DES CHOIX STRATEGIQUES', 3, 44);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (27, 'PROJET INDIVIDUEL/COLLECTIF', 27, ' le business modèle est-il Pertinent?', 'PERTINENCE DES CHOIX STRATEGIQUES', 3, 44);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (28, 'PROJET INDIVIDUEL/COLLECTIF', 28, 'Les moyens sont-ils en adéquation avec les objectifs ?', 'PERTINENCE DES CHOIX STRATEGIQUES', 3, 44);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (29, 'PROJET INDIVIDUEL/COLLECTIF', 29, 'Le timing « fenêtre d’opportunité » a été-t-il  maitrisé?', 'PERTINENCE DES CHOIX STRATEGIQUES', 3, 44);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (30, 'PROJET INDIVIDUEL/COLLECTIF', 30, 'Le projet a-t-il un potentiel de développement ?', 'PERTINENCE DES CHOIX STRATEGIQUES', 3, 44);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (32, 'PROJET INDIVIDUEL/COLLECTIF', 32, 'Le projet est-il réaliste ?', 'EFFICACITE DU PROJET', 3, 45);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (33, 'PROJET INDIVIDUEL/COLLECTIF', 33, 'Est-il capable de provoquer les changements souhaités ?', 'EFFICACITE DU PROJET', 3, 45);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (34, 'PROJET INDIVIDUEL/COLLECTIF', 34, 'Le projet est-il faisable dans le pas de temps proposé ?', 'EFFICACITE DU PROJET', 3, 45);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (41, 'PROJET INDIVIDUEL/COLLECTIF', 41, 'Le projet contribue-t-il au renforcement de la capacité locale de mobilisation des ressources ?', 'VIABILITE ECONOMIQUE', 3, 47);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (42, 'PROJET INDIVIDUEL/COLLECTIF', 42, 'Le projet s’appuie-t-il sur des dynamiques qui pourront perdurer au-delà des appuis ?', 'VIABILITE ECONOMIQUE', 3, 47);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (43, 'PROJET INDIVIDUEL/COLLECTIF', 43, 'Les acteurs territoriaux porteurs du projet ont-ils les capacités de poursuivre les efforts au-delà des appuis ?', 'VIABILITE ECONOMIQUE', 3, 47);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (44, 'PROJET INDIVIDUEL/COLLECTIF', 44, 'Quel est la cohérence du chiffre d’affaires ?', 'VIABILITE ECONOMIQUE', 3, 47);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (45, 'PROJET INDIVIDUEL/COLLECTIF', 45, 'Quelle est la qualité de l’évaluation du BFR ?', 'VIABILITE ECONOMIQUE', 3, 47);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (47, 'PROJET INDIVIDUEL/COLLECTIF', 47, 'Le plan de financement est-t-il cohérent ?', 'VIABILITE ECONOMIQUE', 3, 47);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (87, 'PORTEUR DEVELOPEUR', 5, 'Les produits et services de l''entreprise sont - ils connus et apprécies de la clientèle ?', NULL, 5, 30);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (90, 'PORTEUR DEVELOPEUR', 8, 'Qui sont vos concurrents ?  ', NULL, 5, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (79, 'PORTEUR DEVELOPEUR', 8, 'l''entreprise dispose - t - elle d''un plan d''affaire ?', NULL, 2, 26);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (4, 'PORTEUR INDIVIDUEL', 4, 'Compétences et connaissances:Le porteur a -t-il une expertise sur le domaine d''activité ?', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (8, 'PORTEUR INDIVIDUEL', 8, 'Complémentarité équipe:Le projet dispose -t-il de personnel technique et administratif?', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (2, 'PORTEUR INDIVIDUEL', 2, 'Motivations entrepreneuriale:Quels sont les motivations entrepreneuriales du porteur ?', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (3, 'PORTEUR INDIVIDUEL', 3, 'Profil de l’équipe dirigeante:Quel est le profil de l’équipe dirigeante ?', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (5, 'PORTEUR INDIVIDUEL', 5, 'Expérience de gestion de projet:Le porteur a -t-il une expérience avérée dans la gestion de projet ?', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (6, 'PORTEUR INDIVIDUEL', 6, 'Capacité a renouveler le produit:Le porteur a-t-il la capacité  à renouveler le produit ?', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (10, 'PORTEUR INDIVIDUEL', 7, 'Objectifs profesionnels:Le porteur a-t-il fixé ses objectifs professionnels?', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (7, 'PORTEUR INDIVIDUEL', 10, 'Qualité des relations dans l''équipe:Quelle est la qualité des relations dans l’équipe?', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (61, 'PORTEUR COLLECTIF', 10, 'Circulation de l''information: Un système de circulation de l’information existe au sein de l’organisation?', '', NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (9, 'PORTEUR INDIVIDUEL', 9, 'Environnement familial:L''environnement familial peut-il faciliter la réalisation de votre projet?', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (1, 'PORTEUR INDIVIDUEL', 1, 'Niveau d''instruction:Quel est le niveau d''instruction du porteur?', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (80, 'PORTEUR DEVELOPEUR', 9, 'L''entreprise dispose d''un compte ou d''une caisse ?', NULL, 2, 26);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (81, 'PORTEUR DEVELOPEUR', 10, 'L''entreprise analyse ses comptes ?', NULL, 2, 26);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (82, 'PORTEUR DEVELOPEUR', 11, 'L''entreprise dispose d''un système comptable même simplifié ?', NULL, 2, 26);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (83, 'PORTEUR DEVELOPEUR', 1, 'Votre offre est-elle en phase avec les attentes du marché ?', NULL, 5, 29);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (51, 'PROJET INDIVIDUEL/COLLECTIF', 51, 'L''idée du projet est-elle innovante ?', 'ORIGINALITE ET FAISABILITE DU PROJET', 3, 48);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (52, 'PROJET INDIVIDUEL/COLLECTIF', 52, 'Quelle est la faisabilité de l''innovation ?', 'ORIGINALITE ET FAISABILITE DU PROJET', 3, 48);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (53, 'PROJET INDIVIDUEL/COLLECTIF', 53, 'Le projet a-t-il un délai de mise sur le marché?', 'ORIGINALITE ET FAISABILITE DU PROJET', 3, 48);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (54, 'PROJET INDIVIDUEL/COLLECTIF', 54, 'L’innovation a-t-il un coût ?', 'ORIGINALITE ET FAISABILITE DU PROJET', 3, 48);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (55, 'PROJET INDIVIDUEL/COLLECTIF', 55, 'Un marketing efficace est-il réalisable ?', 'ORIGINALITE ET FAISABILITE DU PROJET', 3, 48);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (56, 'PROJET INDIVIDUEL/COLLECTIF', 56, 'Le projet est-il en cohérence avec la vocation du territoire ?', 'ANCRAGE TERRITORIAL', 3, 49);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (20, 'PROJET INDIVIDUEL/COLLECTIF', 20, 'L''offre est-elle en phase avec les attentes du marché?', 'LE MARCHE', 3, 43);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (31, 'PROJET INDIVIDUEL/COLLECTIF', 31, 'Les activités à mener et la stratégie préconisée dans le cadre du projet permettent- elles d’atteindre les objectifs fixés ?', 'EFFICACITE DU PROJET', 3, 45);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (35, 'PROJET INDIVIDUEL/COLLECTIF', 35, 'La réalisation du projet va-t-il apporter un changement sur le territoire?', 'IMPACT DU PROJET', 3, 46);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (36, 'PROJET INDIVIDUEL/COLLECTIF', 36, 'Le projet  a-t-il une portée en termes de nombres de bénéficiaires directs et indirects sur le territoire ?', 'IMPACT DU PROJET', 3, 46);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (37, 'PROJET INDIVIDUEL/COLLECTIF', 37, 'Le projet est-il sensible au respect de l’environnement et/ou maintient-il ou améliore-t-il l’environnement ?', 'IMPACT DU PROJET', 3, 46);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (38, 'PROJET INDIVIDUEL/COLLECTIF', 38, 'Quel est l’apport du projet en matière d’insertion des jeunes et d’autonomisation des femmes ?', 'IMPACT DU PROJET', 3, 46);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (39, 'PROJET INDIVIDUEL/COLLECTIF', 39, 'Le modèle économique du projet est-il pertinent et soutenable ?', 'VIABILITE ECONOMIQUE', 3, 47);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (40, 'PROJET INDIVIDUEL/COLLECTIF', 40, 'Le projet sera-t-il viable financièrement sur le long terme ?', 'VIABILITE ECONOMIQUE', 3, 47);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (46, 'PROJET INDIVIDUEL/COLLECTIF', 46, 'La prévision  du chiffre d’affaires est-t-elle progressive ?', 'VIABILITE ECONOMIQUE', 3, 47);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (48, 'PROJET INDIVIDUEL/COLLECTIF', 48, 'Le projet a-t-il une rentabilité augmentée ?', 'VIABILITE ECONOMIQUE', 3, 47);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (49, 'PROJET INDIVIDUEL/COLLECTIF', 49, 'Le projet répond-il à un besoin actuellement insuffisant?', 'ORIGINALITE ET FAISABILITE DU PROJET', 3, 48);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (50, 'PROJET INDIVIDUEL/COLLECTIF', 50, 'L''idée sert-elle un marché existant dans lequel la demande dépasse l''offre?', 'ORIGINALITE ET FAISABILITE DU PROJET', 3, 48);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (57, 'PROJET INDIVIDUEL/COLLECTIF', 57, 'Les effets du projet ont t-il une portée ?', 'ANCRAGE TERRITORIAL', 3, 49);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (58, 'PROJET INDIVIDUEL/COLLECTIF', 58, 'Quel est le niveau de coopération du projet ?', 'ANCRAGE TERRITORIAL', 3, 49);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (59, 'PROJET INDIVIDUEL/COLLECTIF', 59, 'Le projet a-t-il une dimension inclusion sociale ?', 'ANCRAGE TERRITORIAL', 3, 49);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (60, 'PROJET INDIVIDUEL/COLLECTIF', 60, 'Le projet a-t-il une dimension durable ?', 'ANCRAGE TERRITORIAL', 3, 49);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (120, 'PORTEUR DEVELOPEUR', 9, 'Etes-vous en concurrence, frontale ou partielle, avec une offre existante ? ', NULL, 5, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (122, 'PORTEUR DEVELOPEUR', 11, 'Pouvez-vous facilement vous différencier et trouver une position de "niche" ?', NULL, 5, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (123, 'PORTEUR DEVELOPEUR', 12, 'Votre savoir-faire vous donne- t-il un réel avantage ?', NULL, 5, 51);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (125, 'PORTEUR DEVELOPEUR', 14, 'Qui sont vos fournisseurs? Sont-ils nombreux ?', NULL, 5, 33);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (127, 'PORTEUR DEVELOPEUR', 15, 'Vos fournisseurs ont-ils un pouvoir d''influence sur la qualité et le coût de votre offre ?', NULL, 5, 33);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (126, 'PORTEUR DEVELOPEUR', 16, 'Est-il facile d''enchanger ? ', NULL, 5, 33);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (15, 'PORTEUR COLLECTIF', 5, 'Les décisions stratégiques sont prises collectivement', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (11, 'PORTEUR COLLECTIF', 1, 'Statut juridique: L’organisation dispose d’un statut juridique (reconnaissance légale)?', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (12, 'PORTEUR COLLECTIF', 2, 'Reglement interieur:L’organisation dispose d’un règlement intérieur?', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (13, 'PORTEUR COLLECTIF', 3, 'Experience du dirigeant dans la gestion des organisations:Le dirigeant principal a une expérience avérée dans la gestion des organisations?', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (14, 'PORTEUR COLLECTIF', 4, 'Qualite du personnel: L''organisation dispose d''un personnel suffisant et qualifié chargé de la réalisation des tâches courantes de l''organisation?', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (16, 'PORTEUR COLLECTIF', 6, 'Participation des membres de l''organisation: Les membres de l''organisation participent à la planification, à l’exécution et au monitoring des activités?', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (17, 'PORTEUR COLLECTIF', 7, 'Transparence dans la gestion de l''entreprise:La vision, la mission et objectifs de l''organisation sont connus et partagés par les membres de l''organisation?', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (62, 'PORTEUR DEVELOPEUR', 1, 'Niveau d''instruction du porteur ?', NULL, 1, 22);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (63, 'PORTEUR DEVELOPEUR', 2, 'Le porteur a une expertise sur le domaine d''activité ?', NULL, 1, 23);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (64, 'PORTEUR DEVELOPEUR', 3, 'Le porteur a une expérience avérée dans la gestion de l''entreprise ?', NULL, 1, 23);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (65, 'PORTEUR DEVELOPEUR', 4, 'Le porteur a -t-il des compétences sur ?', NULL, 1, 23);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (66, 'PORTEUR DEVELOPEUR', 5, 'Le porteur ressent des lacunes dans certaines domaines de l''entreprise ?', NULL, 1, 23);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (67, 'PORTEUR DEVELOPEUR', 6, 'Recherchiez - vous de nouveaux savoir faire/ nouvelles formations ?', NULL, 1, 23);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (140, 'PORTEUR DEVELOPEUR', 4, 'Quelles techniques de marketing utilisez-vous ?', NULL, 7, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (141, 'PORTEUR DEVELOPEUR', 5, 'Envisagez-vous de nouvelles actions pour améliorer votre technique de vente? ', NULL, 7, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (145, 'PORTEUR DEVELOPEUR', 3, 'Avez-vous rencontré des difficultés techniques, logistiques, administratives ?', NULL, 8, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (146, 'PORTEUR DEVELOPEUR', 4, 'Votre niveau de production,  correspond-il au business plan ? ', NULL, 8, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (149, 'PORTEUR DEVELOPEUR', 3, 'Avez-vous  les résultats de vos premiers mois d''activité ? ', NULL, 9, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (152, 'PORTEUR DEVELOPEUR', 6, 'Le chiffre d''affaires réalisé par rapport au chiffre d''affaires prévu ? ', NULL, 9, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (154, 'PORTEUR DEVELOPEUR', 8, 'Avez-vous réalisé les investissements prévus (matériel, immobilier) ?', NULL, 9, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (155, 'PORTEUR DEVELOPEUR', 9, 'Avez-vous obtenu des délais de paiement auprès de vos fournisseurs ? ', NULL, 9, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (156, 'PORTEUR DEVELOPEUR', 10, 'Envisagez-vous de nouveaux achats pour améliorer vos outils de production?', NULL, 9, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (153, 'PORTEUR DEVELOPEUR', 7, 'L''entreprise se finance- t-elle par fonds propre ?', NULL, 9, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (132, 'PORTEUR DEVELOPEUR', 5, 'Le personnel de l''entreprise bénéficie de sessions de formation ?', NULL, 6, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (133, 'PORTEUR DEVELOPEUR', 6, 'L''entreprise procéde à des évaluations systématiques de la formation ?', NULL, 6, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (134, 'PORTEUR DEVELOPEUR', 7, 'L''entreprise organise des reunions avec les membres ?', NULL, 6, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (135, 'PORTEUR DEVELOPEUR', 8, 'La taille de votre marché ?', NULL, 6, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (136, 'PORTEUR DEVELOPEUR', 9, 'L''entreprise dispose d''un patrimoine matériel, logistique et infrastructurel ?', NULL, 6, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (137, 'PORTEUR DEVELOPEUR', 1, 'L''entreprise dispose d''un plan d''affaire ?', NULL, 7, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (138, 'PORTEUR DEVELOPEUR', 2, 'L''entreprise travaille à améliorer son plan d''affaire ?', NULL, 7, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (139, 'PORTEUR DEVELOPEUR', 3, 'L''entreprise dispose d''un plan de marketing ?', NULL, 7, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (142, 'PORTEUR DEVELOPEUR', 6, 'Les informations sur le marché sont collectées de façon très réguliére pour soutenir les prises de décision ?', NULL, 7, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (143, 'PORTEUR DEVELOPEUR', 1, 'Le systéme de gestion en place permet des prises de décision rapide ?', NULL, 8, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (144, 'PORTEUR DEVELOPEUR', 2, 'La mise en route de l''appareil de production s’est-elle bien déroulée ?', NULL, 8, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (147, 'PORTEUR DEVELOPEUR', 1, 'Les outils de gestion sont en place et permettent l''etablissement des états financiers ?', NULL, 9, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (148, 'PORTEUR DEVELOPEUR', 2, 'L''entreprise bénéfice d''une supervision financiére externe ?', NULL, 9, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (150, 'PORTEUR DEVELOPEUR', 4, 'L''entreprise produit des rapports financiers périodiques ?', NULL, 9, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (151, 'PORTEUR DEVELOPEUR', 5, 'Quels sont le chiffre d''affaire envisagé au debut de la création ?', NULL, 9, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (157, 'PORTEUR COLLECTIF', 0, 'L''organisation est formelle et à jour ?', NULL, 10, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (158, 'PORTEUR COLLECTIF', 1, 'Le dirigeant principal a une expérience avérée dans la gestion des organisations ?', NULL, 10, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (159, 'PORTEUR COLLECTIF', 2, 'Les autres postes clés de l''organisation sont clairement assumées par des responsables qui maîtrisent leurs attributions ?', NULL, 10, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (160, 'PORTEUR COLLECTIF', 3, 'Les décisions de l''organisation sont prises par les instances concernées ?', NULL, 10, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (161, 'PORTEUR COLLECTIF', 0, 'La vision, la mission, les valeurs et objectifs de l''organisation sont connus et partagés par les membres de l''organisation ?', NULL, 11, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (73, 'PORTEUR DEVELOPEUR', 2, 'Quel est le statut de l''entreprise?', NULL, 2, 24);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (68, 'PORTEUR DEVELOPEUR', 7, 'Le porteur a-t-il fixé ses objectifs professionnels ?', NULL, 1, 22);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (72, 'PORTEUR DEVELOPEUR', 1, 'Quel est le type de l''entreprise ?', NULL, 2, 25);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (221, 'PORTEUR COLLECTIF', 8, 'Qui sont vos concurrents ?', NULL, 18, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (222, 'PORTEUR COLLECTIF', 9, 'Etes-vous en concurrence, frontale ou partielle, avec une offre existante ? ', NULL, 18, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (224, 'PORTEUR COLLECTIF', 11, 'Pouvez-vous facilement vous différencier et trouver une position de "niche" ?', NULL, 18, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (225, 'PORTEUR COLLECTIF', 12, 'Votre savoir-faire vous donne- t-il un réel avantage ?', NULL, 18, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (228, 'PORTEUR COLLECTIF', 15, 'Vos fournisseurs ont-ils un pouvoir d''influence sur la qualité et le coût de votre offre ?', NULL, 18, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (229, 'PORTEUR COLLECTIF', 16, 'Est-il facile d''en changer ? ', NULL, 18, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (69, 'PORTEUR DEVELOPEUR', 8, 'Le porteur a-t-il atteint ses objectifs professionnels ?', NULL, 1, 22);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (18, 'PORTEUR COLLECTIF', 8, 'Formation du personnel:L’organisation offre ses services de formation à ses membres et son personnel?', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (19, 'PORTEUR COLLECTIF', 9, 'Documentation technique: L’organisation dispose d’une documentation technique adaptée à ses activités?', NULL, NULL, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (70, 'PORTEUR DEVELOPEUR', 9, 'La realisation du projet a-t-elle changé votre condition de vie ?', NULL, 1, 22);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (71, 'PORTEUR DEVELOPEUR', 10, 'L''environnement familial peut-il faciliter la réalisation de votre projet ?', NULL, 1, 22);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (74, 'PORTEUR DEVELOPEUR', 3, 'Les reconnaissances juridiques sont disponibles ?', NULL, 2, 24);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (75, 'PORTEUR DEVELOPEUR', 4, ' L''entreprise dispose - t- elle d''un locale ?', NULL, 2, 25);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (76, 'PORTEUR DEVELOPEUR', 5, 'Le type de  management de l''entreprise ?', NULL, 2, 25);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (77, 'PORTEUR DEVELOPEUR', 6, 'L''entreprise dispose -t-elle d''un organigramme ?', NULL, 2, 27);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (78, 'PORTEUR DEVELOPEUR', 7, 'L''entreprise  dispose -t - elle d''un processus de commercialisation defini  ?', NULL, 2, 27);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (84, 'PORTEUR DEVELOPEUR', 2, 'La production de l''entreprise  est -elle basée sur la demande local ?', NULL, 5, 28);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (85, 'PORTEUR DEVELOPEUR', 3, 'L''entreprise connait -elle avec exactitude sa clientèle cible ?', NULL, 5, 28);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (86, 'PORTEUR DEVELOPEUR', 4, 'L''entreprise fait -elle recours aux médias pour atteindre sa clientélé ?', NULL, 5, 30);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (88, 'PORTEUR DEVELOPEUR', 6, 'Quels sont les critères d''achat de vos clients ? Sont-ils loyaux et captifs ?  ', NULL, 5, 31);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (89, 'PORTEUR DEVELOPEUR', 7, 'L''adoption des produits et services de l''entreprise par de nouveaux clients est continuelle ?', NULL, 5, 31);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (98, 'PORTEUR DEVELOPEUR', 0, 'Quels sont les produits commercialisés par l''entreprise ?', NULL, 5, 29);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (99, 'PORTEUR DEVELOPEUR', 1, 'L''entreprise entretien de bonnes relation avec la communauté locale ?', NULL, 4, 37);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (100, 'PORTEUR DEVELOPEUR', 2, 'L''entreprise développe des relations de collaboration avec l''institution municipale ?', NULL, 4, 37);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (101, 'PORTEUR DEVELOPEUR', 3, 'L''entreprise participe- t-elle à la vie locale ?', NULL, 4, 38);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (103, 'PORTEUR DEVELOPEUR', 5, 'L''entreprise a bénéficié des appuis d''ONG locales et internationales ?', NULL, 4, 39);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (104, 'PORTEUR DEVELOPEUR', 6, 'L''entreprise est appuyée par un partenaire technique ou financier ?', NULL, 4, 39);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (105, 'PORTEUR DEVELOPEUR', 7, 'L''entreprise est membre d''un réseau ou développe un partenariat avec d''autres organisations ?', NULL, 4, 39);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (106, 'PORTEUR DEVELOPEUR', 4, 'L''entreprise a -t-elle bénéficié d''appui des réseaux d''accompagnement à la création d''entreprise ?', NULL, 4, 39);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (121, 'PORTEUR DEVELOPEUR', 10, 'Quels sont les activités principales de l''entreprise ?', NULL, 5, 32);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (124, 'PORTEUR DEVELOPEUR', 13, 'Quel est le poids des différents acteurs ?', NULL, 5, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (128, 'PORTEUR DEVELOPEUR', 1, 'La vision et les objectifs de l''entreprise  sont connus et partagés par tous ?', NULL, 6, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (129, 'PORTEUR DEVELOPEUR', 2, 'L''entreprise dispose de personnel technique et administratif qualifiés ?', NULL, 6, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (130, 'PORTEUR DEVELOPEUR', 3, 'Les décisions de l''entreprise sont prises par les instances concernées ?', NULL, 6, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (131, 'PORTEUR DEVELOPEUR', 4, 'L''entreprise est ouverte aux nouvelles idées proactif dans la recherche d''opportunités ?', NULL, 6, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (162, 'PORTEUR COLLECTIF', 1, 'L''organisation a une couverture géographique ?', NULL, 11, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (163, 'PORTEUR COLLECTIF', 2, 'L''organisation rend des services à ses membres ?', NULL, 11, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (164, 'PORTEUR COLLECTIF', 3, 'L''organisation a mis en place des cadres et mécanismes de participation des membres aux activités ?', NULL, 11, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (166, 'PORTEUR COLLECTIF', 0, 'L''organisation dispose d''un plan de travail assorti d''objectifs clairs annuels ou à moyen ?', NULL, 12, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (167, 'PORTEUR COLLECTIF', 1, 'L''organisation dispose de personnel technique et administratif ?', NULL, 12, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (168, 'PORTEUR COLLECTIF', 2, 'L''organisation bénéficie de sessions de formation ?', NULL, 12, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (169, 'PORTEUR COLLECTIF', 3, 'L''organisation est minée par des conflits ?', NULL, 12, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (170, 'PORTEUR COLLECTIF', 0, 'L''organisation dispose d''un compte ou d''une caisse ?', NULL, 13, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (171, 'PORTEUR COLLECTIF', 1, 'L''organisation dispose d''un patrimoine matériel, logistique et infrastructurel ?', NULL, 13, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (172, 'PORTEUR COLLECTIF', 2, 'L''organisation dispose de procédures écrites ?', NULL, 13, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (173, 'PORTEUR COLLECTIF', 3, 'L''organisation dispose d''un système comptable même simplifié ?', NULL, 13, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (174, 'PORTEUR COLLECTIF', 0, 'L''organisation est appuyée par un partenaire technique ou financier ?', NULL, 14, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (175, 'PORTEUR COLLECTIF', 1, 'L''organisation développe des relations de collaboration avec l''institution municipale ?', NULL, 14, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (176, 'PORTEUR COLLECTIF', 2, 'L''organisation assume une responsabilité sociétale au sein de la collectivité territoriale ?', NULL, 14, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (177, 'PORTEUR COLLECTIF', 3, 'L''organisation est membre d''un réseau ou développe un partenariat avec d''autres organisations ?', NULL, 14, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (178, 'PORTEUR COLLECTIF', 0, ' Les décisions stratégiques sont prises collectivement ?', NULL, 15, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (179, 'PORTEUR COLLECTIF', 1, 'Les priorités sont définies par le bureau ou le CA et non par la seule personne morale de l''organisation ?', NULL, 15, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (180, 'PORTEUR COLLECTIF', 2, 'Les membres de l''organisation participent à la planification, à l’exécution et au monitoring des activités ?', NULL, 15, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (181, 'PORTEUR COLLECTIF', 3, 'L''organisation dispose d''un plan annuel budgétisé de travail ?', NULL, 15, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (182, 'PORTEUR COLLECTIF', 4, 'L''organisation évalue annuellement ses résultats via son comité ou commission technique ?', NULL, 15, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (183, 'PORTEUR COLLECTIF', 5, 'L''organisation dispose d''un personnel suffisant et qualifié chargé de la réalisation des tâches courantes de l''organisation ?', NULL, 15, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (184, 'PORTEUR COLLECTIF', 6, 'L’organisation dispose d’une documentation technique adaptée à ses activités ?', NULL, 15, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (185, 'PORTEUR COLLECTIF', 7, 'Un cahier de PV est tenu à jour par le secrétaire ?', NULL, 15, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (186, 'PORTEUR COLLECTIF', 0, 'Le budget est présenté et approuvé par l’AG au début de chaque année ?', NULL, 16, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (187, 'PORTEUR COLLECTIF', 1, ' L''organisation dispose de reserves financières déposées dans une structure financière ?', NULL, 16, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (188, 'PORTEUR COLLECTIF', 2, 'Les fonds de l’organisation proviennent en partie des membres (cotisations, dons) et les revenus provenant des activités de l''organisation ?', NULL, 16, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (189, 'PORTEUR COLLECTIF', 3, 'Les ¾ des membres de l’organisation s’acquittent régulièrement de leurs cotisations ?', NULL, 16, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (190, 'PORTEUR COLLECTIF', 4, ' L''organisation disposent de ressources régulières tirées de ses activités, de la gestion de son patrimoine et de prestations de services ?', NULL, 16, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (191, 'PORTEUR COLLECTIF', 5, ' L’organisation dispose d’un compte bancaire ?', NULL, 16, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (192, 'PORTEUR COLLECTIF', 6, 'Un système d’inventaire des biens de l’organisation existe et est opérationnel ?', NULL, 16, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (194, 'PORTEUR COLLECTIF', 8, 'L''organisation dispose en son sein d''un ménanisme de collecte et de gestion de l''épargne (tontine, natte, AVEC, Calebasse, etc.) ?', NULL, 16, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (196, 'PORTEUR COLLECTIF', 0, 'Le recrutement du personnel s’effectue sur la base de procédures écrite ?', NULL, 17, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (197, 'PORTEUR COLLECTIF', 1, 'Chaque employé dispose d’un contrat de travail ?', NULL, 17, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (198, 'PORTEUR COLLECTIF', 2, 'Un texte de procédures administratives et financières existe et est appliqué ?', NULL, 17, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (199, 'PORTEUR COLLECTIF', 3, 'L’organisation produit des rapports d’activités et financiers annuels ?', NULL, 17, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (200, 'PORTEUR COLLECTIF', 4, 'Les commissaires aux comptes de l’organisation font des contrôles réguliers ?', NULL, 17, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (201, 'PORTEUR COLLECTIF', 5, 'Tous les décaissements sont autorisés ?', NULL, 17, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (202, 'PORTEUR COLLECTIF', 6, 'Toutes les dépenses sont justifiées par les pièces adéquates (factures, reçus) ?', NULL, 17, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (203, 'PORTEUR COLLECTIF', 7, 'Les opérations financières sont effectuées par un comptable qualifié ?', NULL, 17, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (205, 'PORTEUR COLLECTIF', 9, 'Un système de petite caisse pour les menues dépenses existe ?', NULL, 17, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (206, 'PORTEUR COLLECTIF', 10, 'Une stratégie claire pour une autonomie financière de l’organisation existe ?', NULL, 17, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (207, 'PORTEUR COLLECTIF', 11, 'Les contributions sollicitées des membres sont collectées chaque année ?', NULL, 17, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (208, 'PORTEUR COLLECTIF', 12, 'L’organisation dispose d’un journal de caisse tenu à jour ?', NULL, 17, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (209, 'PORTEUR COLLECTIF', 13, ' Les procédures financières sont claires ?', NULL, 17, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (210, 'PORTEUR COLLECTIF', 14, 'L’élaboration et l’exécution du budget sont participatives ?', NULL, 17, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (211, 'PORTEUR COLLECTIF', 15, 'Le budget est présenté et approuvé par l’AG au début de chaque année ?', NULL, 17, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (212, 'PORTEUR COLLECTIF', 16, 'Le bilan financier est présenté et approuvé par l’AG à la fin de chaque année ?', NULL, 17, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (213, 'PORTEUR COLLECTIF', 0, 'Quels sont les produits commercialisés par l''organisation ?', NULL, 18, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (214, 'PORTEUR COLLECTIF', 1, 'Votre offre est-elle en phase avec les attentes du marché ?', NULL, 18, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (215, 'PORTEUR COLLECTIF', 2, 'Votre production  est -elle basée sur la demande local ?', NULL, 18, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (216, 'PORTEUR COLLECTIF', 3, 'L''organisation connait -elle avec exactitude sa clientèle cible ?', NULL, 18, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (217, 'PORTEUR COLLECTIF', 4, 'L''organisation fait -elle recours aux médias pour atteindre sa clientélé ?', NULL, 18, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (218, 'PORTEUR COLLECTIF', 5, 'Les produits et services de l''organisation sont - ils connus et apprécies de la clientèle ?', NULL, 18, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (219, 'PORTEUR COLLECTIF', 6, 'Quels sont les critères d''achat de vos clients ? Sont-ils loyaux et captifs ?', NULL, 18, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (220, 'PORTEUR COLLECTIF', 7, 'L''adoption des produits et services de l''organisation par de nouveaux clients est continuelle ?', NULL, 18, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (223, 'PORTEUR COLLECTIF', 10, 'Quels sont les activités principales de l''organisation ?', NULL, 18, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (226, 'PORTEUR COLLECTIF', 13, 'Quel est le poids des différents acteurs ?', NULL, 18, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (227, 'PORTEUR COLLECTIF', 14, 'Qui sont vos fournisseurs ? Sont-ils nombreux ? ', NULL, 18, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (230, 'PORTEUR COLLECTIF', 0, 'Avez-vous bénéficié d’appui technique durant ces dernières années ?', NULL, 19, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (231, 'PORTEUR COLLECTIF', 1, 'Si Oui, quelle est la nature de l’appui (Formation ; Matériel ; Autres) ?', NULL, 19, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (232, 'PORTEUR COLLECTIF', 2, 'Quels sont vos partenaires techniques ?', NULL, 19, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (233, 'PORTEUR COLLECTIF', 3, 'L’organisation collabore avec plusieurs partenaires extérieurs ?', NULL, 19, NULL);
INSERT INTO public.questionnaire (id_question, categorie, code, libelle, bloc, phase_id_phase, id_synthese_notation) VALUES (234, 'PORTEUR COLLECTIF', 4, 'L’organisation est ouverte à d’autres structures, réseaux, groupes de réflexion intéressés par le développement économique local ?', NULL, 19, NULL);


--
-- TOC entry 3781 (class 0 OID 188191)
-- Dependencies: 250
-- Data for Name: reference_note; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (30, 1, 'Non', 69);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (81, 2, 'Compte bancaire ou SFD', 80);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (9, 1, 'Non', 63);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (312, 1, 'Non', 178);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (313, 2, 'Oui', 178);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (314, 1, 'Non', 179);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (315, 2, 'Oui', 179);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (318, 1, 'N’existe pas', 181);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (319, 2, 'Application défectueuse', 181);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (320, 3, 'Amélioration souhaitable', 181);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (321, 4, 'Application satisfaisante', 181);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (322, 5, 'Excellente application', 181);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (323, 1, 'Non', 182);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (324, 2, 'Oui', 182);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (325, 1, 'Non', 183);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (326, 2, 'Oui', 183);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (336, 5, 'Fonctionne très bien', 184);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (337, 5, 'Excellente application', 184);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (335, 4, 'Application satisfaisante', 184);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (334, 4, 'Fonctionne bien ', 184);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (333, 3, 'Amélioration souhaitable', 184);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (332, 3, 'Fonctionne assez bien ', 184);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (331, 2, 'Application défectueuse', 184);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (330, 2, 'Fonctionne rarement ', 184);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (329, 1, 'N’existe pas', 184);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (328, 1, 'Ne fonctionne pas ', 184);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (338, 1, 'Non', 185);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (339, 2, 'Oui', 185);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (340, 1, 'Non', 186);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (341, 2, 'Oui', 186);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (342, 1, 'Non', 187);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (343, 2, 'Oui', 187);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (344, 1, 'Non', 188);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (345, 2, 'Oui', 188);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (346, 1, 'Non', 189);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (347, 2, 'Oui', 189);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (348, 1, 'Non', 190);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (349, 2, 'Oui', 190);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (359, 5, 'Excellente application', 191);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (358, 5, 'Fonctionne très bien', 191);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (357, 4, 'Application satisfaisante', 191);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (356, 4, 'Fonctionnant bien', 191);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (355, 3, 'Amélioration souhaitable', 191);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (354, 3, 'Fonctionne assez bien', 191);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (353, 2, 'Application défectueuse', 191);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (352, 2, 'Fonctionne rarement', 191);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (350, 1, 'Ne fonctionne pas', 191);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (351, 1, 'N’existant pas', 191);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (360, 1, 'Ne fonctionne pas', 192);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (361, 1, 'N’existant pas', 192);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (362, 2, 'Fonctionne rarement', 192);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (363, 2, 'Application défectueuse', 192);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (364, 3, 'Fonctionne assez bien', 192);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (365, 3, 'Amélioration souhaitable', 192);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (366, 4, 'Fonctionnant bien', 192);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (367, 4, 'Application satisfaisante', 192);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (368, 5, 'Fonctionne très bien', 192);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (369, 5, 'Excellente application', 192);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (380, 1, 'Non', 194);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (381, 2, 'Oui', 194);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (382, 1, 'Non', 196);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (383, 2, 'Oui', 196);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (384, 1, 'Non', 197);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (385, 2, 'Oui', 197);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (386, 1, 'Non', 199);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (387, 2, 'Oui', 199);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (388, 1, 'Non', 200);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (389, 2, 'Oui', 200);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (390, 1, 'Non', 201);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (391, 2, 'Oui', 201);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (392, 1, 'Non', 202);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (393, 2, 'Oui', 202);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (394, 1, 'Non', 203);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (395, 2, 'Oui', 203);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (396, 1, 'Non', 207);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (397, 2, 'Oui', 207);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (398, 1, 'Non', 208);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (399, 2, 'Oui', 208);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (406, 2, 'Oui', 212);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (407, 1, 'Non', 212);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (1, 1, 'Ne fonctionne pas / Très faible / Non', NULL);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (2, 2, 'Fonctionnant rarement / Faible/ Rarement', NULL);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (3, 3, 'Fonctionnant assez bien / Moyen/ Plus ou moins', NULL);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (4, 4, 'Fonctionnant bien / Bien/ Oui', NULL);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (5, 1, 'Aucun', 62);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (6, 2, 'Alphabétisé', 62);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (7, 3, 'primaire', 62);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (8, 4, 'Moyen/secondaire', 62);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (10, 2, 'Plus ou moins ', 63);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (11, 3, 'Oui', 63);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (12, 1, 'Non', 64);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (13, 2, 'Plus ou moins ', 64);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (14, 3, 'Oui', 64);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (16, 1, 'Comptabilité', 65);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (17, 2, 'technique', 65);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (18, 3, 'commercial', 65);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (19, 4, 'digitale', 65);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (20, 1, 'Non', 66);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (21, 2, 'Partiellement', 66);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (22, 3, 'Oui', 66);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (24, 1, 'Non', 67);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (25, 2, 'Oui', 67);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (27, 1, 'Non', 68);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (28, 2, 'Plus ou moins', 68);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (29, 3, 'Oui', 68);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (31, 2, 'Plus ou moins ', 69);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (32, 3, 'Oui', 69);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (33, 1, 'Non', 70);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (34, 2, 'Plus ou moins ', 70);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (35, 3, 'Oui', 70);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (36, 1, 'Non', 71);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (37, 2, 'Plus ou moins ', 71);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (38, 3, 'Oui', 71);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (39, 5, 'Supérieur', 62);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (40, 2, 'familiale', 72);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (41, 3, 'collectif ', 72);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (42, 1, 'Informelle', 73);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (43, 2, 'En cours de constitution', 73);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (44, 3, 'Formelle pas à jour', 73);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (45, 4, 'Formelle', 73);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (46, 5, 'Formelle et à jour', 73);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (47, 1, 'Non', 74);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (48, 2, 'Oui', 74);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (49, 1, 'Non', 75);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (50, 2, 'Location', 75);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (51, 3, 'Preté', 75);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (52, 4, 'Oui', 75);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (53, 4, 'Personnel', 76);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (54, 2, 'Directif', 76);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (55, 3, 'Participatif', 76);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (56, 4, 'Délégatif', 76);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (57, 1, 'Non', 77);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (58, 2, 'Oui pas matérialisé', 77);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (59, 3, 'Oui en cours de préparation', 77);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (60, 4, 'Oui pas encore actualisé', 77);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (61, 5, 'Oui matérialisé', 77);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (62, 1, 'Non', 78);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (63, 2, 'Oui pas matérialisé', 78);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (64, 3, 'Oui pas encore actualisé', 78);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (65, 4, 'Oui matérialisé', 78);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (66, 5, 'Oui', 78);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (75, 1, 'Non', 79);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (76, 2, 'Oui pas matérialisé', 79);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (77, 3, 'Oui pas encore actualisé', 79);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (78, 4, 'Oui matérialisé', 79);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (79, 5, 'Oui', 79);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (80, 1, 'Caisse interne', 80);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (82, 1, 'Non', 82);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (83, 1, 'individuel', 72);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (85, 3, 'Ni caisse ni compte', 80);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (86, 1, 'Non', 81);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (87, 2, 'Parfois', 81);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (88, 3, 'Regulièrement', 81);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (89, 4, 'Oui', 81);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (90, 2, 'Oui pas formalisé', 82);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (91, 3, 'Oui formalisé pas à jour', 82);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (92, 4, 'Oui formalisé et à jour', 82);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (93, 1, 'Non', 99);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (94, 2, 'Oui', 99);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (95, 3, 'Plus ou moins ', 99);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (96, 1, 'Non', 100);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (97, 2, 'Oui en relation conflictuelle', 100);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (98, 3, 'Oui en relation ponctuelle', 100);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (99, 4, 'Oui en permanence', 100);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (100, 1, 'Non', 101);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (102, 2, 'Oui', 101);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (103, 1, 'Non', 106);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (104, 2, 'Rarement', 106);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (105, 3, 'Oui', 106);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (106, 1, 'Non', 103);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (108, 2, 'Oui', 103);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (109, 1, 'Non', 104);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (111, 2, 'Oui', 104);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (112, 1, 'Non', 105);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (113, 2, 'Oui membre réseau x', 105);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (114, 3, 'Oui pontuellement avec telle organisation', 105);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (115, 4, 'Oui dans le passé', 105);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (116, 1, 'Non', 128);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (117, 2, 'Oui par le seul dirigeant principal', 128);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (118, 3, 'Oui par les membres de l''administration', 128);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (119, 4, 'Oui par une partie de l''équipe', 128);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (120, 5, 'Oui par la majorité des employés', 128);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (121, 1, 'Non', 129);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (122, 2, 'Oui une partie', 129);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (123, 3, 'Oui', 129);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (124, 1, 'Non', 130);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (152, 3, 'Oui martérialisé par un  document de gestion', 136);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (153, 1, 'Non', 137);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (154, 2, 'Oui pas matérialisé', 137);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (155, 4, 'Oui pas matérialisé', 137);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (156, 5, 'Oui', 137);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (157, 1, 'Non', 138);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (158, 2, 'Parfois', 138);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (159, 3, 'Régulièrement', 138);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (160, 4, 'Oui', 138);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (161, 1, 'Non', 139);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (175, 4, 'Oui', 142);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (176, 1, 'Non', 143);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (177, 2, 'Oui', 143);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (178, 1, 'Non', 144);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (179, 2, 'Plus ou moins', 144);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (311, 2, 'Oui', 234);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (310, 1, 'Non', 234);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (309, 2, 'Oui', 233);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (308, 1, 'Non', 233);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (307, 3, 'ONG', 232);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (306, 2, 'Privés', 232);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (305, 1, 'Etats', 232);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (304, 3, 'Autres', 231);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (303, 2, 'Matériel', 231);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (302, 1, 'Formation', 231);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (301, 2, 'Oui', 230);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (404, 2, 'Oui', 211);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (405, 1, 'Non', 211);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (402, 2, 'Oui', 210);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (403, 1, 'Non', 210);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (400, 2, 'Oui', 209);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (401, 1, 'Non', 209);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (408, 1, 'N’existe pas', 198);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (180, 3, 'Oui', 144);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (183, 3, 'Oui', 145);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (182, 2, 'Plus ou moins', 145);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (181, 1, 'Non', 145);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (186, 3, 'Oui', 146);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (185, 2, 'Plus ou moins', 146);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (184, 1, 'Non', 146);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (218, 3, 'Plus-tard', 156);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (217, 2, 'Oui', 156);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (216, 1, 'Non', 156);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (215, 2, 'Oui', 155);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (214, 1, 'Non', 155);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (213, 2, 'Oui', 154);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (212, 1, 'Non', 154);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (211, 3, 'Oui', 153);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (210, 2, 'Parfois', 153);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (209, 1, 'Non', 153);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (208, 4, '5000000 Fcfa et plus', 152);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (207, 3, '2000000 et 5000000 Fcfa', 152);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (206, 2, '5000000 et 2000000 Fcfa', 152);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (205, 1, '0-500 000 Fcfa', 152);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (204, 4, '2000000 Fcfa et plus', 151);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (203, 3, '1000000 et 2000000 Fcfa', 151);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (202, 2, '5000000 et 1000000 Fcfa', 151);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (201, 1, '[0-500 000 Fcfa]', 151);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (200, 4, 'Oui', 150);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (199, 3, 'Parfois', 150);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (198, 2, 'Rarement', 150);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (197, 1, 'Non', 150);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (196, 3, 'Oui', 149);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (195, 2, 'Plus ou moins', 149);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (194, 1, 'Non', 149);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (193, 3, 'Oui', 148);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (192, 2, 'Parfois', 148);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (191, 1, 'Non', 148);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (190, 4, 'Oui', 147);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (189, 3, 'Oui pas encore actualisé', 147);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (188, 2, 'Oui pas matérialisé', 147);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (187, 1, 'Non', 147);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (219, 1, 'Informelle', 157);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (220, 2, 'En cours de constitution', 157);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (221, 3, 'Formelle pas à jour', 157);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (222, 4, 'Formelle', 157);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (223, 1, 'Non', 158);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (224, 2, 'Plus ou moins', 158);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (225, 3, 'Oui', 158);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (226, 1, 'Non', 159);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (227, 2, 'Partiellement', 159);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (228, 3, 'Oui', 159);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (229, 1, 'Non', 160);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (230, 2, 'Rarement', 160);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (231, 3, 'Parfois', 160);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (232, 4, 'Oui', 160);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (250, 3, 'Oui qui fonctionnent', 164);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (249, 2, 'Oui qui ne fonctionnement pas', 164);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (248, 1, 'Non du tout', 164);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (247, 4, 'Appui financier', 163);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (246, 3, 'Accompagnement social', 163);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (245, 2, 'Appui technique ', 163);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (244, 1, 'Formation', 163);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (243, 6, 'Régional ou national', 162);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (242, 5, 'Départemental', 162);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (241, 4, 'Arrondissement', 162);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (240, 3, 'Communal', 162);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (239, 2, 'Village', 162);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (238, 1, 'Quartier', 162);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (237, 5, 'Oui par la majorité des membres', 161);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (236, 4, 'Oui par les membres du bureau', 161);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (235, 3, 'Oui par une partie des membres', 161);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (234, 2, 'Oui par le seul dirigeant principal', 161);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (233, 1, 'Non', 161);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (251, 1, 'Non', 166);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (252, 2, 'Oui pas matérialisé', 166);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (253, 3, 'Oui en cours de préparation', 166);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (254, 4, 'Oui pas encore actualisé', 166);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (255, 5, 'Oui', 166);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (256, 1, 'Non', 167);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (257, 2, 'Oui externe', 167);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (258, 3, 'Oui bénévolat', 167);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (259, 4, 'Oui', 167);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (269, 1, 'Ni caisse ni compte', 170);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (270, 2, 'Caisse interne', 170);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (271, 3, 'Copmte bancaire ou SFD', 170);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (272, 1, 'Non', 171);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (276, 1, 'Non', 172);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (277, 2, 'oui pas appliqué', 172);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (279, 4, 'oui appliqué', 172);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (280, 1, 'Non', 173);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (281, 2, 'oui pas formalisé', 173);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (282, 3, 'oui formalisé pas à jour', 173);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (283, 4, 'oui formalisé et à jour', 173);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (284, 1, 'Non', 174);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (285, 2, 'oui en formulation', 174);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (286, 3, 'oui appui clôture', 174);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (287, 4, 'oui appui en cours', 174);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (288, 1, 'Non', 175);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (289, 2, 'oui en relation conflictuelle', 175);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (290, 3, 'oui en relation ponctuelle', 175);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (291, 4, 'oui en permanence', 175);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (264, 5, 'Régulièrement', 168);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (263, 4, 'Parfois', 168);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (262, 3, 'Rarement', 168);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (261, 2, 'Presque pas', 168);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (260, 1, 'Pas du tout', 168);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (268, 4, 'Oui en phase judiciaire', 169);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (267, 3, 'Oui ouverts', 169);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (266, 2, 'Oui en latence', 169);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (265, 1, 'Non', 169);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (273, 2, 'Oui pas retracé', 171);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (274, 3, 'Oui  pas appliqué ', 171);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (275, 4, 'Oui pas formalisé', 171);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (295, 4, 'oui en permance avec preuves à l''appui', 176);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (294, 3, 'oui pontuellement', 176);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (293, 2, 'oui limité à ses membres', 176);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (292, 1, 'Non', 176);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (299, 4, 'oui membre réseau x', 177);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (298, 3, 'oui ponctuellement avec telle organisation', 177);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (297, 2, 'oui dans le passé', 177);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (296, 1, 'Non', 177);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (127, 4, 'Oui', 130);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (126, 3, 'Parfois', 130);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (125, 2, 'Rarement', 130);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (130, 3, 'Oui', 131);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (129, 2, 'Parfois', 131);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (128, 1, 'Non', 131);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (136, 5, 'Régulièrement', 132);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (135, 4, 'Parfois', 132);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (134, 3, 'Rarement', 132);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (133, 2, 'Presque pas', 132);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (132, 1, 'Pas du tout', 132);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (141, 5, 'Régulièrement', 133);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (140, 4, 'Parfois', 133);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (139, 3, 'Rarement', 133);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (138, 2, 'Presque pas', 133);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (137, 1, 'Pas du tout', 133);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (145, 4, 'Oui', 134);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (144, 3, 'Rarement', 134);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (143, 2, 'Parfois', 134);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (142, 1, 'Non', 134);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (149, 4, 'International', 135);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (148, 3, 'National', 135);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (147, 2, 'Régional', 135);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (146, 1, 'Local', 135);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (151, 2, 'Oui pas retracé', 136);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (150, 1, 'Non', 136);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (164, 4, 'Oui', 139);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (163, 3, 'Oui pas encore actualisé', 139);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (162, 2, 'Oui pas matérialisé', 139);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (168, 4, 'Suscitez l''envie par la notion de rareté ', 140);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (167, 3, 'Appâtez les nouveaux clients', 140);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (166, 2, 'Mettre le paquet sur la fidélisation', 140);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (165, 1, 'Se focaliser sur les besoins du client', 140);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (171, 3, 'Oui', 141);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (170, 2, 'Plus ou moins', 141);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (169, 1, 'Non', 141);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (174, 3, 'Régulièrement', 142);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (173, 2, 'Parfois', 142);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (172, 1, 'Non', 142);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (300, 1, 'Non', 230);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (409, 2, 'Application défectueuse', 198);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (410, 3, 'Amélioration souhaitable', 198);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (411, 4, 'Application satisfaisante', 198);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (412, 5, 'Excellente application', 198);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (417, 5, 'Excellente application', 205);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (416, 4, 'Application satisfaisante', 205);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (415, 3, 'Amélioration souhaitable', 205);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (414, 2, 'Application défectueuse', 205);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (413, 1, 'N’existe pas', 205);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (422, 5, 'Excellente application', 206);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (421, 4, 'Application satisfaisante', 206);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (420, 3, 'Amélioration souhaitable', 206);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (419, 2, 'Application défectueuse', 206);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (418, 1, 'N’existe pas', 206);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (424, 2, 'Oui', 180);
INSERT INTO public.reference_note (id_question, note, reponse, question_id_question) VALUES (423, 1, 'Non', 180);


--
-- TOC entry 3764 (class 0 OID 188126)
-- Dependencies: 233
-- Data for Name: note_question; Type: TABLE DATA; Schema: public; Owner: -
--



INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('87842ccd-eb43-475d-b4ed-d84ba834e8d1', 2, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0b7d9b33-09b6-4776-971f-bfce58b2b63a', 3, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0eef4acb-fed9-4077-b4b8-5e89806f894c', 0, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5f927481-eff2-4b35-bdfa-71adf3b74fb8', 2, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d749f5e0-bd6c-42d7-815a-58fa61ca8dc4', 3, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e652de93-bccf-4019-ab34-acc815606bbe', 3, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('46bb9804-bcc2-4eb0-bab7-758396e38b9d', 0, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5cd3cfeb-d258-4545-b8d8-dc7a57cc9298', 3, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2f665dbf-fbee-40b4-a5c2-bc1f31517b08', 3, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('23e762a6-09d2-4a9f-baed-36b7b940c533', 0, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ea7ccc03-23f4-40da-a590-043e3e94cbde', 3, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('55f44aab-a07d-47d1-920a-d2eddffc95a2', 2, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bbb9084a-8063-4ced-911e-1c9a3d6098aa', 2, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('62894cff-b183-40cf-905d-66b39232c09c', 3, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('66bbed47-27fc-42a7-bb90-40fe2c69dcbf', 3, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0a97afec-aa74-4042-b881-4b1e2724cae1', 2, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('cbc76fa6-7316-4c26-a3b9-2a15fc6a3d4a', 1, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d4a62283-7297-437a-b245-b5dc37dabac8', 2, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7f26b404-546b-483a-ad1f-b28cb4abf536', 3, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('59f7e316-3782-4bd4-a450-bb7498d65f4b', 0, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2616ac4b-bfab-490c-8d61-3dd1a8f2c978', 2, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a0b9b242-3e1f-4e1f-b96f-4df66ffe6b50', 2, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ce832b52-74a9-4fd2-abfb-5ba46c484702', 2, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('18ace12a-741f-4efa-afab-62cb8a4c7d7c', 3, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('91adea3e-005e-43d0-b613-4d17531325e4', 3, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('061a26d3-1b36-4885-a018-9d5a24cc5544', 3, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('be2727a9-682d-461e-b28c-38f30b8db6ae', 0, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7c8afb4e-368f-4c3d-9196-e422895b2b4c', 2, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('71ba1075-974d-42b1-9223-ff92e590ef2a', 3, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('223243a9-8323-4f17-8d8c-1ceccb1ce4c8', 0, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bddbdff1-fc73-4a6e-9d3e-21dad8f536a2', 3, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3e4b6cd7-cd4c-42d6-b2a2-387699c4961b', 3, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bd3bd0ee-074f-47e4-88b7-79da97b37701', 1, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('529746e9-ba34-4fca-8462-b87830709432', 2, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4ab715f6-c9af-49cd-8653-219f796905e0', 3, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('791aa7ac-a5ab-48a8-b536-7f80fdec622b', 2, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e89ec50f-9265-406d-8933-3dc1cf37031e', 2, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('688da420-892f-4c0e-b5b8-43ff67204cda', 3, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6422468b-e9ed-4272-9a50-fc6049bb0374', 3, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0b7cfebf-5e25-4a0d-a3c4-b73a0400b09c', 2, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1fb01f0b-6a0f-4ef6-8830-993fed92a970', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0f580ace-2c4d-4411-bce9-f62ef32919bc', 2, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e36fb9b1-6cef-4ac1-9611-a415e873cd79', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f9fceb44-d627-4706-8ac3-3c8c680cda17', 3, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3e46a25a-0a9c-445f-9045-042a277d8cd3', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fabf466f-1937-4e99-b3cf-5747e91ab482', 2, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f05eb9d7-52f2-4f69-8ddd-053743e7c113', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ec726d66-ee7b-4188-915a-5c5a6b8a9a20', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1abccc52-1f3f-47b8-8e5e-72438d742c78', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('43c799fd-e81e-4aca-8845-c5e52db35a39', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9e112f17-f800-432a-b279-a4b358a8de32', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('13e69ccf-7780-49da-bcf0-3a5060294eb8', 2, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c6dfb6ea-121d-47b6-af57-6a454661185a', 2, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8a6fd381-1f30-4fc2-b4f8-7ad58d7d6efa', 3, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('65646e59-abda-44ae-93c0-0dbfbd777601', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4ea82abd-86f6-4217-98cd-ea62881ba641', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('103e1909-bd62-439f-87a1-2f213d87246b', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a1fbb74b-c77f-425e-b83b-08fdccb91970', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('afc802d8-6014-4d2f-a7d5-3fce4b40ebf5', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('07a10e96-b777-4836-9109-2cdee79b3d57', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d8664215-f625-4bed-92c4-1162b9d32e5c', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3ef39aa0-e8a9-40d5-be33-98a7c0dd0dcd', 2, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c0c03d00-c35d-4604-a197-514c78c570ab', 0, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('feac342e-c2d1-41ec-9ac6-cddbc3955a47', 2, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('222eaf1b-7f3d-42dc-8c37-de599d810429', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('977ccd0f-0d5c-4456-96ec-eeb4e3822ae3', 2, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('48efcfc2-494c-4f96-8fa9-c0da9fbddb30', 0, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d893c547-7d18-4db3-818c-13622b609025', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7018f6cb-7f72-45fd-a255-d75e3cd89da1', 3, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2c598b86-7026-4239-b8fb-bf4cb566e36c', 2, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b7921c17-380f-481b-8bbf-d130837162e0', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('02b19ff0-ae9a-492b-aca2-486802f85e44', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('eccc5a54-e1e2-4cdc-84fb-9f70e5e168d3', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4c94d05c-eeac-4296-84b0-113c02b263a4', 3, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('91bd1927-95bc-489a-bf7d-1a04eb9aae27', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('edc4d294-1b73-443a-acd3-5e88adef3b1f', 0, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('506fbeb3-3cdb-40a3-afd5-906d67bba65c', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('801c3156-c83e-42cc-8fb8-8dc045043010', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9e8d3b01-8df1-4341-954c-e4cdcef5817c', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8bbc80cf-f794-48b7-afb7-09bc90b15bd2', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('16598772-a2fb-49a9-a6c4-0b590e167a20', 2, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('33111ea1-ea10-46ea-908c-8d7197077c62', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f927a059-ecb7-4c9e-9371-745a9681f8ab', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4367f359-8572-42a3-847b-fe6521cfbdb4', 3, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('32f71f32-9e87-4630-b0c7-056549da9ab8', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0bdcc784-a555-4fac-8b02-e26e81e79d8e', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5d560a57-fd16-4b63-a6a7-85e5b9967320', 2, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('db5335fa-b73c-4626-a7bf-19e30917f457', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ae3c8ac2-a7c6-4d4d-a88a-88c46eb73837', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1580f2e2-d2db-4dcb-9a07-b6c79740612c', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3c2c7eda-fd3c-4928-9fcb-1d6a67a2a354', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a4ca3da7-5fa0-4127-9b53-42307272ab45', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a5328a39-3bb4-4a66-b085-490b9768e52a', 2, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4de292f8-0652-4d67-9ead-853148dab7d2', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c05514d9-6d65-436c-9602-5f567f646e36', 2, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8da3a144-5630-4a9b-aae0-c8028df7ffd2', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8edd8e44-8312-4b0c-b58b-a67734983230', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fe6a7e87-1efc-4719-b94a-f19a9d65899d', 2, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('190a4107-e859-436b-b74e-ad6e3af5bd23', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e2c76d1a-7f67-450a-a6ae-03d6a5967657', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d1af2482-c237-4a96-99df-1b06e7f89283', 2, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('be7df270-6938-4174-9c57-80cc6908ad47', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5b75a474-3797-4db6-a1a2-7d49dd0c9315', 0, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7540186e-5ebf-417c-980a-f4361a848bb5', 3, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2567ad37-412c-4eb7-ad75-c5f77fdbecf9', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2d0de99b-4d9c-4cf6-b76f-c6d81f56600a', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('70d9e62d-03cc-494b-b525-4d28154efc2e', 0, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('af373eb8-790a-4e05-89b9-584b3536a50e', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('cae15ae3-43d3-404d-adec-449231263011', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2eb38f49-3bb6-4674-8e3c-cb1c6c04ec73', 2, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('cd170adf-869c-4e32-827d-77e11a284a5b', 2, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8dccdd9f-bdc3-4e0e-9a3a-ddbdde4bcc15', 0, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4908a8c6-1adc-49f3-a3fb-0a645ac6b7e2', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0320c509-0a76-4c50-9edf-321fbeab7284', 2, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('407bfdeb-9c33-498c-8bfd-33d2342e9cf6', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f9fc8115-3eff-43cd-88a5-53d92762893f', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('414c6f3f-9e6f-44f2-bf5e-65d067cf4426', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d44bc5b7-968b-409d-a4b5-ad4ba68c8866', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6797ccc6-5aa5-4cad-a557-52c9101e5353', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0bf346e4-8e42-4b18-98d5-df40c111452c', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b477e2d9-c663-4454-8889-8ae352c34756', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c2251604-130c-4e78-9a88-bf0fd2d4fd55', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b718453f-2bd3-4854-8556-87d53c111caa', 0, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9d8aa9f7-b92d-4918-be07-6f33aa909c34', 3, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('80c46912-4796-4421-819f-d0bbbaa9185c', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1182b119-14f9-4e7e-9b65-6d85a31ebe57', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b6c31cf6-91cb-4ec2-9a5a-64251c71702d', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('753dd773-4117-469d-ad98-c301d76e180e', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a9c7ca97-57a5-4558-ab9d-a06d64a09396', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e0a4cabe-a151-4613-9eb9-c79eace43dd6', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ba1eef8c-17bf-46cd-a8ef-d245db8ae2c7', 0, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('37e1e442-7de8-40cf-a0da-028811d6624d', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5e763b9a-3456-4498-98ae-5ea3bd70b8f8', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('016eef0a-4944-42fd-94fc-79db38683cc8', 3, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('730e7551-6188-4d7f-add3-12e63d3f85e1', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fb901719-fed8-41a2-8798-63a0158c1d66', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e7420f3c-6e16-41b5-9b9d-c2ccf1bdd2d3', 2, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bc41d03c-f184-43ee-af0c-d58de57ae519', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5eae46b1-89d6-4693-af89-6e836c511b83', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0dbaeaab-4e42-40ff-9d19-6933b9e420ec', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4c9f96da-10bb-4bad-a642-e93f516aee2c', 0, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4c80fb30-350b-42eb-b355-492d08bc4867', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('cddcea92-5db9-4c81-b34e-b1839dd4b1ad', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('22d8e32b-ee4e-46c0-8bde-2bcd678e420a', 3, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fd9aeaef-ade4-464b-9522-ee2f4376b239', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('77779cd6-3d6e-43b2-a270-9f39350c60d7', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('47e60afa-c1f5-430c-9b16-77bdac0a9f11', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2ef22948-0c84-4bda-b4c6-256dcc08a2a3', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5f1d0083-c20d-4eab-be0d-d8c92881413d', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('16e30009-d602-4875-b940-9826e47359e3', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6cd476f8-0025-422f-80e9-18448176eb2c', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bbab45b4-3aa2-43c8-8ac7-416281598283', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ea2bdfc8-84ac-49ac-ba65-1f6fae4cd9e6', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9b53b58c-3a09-41c3-9c2d-6a0de251fa9f', 2, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('94421d9c-def5-4d9b-abd5-7076c6d17f62', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c272bcc1-cd5d-4f73-ad13-edcc3c3d7213', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1d594dce-bc9b-4c09-8b94-8e00a8886746', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('44ee096f-329c-4ddc-b664-b7d98028a4d9', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a4c41cbc-1a1c-44d8-b93c-f544a7420c2c', 3, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('53c5271a-1bc0-4673-b69f-3b92aa352b0a', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1da49d33-edec-4907-9de1-19b43d0b5dad', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('397b1cee-c030-43f6-b4c6-4cbabcaab4eb', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b00938ce-8b53-43cb-90d1-05ed1a8a1e69', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('91fa5feb-0717-4180-8305-c806d68454b4', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f36deb92-bf8d-4fae-a80f-3e261b223458', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('14f6ee0b-0a01-47ec-bf68-c370e4507d6a', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3b79a60e-6f08-42d6-9562-5a77e7498104', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9b3a3528-e825-4906-bee9-a7883f3bd07a', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bc43fa08-f973-44d0-80c1-c72f95451f24', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ad662fa0-148e-49d3-8635-fc0977f4c7c0', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4615f4e8-ccd8-42a1-ac4a-8b3f5e88eda7', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3c513593-9444-45c6-899e-5ca7b1edd9b9', 2, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('86cd29de-9ba6-453c-a0f0-c79b900adc06', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9a218436-bd69-4eff-9569-4a1874408761', 3, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0df3eaf9-ef3e-4868-961a-40035a3cda38', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a7aa97d2-daa2-40dc-8362-ac97f2a4d40e', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9c30e452-ce55-44d7-9d73-b097fc001ff4', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('babe14d1-da91-42c8-9d31-0aaab9e051eb', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9b0cee10-8451-40b6-b6fe-6f0ea20803e7', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c5477e4d-dd0d-413d-93ec-1e4a705ba8e5', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5f1cae3a-c2d5-4165-8ac1-d7bdfe51a66f', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('779327c5-8b76-45ea-a0b0-602213416766', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6a527d2f-70a0-49dd-ae21-f6e3fb64f510', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('06bdee4f-a0ee-4c4f-bd50-f675e03467af', 3, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c3f70ca1-a99f-495d-82d2-669d3f65a0fc', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('59b7e802-94f9-4ddb-9cdf-0923ffdd1ee9', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('257ccc25-f605-4994-a0d6-d8a37615659d', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('cf7916f6-1898-42e2-97f2-48a65c558f4b', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7ace746e-b499-4f67-a411-dc28141fe1b9', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b6dc9497-cd30-4033-8e22-6a2703dce085', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ba48289d-5be7-4e22-8f7e-8acf35fa9ce2', 2, NULL, 132);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9cb3f68e-c36d-4c1b-ab40-2fec88c6cfc7', 5, NULL, 133);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4de326e4-2f69-4a66-a557-bd41b9e227aa', 3, NULL, 134);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1e5b05f3-15ac-408e-8fb9-39714fbba9fc', 0, NULL, 135);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fcca6cd4-899a-42c9-b399-edc154e61594', 2, NULL, 136);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d592f9ea-fe8b-469e-8642-866500b0ddd9', 1, NULL, 128);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a33f9795-ab4c-435d-bd39-10ceb652a424', 4, NULL, 140);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('597b57e1-5fb1-48a4-bf5e-ae38623fe1ad', 3, NULL, 141);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('79f96bc8-da27-4915-bab6-db9da3c3b959', 2, NULL, 138);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8b0ba3a9-6053-4296-85e8-fdad5d436f33', 3, NULL, 142);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c1f07b6d-9a12-4140-ad3b-97da48299c10', 2, NULL, 137);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('598f3a7c-c395-4632-9fa2-8b94ed287993', 4, NULL, 139);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('09c13c9f-af11-4ca6-980b-e8b904d32a71', 1, NULL, 145);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('470e57f2-6ef0-4603-a455-595c51912cfe', 3, NULL, 146);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0d3ffa01-2138-4196-a874-53252fd84758', 2, NULL, 144);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b0696ffc-011d-47e8-89b4-51f355ded171', 2, NULL, 149);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ee26007a-96a9-47f0-8b8c-411978ededa5', 3, NULL, 152);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1267a212-f99b-44fa-b3c3-e36e3246cd8d', 1, NULL, 154);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('236bc854-242f-4901-8557-dee3090f50a0', 1, NULL, 155);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('459f9cb4-b69e-4f6c-af5f-99dcb7ab233f', 0, NULL, 156);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1b330fe2-cb53-45df-a5db-7d6fc81ac054', 2, NULL, 153);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a0442109-efa8-4dfc-8514-4ae79258b000', 2, NULL, 129);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1b0e3b27-bdbd-4add-9769-f200f6f5b575', 1, NULL, 143);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5d2afede-26fa-4149-ac87-4169226b174d', 3, NULL, 147);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ef84a272-fd9a-44da-85f1-3be329f04510', 3, NULL, 148);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3fe7db7a-b50b-4fa2-9531-d29269fac146', 4, NULL, 150);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('376d1f1a-ac7e-4161-8051-30621507d808', 3, NULL, 87);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2776668e-fb65-408a-b601-a87cb79176c9', 0, NULL, 90);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b03dd311-6e99-4c90-97db-059334f93c6f', 3, NULL, 83);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1b2766ce-d4af-42bc-a8fa-79bf505bf8eb', 0, NULL, 120);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3089ae30-0ac2-42a1-a326-50e9d7e34e4c', 3, NULL, 122);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('03919369-e358-4129-9a48-a3896eb82cc4', 0, NULL, 123);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7de8a858-c7b3-4bf2-bc5f-98b85f48de80', 4, NULL, 125);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e68dbd15-0675-480e-98b9-f08e5164274e', 0, NULL, 127);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9429c751-f28d-4f7c-8c66-26fa3e6ccd0f', 0, NULL, 126);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('554ec305-2d59-40d6-9899-3dd0ad30da81', 0, NULL, 84);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7799b03d-bdb7-4fe9-b363-b567efcdb490', 0, NULL, 85);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b790c24f-8488-47f6-8ae7-d9f82a83cdb6', 0, NULL, 86);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('15cfd91e-c3af-457e-8bac-a1f273b2a2e8', 0, NULL, 88);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('889d48e0-1109-4283-a08e-8ed939837bbb', 0, NULL, 89);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7a21319c-6ed7-44b8-974f-6892faf630c2', 0, NULL, 98);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ae9c86fa-7d38-40e8-a98a-550da0c568d5', 0, NULL, 121);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9a3d96ac-52ea-48ee-ba8b-a2c994cb097b', 0, NULL, 124);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('598d5e2f-f140-4775-b9e7-ac152d601ff1', 3, NULL, 62);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ad4af27c-646f-437e-9ab1-d09be3c81259', 2, NULL, 63);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3fc16087-96a7-4a33-b1a2-1270c0bd4c42', 1, NULL, 64);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a0766404-062e-496d-89c1-c1233972ba31', 3, NULL, 65);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('15bb6d52-23d1-48c9-bcea-c3106a1e1dcc', 3, NULL, 66);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('48bf9bbc-4eb8-4b3b-9d65-d6b63d20170e', 1, NULL, 67);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('30b54ab1-78cd-4c4d-8597-44b5f5199b4a', 2, NULL, 69);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('23e01149-9215-4308-b376-edf3e0ae2e70', 1, NULL, 79);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b5ada0aa-c3dc-4993-9488-949693ad0ae0', 2, NULL, 80);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8bb2b362-2af4-40d0-a8a9-ecb9dea8d7bb', 3, NULL, 81);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('57386521-ff80-4c9f-b626-691e1427ab0b', 2, NULL, 82);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('086599ca-e36e-449c-8138-d2bf1c4c7bca', 2, NULL, 73);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c46a8ddf-db94-4fca-9757-3487259006bd', 3, NULL, 72);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bddd8f06-459b-493a-9518-8ffc53724693', 2, NULL, 74);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ba0f3213-1aa2-4f4f-88b6-aaaec60d68d7', 2, NULL, 75);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a97a3bb7-0809-4798-869e-fb81e62f907d', 2, NULL, 76);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1621bfa9-b9a1-4873-b061-0f8c5065dfa1', 2, NULL, 77);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3a385baa-e1c4-4afc-a44c-a57dae0cae38', 2, NULL, 78);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c0d9e5eb-b4b0-4866-aa31-233c1227e912', 2, NULL, 99);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0c4b5b1b-7c90-443e-b80a-db94af676b6b', 2, NULL, 100);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9eb242af-b9de-4d68-b1b3-afbd9b2b54fd', 1, NULL, 101);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e0d19ca9-09e5-4d49-98ef-72bc17b261b2', 2, NULL, 103);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3fa6e926-c4a4-4841-8554-e8bb933a7660', 2, NULL, 104);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ed53eac9-b322-4039-9ee7-af5cbfb9b00f', 4, NULL, 132);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3bd0cdfe-c512-427f-8a72-8e21cd53b98e', 4, NULL, 133);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a2438a1c-7f46-4fa1-a676-a93c7a853fa9', 3, NULL, 134);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2557da68-f0dc-4213-ae0c-98e7c10c7b7a', 3, NULL, 135);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('279d769f-f50f-46b4-a604-41af7d4ff39f', 2, NULL, 136);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a66f88a8-4a3a-4aed-a719-4145f9f9fb37', 1, NULL, 128);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d6754d16-d372-417f-9151-2c62ec9294ad', 3, NULL, 140);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('30c19b8f-e461-4b87-aeab-f5b9740eaa15', 2, NULL, 141);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a3fb59a5-5238-4e65-9dbe-56ec8d40fd0c', 3, NULL, 138);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b219510b-39c7-4ba1-a03b-318f832988df', 3, NULL, 142);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1750908f-0e83-44fd-86e0-91a3a034c3cd', 2, NULL, 137);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5eaff60a-f3c3-4aff-a765-49e0d317edd6', 3, NULL, 139);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6ec2378d-9e0a-4aca-9eb9-0580b11bf8c3', 1, NULL, 145);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7cb0eaea-d263-4b4f-bbe7-3f0bba2f1453', 3, NULL, 146);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b3204cab-155c-4be6-809b-fc42e03f1c86', 2, NULL, 144);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fc43af7b-2db4-483f-87f1-2324c5a4c606', 1, NULL, 149);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fa40cdd3-106d-43de-bbad-05407969852b', 4, NULL, 152);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('cc8db4bd-986a-453b-b6a1-73f3b53b3c66', 1, NULL, 154);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8491f0ee-faa5-4728-b74d-be6105b2e242', 1, NULL, 155);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f02b28c3-b79b-43e5-8c51-3d89dbab21ae', 2, NULL, 156);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('015ad776-2dae-4cd2-a473-e1784abfe46c', 3, NULL, 153);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('92be7d55-6337-4ba0-bae4-9891d3d2b75f', 4, NULL, 87);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fe3c4e9e-3f22-4f3d-8e98-7f86e0321b70', 0, NULL, 90);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f8779961-fd34-49a5-b2e5-ead9d5a69e5e', 3, NULL, 83);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('180f4721-e828-4c03-a467-e942932a57a8', 0, NULL, 120);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c8787908-4945-431b-8b1c-510a5b0666a6', 3, NULL, 122);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6ac6aa7e-73bf-4223-9453-028a8a5357b1', 3, NULL, 123);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('213d93d0-ad3e-401c-a2ee-2744803ee170', 3, NULL, 125);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('efbfa352-d9ca-45bc-8d9f-6262a0c78aec', 3, NULL, 127);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0dde3a67-e8af-43bc-a245-80eb830853cc', 3, NULL, 126);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('13b85b0d-3916-42d2-b233-b3591e74bfd3', 2, NULL, 84);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('22f0af76-9fe2-42e8-8557-6b9e305a2557', 3, NULL, 85);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9c7d73ae-c708-4d34-903a-ab119be10d59', 3, NULL, 86);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2a42cf95-c043-4810-830d-f94670cde797', 0, NULL, 88);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('df4d25ce-b5ca-4f64-9e6c-ad3568704296', 0, NULL, 89);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('779b577d-202e-403c-9c2b-fcc7d0aecd42', 0, NULL, 98);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8ab8f83c-c948-481d-8118-3b92e31c872e', 0, NULL, 121);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4aa51e3a-8f8f-41b7-8119-d0cc98fd1d1e', 0, NULL, 124);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3a5afe4a-ee00-49f3-b5cb-900f2bcb8049', 2, NULL, 62);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1d7457de-6b9b-47d7-b658-84c5eee5daf0', 2, NULL, 63);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8e4e6780-ee1c-40e3-b4f4-ababec6ebf1c', 2, NULL, 64);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b7b7c396-3da4-4dd0-a491-4ea26aa01ef4', 1, NULL, 66);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0ab845a7-c8ab-4dbc-a38e-8210f959d856', 2, NULL, 67);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c4c0a604-dd6d-41af-9bda-c5b58977befe', 2, NULL, 68);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('68ec7173-c2f5-4f37-a7cc-fb56e8555daf', 2, NULL, 69);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('741b972a-43e5-42a6-b061-03a1d1f633ed', 2, NULL, 70);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2111bbcc-4503-403f-8d8b-3ea9dddded41', 3, NULL, 79);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d47c1ea5-4bdd-4755-b48f-bdad2167cbc4', 1, NULL, 80);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b90206c2-3445-4727-a9ae-c9898f4f63f9', 2, NULL, 81);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('89c4bc2c-991a-46d8-a551-af579fe7f715', 2, NULL, 82);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d577eae6-9d1b-42bc-9553-8c8c30f42550', 2, NULL, 73);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bb1ef21c-4eb9-4cd9-b5e5-e7a56cd5d497', 3, NULL, 72);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('982bbb13-5a1d-4e72-8edb-3fca590c1e57', 2, NULL, 74);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5f9dc0e8-9d83-4f72-a939-3cd546d16e5c', 2, NULL, 75);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a6d0d46d-3884-4619-ad74-a369db2b6a83', 2, NULL, 76);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2974758e-f1db-4408-931f-bf83cd3c2544', 2, NULL, 77);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a639265b-2a58-452b-b473-73936b70f4b5', 3, NULL, 99);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('520888d1-cd7e-45d9-9a7a-29ab5c3b59ee', 2, NULL, 100);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('22ec02ae-b964-4dca-952f-41dfa8e5895f', 2, NULL, 101);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7f0db169-365e-4e10-8bd1-648b7eaa3a4f', 1, NULL, 103);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ef165b00-14a8-4225-adc1-fbcbf8b6b9fe', 2, NULL, 104);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('04ecc1bc-1adf-42a3-ad3d-53d21f99ad4f', 2, NULL, 105);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c8cd9688-3d58-496b-bf70-1b6517b0436c', 2, NULL, 106);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('62b2a589-b6a9-4ee4-a7d8-6b8c6bcad790', 4, NULL, 132);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ba811539-6b44-4cca-a746-f9578223a320', 4, NULL, 133);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('659ea4a7-2288-4700-ba27-b53148a6f2bb', 3, NULL, 134);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('569a6b92-2c07-47a6-a543-e78191fea3dc', 3, NULL, 135);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a3771d89-391d-4f69-8cb2-a3116fc7e7ba', 2, NULL, 136);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7fceee18-a6ac-443e-b054-4d355d987e18', 2, NULL, 128);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4e7be782-0bc6-4e19-b94c-fb61c0a824b8', 3, NULL, 140);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6b9df2e2-c566-41de-8528-f072de32a0d7', 2, NULL, 141);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8d03dffc-1524-475c-a191-c661a3119735', 2, NULL, 138);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e535f3d1-0c53-44cf-b5df-7012fbb12e76', 3, NULL, 142);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d9fbae3a-6f67-41bf-9de3-c2c36235027e', 2, NULL, 137);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7188430f-db06-47b1-a99b-19464442af6e', 4, NULL, 139);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7b8eb434-9636-4278-9ec2-fd8e3c089b42', 2, NULL, 145);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('483951b2-a4bc-4f20-93b3-c15986545968', 2, NULL, 146);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7ad35259-eace-442a-b717-a0b2e2c0332e', 2, NULL, 144);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('97414e90-25bb-4ead-a148-785dbf44f789', 1, NULL, 149);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ae0e9af0-b57a-4c55-8d50-3700c922294a', 3, NULL, 152);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('47299a4b-d7c6-4ef6-9a7a-1a001db04fc8', 2, NULL, 154);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('63d5a57e-4955-4712-8d54-34ab1c92981f', 1, NULL, 155);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('03b9855b-609f-445b-8b0e-8291ebe184e3', 2, NULL, 156);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1938904b-c862-4b56-907f-8c58038449b5', 2, NULL, 153);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bceb54e9-29ea-4bc3-a485-eaf994abbaf8', 2, NULL, 129);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a77438b5-5c28-4087-88e7-ea09810ccc14', 1, NULL, 143);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('aa12eeac-947b-4990-92ab-fc8481fed12c', 3, NULL, 147);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bd5797b5-85be-43e3-b48e-608aad5365a5', 2, NULL, 148);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1628c5ba-4c87-412d-be05-574ef2d5c57a', 3, NULL, 150);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8314c155-4a55-4236-886e-e7a481a1a4c0', 5, NULL, 87);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1c48fc3d-8bd9-4165-97bd-ddcde645003d', 5, NULL, 90);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f730ccf6-990e-4bcd-b256-2e8b876a8f34', 5, NULL, 83);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b1e90170-d40c-4078-906c-f6ab645f705c', 5, NULL, 120);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e0c81b22-6dbf-4e26-b2e5-e2306210124b', 0, NULL, 122);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('21c00185-6755-4418-9f98-a72d4a6ade06', 5, NULL, 123);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('43f010ff-e092-461a-92b3-1c7d74645ded', 0, NULL, 125);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bcf0a4c7-01e2-40c4-837f-f222c0727fcd', 5, NULL, 127);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fe9d8ce3-784a-4da1-9390-d7bf27c402f7', 0, NULL, 126);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('20aa1d74-d8b8-47a6-a239-a691ed28073f', 0, NULL, 84);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('017f28a8-ab7e-49f2-8cd1-2bd6449572d4', 0, NULL, 85);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fa27f9e5-9933-4b03-adc3-abd0d16765d9', 0, NULL, 86);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('96550a9f-1537-4c01-a07e-dcd8af4655e0', 0, NULL, 88);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b9c7f6ca-6394-43e0-8f84-de480684ebbf', 0, NULL, 89);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5a0592e8-5e59-48d4-b673-116f820fbb8c', 0, NULL, 98);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('921494d2-fe61-4a97-8f07-98aac82f779a', 0, NULL, 121);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f14cb91b-1d18-46a8-aff2-ee91f40e9d59', 0, NULL, 124);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('48b85236-30f5-4bdd-b374-767c2341f570', 2, NULL, 62);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6c92c194-344c-441f-965f-b305b672eb57', 2, NULL, 67);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1dcb46a9-3215-4e62-86d2-05418f9df2d7', 2, NULL, 69);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8376a0e1-5265-4dd7-b56b-4108b4e74831', 3, NULL, 79);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0b1f8f1e-df94-49be-9891-a256c1c9e55f', 1, NULL, 80);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ad6f7327-d785-47d3-bd21-93eb6d713544', 2, NULL, 81);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bce371bc-6374-4d60-a0af-89edaafbe609', 2, NULL, 82);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c250f587-0414-433f-a917-c3137b1ce24a', 1, NULL, 99);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4e816e2f-d90a-4572-9982-42ef96381646', 2, NULL, 100);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d8062f46-f1b6-49b2-8b5e-a94a6f6032b1', 2, NULL, 101);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a5215a7b-ec6b-4381-bf59-c1d56fd8e7ac', 1, NULL, 104);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0d98b205-483e-4772-8a7b-7a9e7ca68c1f', 1, NULL, 105);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4f714e9a-5a23-4d8b-ae07-646b7eea0c8c', 2, NULL, 106);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('32812bbd-3ae7-426c-964d-df393ccfabf1', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9361aa9e-b606-4f6a-ba55-3bb5a6970656', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9f2ee7a1-c432-49cd-bb5f-c5a7dd88643e', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e0d52cf0-a41a-4e77-8aff-dbf91ab234b1', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('cd9a5136-a4f9-4f23-9d3d-a3208608ecf5', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e72bf56d-317a-413b-aa74-c9828311cf80', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c5f0b0cf-0fb4-4706-8fef-d75bc6911074', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('44861eff-7d30-4cd2-b4ee-bcedfcc7701f', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('db969fb7-80cd-4dd4-8e73-36d24087dc1e', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('35e81966-2601-460c-b1f8-06ddbcd8b931', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('67516764-6050-43c1-a851-f6c2f4b0f27c', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('75444fcc-fe42-4e51-b5c0-4596bdb597de', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('71dc6d73-400f-4aab-aee8-ccc00fc7912d', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('53309069-02f4-4f35-af17-ec44e2339175', 2, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('42d5c291-d507-4bdc-ae1e-cbf34290ad2c', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9d9501c8-1f52-4a22-8e13-c90a605f7dec', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('acde85aa-b2f8-46be-ada7-5f692c775392', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('97fdf81e-7f3a-4025-80ea-32c6920197b3', 2, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a7888e33-f27e-4796-ac0f-bb53f6ec70e5', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('05390cc7-5ed4-4aa1-ac0b-c86ec76fe276', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('22859706-76c9-41e6-940a-77b1c2e7768f', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('163415d4-ad72-47d0-a464-dd2000bdf279', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2aa2d5ac-b411-4769-ac57-6ce4f7d7f13f', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('48a5703a-2301-4457-b4e0-30ba089fda17', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('186a8c84-f7ee-4baa-b684-aae84ad09656', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b5f3a80a-9665-4530-bc7e-c31976b0e027', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('78a7e929-e8e0-4bb9-b248-11510d8a344e', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3fa8ff33-12bc-4fe9-b758-8c7b8a698435', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('15eb1c36-6594-41a7-bfd2-b7a4f127e4e0', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d1f3f3ac-0b2a-4fe6-addf-6719b1652e75', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('76439ad5-c3fe-4619-b707-878dd31dd7bb', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bdb5bc26-d5f3-4e62-8276-67a3df20cb62', 0, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d5cde0c3-cb2f-4a76-a552-31d3b5035549', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7e49a8b8-3f68-405c-bc78-d5e643c4b8a6', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('596bdd2f-5b64-4a95-ab37-75216af143ed', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('86732cb8-db03-488d-a959-dc489df253b8', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('aa6c6a36-5e97-4d3e-b151-1b847349bd13', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4110c3db-bfaf-406b-9968-23da350698bb', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f7adb435-a046-4c77-81bd-21a8f961b454', 3, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f1099eef-3b9f-472f-bd64-90e233977288', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b610e529-241a-435f-8528-29d36bc35720', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('61a804a4-6e3b-4aeb-a10f-0276246d8bc0', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5572185b-a927-4bd2-b513-63d5732c07e1', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a6d30a4a-cdc3-47b8-b671-fd36879cb39c', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a749cc79-d692-414d-89ab-6ac7a2689a98', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('828fd749-b7b1-41bb-b627-be773891f69f', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ce0b1dae-1419-4d92-b541-f889516d84b7', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fab7a4d0-1ca7-445a-861b-761412158941', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f56094a6-64ea-4abb-af2b-ba43c7e78548', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a3a756b3-c615-45fd-8042-91a65ef8ed4a', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b6f781ee-4138-446f-bd77-636b9730c2a0', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f570c461-ad7c-40ce-8124-df2288dc5e6b', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4b493c05-bc85-4b4e-8267-750d2fa00db3', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a7237748-ea2c-4eb8-800a-e1ba03c76d2a', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('59864c7d-7694-43f1-a281-26a7c3646a24', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fb53c2b9-fe22-435d-9483-da315cdec0bf', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3eabcaec-d9e7-4514-bdd1-b7b634f6f027', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('036fde5e-bfc7-4d5f-a37d-b27222d5097a', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('77190223-35fb-4d94-9558-f6b5de8f1948', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('83630814-f17c-4d29-8eb1-ac3e12c6767c', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('382338d4-0ba4-44f5-a825-a97ab61c870a', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a9db2037-3394-4148-bc09-fb7efa8eeece', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8f42548a-fcb8-4388-8827-f7873be2bc4c', 0, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7f3c8a2b-636e-43ec-ac92-0df74e0a448b', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9694e82c-92fd-4908-a7a5-7a817599d11b', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2c37654f-547e-4aa3-bcf3-1b55f3220006', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('451a7695-96d7-4a26-a204-d19790167081', 0, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('78c0b89d-f3b3-4178-8679-b2ea9107754c', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3cb19a51-25d4-4281-8342-f314a8380510', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d4956508-6f8a-47cd-ade1-44d22d9a282f', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4b76164e-26b0-4323-9b9b-1181cb643147', 2, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('60e937e3-6b9c-41d2-8069-fdd1d5631331', 3, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4441b0f2-3e86-48de-81cf-af42fc2a02fd', 3, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('50aa0a6a-b1d9-4da3-a7a8-c8ce700e5fb4', 3, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ee556e94-93c1-4b37-aa8e-d65671661881', 0, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('66f2e2dd-5852-47c6-b99c-e5a05db594b8', 3, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b9c2d62b-9a39-4fbe-9448-b8484c8f80ad', 3, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('41003a7e-1806-4706-89a9-e17618b21abd', 3, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a38fa382-b5e0-41b1-abc5-9bcb191bfbc9', 0, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8dce700e-5098-4a95-9e1c-38bd0fd99d12', 0, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1330f2f0-3fa3-47cd-9b1c-0167ed80fa17', 2, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('959795f0-5921-47ac-85c7-883f2e7c256d', 2, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b7519c37-483c-4b06-b8aa-8e16e68991ea', 0, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2aeba982-2506-4f42-b858-a0a0ee840b6c', 3, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c3aee995-fed8-4ae8-9de4-1ce326a6b5bd', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('db2dde14-d4a9-479f-9ed8-a85dceab790c', 2, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a2461f99-8c13-4c4c-82f3-11ba4ddd0cc9', 2, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bd8324f5-4a5e-4483-a3e9-cb007877a731', 2, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b40c14cb-2b87-45e1-8f65-b2e69ec6c54c', 3, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('cea64460-e96f-4053-81f6-ff671ad98e64', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8e50ecf9-5237-4373-835f-b406dfac65c6', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d4f002e9-1738-415b-9643-b31209fe42b7', 2, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ffb1a556-2214-4e98-8529-e57652d2935f', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e92fdf2d-b744-41ac-9f37-32fee44b6389', 3, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4e9cfbc4-f863-4c88-a0b9-3cde3b2f386c', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7faa6a7d-0f35-4346-947f-ee4835f917ab', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1326cdec-93e6-4954-8d98-b447544496b8', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('967812e6-9c35-4008-8ccc-91af540cfafc', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b9c28125-4697-4038-8d25-8891202157ee', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ee46f18d-db0c-481e-b364-376d9b78a9f3', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('00b2dbff-2ad1-4a9f-92c8-da75b5c79e38', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d64e0c12-b479-4bf6-98f5-b3ddb9f40f59', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c668d9f7-ecd5-4ab9-ae49-f2dbb9393edf', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('085800d3-7513-458f-8ce9-0dd975cb4448', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fc40a620-b692-4461-a786-d1f8de544d5d', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c6f63618-1a5a-4295-a747-1b8571ad0fe4', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3d44052c-5c7d-4bad-8431-80dc67f23c97', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8d85a7f5-7f9b-4424-a6d8-a8bd9072df9e', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ac7152be-b114-425c-8ae8-22670900c96e', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6958fafb-8fcb-4bf7-8af4-7ba5bee54bcb', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ee6e98e0-60e6-4a3b-984f-17eb644a5ec1', 3, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('053d9e67-9d99-4b59-9a1e-2b361ac52331', 3, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b7f30bfc-26c4-4d53-9137-747667c469a4', 3, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('30dde0af-774b-4876-8e6f-273a03e9c304', 2, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4c672e3e-771f-40af-b109-aa47884d74dc', 2, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8a3e2611-b44c-4ac7-82bd-8005755266c5', 2, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9bbbb6e2-7df6-4309-9b93-cae1a7ff5ca0', 3, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f8e9fcbb-32ef-4bf5-a420-f98924a3f89d', 2, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8fda4e01-e99c-4b1c-9480-5533d67e74da', 0, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7aba2f3f-9c1c-4fe6-9f70-e53055126783', 3, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('37264e0c-e403-42c6-bf2b-8e0ef69e1e30', 2, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('902ce382-2201-4331-b29f-4f41b3decf6e', 0, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('893081ca-e7bc-48e7-bba4-f5c8cc13f391', 3, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('82bea216-7719-4969-9fbe-b70ac75f6b05', 3, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3868efe4-d575-43a8-a3e1-24d2600b6ba6', 2, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('737349eb-c225-47c0-ad3e-4059096073fe', 0, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b3c7640a-e7e7-406d-a1eb-8b276b4a2563', 3, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9469a580-b725-455e-891b-0395d1247172', 3, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e1fd9af2-5559-47a6-a4cd-11d1cec98ebc', 3, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0ac04c57-d946-4de9-8618-df80e18754c9', 3, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('596b90a8-4c2f-4044-97a0-8c358314d4a9', 3, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ce06056a-9d0a-4b67-8e92-3b3f4d5220fb', 3, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2c0438d1-6f9a-4c57-b338-170ab181399f', 3, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f3be7c5e-cbb5-485b-b9d9-29fdb0a0da3b', 3, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c452ec0c-9f35-4106-8e86-49f64dc96964', 3, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c40da409-e55f-4407-9e4a-ee9a6fa2f86a', 3, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('72fa6b75-60f4-41d4-817b-ca67faea8a02', 3, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d9435171-7de4-4bed-a241-40ae0ca5f29b', 2, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('69968dea-1a32-4697-8970-0d0110ffccd3', 0, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c4abd0f2-524c-4bd1-a4e5-c5746e914619', 0, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('aabcd70b-dd09-4fd0-9ad4-089e9b76fa01', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ccaf31f1-e511-4b59-804c-0e4e86d1ee1e', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('54195db5-515d-43ee-8124-bd78b4b2fbf9', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d1593ee4-1069-4df4-aca2-a9a539250433', 3, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ef2c39c0-2b89-4639-9e37-2420f2d44e1e', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7719036f-069b-422a-a416-0bffdeb7f5eb', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a46ba8d7-3cc7-490a-9d4f-f80a85c84a36', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ca7abcce-3c2e-471c-be34-f70a77649483', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ef00f97a-d59f-4ecb-bf8f-1c4e763f0603', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e770e554-c6d9-4b7f-8f08-ab7694d5a9cd', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('375ff84b-1a26-4ec8-9124-c3aa3044487c', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4e9b03a3-5f82-4aee-bce4-8db335cc9878', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('85481bde-ea32-469d-aafe-755e5e0de619', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1ad99309-9f45-4f6c-8ba2-9b544944f7bb', 3, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7a08daa5-730d-4616-b79c-a1fd708048fa', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('72eaa363-eb3b-4cca-a286-3291b7f85e94', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a22b3ccd-631a-48ff-be3e-f8d762e52c1e', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a25af239-2d0d-445e-bc19-8b6875947046', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5a283f43-d73b-457e-8b7c-bd8ec5d8ba94', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c46b788c-a926-439e-b222-36bdd080d7c7', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3fc57417-afd7-4e97-912f-a6e5530e8776', 3, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('eebda61c-6711-40fb-8583-12f7a7671238', 3, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('886bdbc7-d5b4-4b47-bc14-c44500a5cda5', 3, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4b073334-7e8c-4a2b-9ecc-02b8b155cc00', 0, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c2713469-2746-4d1a-99d2-357b36774922', 0, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7eb291c7-b36f-4668-b3db-1c61df8b202e', 3, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('cd44f8a4-7c87-48e1-8600-bee5be283c55', 3, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d4bae2f2-358e-4f6a-b014-c2dd958d78b6', 3, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8e7c43ad-c06b-4176-ade6-f6803a1d3d3e', 0, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e582ad3e-a003-482a-a084-47e78e814607', 0, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e587c856-0c36-4e3d-bcfb-b5df3541bb6e', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c046f8c0-dba8-40ef-831c-dff4cbd1f591', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4c5dcd9a-76e4-4b68-b6d7-20d872d895e0', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3c3c061c-9191-4549-a0d3-f52a0831a21f', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c3cb9043-8ae4-40e6-8324-c4660b5a2474', 2, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('24561fe3-4b3d-4bcb-9495-f1aea76761ab', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f27ff8e4-1922-4c73-87c4-2057841dfa49', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0cfaa206-04e7-4423-9040-be0c2ce50867', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1f1958c4-7da9-4acf-97cb-d638a16b544e', 3, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1aeb27ec-c31a-4a87-a24f-57d2cf3cdd38', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('986527ff-8b3d-4945-a6c8-b2ff277ed886', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('399c6aae-bb7c-4feb-b94e-3e0cdd15eb9d', 2, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('47d0f703-504f-4ad5-af34-e8a3945ca01a', 0, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fbd65c8f-f30a-4a14-bec3-540934c6887e', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('77f60e8d-9f31-4b96-a472-9380e8a93996', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('17e793e9-fe0f-4c5a-bea4-80de261f73be', 2, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8cb7e09d-bec3-4607-9708-1659127f60a4', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9be6af9f-4f99-4aeb-a40d-8fdc11841acc', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e3e2b997-0f6b-4775-80a3-3cf9536b5fdf', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0d44b880-9ade-41df-ae46-312a15f3ac30', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2424017a-7b76-4b00-ae58-5574d9344d0f', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f364540a-2527-4828-ba08-a283c0e184f4', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('202a1bc7-0650-43e1-ab9d-3bff3e946f79', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a5296e0f-1a19-4803-99f5-707478086ec7', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9e8efbcb-502e-41be-8dc0-3e79b3fd77df', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bc4b79af-385c-4be0-a870-b051c98b4410', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9a746bd4-867e-42ef-adce-b7bb936e34d4', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('06144937-61fb-49b1-b5a8-6218deb0db2d', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7f1bbd36-eed8-4dd0-936c-8f9f42194932', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('898a7371-4450-4215-9df8-f798bbf36a72', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7c16e2ce-d3a1-4f41-8631-352c5eb8a15d', 3, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6820741d-4f3a-4ad4-aeee-83f09c2dfd97', 3, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b6c20edb-9670-4254-b3ae-6087f9d1fe5b', 0, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('68014677-664e-4cc4-ba26-1b9caf9c0339', 0, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6d94e8a3-7433-460b-94aa-a39ef5436d61', 3, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('88b7a7f9-f49a-4ffa-b391-51c2ba45fb32', 2, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('583b2815-e227-4316-97a9-5deaf33dbfa4', 3, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9b24fbf9-25ae-42f4-928c-d39af8e73a9c', 0, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e43b6e1e-caad-4443-9e5a-3ad2fb1cab02', 3, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f71ee32b-f394-4f4e-8f51-e393ca853eab', 3, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7cfe3b52-5129-4919-908d-e9a430b93e35', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5b8f1866-7aae-4a7c-a9af-695bd4aec125', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d9d5f498-ff0c-45a3-aa1b-223bc8c00abb', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c1f1fdea-5707-44fc-8cad-61d6a654b5e2', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6238693d-1017-4a95-94be-b32f278f1fed', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('41843253-78f8-4655-a877-a18ec4355052', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ee55660f-a680-4241-9c90-4a5a9a0a502e', 2, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('af7b2123-6a8c-40a2-8f25-91aa89685884', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('95daa39f-0adc-418c-88b4-f193aabb56ff', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('32d75211-8e62-4ea0-8e95-26d8a0451a1a', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6c0557a8-ca7c-4a3f-8b83-d96e3e96a32a', 2, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('469f6582-cd47-4ed5-ba6b-81bdea608316', 2, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6544d2d2-f941-48ff-8eb3-ffb6be13cefc', 0, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('641500be-70c9-45d7-8976-4f1e51724f06', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('77c8eda2-7c38-4416-970b-cfcd5bacf2e4', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7797fef9-ce00-48ae-9b52-750a6b740963', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b30dff46-d0bb-4540-9bdd-f1d87b15c1e7', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('80319a20-8a70-46e3-a39a-743ec0204397', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d851bb1c-2800-430a-80af-e83a38a76849', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6de2411d-fa60-4d18-b26c-cb0959559906', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b251e753-f100-42de-82c3-9d90795d6b07', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('da55f053-ff0b-43b9-ace6-4996041e756a', 2, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('69242c23-0bcc-41d7-9b22-1659feea2c54', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1084ae58-ad2d-45ef-9a91-01fb4afb0dbc', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('899272d4-3213-458d-a65f-46c529a11c85', 2, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('14190346-fd31-45d9-8f3a-8e232747d301', 2, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bcde96a5-35e1-47ce-b94f-92ccb1d9c927', 2, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f6c7c3d8-426e-4ef3-ab4e-87c2084af17b', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8810c67f-4051-4450-bbf3-1526ea07f0fe', 3, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('016ef1cd-d7aa-4cdb-9ebb-42fc8244e52d', 2, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9506df91-c257-4619-84d7-261f91afab77', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('95be3f1b-3546-4a41-b77e-c7ab3e30f8e6', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('cbc4c8be-c61d-4fed-9d7d-df8770f1ef6f', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('dbe2a923-b611-4298-8cc3-a1794f1f916c', 3, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d91691aa-3068-4e86-b9cc-83c8e18ca05d', 1, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('29c670df-179d-4d1f-9d67-b7bb854a0f39', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ab144a65-a4cf-4933-84ac-c62b09c7f3eb', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e08d692b-78c1-42c9-9a63-c4cb25905f80', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e290d623-f903-4924-9d37-4bf7a6784a59', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('abdbb063-ce2c-43ef-a0bd-f1d5ece43022', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('92e7caa4-0f34-48d1-b183-356316ba5ad3', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('310d8eaa-59b4-4a5e-9f6f-4a6e4b06ac14', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7c28c6da-34c5-42a1-be18-622523586f5f', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('77215767-70e1-4eaa-a22f-089d1fb6fcc6', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bc93a9d7-8e80-4693-993c-789a79fe38cb', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f49c2d9a-e281-4088-9187-b4e998c4b354', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('805fa433-5bff-4ed0-b70a-64a34990828d', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('460107d9-a540-431f-b026-65026d8cf5d4', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a2304f30-7304-45d1-b005-9f8dcf15457b', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('940c5798-e2fa-494f-934c-e41cb3d5c3ba', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('42dd60dc-9ac3-4c72-ba27-26d7248e3001', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a0fd8fb1-937c-49d9-80f8-57b7f770164e', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e1ac066d-e68c-4e45-8a0d-b11ecf9820ae', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b1f87269-39b4-4726-a00a-d92e3c235517', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d73e3347-e76b-415f-91ca-f9872e3fbb18', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5ff8c85f-deab-444a-a174-ee68f61c0b3c', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('988fcc27-d0fe-4d36-8da2-6296885d4f39', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7f6ef0dc-0df2-412f-912e-6dd1f0762333', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('afcfa1a9-5634-42ce-8025-42c03d15f3c1', 3, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e3659145-e636-4683-87e7-1245a493d515', 2, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9ecbfba9-3dba-42d5-8d1f-db35af0a4c8c', 3, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('eda2993f-8472-4a26-9fdd-10b17ef4f22f', 2, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b0fc037b-b21b-41cb-ae24-334bbae03144', 0, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('44c6b10f-be7b-4230-887f-ef1efbf2b486', 3, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('68dc29eb-a661-4e0b-850b-feffc92e7761', 3, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('384d6a05-d601-419f-b1ad-4672b0fc888b', 3, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9f8348b3-e857-4ac3-b3fd-ce7c580910d2', 3, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('620d5737-40c5-44af-94d8-fbd24903ff03', 3, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('76ef4466-52e7-4020-b45e-f289895bb606', 0, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('83b388ee-2d11-4abe-b2cb-04afcdf0ec39', 0, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('584c2004-69f6-4f45-adc4-0b7e018d1bc5', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d45c5187-71e2-4e0b-a44b-00aeaf18f560', 0, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4719035d-e692-4366-ad77-8bc108641b39', 0, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('51251bf5-fa19-4c1e-a4ed-571641e19566', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c0da6800-b795-4ef1-9fc9-3f54c6ead215', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('218361cb-bf9e-4c0a-88f8-240bdcc1b97f', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('49f4d772-d9e3-491f-b8f9-ee1ab82b4690', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f830db65-94c5-4fb5-a012-5719153f8496', 2, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('67a9a49a-f381-4215-b71b-43970ab493e6', 3, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8160bb69-7046-43f9-80ae-508bb11711cf', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8a80fd59-2402-4f45-a791-f64a7d78511c', 3, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ee5aa451-5b15-40df-bce8-3f90c496c512', 2, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f9d5478a-0000-4985-8466-dd38575552cc', 3, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7a5533ac-48c7-4a68-86e7-3f5c25a224b3', 0, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2de04b69-7efb-4174-8df3-63e8473fba7a', 3, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f0df53ad-4926-45ff-b2eb-67eb988c865f', 3, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('38fbe9f7-3175-4a0f-86a5-405d0dba428b', 2, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('025891f2-8ea8-4077-8dbe-6510703ca06c', 3, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3720c5d5-4d3b-4776-9001-8ce8230c658b', 0, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4c05eab1-17b7-4a5b-acaa-026e811b33a2', 3, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a6cebd09-c017-4926-a0ce-a88a5d37167b', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1e341676-64d3-49f1-8a24-15fd42c9e546', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('09e19fb2-915b-455a-8291-4508c5775daf', 0, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('702bd684-eb23-410f-89d3-ddde7c4b134c', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6d83dedc-5fc3-4040-bc9d-edb5e4af7482', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a8a37630-6cf6-4d4a-9040-0e6fd849796b', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a5d09479-d4c8-4656-86b1-99553504e29b', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b075cebc-b125-4467-a6cf-8355452c0740', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6bca1aa8-3836-4488-87ef-3556d0f6e278', 3, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('275a1f80-20fd-4d30-ad2c-492f209546e6', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('716aa6e4-78eb-4adb-9f0c-e0e8f8637155', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('399afc37-8490-41c2-bc5b-7e7ad70ae994', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ee0d92a5-3942-4230-b718-f0317f172c88', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b65464a1-509e-4c2d-ba53-59fb99daf3c1', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6a6d7322-b47d-4f9c-9369-3306ffd1976f', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fff5b9f7-1d4f-4594-8c6a-79689c36d0ff', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c2b831cc-206f-4ac6-bb87-8fcd08350ff2', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6cd947ab-7e9d-4549-b6fd-00711b0f8c67', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b840ce3d-fab7-4675-bb63-cebd5aad9214', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c86f016d-ab1c-4dec-8f19-ca62a699e3df', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('597cbdc6-6ead-42ff-ab26-2010c80e6b3d', 3, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3de69c08-4b60-46e4-8c35-be937967744a', 2, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('07aea308-63ee-4167-9d27-80fbeb727547', 3, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ab4f06a7-438f-4c56-9db8-13ef2688f5c8', 3, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('26836dc6-331e-426b-a6d6-719af0ed50be', 3, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5238baaa-6a06-485d-aeaa-88c75473648f', 2, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('daca5dc5-4fea-48fa-b47d-e131aa4f2307', 3, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6e93d497-d3db-4754-99b6-675763c3e89c', 3, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fe0f17ed-f260-42f6-a914-6297aba421f4', 0, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b0ce4b76-49fb-40d4-9a74-e8681fbcb6d0', 0, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5811dda2-0d27-4716-b3cc-b41773d3bfc7', 3, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9b764019-5eb5-4ccf-971e-2e4e0dfddcee', 0, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3e66ef35-408c-4e94-944e-4bfbbefe4d87', 2, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2eb72eb5-db6f-46ac-ae13-df34060ad35e', 3, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('92f3e64d-4ea7-4203-8598-a55eb195f919', 0, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ae3aba80-888c-4654-a315-160c7b5ec072', 3, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1300390d-10eb-4db9-b9fb-182e728445cb', 3, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e1e39f13-7aaa-470a-b1db-992e10267a65', 3, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f63e8aa1-d736-492f-8912-4029c5682cda', 0, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7d9e4d58-0233-4fb0-b552-b94467d29154', 0, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2238bd6e-ac1b-455e-b73f-89117b44a37f', 0, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2ce5ef78-0f26-4955-b192-d503a94af76d', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8e4e285b-275d-49e0-8191-0fda82e4cad2', 0, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f652a6ab-1237-474a-8886-ca74f5778334', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7fdeb130-3ce5-4e6c-b90b-8e78ebecb63a', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8cba963a-dc72-4f5f-9051-a5772bd98ede', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('866c7563-de4a-497e-8b30-098c592d94cc', 0, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d0330719-b714-4251-9cf3-077c3c8afc75', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('443bc023-cf2c-48b0-b7e7-dc987f013b40', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3e3740c6-d424-44da-a54e-ad6e85096020', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('31c7e6b0-62a4-4a2b-b6c5-c5eb45ac0ee5', 0, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4ed1069f-f316-4232-8053-3798837f7b06', 0, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a979be80-351a-467f-82a9-511319a81b71', 2, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9f70e2c5-496d-461e-adb0-a9ac41eaca18', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7eb95f5e-7a76-4944-a5ca-481f54e11e7f', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1c41397f-4b70-4639-8793-8a18d1c54180', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4ab44d1e-33bf-4911-bfe7-357ff592ab10', 1, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ecaa5580-554b-43a4-bc3b-1282782639f6', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f6b6d036-31e5-4766-bad4-320efdf30de3', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bda7df99-5d31-46af-bed7-4d1949219e32', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a73f30a8-8211-488e-9d55-5b5a684c2bf3', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0366fcc7-bde2-4bab-9c12-d19290179ad7', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8ece8c76-a7b3-4c9d-87dd-25871744f9fd', 0, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6668c763-97f2-4db0-a519-75a22ba9d16e', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3ce2148a-f921-4bf1-ad2f-8c1cddcd0a1b', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8a14ab0a-d8ae-4434-ae44-8a8b74265b2a', 0, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7ee4ff26-09a4-4213-a8a1-cfbbc88d9ec2', 0, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f867dddf-ce6d-48e9-bc2d-5dde463160cd', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7efa14ef-38df-4f33-9149-1afcde0868b1', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('876bd89c-5f74-4ee5-8fde-84d2c4dc3b28', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4bfc8029-359b-474c-9c64-6522309f5b0c', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a025ccab-a94e-470f-850e-991f72905fb3', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2cabd5bb-2dea-4159-ba7a-883e61f6f2ff', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1452fe07-3ca7-484d-80f7-4aba980cb2a3', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ca73ade4-f838-4f3f-b1d6-98309948b126', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('36b8839f-d08e-4bde-a81d-f982ee8ec152', 2, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1fd9895c-7b0e-4f4f-86fd-6f00188ecc6b', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('dfd32424-6c4f-443d-8619-6f5e91de51b6', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c1f7d848-f5e3-47e6-b182-97c1fe922fc6', 3, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4f956fff-162d-4450-9d58-c84921f9f7c2', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('031b146f-d9c2-4541-9bc5-438c8562c61f', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7b8a14c4-4ad4-4006-b760-d00ce6f33e8d', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8b98875c-709c-49d5-b0bc-89975c408ce0', 2, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d01fe394-49d5-4dac-806c-47a9565b5ac0', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b42808f5-2057-4590-a894-ea648567ba1b', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b88dcf27-bf8d-4eaa-90df-8261458f92c1', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('31348459-57f0-4b0c-8d35-bff78e741fbb', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('aca585e2-6229-4c8e-9745-10fdb445f393', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('555a8bf3-f715-4772-b58f-17a1d6c11cf1', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('78f2170d-32b5-43cb-aa87-79f56ff14032', 2, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b4836226-217e-4ac1-8ca8-7ef0405bf8ba', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('05b43dcc-176f-4efb-9ea4-6171b420236e', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('84a9d147-afae-4192-8a2a-cc04497129fb', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4d84df09-4fa0-4882-9c9d-63363e724b5d', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ccf18bef-a2bb-4cfa-ac71-9ecbd35cd73e', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('499a3423-1449-4335-8e60-8a6ce760cd2e', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6fa5470c-51ea-4f9c-99ca-f29dc9753487', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6579e036-bde1-41a8-a595-a3a5448b8e84', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('01d84c4b-0eb1-4238-b8de-9dc0de970cfe', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('82519673-0a64-4c25-b16b-89b1604cb936', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f60bc554-ea0a-4b69-9e7a-153fad9c73e3', 4, NULL, 132);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('daf660d8-3338-4428-a4b7-99bc420d57fd', 3, NULL, 133);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6bf921cb-86b9-413a-bf2f-5b71d60876b9', 3, NULL, 134);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c7581beb-595b-40cf-8fef-78799c86e682', 3, NULL, 135);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ae4483eb-0a1d-4315-a303-9eebfb79a1a2', 3, NULL, 128);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6891d262-5bb4-4fae-9824-308529b575a4', 2, NULL, 136);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3938d2aa-0709-49bb-b355-2b2b9338033c', 4, NULL, 130);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b9ff606e-926d-4b8d-ba12-4b7a58522c8e', 2, NULL, 129);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('229f7f3e-2d12-4fcf-a1d4-b36f643fd9b8', 2, NULL, 140);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8bc962e7-726a-4e10-82d6-60289fe26bdf', 2, NULL, 141);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('37c86b7b-e913-45e4-aef6-7fd9f63ff156', 2, NULL, 138);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7610f825-16a3-427b-bb17-f9f89d2fee98', 2, NULL, 137);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8ad27bfa-e081-4efb-a88c-df1daa3bb99b', 4, NULL, 139);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8a2b0372-496a-4213-a6f2-9c63cf396579', 3, NULL, 145);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d4148152-3675-4860-801a-7c651f670e1c', 3, NULL, 146);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b4dbe2af-0987-483f-84bf-4a3e10a29641', 2, NULL, 144);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a2a099dc-1186-441a-83cb-9de4e5915d7a', 2, NULL, 143);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e7f23261-908d-4e5f-a0d8-a0ec5d38a179', 3, NULL, 149);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6d6b0a99-ae68-485d-b19a-3e46e43d1c52', 2, NULL, 152);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3238a0c0-92c1-498e-accd-cd76be0c0709', 2, NULL, 154);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4e4bc2a4-455b-4846-8739-c9fce32e344f', 2, NULL, 155);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ef75492d-a160-4f22-884c-244012a4c285', 3, NULL, 153);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5e3ca766-edba-4761-9525-9320c65b638c', 3, NULL, 156);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('42546f9a-4044-468a-b66d-f1edc3b38517', 3, NULL, 148);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5cbf51cb-cefb-4d84-bfb0-b55d1bd22f01', 2, NULL, 147);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('adb3cc8f-ead6-41a7-9477-c57937b3658b', 3, NULL, 87);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('24f59a41-6fca-4f21-a7c3-4a38f462af30', 0, NULL, 90);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b45c1712-2e1e-4e05-8ef1-b3afda24de25', 3, NULL, 83);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('af3de04f-8e5f-4074-bde0-13c874e782a9', 0, NULL, 120);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d00de4e7-5bf4-425d-b5d6-c15af488ea64', 0, NULL, 122);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c40ea489-da74-43a7-9ddf-94eef2388759', 0, NULL, 123);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('13cac2e2-7534-4a05-8ae1-ddfe50acf8d0', 3, NULL, 125);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6a38d83f-13bc-468f-8fbc-8cc79e521db8', 3, NULL, 127);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('55cd2a22-c53b-4517-a096-872cd1f2c24e', 0, NULL, 126);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bdad490a-637e-4379-b2c5-01aa3da294f6', 3, NULL, 84);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('de980981-71b7-490c-b2f5-3164892b7c02', 4, NULL, 85);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6a3d79b0-0e16-468e-bebb-0c1e534b4f7f', 0, NULL, 86);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('57acbda3-520a-4d88-b4ee-5d10f9fdf41f', 0, NULL, 88);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ea5baf31-dafc-4639-8f89-2c5657c3d462', 0, NULL, 89);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('720671f6-b46f-4bd1-b6af-8e380b03e5fe', 0, NULL, 98);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('10ca9acd-7e9f-42db-9d73-2840c223be72', 0, NULL, 121);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b0149d15-cbe3-4fda-8d86-adf3cad2d155', 0, NULL, 124);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('262c5e59-c22e-49fe-88d4-f831495fd432', 2, NULL, 62);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ce9b7d37-e783-4c21-a9e5-ff8655916f11', 2, NULL, 63);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f02ea692-01af-4584-9fa2-dc3f1a79db69', 2, NULL, 64);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('203a35e1-2a74-4da3-9d0a-7b192c4d4a0d', 2, NULL, 65);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('15ae7575-4794-405f-a974-be527ef9a35e', 2, NULL, 66);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7f08ce84-04b0-45d6-8ea8-4cac90ca0e0f', 2, NULL, 67);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('60c0350d-4270-414c-8aa2-0312079df225', 2, NULL, 68);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bf0f7643-63bb-4c7d-b3bb-c7964669949d', 3, NULL, 69);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('04da5e78-f884-4972-b754-071db4147f5d', 2, NULL, 70);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('41f55d50-fe5a-46e4-8be6-b865a7abb34f', 2, NULL, 71);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('62ad69f0-1f36-4c4b-bd73-b6afd22f0bff', 3, NULL, 79);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e24ddd4f-ab7e-44ef-89ea-00134658673d', 1, NULL, 80);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('995efc06-4a0f-4cef-a067-06618e8b4985', 2, NULL, 81);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d3d8fe3c-62f1-4c07-9549-69c86625e235', 2, NULL, 82);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('90c54c01-de4f-4d04-a60d-f4b826cd3b8f', 2, NULL, 73);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c4a9d67a-b461-410d-acbe-90fcc1db769a', 3, NULL, 72);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6a66a6ac-8f34-4373-8031-ebaf399aca0e', 2, NULL, 74);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('72c77293-a54f-4428-bce4-be0eb4863c29', 1, NULL, 99);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ecdc0d32-8a79-40a6-9abf-e7009fd89e6f', 2, NULL, 100);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2351f66f-6600-4d4b-a7d4-91833fc1585c', 2, NULL, 101);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('895d63f2-2ff1-4a37-a6cc-7377664b2fb2', 2, NULL, 103);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('da5cc7e3-0639-424b-abee-83c2b5128631', 1, NULL, 104);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('50298c3e-0c5f-4eef-b4e4-20009bf68ea6', 2, NULL, 105);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4c643754-3948-4000-afba-486355b2d1be', 2, NULL, 106);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f188143b-b042-499e-b86b-84ba36d8dfbd', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b91d3442-4c22-4fea-a534-59d06154df78', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7965a5c7-7700-47c3-9307-11cadd9601ff', 2, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f8adda58-31be-470e-958b-2e8c29d90381', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8fbe4b02-a215-4d35-9af8-61ed6d0a09fb', 2, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f6c80b56-241e-4ff1-957f-9468eb9f8e4c', 0, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('196b7b12-c481-4fc7-b0e9-40b106406821', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a959e2ce-6da5-4415-9b89-e0bcfe60e2bd', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a767c3e6-13d3-4d03-8578-e008928ab434', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('78b7ac4f-97dd-4d8e-a6c4-fadb8c89f464', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f35c8e08-6559-4fda-b127-82e64c6436f5', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7a58ad31-5cd3-425d-8322-f16afda8191c', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9b7e0cdc-d57e-40d9-8c9f-ee63c80e5b5b', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('07d33f8e-bfb5-40e9-b036-c24f8d285bd7', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b5077184-4476-4fb4-ba21-339354975f59', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fb2754b0-8836-4e2c-9ce1-65999e06aa30', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('26520647-1089-4d01-8a6d-7b15b0760c30', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4b9ec600-1c00-4d4e-9479-f491cf79e74f', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ef42020b-6412-45eb-8d25-a55ae5db989f', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('495a6204-95d7-4fc8-9bb5-4c5ee151c8d5', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('86d50332-b5a8-4cd8-954c-ff29c8dec145', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6294b356-f7d5-430b-affd-4d6ea532d8bc', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ecb956ad-4ca4-4b55-8e6a-a0c84e0249c6', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ad734dd2-2b1c-4c89-84c6-d6f781675209', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e8a146c7-4d2c-49fb-ad3c-930c9ab2a425', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('034fb87f-6859-4bd3-ad19-9069da2bd039', 2, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('02125d88-35df-42bc-b682-58e2c71eb6d8', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('da989d85-d23d-4b62-ab81-0541db883886', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b2cf83c9-d44c-408f-8d09-996e577b899f', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0ec1370f-3627-4271-8056-ee88c5c40d03', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a6161e70-02b1-415e-a9db-35c6ca7c5e35', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3b5e8340-da21-43be-9539-1552528459ce', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4ffefbea-363c-4d28-9204-fdda72f097f4', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('108ca353-8861-4ac6-aa83-3b00ff48463c', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('52da33fc-02a7-449f-a4b9-1b9550ea61ed', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('01b50c27-45c7-48e0-a0ec-2c7f66129347', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0f469d34-3849-4fe5-9b05-53dd260e6590', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9bd2fdc3-58c7-40bb-924d-3831a3097a81', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('010b1bb7-7ebd-4804-9b89-cbeb9b382377', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('45486c5b-d745-4a26-a201-45c41ba343da', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b66333c0-d1a9-4155-a194-7e6a965bbfd9', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1876c3f9-76ff-4b3e-98e7-bb3f05d2b712', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0c60889d-24bb-46dd-8ba6-ff6949a7519b', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c2f05c78-0c4e-41fa-940a-65e1a2b51891', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8a6faa27-4464-4cf3-bbc1-e6b9c248f5aa', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5ceb88be-acdc-48e6-a60b-66ea95cf6169', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7a214a3c-cb29-4fe4-85e2-48293bba586d', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('67e2e52b-630b-424d-ab4d-53766485b724', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('947b2796-77d6-40cc-8727-83fd8ed9e218', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('579d7bc6-dc31-4d6e-b2f5-182bda5a3992', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c6a5607f-25f8-43ce-983a-0eae020e5c48', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b8a309a9-08dd-4173-b1fb-deff5bf9ccd7', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bc4ebc32-0d92-4955-8586-1ab84e5a321e', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('199b1f9b-9c55-4158-9784-ec0d8d2784fa', 3, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f6fdcfef-f0a3-4a02-b432-ba6ed03f2cc8', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7a9e6df7-b678-4cbe-b8e4-e344b0498f4e', 0, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5e66265e-481c-453b-bf53-240a05b0d94c', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c82c4de7-b44c-4edf-ac6e-bc2ff86aa90b', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bf16f0fe-7702-460d-9de7-d328189548e8', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('07777250-055d-4171-b012-434d28f5f55b', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7171daf3-fd2f-4a2a-8bf4-0fb8d87b750b', 3, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b1f1042c-bd98-48ac-aa59-14060d6d96bb', 2, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b3c57110-caa2-4d60-87e4-098e546d1f5a', 3, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8487575f-0914-4b53-8d5f-9a455c3af909', 3, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2d6f64d2-f82b-43cc-b391-8285120191e8', 0, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('528b37f9-1b9f-4771-90c1-e9ce40e51300', 0, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('787beb9b-58b1-4c5a-aeb5-0fb4398e4297', 3, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6c7d4b9a-e3db-4045-ad5c-9bf8f5df69a8', 3, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('01d2ea1e-1935-4fc5-b2b1-ae613e649323', 0, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b602f90e-874b-4e3c-9d68-7ea3db939e4c', 0, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('804bb495-deca-419d-abf5-11be8fd692b6', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9456144a-b970-4c36-976b-9fdbaff95c56', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('65f136c5-30d6-4960-a768-116eb3598b7a', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9bb8382a-46df-45b3-9090-a11a50057068', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('60df4fd7-72eb-45f2-afe9-92e340cc0cfe', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('543da62c-5785-4a06-9e61-288effd7c45d', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2c64a69a-f96f-4fd6-982e-90e8005bf142', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('94e68ccb-9079-4ebc-bbab-eb312fe0ad31', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('cb027914-f8e4-4312-96d0-59c73942936b', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c3f530a2-c83b-4a11-9c0d-71e777aa32e0', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2e152ee4-e1bb-4035-807f-c45663d4b8a0', 3, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('dd3ec631-439f-4534-9203-25cd53a79481', 3, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f8d8d28e-ac4d-4173-807f-f128f9187044', 3, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a599af38-0602-464e-a836-2ab788b0b46a', 0, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('45a07dac-3c2f-4fd8-b617-60f396e21164', 3, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c6df29b7-583f-4827-99c7-60fa9a16c28e', 3, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('690a4f10-b60e-4965-bccf-c36d5143082b', 3, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('573c82a4-4829-479e-81d9-6b5afd979a26', 0, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('dcbd3775-3df1-4b43-9822-22337225c72e', 0, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8ebe9122-7846-4adb-a90b-e99312f31fa3', 0, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('34f0b79b-4375-4782-a5dc-a78f67892c74', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e3705e88-e522-45a5-b357-a54aaf9dca87', 0, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('020d310b-f440-4a34-a25d-8fa3c678ba92', 0, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1f5f4063-f7e7-429b-b840-b781f4ff43df', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('412f771d-82a8-43c2-bb12-d3212ad3a268', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c298dcb8-8837-433b-9085-131899cba27f', 0, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('53a765bc-b1ed-4e50-bcad-4ef24e36f5fe', 0, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f1ee7e39-c476-4d3b-81f7-c00047355415', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('cd58dbd6-dd58-4cbe-9cea-2a82eeb60e1c', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d9b64fb1-0cdc-4a40-863c-390ab917928c', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6782c650-946a-439a-a369-592eb2ff2a90', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5481a64b-624f-4aa3-8070-bdb5297c19db', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b113a944-06c6-4bc8-a413-527a89013831', 0, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c8b4d57e-7d4f-45a8-b792-b186ce2737a3', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('63975c76-04ac-4467-8f37-a914ed32986f', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('284ec37e-765b-4c75-9606-1196883fd603', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('73560506-8aa9-451d-a74e-d1a5f8b42d00', 2, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a0dc0d48-918d-4d31-ba47-156f674a9510', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('76b2e7af-995e-4a23-9dc6-f983435a8faa', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f6406b4a-438a-4a8b-86f0-5a0ec4cddf1e', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c94999ab-352f-4f30-bc4c-4ac330ea1399', 2, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8157419f-c729-44a4-ad93-17e45b36957c', 0, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a1402318-712d-4c6a-bbae-183629d66043', 0, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a82205e3-3b93-4886-be90-5092b17f77a6', 0, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('110b1dd4-967c-4105-abfb-e69778671a6e', 3, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('cb804c41-7ba5-477e-b934-4f7366a8e2cf', 0, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('474d8a6c-d0a9-483e-8b4d-829aafbcbd39', 0, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d0c2032b-f812-4ebf-961d-f16126617f8f', 0, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a08670f4-4593-41dd-aa64-8bd5343f7267', 0, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('67088713-83ed-49e3-a9d0-e8289bcdee92', 0, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e771079c-d4e7-458d-9e58-50f9234081ff', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7ad46052-ff6d-4cb9-ac3e-054c942a91df', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a51b3280-1ac8-42e4-b70d-2d066c37851b', 0, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ebfc8661-4d66-4c79-9700-1019966409b7', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c4f81e1f-f07f-4ff5-826e-79fdf7421282', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ed9201d5-3323-4357-bc40-3f593a992c8c', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1197090e-d72b-40e5-b9b1-9df42d57ea90', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('58cff854-7f59-4ac0-b768-b0ff91689685', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('799c9b3d-6e0e-490e-ab44-517d5bee8679', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b8307374-b1a4-48ca-9d4f-57433d208d27', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4809a8a6-049b-44b3-bfac-dea9b1ed3fef', 3, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8ad11c69-4be1-4f56-ab59-c63e4441d496', 3, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c8371b03-f47b-4dc3-a03c-0e72b013b7ab', 3, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c7dd072d-7b83-458a-9632-33beb7808042', 0, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d039947d-6c77-4540-b251-972185275642', 0, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('86ea8462-c540-4a5a-a24e-9edfb839dc22', 3, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('43da83d5-9ae6-473e-92f0-b85bef30ece8', 3, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3e30eef8-4ad6-42d9-b5dc-789e76a5a61d', 2, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1cff2de0-c872-4803-a3fd-b65d536d7fec', 0, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('57f3da4a-38ef-47c1-adf1-457f213695e1', 0, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('75c1dafd-d315-4232-a055-01cf1d51723e', 3, NULL, 61);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e4d9be14-dec4-4bb7-b825-f883af4e6a71', 2, NULL, 15);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('167e7c56-d8cc-4105-8207-f74c1a5542be', 0, NULL, 11);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8d1e717e-8811-4047-8a1c-ee39fc657a31', 0, NULL, 12);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fc074502-ebca-45d6-a85f-35b7d4fbee84', 3, NULL, 13);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('66b89bc2-52cd-4e19-85bd-86c2e4b02fc0', 3, NULL, 14);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('cc082637-8326-4ea3-9be0-f6f53b9b4907', 3, NULL, 16);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c27b0bdb-bbfe-4172-b2f2-e0c36b0406cf', 3, NULL, 17);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7c65d681-203d-48b8-a512-ec038e6df5b2', 0, NULL, 18);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3c6f027e-964e-40a1-a268-4f5100a3abdd', 0, NULL, 19);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fed42f68-2030-405d-b018-401259928a6a', 4, NULL, 132);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9b534430-069a-470b-8296-344104567969', 4, NULL, 133);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8271db97-0b5f-47f3-afd5-3cb2a1053f45', 3, NULL, 134);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c6a82de2-d34c-4052-8829-525d4f37df50', 3, NULL, 135);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b67ff83e-7e72-4baa-8ad9-c79563f6d049', 2, NULL, 136);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('358e534e-5b29-4849-ad20-42bcaa95f966', 2, NULL, 128);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('49e88573-fa49-48d5-b5af-622c415871af', 4, NULL, 130);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ed4a89cc-25d4-44f6-8ccd-9e12b05d5036', 4, NULL, 140);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a2686d61-0d05-4350-a797-0b761e4e4090', 3, NULL, 141);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('39243d2d-40ae-42d9-b3f1-58875e096521', 2, NULL, 137);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('661e5adc-87f3-4aa4-bdc5-0b7f2b4ab5ee', 2, NULL, 138);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('56e8a2d2-0ef6-4c32-a4c9-18330c9f1838', 4, NULL, 139);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e738843f-fa30-4c37-99b2-d7525b85d22c', 4, NULL, 142);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a2e586ef-ea7f-412d-a928-8a2581f6555c', 2, NULL, 145);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3b7511b2-5db3-4a91-b8e1-008a9b8abce3', 3, NULL, 146);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5e55c9e5-56ec-4050-93a1-d22585ec5a5b', 2, NULL, 143);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('06c96789-1255-4109-b97d-90eaa05b4f47', 1, NULL, 144);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0795ef2e-32e8-4332-979b-71eb50600205', 1, NULL, 149);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f887ace4-af88-43df-a632-506d287143ce', 3, NULL, 152);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0dc999f7-bd4a-4beb-a1e0-9aa10be8be81', 1, NULL, 154);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ea81e9de-289f-4706-a632-9586e740f64a', 1, NULL, 155);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c7042085-ac81-4d82-a257-4bd763dfea30', 3, NULL, 156);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('54dec2c9-6961-4dfd-a21e-eeb706a315ea', 3, NULL, 153);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e5898207-a416-43ca-ba38-1b5da1062045', 3, NULL, 147);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('03eedd98-1931-4321-8305-57eb51c0bab3', 3, NULL, 148);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9ea03a1a-d85c-4d70-b29b-6a1aed981f2a', 2, NULL, 129);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d29a1922-185f-41d8-8c67-9f3b08b9353c', 4, NULL, 150);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('44ab3da7-52c4-4433-b97e-2e367b30b4eb', 4, NULL, 151);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2a9cb2ab-8d70-4196-87df-bd3f230297d7', 3, NULL, 87);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('de47e3fc-3577-4e59-9c15-3a7d70dc83ce', 0, NULL, 90);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e3812d4d-a078-44df-9d92-c7fd053fd8fb', 2, NULL, 83);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9d9cdd3a-7328-4e06-b179-6f0798ad662c', 0, NULL, 120);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bd318040-070e-4c21-8772-96fc1aa80b72', 3, NULL, 122);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('51478557-622b-4eb5-aebf-3e4140185f38', 0, NULL, 123);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('755537e8-2785-43ce-915d-9056d3f51f29', 2, NULL, 125);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('276d4b46-eab1-40c5-97c7-77084604383c', 0, NULL, 127);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c0af98d1-4311-4619-8415-9466d3cb5843', 2, NULL, 126);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ee993121-ed01-4322-bd7f-03390d8a1a91', 0, NULL, 84);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c304a05d-3341-483e-8d1f-4ad47d4ba806', 0, NULL, 85);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('badf2bcb-f46e-406e-8797-4573d37bc236', 0, NULL, 86);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('be0b0ba8-b8e5-4fdb-81be-8d4d9a2a7e97', 0, NULL, 88);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('dd972526-d0b1-44c6-a375-b7931f117ff0', 0, NULL, 89);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('456c4faa-db61-4fc2-a41f-a51874711a6b', 0, NULL, 98);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1a4a9b02-0dae-4035-9d86-35263c685fbd', 0, NULL, 121);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9294c26a-73c1-41b1-bca6-d15842577bdc', 0, NULL, 124);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b332e102-4d3a-4927-96fb-3daf3b98756a', 2, NULL, 62);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('960a004d-bcd5-485b-bf1b-cc2b12e33fce', 2, NULL, 64);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c84de425-553e-4ca3-9e3c-96179186c4af', 2, NULL, 63);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0cca7bc6-eee7-41c2-8a84-decf0c21767e', 3, NULL, 65);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('59ed6d02-bcd7-40fc-99d5-b094746d30f9', 2, NULL, 66);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('caeefe19-be22-4f2c-848a-c93bbe53ec8a', 2, NULL, 67);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a8a8467d-859a-4d22-9c40-9e86cbf812e1', 2, NULL, 69);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('06a27e03-0b36-4169-be45-c33e6048ad3c', 3, NULL, 79);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a44000fe-9aaf-4333-98c8-d3d671e218b1', 1, NULL, 80);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a06faa21-cffd-453e-9b2f-083f8d10d604', 2, NULL, 81);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bdcf39eb-1329-4061-a78d-90eb465812f5', 2, NULL, 82);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bf210bbb-5017-4040-a2e7-3de71f75c38f', 2, NULL, 73);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('96eb17c1-4888-4060-8de5-f3e36725d29f', 3, NULL, 72);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0bec20c3-9730-4a1e-b12b-44796635471d', 2, NULL, 74);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('68fcd84d-87e4-41f8-8ec7-e4953a1e434a', 1, NULL, 75);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6924c1a0-701d-410c-a5b0-0c3f5d095cee', 1, NULL, 99);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('873cce2f-83b3-4a8e-b4ba-86f3c0151e1f', 2, NULL, 100);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8503c901-ae46-4a6c-82ab-61ac3c30ba31', 2, NULL, 101);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6d876240-f48a-4fd3-9ecb-d97a1e1cc260', 2, NULL, 103);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8edab1cd-f9c2-416c-8e08-c5e998a0c2c5', 2, NULL, 104);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('56b5e088-1fed-444e-b6ea-a086ccb4d401', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8b2e84d2-f043-4c5e-b1cf-8d7b2f0c5d0d', 2, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9470effc-956e-4228-b5b2-52f28b13c213', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ece70ea1-3e87-48eb-ba42-a9378e286ce0', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('72e90409-b18e-4887-aea6-951c7a7a40da', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('efbaeb9c-5899-4f43-ab9b-ce181da58958', 0, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c9312f7c-d658-4caa-8724-717ccb3db758', 2, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3e968ee0-c892-4367-b869-25703ba731d8', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8a83e5e6-5f6f-4656-938b-a8c060212f59', 3, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('01f7d598-7abd-4920-8f34-a107e60d3cff', 2, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fd7ab158-a6b1-422f-8ab1-1bff551eb85c', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1fbf2408-2330-44c2-8554-ef1f4a0a2ba4', 2, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6e450ae4-f60f-4b08-82ec-a88c51d53586', 2, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7196a160-779a-4620-80e8-fa385660dfcb', 2, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b83e73e4-33fe-43b7-b3d7-f8147c1da70e', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ef521f09-8af8-45c6-9890-213966e426d4', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('81b66101-e642-41fb-8ee9-f367993431fd', 2, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d5ed2957-f6af-4f67-a8f7-0ad16d56e8bf', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('164114f5-88e1-44a4-8650-922dbbb2ff14', 3, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('40204979-a4b4-42c1-8a47-c9464c5dfe8e', 2, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6feca369-19fd-4cfe-b5e2-d41e0790d7e9', 2, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2511d67f-3f92-4c78-8293-536148e1f4ae', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('64ca856f-da67-461d-aa1a-d2d67c2d55ba', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8139b2f6-ecd4-4585-bcf8-b51f06eab7c9', 2, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('67bfa588-82b7-4c85-8f70-96cb5072f358', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2a448b88-bb5b-4dcd-9d47-3587a445897a', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('235d0f9b-1541-4ea1-a705-9194ec5452e9', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('481c4fc5-0b06-497c-91bc-f085c053bce7', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('48cdff18-9697-4b08-b615-ead7cc082792', 3, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('875e4e57-0acc-40c9-9fa4-86e179919e56', 2, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('93cbde21-2492-4fbc-967f-9a484dd700a6', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('70c7112a-1f71-4269-b039-fac55c2315f8', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c071a30b-ef29-437b-ae08-80e681380ac2', 2, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ab65ce1b-f69d-4636-a1ad-ed5a3764540d', 2, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('dadb9515-f5e7-414f-a365-ec6cffd2a5ca', 2, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a233b7f9-89b1-4fd9-ab51-c41ceed904bc', 2, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5c406940-f8e0-4207-b2e3-b12db0163613', 2, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c1cf3097-3bea-4aa0-8a9f-d01d18435477', 2, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7a401a99-ce0b-41e5-b47b-54295703c732', 2, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a36a5c6a-d7e5-4049-893f-5d856c7ba312', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('619e67dc-e857-407b-82f6-6d47b18c3e51', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2a321bdc-caed-406b-b74f-824e78022641', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('58eeee7d-1049-415b-bb5a-585dca3429e7', 0, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ff3c5b3b-2154-4b5c-8721-eb693989f820', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9385ea22-350b-41e2-bb16-0926b9a42f6e', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d4e332ff-16ac-49cb-aa32-3c65fba1221a', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3abc5081-3dc9-4a4f-bc63-426e7bc77a98', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6292c8e8-8057-4fbb-b656-16b3fcd40267', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b2439b01-1b4f-425a-b0c8-85a01be3a22e', 3, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('499a6526-38b7-43d8-994d-0a336e342a32', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('26d56475-fddc-4d6c-9f57-c2e53ffe7747', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a29aba8f-4339-4caf-ae32-68415cd11ee5', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('558b2507-289a-4e8c-a9ea-02b2badcad99', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('803da34d-0535-45b6-8887-fb38b24d5e3e', 3, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('289c1ed1-1e4b-4e61-926d-2b19d21ca1f1', 0, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('eab307ce-11cf-464e-8cf6-a23e7fa1d197', 0, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('49cfe36d-2a38-4fb1-9322-f5d9f8c84f7a', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('651081e0-88c5-47c1-b0b2-9b9defb0cedf', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6b0c5467-c704-4cca-9ee6-7e09e677f059', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b1cb8ffe-6093-434e-93b0-22b30a407baa', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('31a797a8-1657-473e-9ce7-02148f6e9fde', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4244e43e-6807-49b7-891c-37041a7953ab', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bc0c1321-ffad-4aba-afd2-861f07d62d1a', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f0e7800f-1217-4641-81a7-a7fc5754e157', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e5c32e6f-0bd5-4acd-b300-7422b4152b87', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('67ff21c1-35b4-460e-b43d-542349726e54', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4309c8a7-9640-478f-ba14-46887d4840b9', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('43ea3d09-f8b7-4f39-8751-c73035fdb314', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9d7a3f32-5105-4c20-8bc6-bcbbc95dd919', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c71c4ad3-9445-437b-b2c9-614c3e88877c', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bfc934d6-4103-42e7-9d93-770baa638471', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('66575109-e465-4868-9010-bc652a7b2e97', 2, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c16c5196-0239-418b-8d5d-4cf79e8cfc18', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8deb5219-b2b4-466c-89c8-789d2323ea09', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('82823c75-34ff-4d32-9cd6-532792b38347', 2, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('14b17415-9bde-46ae-ac53-20ba21e8af19', 2, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('633ef1c6-4725-4b2b-bcbc-291d1565d2a3', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ef851ad2-483b-4a7c-b217-6e9b5e1363e4', 0, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('cf5805e6-b2ed-4c8e-81f2-4837705d1515', 3, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2b472b8c-97c7-48a5-b72e-0e4127a6c71b', 3, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a69e2686-f11e-4ebd-9169-695509f28d7d', 3, NULL, 4);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('593e5ea9-32eb-418f-95d2-002c97bfaf2e', 3, NULL, 8);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b04b3a04-b0fb-4251-aeb6-72e91faf6baa', 3, NULL, 2);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('62da3fc3-59da-4653-ba19-726d6cd6d19f', 0, NULL, 3);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('96b87f86-c3c9-417f-8540-d202a0568ded', 3, NULL, 5);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('69d8df7d-3c40-4091-860b-01db835afc1f', 3, NULL, 6);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('35e4b0fe-2b3f-49d8-be08-3ace3ec047ff', 3, NULL, 10);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('08b35c8d-2e70-4eea-9dd1-e2c339ff692b', 3, NULL, 7);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0421235c-b8cb-4c7f-9993-ce4c9f59915a', 0, NULL, 9);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('528f090c-711f-4421-88fa-aa90c1329b05', 0, NULL, 1);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a57b2da7-6ba0-4f3b-b8ff-239e0edcc132', 3, NULL, 132);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('ad72c43e-26b7-49ff-85d4-54623bfedec4', 3, NULL, 133);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0fad5886-345c-491b-9598-f77ff92513d9', 3, NULL, 134);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2f2d2279-46bb-429a-b2d0-a8407eeaabfd', 3, NULL, 135);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('9a55d4d6-5e79-4448-ba8c-53eeb1841940', 2, NULL, 128);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('19207fd6-e578-45f0-8e23-9e269ccc1990', 3, NULL, 136);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f7dbb111-1daa-444d-8e54-29a7cff4d206', 1, NULL, 129);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('82274685-8d4e-42de-8312-d240cb6d98fe', 3, NULL, 130);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('052b82ca-ecfe-4060-b322-8b59223e912a', 4, NULL, 140);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a08b15aa-2b14-41f5-bfc8-fa6ef332ecfc', 2, NULL, 141);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5c23b39f-218b-4814-afcb-38e4696d88ea', 2, NULL, 137);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('db6858ac-cd89-4636-9db8-c9c3a2443611', 2, NULL, 138);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d3c379be-96e9-4687-859e-3054028352a4', 4, NULL, 139);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('258f8366-f858-4a81-8b35-b10cc066c998', 3, NULL, 142);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2ae067d0-5793-4659-a70b-440838cb675e', 2, NULL, 145);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('95399be5-0dab-4d1d-b451-2018e15a5fbd', 3, NULL, 146);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('bf74ddd3-239d-4413-9f90-c613c41e5f22', 2, NULL, 143);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('53bae90c-e01a-413c-834e-bf0858d09bdd', 1, NULL, 144);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('f787e574-60a3-4f0b-b913-6897ce088848', 3, NULL, 149);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('10962a6c-4ac4-4da7-8ee7-88a0d5ec66b4', 3, NULL, 152);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6e506670-7335-4fc0-a3ce-173da58c7b93', 1, NULL, 154);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1db48459-515a-4105-a7c9-03d8b0631329', 1, NULL, 155);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c4b5b48c-c0e4-46b0-99ad-26df2aedd750', 2, NULL, 156);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('229423d4-aeb5-4643-bba1-14cd35adae41', 1, NULL, 153);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e04b1505-cf14-404b-9b35-04c612e38f16', 4, NULL, 147);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d41e152d-e236-4648-a3f7-cf1ff46363ea', 2, NULL, 148);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('4116713a-0bb5-4463-ae92-096e14693f2f', 3, NULL, 150);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e4690394-b02c-4cae-b4a7-7f8173b84c3e', 3, NULL, 151);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a10c5b22-2587-4bfe-bf4a-1891851a9a5b', 5, NULL, 87);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2c543058-c097-4b07-becd-15fb0c9d6bd9', 4, NULL, 90);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b3039c19-f958-4fa1-9d34-223f50eed02a', 3, NULL, 83);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8f45b169-19c9-47ff-89b7-d3ae03e4ace8', 3, NULL, 120);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('095c3328-d52b-4613-ad16-b39aa2dc5d4d', 4, NULL, 122);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b707a8c2-cb15-4c12-b617-b6e88554a1aa', 2, NULL, 123);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('1aa08b4c-010a-42dc-aec5-832fe18608d8', 4, NULL, 125);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('baead23d-012a-4380-8f80-2410909da70f', 3, NULL, 127);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('65be7fbd-621e-4b75-ae20-4dfbf80836d9', 2, NULL, 126);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('e6cfb62e-8713-4f89-9cb3-993249da7b52', 3, NULL, 84);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('946d400f-40ee-4a56-af59-60127054af6a', 0, NULL, 85);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b75bd8c8-bd84-4d56-8a7b-f05ea6fd6151', 0, NULL, 86);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('67de4479-bcbc-476e-969f-2f598ae4499e', 0, NULL, 88);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7545c872-8cef-4c3f-adec-9ab6fe7ce333', 0, NULL, 89);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('c5020344-b1e4-4d16-8f56-99619aaa70c5', 0, NULL, 98);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6ee37168-c8db-4035-bb27-4ccde93aed2f', 0, NULL, 121);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d3c23429-675f-4bc4-a8c2-020722bbb30c', 0, NULL, 124);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('6f3cc142-23c8-409a-af99-9bb201fb2f66', 3, NULL, 62);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('a0f1682a-24be-4d0b-bb83-5c31cc25ee68', 2, NULL, 63);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('b61cd6a7-51b5-43d1-93f2-a23a7c15ba0f', 1, NULL, 65);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('07272b45-b547-4fa8-98c6-27fbd8c02086', 2, NULL, 64);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('13c7be40-d1ac-4594-8758-10fc71c2de8f', 2, NULL, 68);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('70ea7727-5dce-4095-9163-94b6cef5d0d9', 1, NULL, 66);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d7a0e63d-3129-482e-9e7d-53c5a1d735a6', 2, NULL, 71);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('10f6e9a4-cee2-4336-a43c-880bfd22fa3c', 2, NULL, 69);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('2ada2e29-856d-48dd-8ceb-df55e1f73b6e', 1, NULL, 67);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8642093a-daf0-4c6c-af23-2522f9fd7071', 2, NULL, 70);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0c053f36-3389-4cfc-b1bd-cf0d23138ba1', 4, NULL, 79);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('d41dc8a8-0d55-4808-9e78-7a5d375c864f', 1, NULL, 80);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3e290b21-8f89-4c47-8d4f-115e9dc41620', 3, NULL, 82);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('3b0b7185-4676-416c-8919-84752b8b8b9b', 3, NULL, 81);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5bf1ece1-48f5-4d31-861b-0c1ccadfce18', 1, NULL, 72);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('91fc43eb-4543-40da-9da3-f436935158f2', 2, NULL, 73);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('fc9995ac-fe50-473a-9b78-1497eaa864bf', 3, NULL, 75);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0eef31ac-812f-427a-ad66-ff4badacb862', 2, NULL, 74);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('53f62a5c-1cb7-4bf8-adb5-c18c73365b67', 4, NULL, 77);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('5a9b20c8-9724-461d-9005-3139384a8854', 2, NULL, 99);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('8b259b6f-bc08-41c0-9827-f944697e6e92', 2, NULL, 100);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('83259776-29bf-4933-99d0-775311563fad', 1, NULL, 103);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('7219b64d-7465-4c98-b91c-8df1f7e9194e', 2, NULL, 101);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('0fab8514-08fc-4f92-ae21-58c25e67515a', 2, NULL, 105);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('91383397-5f22-4249-a216-90df84c5f301', 2, NULL, 104);
INSERT INTO public.note_question (id_note, note, reponse, question_id_question) VALUES ('350685b4-2fcc-490a-aeae-55572e08755f', 2, NULL, 106);


--
-- TOC entry 3765 (class 0 OID 188129)
-- Dependencies: 234
-- Data for Name: observation; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.sous_categories (id, libelle, categories_id) VALUES (1, 'Recherche de qualification professionnelle', 1);
INSERT INTO public.sous_categories (id, libelle, categories_id) VALUES (2, 'Qualifié à la recherche d’un emploi ', 1);
INSERT INTO public.sous_categories (id, libelle, categories_id) VALUES (3, 'Primo demandeur auto emploi', 2);
INSERT INTO public.sous_categories (id, libelle, categories_id) VALUES (4, 'Déflaté ou licencié', 2);
INSERT INTO public.sous_categories (id, libelle, categories_id) VALUES (5, 'Reconversion professionnelle', 2);
INSERT INTO public.sous_categories (id, libelle, categories_id) VALUES (6, 'Informelle/Formelle', 3);
INSERT INTO public.sous_categories (id, libelle, categories_id) VALUES (7, 'Résident/Non résident
', 3);
INSERT INTO public.sous_categories (id, libelle, categories_id) VALUES (8, 'Migrant en préparation de son retour', 4);
INSERT INTO public.sous_categories (id, libelle, categories_id) VALUES (9, 'Migrant de retour', 4);
INSERT INTO public.sous_categories (id, libelle, categories_id) VALUES (10, 'Migrant retourné', 4);

--
-- TOC entry 3767 (class 0 OID 188135)
-- Dependencies: 236
-- Data for Name: parcours; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.parcours (id, libelle, sous_categories_id) VALUES (1, 'parcours1', 1);
INSERT INTO public.parcours (id, libelle, sous_categories_id) VALUES (2, 'parcours4', 2);
INSERT INTO public.parcours (id, libelle, sous_categories_id) VALUES (3, 'parcours3', 3);
INSERT INTO public.parcours (id, libelle, sous_categories_id) VALUES (4, 'parcours3', 4);
INSERT INTO public.parcours (id, libelle, sous_categories_id) VALUES (5, 'parcours3', 5);
INSERT INTO public.parcours (id, libelle, sous_categories_id) VALUES (6, 'parcours1_2_3_4', 8);
INSERT INTO public.parcours (id, libelle, sous_categories_id) VALUES (7, 'parcours1_2_3_4', 9);
INSERT INTO public.parcours (id, libelle, sous_categories_id) VALUES (8, 'parcours1_2_3_4', 10);
INSERT INTO public.parcours (id, libelle, sous_categories_id) VALUES (9, 'parcours3', 6);
INSERT INTO public.parcours (id, libelle, sous_categories_id) VALUES (10, 'parcours2_3', 5);
INSERT INTO public.parcours (id, libelle, sous_categories_id) VALUES (11, 'parcours2_3', 3);




--
-- TOC entry 3798 (class 0 OID 188655)
-- Dependencies: 267
-- Data for Name: pie_list_soutiens; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3771 (class 0 OID 188149)
-- Dependencies: 240
-- Data for Name: pie_note_questions; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3772 (class 0 OID 188152)
-- Dependencies: 241
-- Data for Name: pie_observations; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3773 (class 0 OID 188155)
-- Dependencies: 242
-- Data for Name: pie_rencontres; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3774 (class 0 OID 188158)
-- Dependencies: 243
-- Data for Name: profil; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3799 (class 0 OID 188658)
-- Dependencies: 268
-- Data for Name: profil_membre; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.profil_membre (id, code, libelle) VALUES ('c7827bff-f9bc-4391-96d9-878a56638d3e', 'homme', 'Homme');
INSERT INTO public.profil_membre (id, code, libelle) VALUES ('b3e9fb14-b4ef-4980-8973-1dc0a6a4de17', 'femme', 'Femme');
INSERT INTO public.profil_membre (id, code, libelle) VALUES ('29c3a845-856d-4f6b-8e24-6b2ff1d845bf', 'handicape', 'Handicapé');




--
-- TOC entry 3775 (class 0 OID 188161)
-- Dependencies: 244
-- Data for Name: projet; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('75f724c8-462f-4545-a285-afda2d72ec93', NULL, NULL, 'A', NULL, NULL, NULL, NULL, NULL, NULL);
--INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('b47060f5-c045-4cec-9b37-62a2b5581cae', NULL, NULL, 'test 2', NULL, NULL, '81bc9192-01dd-4726-bc57-dd11b126d9cf', NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('6b8aae4a-8c68-43bb-abca-4175fdd62552', NULL, NULL, 'idea 2', NULL, NULL, 'eeff1f3c-c7b6-45bb-9894-72048f70f600', NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('460251e6-efb6-4014-bd41-0ec40a64dc5c', NULL, NULL, 'test 3', NULL, NULL, '8d590540-fa9f-4793-ba1f-e3b97139dd43', NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('a23a4b4a-5384-4fe4-9eff-778e1cfce949', NULL, NULL, 'awesome idea', NULL, NULL, '5d0e71b9-a638-4f1f-a445-bf28721ce4cd', NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('2d32f628-1df4-4dfc-8663-ff0c917490e3', NULL, NULL, 'one', NULL, NULL, 'cdcbe722-c7b6-455f-8942-e7f4dd693a7b', NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('1727a742-bcca-4967-8244-fabb7ef487b7', NULL, NULL, 'BOULANGERIE', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('994bd644-1ee0-4d82-8e8c-71c5a8bc8977', NULL, NULL, 'TEST PROJET', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('efdff626-f656-4dda-bf66-10ccbc7768f6', NULL, NULL, 'projet testing', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('3ccf355a-12d5-48b6-8bfa-80a1cfb5a140', NULL, NULL, 'TEST 2', NULL, NULL, 'b1678a91-9479-419c-a31b-cb3572bf27db', NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('a36ae0e2-d5c0-4c28-a8ac-d9e86271b3a2', NULL, NULL, 'useful idea', NULL, NULL, '05ed93d7-c40c-4068-bcea-50f04b505ae0', NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('3836354b-40f9-40c5-995d-949952e9096f', NULL, NULL, 'idea 2', NULL, NULL, '8047b83d-acba-40a0-a20b-7d7a26a8c6ba', NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('b8b83e48-eae1-41ec-a6a6-bf4520dcbdb0', NULL, NULL, 'test 2', NULL, NULL, 'dab51a86-578c-4e63-841f-ddb0eeba1d17', NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('5b1fa3c1-f0a4-4941-8274-dacffd9b1ef5', NULL, NULL, 'Awesome project', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('c27dc7ca-d8b0-49d5-b112-572b70a6ea30', NULL, NULL, 'idea 3', NULL, NULL, 'b145aacd-cf31-437d-9d87-93645fe90b12', NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('0af23de6-de76-4021-9441-f996ee3a9407', NULL, NULL, 'test 2', NULL, NULL, '52c1ba8a-82b5-4830-b63e-5a3f21a11aa7', NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('a6de745c-082d-463d-b4a9-007fcd7f98c4', NULL, NULL, 'useful idea', NULL, NULL, '1b0c651f-ca16-4ca3-96f2-b29353cbee33', NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('f9d372dd-0e3f-4e23-9eb0-3d5e6efad146', NULL, NULL, 'agriculture', NULL, NULL, 'e771f858-1c25-4f3b-bc6b-ca7e8822bf50', NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('c2be862c-4f83-4f73-a79f-24d99294007b', NULL, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('a8d8fbee-3a0a-4757-aef4-7d9c908829cf', NULL, NULL, 'boulangerie', NULL, NULL, '47a68426-09ec-49ca-a21a-3ad1e920fc27', NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('21fdab96-4dbb-49ee-8527-bd1995a77fe5', NULL, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('9c8bb829-5189-4015-97c6-b2e615f2b92a', NULL, NULL, 'idea 2', NULL, NULL, '08c1cbfd-6509-4dd5-b8d6-74ff35f16cd4', NULL, NULL, NULL);
INSERT INTO public.projet (id_projet, description, information_supplementaire, intitule_projet, motif_retour, objet, model_economique_id, secteur_activite_id, statut_id, type_projet_id) VALUES ('9bd316cb-6c8d-4cd5-a8c3-13a5c093f81d', NULL, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL);



--
-- TOC entry 3777 (class 0 OID 188171)
-- Dependencies: 246
-- Data for Name: rapport_analyse; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3778 (class 0 OID 188176)
-- Dependencies: 247
-- Data for Name: rapport_analyse1collectif; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3779 (class 0 OID 188181)
-- Dependencies: 248
-- Data for Name: rapport_analyse2; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.rapport_analyse2 (id, faiblesse, fonctionnement, gestion, pratiques_manageriales, ressources_financieres) VALUES ('c9e648e9-5ffc-47d6-be9f-537f17c37143', 'Besoin de sous', '<p class="ql-align-justify"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><br></p>', '<p class="ql-align-justify"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><br></p>', '<p class="ql-align-justify"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><br></p>', '<p class="ql-align-justify"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><br></p>');
INSERT INTO public.rapport_analyse2 (id, faiblesse, fonctionnement, gestion, pratiques_manageriales, ressources_financieres) VALUES ('93e35ef2-5709-4059-b3fb-4865e88e406b', 'Besoin de financement', '<p class="ql-align-justify"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><br></p>', '<p class="ql-align-justify"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><br></p>', '<p class="ql-align-justify"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><br></p>', '<p class="ql-align-justify"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p>	</p>');
INSERT INTO public.rapport_analyse2 (id, faiblesse, fonctionnement, gestion, pratiques_manageriales, ressources_financieres) VALUES ('691d3221-0e9a-499d-810d-cf6ecb7454ed', 'Besoin de matériel', '<p class="ql-align-justify"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><br></p>', '<p class="ql-align-justify"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><br></p>', '<p class="ql-align-justify"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><br></p>', '<p class="ql-align-justify"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><br></p>');
INSERT INTO public.rapport_analyse2 (id, faiblesse, fonctionnement, gestion, pratiques_manageriales, ressources_financieres) VALUES ('5c13fdfe-c280-47dc-afaa-f52173477e39', '
', '', '', '', '');
INSERT INTO public.rapport_analyse2 (id, faiblesse, fonctionnement, gestion, pratiques_manageriales, ressources_financieres) VALUES ('b093b226-e455-45ca-b5e9-e52bbe633a97', '
', '', '', '', '');
INSERT INTO public.rapport_analyse2 (id, faiblesse, fonctionnement, gestion, pratiques_manageriales, ressources_financieres) VALUES ('7b2a4be6-182c-4f7d-ac14-53137de2e33d', 'Besoin de financement', '<p class="ql-align-justify"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><br></p>', '<p class="ql-align-justify"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><br></p>', '<p class="ql-align-justify"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><br></p>', '<p class="ql-align-justify"><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p><br></p>');


--
-- TOC entry 3780 (class 0 OID 188186)
-- Dependencies: 249
-- Data for Name: rapport_analyse2collectif; Type: TABLE DATA; Schema: public; Owner: -
--





--
-- TOC entry 3783 (class 0 OID 188197)
-- Dependencies: 252
-- Data for Name: rencontre; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.rencontre (id_rencontre, commentaire, date, heure, lieu) VALUES ('e903819e-06c7-4a81-845f-32f4a8dd208b', NULL, '2023-03-28', '04:00:00', NULL);
INSERT INTO public.rencontre (id_rencontre, commentaire, date, heure, lieu) VALUES ('80e3ec4e-aaad-469a-9d80-1c8a2224e8ee', NULL, '2023-03-30', '03:00:00', NULL);
INSERT INTO public.rencontre (id_rencontre, commentaire, date, heure, lieu) VALUES ('2ba2c08b-ea48-4a1f-b1b5-8887079b637b', NULL, '2023-03-30', '02:00:00', NULL);
INSERT INTO public.rencontre (id_rencontre, commentaire, date, heure, lieu) VALUES ('e80900d7-77fd-401a-9116-4c3aa9bbcdac', NULL, '2023-03-30', '00:00:00', NULL);
INSERT INTO public.rencontre (id_rencontre, commentaire, date, heure, lieu) VALUES ('00779167-0b36-4cd0-a5e1-172891295d1d', NULL, '2023-03-28', '02:00:00', NULL);
INSERT INTO public.rencontre (id_rencontre, commentaire, date, heure, lieu) VALUES ('f0e376d0-a121-4b26-aaea-24d1b4852a2d', NULL, '2023-03-30', '01:00:00', NULL);
INSERT INTO public.rencontre (id_rencontre, commentaire, date, heure, lieu) VALUES ('0d4a4fec-d6f1-4754-9485-2e9eab5e25c4', NULL, '2023-04-21', '01:00:00', NULL);
INSERT INTO public.rencontre (id_rencontre, commentaire, date, heure, lieu) VALUES ('31a1e37c-df44-4113-a1d9-126de804b021', NULL, '2023-04-21', '03:00:00', NULL);
INSERT INTO public.rencontre (id_rencontre, commentaire, date, heure, lieu) VALUES ('3830ef03-32bc-48fc-898e-f96b5a0d10a6', NULL, '2023-04-23', '02:00:00', NULL);
INSERT INTO public.rencontre (id_rencontre, commentaire, date, heure, lieu) VALUES ('fd3e8581-f50e-4311-b145-0219b170c789', NULL, '2023-05-14', '01:00:00', NULL);
INSERT INTO public.rencontre (id_rencontre, commentaire, date, heure, lieu) VALUES ('ddd44ee8-46f9-4d49-979a-866439155063', NULL, '2023-05-11', '17:00:09', NULL);


--
-- TOC entry 3784 (class 0 OID 188202)
-- Dependencies: 253
-- Data for Name: secteur_activite; Type: TABLE DATA; Schema: public; Owner: -
--



--




--
-- TOC entry 3787 (class 0 OID 188213)
-- Dependencies: 256
-- Data for Name: soutien; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.soutien (id, libelle, code) VALUES (1, 'Formation', 'FORMATION
');
INSERT INTO public.soutien (id, libelle, code) VALUES (2, 'Financement', 'FINANCEMENT');
INSERT INTO public.soutien (id, libelle, code) VALUES (3, 'Formalisation', 'FORMALISATION');
INSERT INTO public.soutien (id, libelle, code) VALUES (4, 'Foncier', 'FONCIER');
INSERT INTO public.soutien (id, libelle, code) VALUES (5, 'Fiscalité ', 'FISCALITE');
INSERT INTO public.soutien (id, libelle, code) VALUES (6, 'Formation', 'FORMATION');


--
-- TOC entry 3788 (class 0 OID 188216)
-- Dependencies: 257
-- Data for Name: statut; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.statut (id, code, libelle) VALUES (2, 'À noter', 'Note et sélection des PIE par l AER');
INSERT INTO public.statut (id, code, libelle) VALUES (5, 'Rejet DET', 'Notes invalides par le DET');
INSERT INTO public.statut (id, code, libelle) VALUES (50, 'En cours RPIE', 'Visualise par le RPIE');
INSERT INTO public.statut (id, code, libelle) VALUES (6, 'Sélectionné', 'Validé par RPIE (sélectionné)');
INSERT INTO public.statut (id, code, libelle) VALUES (8, 'Rejet RPIE', 'Rejeté par RPIE');
INSERT INTO public.statut (id, code, libelle) VALUES (41, 'Contrat en cours', 'Lorsque l on écrit le contrat d accompagnement');
INSERT INTO public.statut (id, code, libelle) VALUES (43, 'Validation RPA', 'Validation du contrat d accompagnement par le R PA');
INSERT INTO public.statut (id, code, libelle) VALUES (45, 'Contrat rejeté RPA', 'Contrat d accompagnement rejeté par le RPA');
INSERT INTO public.statut (id, code, libelle) VALUES (46, 'Contrat en cours PIE', 'Contrat en cours de validation par le PIE concerné');
INSERT INTO public.statut (id, code, libelle) VALUES (48, 'Contrat rejeté PIE', 'Contrat rejeté par le PIE');
INSERT INTO public.statut (id, code, libelle) VALUES (49, 'Contrat validé ADEL', 'Contrat validé et signé par l administrateur d ADEL (etat final sous process 3)');
INSERT INTO public.statut (id, code, libelle) VALUES (51, 'Evaluation PIE', 'PIE à traiter');
INSERT INTO public.statut (id, code, libelle) VALUES (3, 'Noté ', 'Transmettre les notes noté par AER');
INSERT INTO public.statut (id, code, libelle) VALUES (4, 'Validé DET', 'Notes valides par le DET');
INSERT INTO public.statut (id, code, libelle) VALUES (7, 'Non sélectionné', 'Porteur non sélectionné');
INSERT INTO public.statut (id, code, libelle) VALUES (9, 'Idées projets renseignées', 'Renseignement des idées de projet du porteur');
INSERT INTO public.statut (id, code, libelle) VALUES (10, 'Projet choisi', 'Sélection de l''idée prioritaire');
INSERT INTO public.statut (id, code, libelle) VALUES (11, 'BMC à générer', 'Modèle économique à générer');
INSERT INTO public.statut (id, code, libelle) VALUES (13, 'BMC à corriger', 'Modèle économique à corriger');
INSERT INTO public.statut (id, code, libelle) VALUES (14, 'BMC en cours de rédaction', 'Modèle économique en cours de rédaction');
INSERT INTO public.statut (id, code, libelle) VALUES (16, 'Rencontre 1', 'Remplissage de calendrier de rencontre 1');
INSERT INTO public.statut (id, code, libelle) VALUES (20, 'Rapp d''analyse 1 a evaluer', 'Rapport d''analyse 1 à évalué');
INSERT INTO public.statut (id, code, libelle) VALUES (22, 'Rencontre 2', 'Remplissage de calendrier de rencontre 2');
INSERT INTO public.statut (id, code, libelle) VALUES (30, 'Sélectionné entretien n°2', ' les PIEs devant continuer pour l''entretien n°2');
INSERT INTO public.statut (id, code, libelle) VALUES (44, 'Contrat validé RPA', 'Contrat d accompagnement validé par le RPA');
INSERT INTO public.statut (id, code, libelle) VALUES (33, 'PIE accompagné', 'les PIEs devant continuer pour contrat d''accompagnement');
INSERT INTO public.statut (id, code, libelle) VALUES (34, 'PIE non accompagné', 'les PIEs devant pas continuer pour contrat d''accompagnement');
INSERT INTO public.statut (id, code, libelle) VALUES (35, 'Contrat à évaluer', 'Contrat d''accompagnement à évaluer');
INSERT INTO public.statut (id, code, libelle) VALUES (19, 'Rapp d''analyse 1 en cours', 'Analyse du rapport d analyse');
INSERT INTO public.statut (id, code, libelle) VALUES (21, 'Rapp d''analyse 1 validé', 'Validation du rapport d analyse 1');
INSERT INTO public.statut (id, code, libelle) VALUES (26, 'Rapp d''analyse 2 validé', 'Rapport d analyse 2 validé par le RPIE');
INSERT INTO public.statut (id, code, libelle) VALUES (27, 'Rapp d''analyse 2 à évaluer', 'Rapport d ''analyse 2 à évaluer par le RPIE');
INSERT INTO public.statut (id, code, libelle) VALUES (28, 'Rejet Rapp d''analyse 2', 'Rapport d analyse 2 rejeté par le RPIE');
INSERT INTO public.statut (id, code, libelle) VALUES (29, 'Rejet Rapp d''analyse 1', 'Rapport d''analyse 1 rejeté par le RPIE');
INSERT INTO public.statut (id, code, libelle) VALUES (25, 'Rapp d''analyse 2 en cours', 'Rensignement de la grille d’analyse 2');
INSERT INTO public.statut (id, code, libelle) VALUES (52, 'Non sélectionné entretien n°2', ' les PIEs devant pas continuer pour l''entretien n°2');
INSERT INTO public.statut (id, code, libelle) VALUES (17, 'Entretien 1 ', 'Dispatching grille d''analyse 1');
INSERT INTO public.statut (id, code, libelle) VALUES (18, 'Entretien 1 en cours', 'Rensignement de la grille d’analyse 1');
INSERT INTO public.statut (id, code, libelle) VALUES (23, 'Entretien 2', 'Dispatching grille d''analyse 2');
INSERT INTO public.statut (id, code, libelle) VALUES (24, 'Entretien 2 en cours', 'Rensignement de la grille d’analyse 2');
INSERT INTO public.statut (id, code, libelle) VALUES (32, 'Contact actualisé', 'Actualisé contact PIE');
INSERT INTO public.statut (id, code, libelle) VALUES (37, 'Contact actualisé 2', 'Actualisé contact PIE Entretien n°2');
INSERT INTO public.statut (id, code, libelle) VALUES (36, 'Calendrier rencontre', 'Fixer rendez-vous');
INSERT INTO public.statut (id, code, libelle) VALUES (1, 'Initialisé', 'PIEs nouvellement uploadés depuis BADEL et classés suivant les parcours');
INSERT INTO public.statut (id, code, libelle) VALUES (31, NULL, NULL);
INSERT INTO public.statut (id, code, libelle) VALUES (12, NULL, NULL);
INSERT INTO public.statut (id, code, libelle) VALUES (15, 'Modèle économique validé', NULL);
INSERT INTO public.statut (id, code, libelle) VALUES (53, 'Mode signature choisi', NULL);
INSERT INTO public.statut (id, code, libelle) VALUES (47, 'Contrat validé PIE', NULL);
INSERT INTO public.statut (id, code, libelle) VALUES (60, 'BADEL', 'PIE Recupérés depuis Badel ');



--
-- TOC entry 3790 (class 0 OID 188226)
-- Dependencies: 259
-- Data for Name: tranche_age; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.tranche_age (id, libelle, code, description) VALUES (1, 'moins de 25 ans', '15_25
', 'moins de 25 ans ');
INSERT INTO public.tranche_age (id, libelle, code, description) VALUES (2, '26-35 ans', '26_36', 'de 26 à 26');
INSERT INTO public.tranche_age (id, libelle, code, description) VALUES (3, '36-45 ans', '36_45', 'de 36 à 45 ans');
INSERT INTO public.tranche_age (id, libelle, code, description) VALUES (4, '46-55 ans', '46_55', 'de 46 à 55');
INSERT INTO public.tranche_age (id, libelle, code, description) VALUES (5, '+55 ans', '+55', 'plus de 55 ans ');


--
-- TOC entry 3791 (class 0 OID 188229)
-- Dependencies: 260
-- Data for Name: type_bloc; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3792 (class 0 OID 188232)
-- Dependencies: 261
-- Data for Name: type_document; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3793 (class 0 OID 188237)
-- Dependencies: 262
-- Data for Name: type_projet; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3800 (class 0 OID 188671)
-- Dependencies: 269
-- Data for Name: type_soutien; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3794 (class 0 OID 188242)
-- Dependencies: 263
-- Data for Name: utilisateurs; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3795 (class 0 OID 188247)
-- Dependencies: 264
-- Data for Name: zone; Type: TABLE DATA; Schema: public; Owner: -
--



