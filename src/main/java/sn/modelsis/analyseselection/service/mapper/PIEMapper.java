package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.PIEDTO;
import sn.modelsis.analyseselection.service.entity.PIE;

@Mapper(componentModel = "spring")
public interface PIEMapper extends GenericMapper<PIE, PIEDTO>{

}
