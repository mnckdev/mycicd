package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.Cohorte;
import sn.modelsis.analyseselection.service.entity.Collectif;
import sn.modelsis.analyseselection.service.entity.Commune;
import sn.modelsis.analyseselection.service.entity.Statut;

import java.util.List;
import java.util.UUID;

@Repository
public interface CollectifRepository extends JpaRepository<Collectif, UUID> {

    List<Collectif> findAllByCohorte(Cohorte cohorte);
    List<Collectif> findAllByCommune(Commune commune);
    List<Collectif> findAllByCohorteAndCommune(
            Cohorte cohorte, Commune commune
    );
    List<Collectif> findAllByCohorteAndCommuneAndStatut(
            Cohorte cohorte, Commune commune, Statut statut
    );

}
