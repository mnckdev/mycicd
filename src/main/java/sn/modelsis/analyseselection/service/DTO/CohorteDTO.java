package sn.modelsis.analyseselection.service.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CohorteDTO {

    private Integer idCohorte;

    private Date dateDebut;

    private Date dateFin;

    private String statut;

    private String processInstanceId;
}
