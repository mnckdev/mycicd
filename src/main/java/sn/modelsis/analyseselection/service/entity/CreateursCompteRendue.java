package sn.modelsis.analyseselection.service.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class CreateursCompteRendue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String prenom;

    private String nom;

    @Column(length = 2000)
    private String ideesPrioritePIE;

    @Column(length = 2000)
    private String ideesPrioriteCommune;

    @Column(length = 2000)
    private String commentaires;
}
