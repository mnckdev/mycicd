package sn.modelsis.analyseselection.service.DTO.externalDTO.realtimeNotification;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Utilisateur {
    private Long id ;
    private String username ;

    private Set<Role> roles ;

    private Set<Message> messages ;
}
