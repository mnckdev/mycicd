package sn.modelsis.analyseselection.service;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@SpringBootApplication
@OpenAPIDefinition(info =
@Info(title = "Analyse API", version = "1.0", description = "Analyse  API v1.0")
)
@EnableEurekaClient
public class AnalyseEtSelectionApplication {
	public static void main(String[] args) {
		SpringApplication.run(AnalyseEtSelectionApplication.class, args);
	}

	@Value("${adel.frontlink}")
	public   String frontLink;
//	@Bean
	public WebMvcConfigurer corsConfigurer(){
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**")
						.allowedOrigins(frontLink)
						.allowedMethods("GET", "POST", "PUT", "DELETE", "PATCH")
						.allowedHeaders("Content-Type", "Authorization")
						.allowCredentials(true)
						.exposedHeaders("X-Custom-Header");
			}
		};
	}

	@Bean
	public void sendMessage(){

	}

}
