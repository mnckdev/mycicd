package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.CohorteDTO;
import sn.modelsis.analyseselection.service.entity.Cohorte;

@Mapper(componentModel = "spring")
public interface CohorteMapper extends GenericMapper<Cohorte, CohorteDTO>{
}
