package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.Profil;
import sn.modelsis.analyseselection.service.repository.ProfilRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProfilService {

    private ProfilRepository profilRepository;

    public ProfilService(ProfilRepository profilRepository){
        this.profilRepository=profilRepository;
    }

    public Profil newProfil(Profil profil){
        return  profilRepository.save(profil);
    }

    public Optional<Profil> findById(UUID uuid){
        return profilRepository.findById(uuid);
    }
    public List<Profil> findAll(){
        return profilRepository.findAll();
    }
    public void deleteProfil(UUID uuid){
        profilRepository.deleteById(uuid);
    }

}
