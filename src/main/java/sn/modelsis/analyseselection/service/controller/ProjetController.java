package sn.modelsis.analyseselection.service.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.Projet;
import sn.modelsis.analyseselection.service.service.ProjetService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


@RestController
@RequestMapping("/api/v1/projet")
@Api(
        value = "Projet  REST Endpoint",
        tags = {"Projet  Controller (v1)"}
)

public class ProjetController {


    private final ProjetService projetService;

    public ProjetController(ProjetService projetService) {
        this.projetService = projetService;
    }

    @ApiOperation(value = "find All Projet", notes = "find all  Projet")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<Projet>> findAll(){
        List<Projet> projets = projetService.findAll();
        return new ResponseEntity<List<Projet>>(projets , HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID Projet", notes = "find By ID  Projet")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<Projet>> findById(@PathVariable UUID uuid){
        return new ResponseEntity<Optional<Projet>>(projetService.findById(uuid), HttpStatus.OK);
    }


    @ApiOperation(value = "create  new  Projets", notes = "create a new Projets")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @PostMapping
    public ResponseEntity<Projet> newProjet(@RequestBody Projet projet) {
        return new ResponseEntity<Projet>(projetService.newProjet(projet), HttpStatus.OK);
    }

    @ApiOperation(value = "delete  Projets", notes = "delete  Projets")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })


    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable UUID uuid) {
        projetService.deleteProjet(uuid);
        return new ResponseEntity<>("deleted successfully ...",HttpStatus.OK);
    }


}
