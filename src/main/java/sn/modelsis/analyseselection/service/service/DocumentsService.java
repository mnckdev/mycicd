package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.Documents;
import sn.modelsis.analyseselection.service.repository.DocumentsRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class DocumentsService {


    private DocumentsRepository documentsRepository;

    public DocumentsService(DocumentsRepository documentsRepository){
        this.documentsRepository=documentsRepository;
    }

    public Documents newDocuments(Documents documents){
        return  documentsRepository.save(documents);
    }

    public Optional<Documents> findById(UUID uuid){
        return documentsRepository.findById(uuid);
    }
    public List<Documents> findAll(){
        return documentsRepository.findAll();
    }
    public void deleteDocuments(UUID uuid){
        documentsRepository.deleteById(uuid);
    }
}
