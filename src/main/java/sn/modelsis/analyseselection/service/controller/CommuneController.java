package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.DTO.CommuneDTO;
import sn.modelsis.analyseselection.service.entity.Commune;
import sn.modelsis.analyseselection.service.service.CommuneService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/commune")
@Api(
        value = "Commune REST Endpoint",
        tags = {"Commune Controller (v1)"}
)

public class CommuneController {


    private final CommuneService communeService;

    public CommuneController(CommuneService communeService) {
        this.communeService = communeService;
    }

    @ApiOperation(value = "find All Commune", notes = "find all  Commune")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<CommuneDTO>> findAll(){
        List<CommuneDTO> commune = communeService.getCommunes();
        return new ResponseEntity<List<CommuneDTO>>(commune, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID Commune", notes = "find By ID  Commune")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<Commune>> findById(@PathVariable Integer id){
        return new ResponseEntity<Optional<Commune>>(communeService.getCommune(id), HttpStatus.OK);
    }




}
