package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.Rencontre;
import sn.modelsis.analyseselection.service.repository.RencontreRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RencontreService {
    private final RencontreRepository rencontreRepository;

    public RencontreService(RencontreRepository rencontreRepository) {
        this.rencontreRepository = rencontreRepository;
    }

    public Rencontre create(Rencontre rencontre){
        if (rencontre == null)
            return null;
        return rencontreRepository.save(rencontre);
    }

    public Optional<Rencontre> read(UUID id){
        if(id == null)
            return null;
        return  rencontreRepository.findById(id);
    }

    public List<Rencontre> read(){
        return rencontreRepository.findAll();
    }

    public void delete(UUID id){
        Optional<Rencontre> rencontre = rencontreRepository.findById(id);
        if(rencontre.isPresent())
            rencontreRepository.delete(rencontre.get());
    }
}
