package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.DTO.QuestionnaireDTO;
import sn.modelsis.analyseselection.service.entity.Phase;
import sn.modelsis.analyseselection.service.entity.Questionnaire;
import sn.modelsis.analyseselection.service.mapper.QuestionnaireMapper;
import sn.modelsis.analyseselection.service.repository.PhaseRepository;
import sn.modelsis.analyseselection.service.repository.QuestionnaireRepository;

import java.util.List;
import java.util.Optional;

@Service
public class QuestionnaireService {
    private final QuestionnaireRepository questionnaireRepository;

    private final PhaseRepository phaseRepository;

    private final QuestionnaireMapper questionnaireMapper;

    public QuestionnaireService(QuestionnaireRepository questionnaireRepository,
                                PhaseRepository phaseRepository,
                                QuestionnaireMapper questionnaireMapper
    ) {
        this.questionnaireRepository = questionnaireRepository;
        this.phaseRepository = phaseRepository;
        this.questionnaireMapper = questionnaireMapper;
    }

    public Questionnaire create(Questionnaire questionnaire){
        if (questionnaire == null)
            return null;
        return questionnaireRepository.save(questionnaire);
    }

    public Optional<Questionnaire> read(Integer id){
        if(id == null)
            return null;
        return  questionnaireRepository.findById(id);
    }

    public List<QuestionnaireDTO> read(){
        return questionnaireMapper.asDTOList(questionnaireRepository.findAll());
    }

    public void delete(Integer id){
        Optional<Questionnaire> questionnaire = questionnaireRepository.findById(id);
        if(questionnaire.isPresent())
            questionnaireRepository.delete(questionnaire.get());
    }

    public List<QuestionnaireDTO> findAllByPhase(Integer id){
        Phase phase = phaseRepository.findById(id).get();
        List<Questionnaire> questionnaires = questionnaireRepository.findAllByPhase(phase);
        List<QuestionnaireDTO> questionnaireDTOS = questionnaireMapper.asDTOList(questionnaires);
        return questionnaireDTOS;
    }
}
