package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.Phase;
import sn.modelsis.analyseselection.service.repository.PhaseRepository;

import java.util.List;
import java.util.Optional;

@Service
public class PhaseService {

    private final PhaseRepository phaseRepository;

    public PhaseService(PhaseRepository phaseRepository) {
        this.phaseRepository = phaseRepository;
    }

    public Phase create(Phase phase){
        if (phase == null)
            return null;
        return phaseRepository.save(phase);
    }

    public Optional<Phase> read(Integer id){
        if(id == null)
            return null;
        return  phaseRepository.findById(id);
    }

    public List<Phase> read(){
        return phaseRepository.findAll();
    }

    public void delete(Integer id){
        Optional<Phase> phase = phaseRepository.findById(id);
        if(phase.isPresent())
            phaseRepository.delete(phase.get());
    }
}
