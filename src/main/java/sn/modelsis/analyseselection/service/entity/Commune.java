package sn.modelsis.analyseselection.service.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Commune {

    @Id
    private Integer id;

    private String libelle;

    private String code;

    @ManyToOne
    private Departement departement;
}
