package sn.modelsis.analyseselection.service.repository;

import feign.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.Cohorte;
import sn.modelsis.analyseselection.service.entity.PIE;
import sn.modelsis.analyseselection.service.entity.Statut;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface PIERepository extends JpaRepository<PIE, UUID> {

    public Optional<PIE> findByCni(String cni);
    public List<PIE> findAllByStatut(Statut statut);
    public List<PIE> findAllByIdPIEIsIn(List<UUID> piesIds);
    public List<PIE> findAllByParcours(String parcours);
    @Query("SELECT p FROM PIE p " +
            "WHERE (TYPE(p) = Individuel OR " +
            "TYPE(p) = Collectif ) " +
            "AND p.commune.code = :commune")
    List<PIE> findAllByCommune(@Param("commune") String commune);
    @Query("SELECT p FROM PIE p WHERE " +
            "EXISTS (SELECT ts FROM p.listSoutiens ts WHERE ts.code IN :soutienCodes)")
    List<PIE> findAllByListSoutiensContainingCodes(@Param("soutienCodes") List<String> soutienCodes);

    public List<PIE> findAllByCohorte(Cohorte cohorte);
}
