package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.DTO.CategoriesDTO;
import sn.modelsis.analyseselection.service.entity.Categories;
import sn.modelsis.analyseselection.service.mapper.CategoriesMapper;
import sn.modelsis.analyseselection.service.repository.CategoriesRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CategoriesService {

    private final CategoriesRepository categoriesRepository;
    private final CategoriesMapper categoriesMapper;


    public CategoriesService(CategoriesRepository categoriesRepository, CategoriesMapper categoriesMapper) {
        this.categoriesRepository = categoriesRepository;
        this.categoriesMapper = categoriesMapper;
    }

    public CategoriesDTO getCategorie(Integer id){
        Optional<Categories> categories = categoriesRepository.findById(id);
        if(categories.isPresent())
            return categoriesMapper.asDTO(categories.get());
        return null;
    }

    public List<CategoriesDTO> getCategories(){
        List<Categories> categories = categoriesRepository.findAll();
        return categoriesMapper.asDTOList(categories);
    }
}
