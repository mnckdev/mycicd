package sn.modelsis.analyseselection.service.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import sn.modelsis.analyseselection.service.entity.Zone;
import sn.modelsis.analyseselection.service.service.ZoneService;

@RestController
@RequestMapping("/api/v1/zone")

public class ZoneController {

    private final ZoneService zoneService;

    public ZoneController(ZoneService zoneService) {
        this.zoneService = zoneService;
    }

    @ApiOperation(value = "find All Zones", notes = "find all  Zone")
    @ApiResponses({
        @ApiResponse(code = 200, message = "success"),
        @ApiResponse(code = 400, message = "Bad request"),
        @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<Zone>> findAll() {
        List<Zone> zone = zoneService.findAll();
        return new ResponseEntity<List<Zone>>(zone, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID Zone", notes = "find By ID  Zone")
    @ApiResponses({
        @ApiResponse(code = 200, message = "success"),
        @ApiResponse(code = 400, message = "Bad request"),
        @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<Zone>> findById(@PathVariable Integer id) {
        return new ResponseEntity<Optional<Zone>>(zoneService.findById(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ApiResponses(
            {
                @ApiResponse(code = 200, message = "delete"),}
    )
    @ApiOperation(value = "delete a Region", notes = "delete a  Zone")
    public ResponseEntity<String> delete(@PathVariable Integer id) {
        zoneService.delete(id);
        return ResponseEntity.ok().body("deleted");
    }

    @PostMapping
    @ApiResponses(
            {
                @ApiResponse(code = 200, message = "created"),}
    )
    @ApiOperation(value = "post a Zone", notes = "post a Zone")
    public ResponseEntity<Zone> create(@RequestBody Zone zone) {
        zone = zoneService.create(zone);
        return ResponseEntity.ok().body(zone);
    }

}
