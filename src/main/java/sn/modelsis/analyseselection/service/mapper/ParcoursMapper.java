package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.ParcoursDTO;
import sn.modelsis.analyseselection.service.entity.Parcours;

@Mapper(componentModel = "spring")
public interface ParcoursMapper extends GenericMapper<Parcours, ParcoursDTO>{
}
