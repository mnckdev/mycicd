package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.ProjetDTO;
import sn.modelsis.analyseselection.service.entity.Projet;

@Mapper(componentModel = "spring")
public interface ProjetMapper extends GenericMapper<Projet, ProjetDTO>{
}
