package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.modelsis.analyseselection.service.DTO.*;
import sn.modelsis.analyseselection.service.entity.*;
import sn.modelsis.analyseselection.service.repository.*;
import sn.modelsis.analyseselection.service.serviceinterface.IndividuelServiceInterface;
import sn.modelsis.analyseselection.service.utils.Constantes;
import sn.modelsis.analyseselection.service.mapper.*;

import java.util.*;

@Service
public class IndividuelService implements IndividuelServiceInterface {

    private final IndividuelRepository individuelRepository;
    private final CommuneRepository communeRepository;
    private final TypeSoutienRepository typeSoutienRepository;
    private final TrancheAgeRepository trancheAgeRepository;

    private final IndividuelMapper individuelMapper;
    private final StatutMapper statutMapper;
    private final CohorteMapper cohorteMapper;
    private final CommuneMapper communeMapper;

    private final IdeeProjetMapper ideeProjetMapper;

    private final IdeeProjetRepository ideeProjetRepository;

    private final StatutRepository statutRepository;

    private final SousCategoriesRepository sousCategoriesRepository;
    private String returnField;

    public IndividuelService(IndividuelRepository individuelRepository,
                             CommuneRepository communeRepository,
                             TypeSoutienRepository typeSoutienRepository,
                             TrancheAgeRepository trancheAgeRepository,
                             IndividuelMapper individuelMapper,
                             StatutMapper statutMapper, CohorteMapper cohorteMapper, CommuneMapper communeMapper, IdeeProjetMapper ideeProjetMapper,
                             IdeeProjetRepository ideeProjetRepository,
                             StatutRepository statutRepository, SousCategoriesRepository sousCategoriesRepository) {
        this.individuelRepository = individuelRepository;
        this.communeRepository = communeRepository;
        this.typeSoutienRepository = typeSoutienRepository;
        this.trancheAgeRepository = trancheAgeRepository;
        this.individuelMapper = individuelMapper;
        this.statutMapper = statutMapper;
        this.cohorteMapper = cohorteMapper;
        this.communeMapper = communeMapper;
        this.ideeProjetMapper = ideeProjetMapper;
        this.ideeProjetRepository = ideeProjetRepository;
        this.statutRepository = statutRepository;
        this.sousCategoriesRepository = sousCategoriesRepository;
    }

    public IndividuelDTO create(IndividuelDTO individuelDTO) {
        if (isNull(individuelDTO)) {
            return null;
        }
        Individuel individuel = individuelMapper.asEntity(individuelDTO);
        setInitialAttributes(individuel);
        return individuelMapper.asDTO(individuelRepository.save(individuel));

    }
    public IndividuelDTO update(UUID id, IndividuelDTO individuelDTO) {
        Optional<Individuel> oldIndividuel = individuelRepository.findById(id);
        if (oldIndividuel.isEmpty() || isNull(individuelDTO)) {
            return null;
        }
        Individuel individuel = individuelMapper.asEntity(individuelDTO);
        updateAttributes(individuel,oldIndividuel.get());
        return individuelMapper.asDTO(individuelRepository.save(individuel));

    }

    private void setInitialAttributes(Individuel individuel) {
        individuel.setDate(new Date());

        setInitialStatus(individuel);

        setListSoutien(individuel);

        setSoutienImmediat(individuel);

        setTrancheAge(individuel);

        setCommune(individuel);

        setSousCategories(individuel);
    }
    private void updateAttributes(Individuel individuel,Individuel oldIndividuel) {
        individuel.setDate(oldIndividuel.getDate());
        setStatus(individuel);

        setListSoutien(individuel);
        removeOldListSoutien(oldIndividuel);

        setSoutienImmediat(individuel);
        removeOldSoutienImmediat(oldIndividuel);

        setTrancheAge(individuel);

        setCommune(individuel);

        setSousCategories(individuel);
    }

    private void setInitialStatus(Individuel individuel) {
        individuel.setStatut(statutRepository.findStatutByCode(Constantes.STATUS_INITIALISE));
    }private void setStatus(Individuel individuel) {
        individuel.setStatut(statutRepository.findStatutByCode(individuel.getStatut().getCode()));
    }

    private void setListSoutien(Individuel individuel) {
        List<TypeSoutien> typeSoutienList = individuel.getListSoutiens();
        if (typeSoutienList != null && !typeSoutienList.isEmpty()) {
            typeSoutienList = typeSoutienRepository.saveAll(typeSoutienList);
            individuel.setListSoutiens(typeSoutienList);
        }
    }
    private void removeOldListSoutien(Individuel individuel) {
        List<TypeSoutien> typeSoutienList = individuel.getListSoutiens();
        if (typeSoutienList != null && !typeSoutienList.isEmpty()) {
            typeSoutienRepository.deleteAll(typeSoutienList);
        }
    }

    private void setSoutienImmediat(Individuel individuel) {
        individuel.setSoutiensImmediat(typeSoutienRepository.save(individuel.getSoutiensImmediat()));
    }
    private void removeOldSoutienImmediat(Individuel individuel) {
        typeSoutienRepository.delete(individuel.getSoutiensImmediat());
    }

    private void setCommune(Individuel individuel) {
        individuel.setCommuneRattach(communeRepository.findCommuneByCode(individuel.getCommuneRattach().getCode()).orElse(null));
        individuel.setCommune(communeRepository.findCommuneByCode(individuel.getCommune().getCode()).orElse(null));
    }

    private void setTrancheAge(Individuel individuel) {
        individuel.setTrancheAge(trancheAgeRepository.findTrancheAgeByCode(individuel.getTrancheAge().getCode()).orElse(null));
    }
    private void setSousCategories(Individuel individuel) {
        individuel.setSousCategorieInd(sousCategoriesRepository.findSousCategoriesByCode(individuel.getSousCategorieInd().getCode()).orElse(null));
        if(individuel.getSousCategorieMig()!=null)
            individuel.setSousCategorieMig(sousCategoriesRepository.findSousCategoriesByCode(individuel.getSousCategorieMig().getCode()).orElse(null));
    }
    public String verifyCheck(Boolean check,IndividuelDTO individuelDTO){
        if(Boolean.TRUE.equals(check)){
            String checkProperties = this.verifyIndividuel(individuelDTO);
            if(!checkProperties.isEmpty())
                return "Veuillez vérifier le champ:" + checkProperties;
        }
        return "";

    }
    public String verifyIndividuel(IndividuelDTO individuelDTO) {
            Map<String,Object> properties  = new HashMap<>();
            properties.put("Prénom",individuelDTO.getPrenom());
            properties.put("Nom",individuelDTO.getNom());
            properties.put("Tel1",individuelDTO.getTel1());
            properties.put("Quartier",individuelDTO.getQuartier());
            properties.put("Sexe",individuelDTO.getSexe());
            properties.put("CNI",individuelDTO.getCni());
            properties.put("Commune",individuelDTO.getCommune());
            properties.put("SousCategorieInd",individuelDTO.getSousCategorieInd());
            properties.put("ListSoutiens",individuelDTO.getListSoutiens());
            properties.put("SoutienImmediat",individuelDTO.getSoutiensImmediat());
            properties.put("TrancheAge",individuelDTO.getTrancheAge());
            properties.put("DomainActivité",individuelDTO.getDomainActivite());
            properties.put("SecteurActivité",individuelDTO.getSecteurActivite());
        return this.checkListObject(properties);
    }

    private String checkListObject(Map<String,Object> properties) {
        this.returnField = "";
        properties.forEach((s, o) -> {
            if(isNull(o))
                this.returnField = s;
        });
        return this.returnField;
    }

    public IndividuelDTO read(UUID uuid) {
        Optional<Individuel> individuel = individuelRepository.findById(uuid);
        if (individuel.isEmpty())
            return null;
        return individuelMapper.asDTO(individuel.get());
    }

    public List<IndividuelDTO> read() {
        List<Individuel> individuels = individuelRepository.findAll();
        List<IndividuelDTO> individuelDTOS = individuelMapper.asDTOList(individuels);
        return individuelDTOS;
    }

    public void delete(UUID uuid) {
        Optional<Individuel> individuel = individuelRepository.findById(uuid);
        if (individuel.isPresent())
            individuelRepository.delete(individuel.get());
    }

    @Override
    public List<IndividuelDTO> getDeveloppeurs() {
        return individuelMapper.asDTOList(individuelRepository.findAllByTypeCategorieIgnoreCase(Constantes.DEVELOPPEUR));
    }
    @Override
    public List<IndividuelDTO> getCreateurs() {
        return individuelMapper.asDTOList(findAllByTypeCategorieIgnoreCase(Constantes.CREATEUR));
    }

    private List<Individuel> findAllByTypeCategorieIgnoreCase(String parcours) {
        return individuelRepository.findAllByTypeCategorieIgnoreCase(parcours);
    }

    @Override
    public List<IndividuelDTO> getCreateursFromACommune(Commune commune) {
        return individuelMapper.asDTOList(findAllByCommuneAndTypeCategorieIgnoreCase(commune, Constantes.DEVELOPPEUR));
    }

    @Override
    public List<IndividuelDTO> getDeveloppeursFromACommune(Commune commune) {
        return individuelMapper.asDTOList(findAllByCommuneAndTypeCategorieIgnoreCase(commune, Constantes.DEVELOPPEUR));
    }

    @Override
    public List<IndividuelDTO> getCreateursByCohorte(Cohorte cohorte) {
        return individuelMapper.asDTOList(findAllByCohorteAndTypeCategorieIgnoreCase(cohorte, Constantes.CREATEUR));
    }

    @Override
    public List<IndividuelDTO> getDeveloppeursByCohorte(Cohorte cohorte) {
        return individuelMapper.asDTOList(findAllByCohorteAndTypeCategorieIgnoreCase(cohorte, Constantes.DEVELOPPEUR));
    }

    private List<Individuel> findAllByCohorteAndTypeCategorieIgnoreCase(Cohorte cohorte , String parcours){
        return individuelRepository.findAllByCohorteAndTypeCategorieIgnoreCase(cohorte, parcours);
    }    
    
    private List<Individuel> findAllByCommuneAndTypeCategorieIgnoreCase(Commune commune,String  parcours){
        return individuelRepository.findAllByCommuneAndTypeCategorieIgnoreCase(commune, parcours);
    }
    @Override
    public List<IndividuelDTO> getCreateursByCohorteAndCommune(Cohorte cohorte, Commune commune) {
        List<Individuel> individuels = individuelRepository
                .findAllByCohorteAndTypeCategorieIgnoreCaseAndAndCommune(
                        cohorte, Constantes.CREATEUR, commune
                );
        List<IndividuelDTO> individuelDTOS = individuelMapper.asDTOList(individuels);
        return individuelDTOS;
    }

    @Override
    public List<IndividuelDTO> getDeveloppeursByCohorteAndCommune(CohorteDTO cohorte, CommuneDTO commune) {
        return  individuelMapper.asDTOList(individuelRepository
                .findAllByCohorteAndTypeCategorieIgnoreCaseAndAndCommune(
                        cohorteMapper.asEntity(cohorte), Constantes.DEVELOPPEUR,communeMapper.asEntity(commune)
                ));
    }

    @Override
    public List<IndividuelDTO> getCreateursByStatut(StatutDTO statut) {
        List<Individuel> individuels = individuelRepository.findAllByTypeCategorieIgnoreCaseAndStatut(
                Constantes.CREATEUR, statutMapper.asEntity(statut)
        );

        List<IndividuelDTO> individuelDTOS = individuelMapper.asDTOList(individuels);
        return individuelDTOS;
    }

    @Override
    public List<IndividuelDTO> getDeveloppeursByStatut(StatutDTO statutDTO) {
        List<Individuel> individuels = individuelRepository.findAllByTypeCategorieIgnoreCaseAndStatut(
                Constantes.DEVELOPPEUR,  statutMapper.asEntity(statutDTO)
        );

        List<IndividuelDTO> individuelDTOS = individuelMapper.asDTOList(individuels);
        return individuelDTOS ;
    }

    @Override
    @Transactional
    public List<IdeeProjetDTO> saveIdeesProjet(UUID id, List<IdeeProjetDTO> ideeProjetDTOS) {
        List<IdeeProjet> ideeProjets = ideeProjetMapper.asEntiteList(ideeProjetDTOS);
        ideeProjetRepository.saveAll(ideeProjets);
        return ideeProjetMapper.asDTOList(ideeProjets);
    }

    public List<IndividuelDTO> findAllByStatut(Integer id) {
        Optional<Statut> statut = statutRepository.findById(id);
        List<Individuel> individuels = individuelRepository.findAllByStatut(statut.get());
        List<IndividuelDTO> individuelDTOS = individuelMapper.asDTOList(individuels);
        return individuelDTOS;
    }

    public List<IdeeProjetDTO> getIdeeProjetByIndividuel(UUID id) {
        List<IdeeProjet> ideeProjets = new ArrayList<>();
        return ideeProjetMapper.asDTOList(ideeProjets);
    }

    private boolean isNull(Object object) {
        return object == null;
    }

}
