package sn.modelsis.analyseselection.service.utils;

import java.util.List;

public class Constantes {
    public static final String DEFAULT = "Default";
    public static final List<String> CAN_SIGN_LIST = List.of("adel-PIE","adel-admin","adel-ADMIN", "adel-RPIE", "adel-DG","adel-pie", "adel-rpie", "adel-dg");
    public static final String ADEL_EMAIL = "adel@modelsis.sn";
    public static final String ADEL_EMAIL_PATH = "email/with-template";
    public static final String ADEL_EMAIL_OBJECT = "Creation de compte";
    public static final String ADEL_TEMPLATE_CREATE_ACCOUNT = "adel-creation-compte-pie";

    public static final String DEVELOPPEUR = "DEVELOPPEUR";

    public static final String COLLECTIF = "COLLECTIF";

    public static final String CREATEUR = "CREATEUR";

    public static final String INDIVIDUEL = "";
    public static final String STATUS_INITIALISE = "Initialisé";
    public static final String STATUS_A_NOTER = "À noter";
    public static final String COHORTE_ENCOURS = "EN COURS";
    public static final String COHORTE_TERMINE = "TERMINE";


}
