package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.TypeSoutien;
import sn.modelsis.analyseselection.service.repository.TypeSoutienRepository;

import java.util.List;
import java.util.Optional;

@Service
public class TypeSoutienService {

    private TypeSoutienRepository statutRepository;

    public TypeSoutienService(TypeSoutienRepository statutRepository){
        this.statutRepository=statutRepository;
    }

    public TypeSoutien create(TypeSoutien statut){
        return  statutRepository.save(statut);
    }

    public Optional<TypeSoutien> findById(Integer id){
        return statutRepository.findById(id);
    }
    public Optional<TypeSoutien> findByCode(String code){
        return Optional.of(statutRepository.findTypeSoutienByCode(code));
    }
    public List<TypeSoutien> findAll(){
        return statutRepository.findAll();
    }
     public void delete(Integer id){
        statutRepository.deleteById(id);
     }


}
