package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.FormeJuridiqueDTO;
import sn.modelsis.analyseselection.service.entity.FormeJuridique;

@Mapper(componentModel = "spring")
public interface FormeJuridiqueMapper extends GenericMapper<FormeJuridique, FormeJuridiqueDTO>{
}
