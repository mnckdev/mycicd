package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.ReferenceNoteDTO;
import sn.modelsis.analyseselection.service.entity.ReferenceNote;

@Mapper(componentModel = "spring")
public interface ReferenceNoteMapper extends GenericMapper<ReferenceNote, ReferenceNoteDTO>{
}
