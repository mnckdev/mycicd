package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.MarqueurDTO;
import sn.modelsis.analyseselection.service.entity.Marqueur;

@Mapper(componentModel = "spring")
public interface MarqueurMapper extends GenericMapper<Marqueur, MarqueurDTO>{
}
