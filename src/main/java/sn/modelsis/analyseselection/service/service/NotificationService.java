package sn.modelsis.analyseselection.service.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import sn.modelsis.analyseselection.service.DTO.externalDTO.SmsDTO;

@Service
@Slf4j
public class NotificationService {

    @Value("${notification.host}")
    private String notificationHost;
//    private String notificationSMSResourceUrl
//            = notificationHost+"sms";

    @Value("${notification.host}")
    private String notificationMessageResourceUrl
            = notificationHost + "message-in-app";
    private final static RestTemplate restTemplate = new RestTemplate();

    public SmsDTO envoyerSMS(
            SmsDTO smsDTO
    ){
        HttpEntity<SmsDTO> request = new HttpEntity<>(
                smsDTO
        );
        SmsDTO smsDTO1 = restTemplate.postForObject(
                notificationHost+"sms",
                request,
                SmsDTO.class
        );
        return smsDTO1;
    }
}
