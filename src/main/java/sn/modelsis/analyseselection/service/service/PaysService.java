package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.Pays;
import sn.modelsis.analyseselection.service.repository.PaysRepository;

import java.util.List;
import java.util.Optional;


@Service
public class PaysService {

    private PaysRepository paysRepository;

    public PaysService(PaysRepository paysRepository){
        this.paysRepository=paysRepository;
    }

    public Optional<Pays> findById(Integer id){
        return paysRepository.findById(id);
    }
    public List<Pays> findAll(){
        return paysRepository.findAll();
    }

}
