package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.DocumentsDTO;
import sn.modelsis.analyseselection.service.entity.Documents;

@Mapper(componentModel = "spring")
public interface DocumentsMapper extends GenericMapper<Documents, DocumentsDTO>{
}
