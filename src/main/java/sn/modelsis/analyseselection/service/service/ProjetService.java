package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.Projet;
import sn.modelsis.analyseselection.service.repository.ProjetRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProjetService {


    private ProjetRepository projetRepository;

    public ProjetService(ProjetRepository projetRepository){
        this.projetRepository=projetRepository;
    }

    public Projet newProjet(Projet projet){
        return  projetRepository.save(projet);
    }

    public Optional<Projet> findById(UUID uuid){
        return projetRepository.findById(uuid);
    }
    public List<Projet> findAll(){
        return projetRepository.findAll();
    }
    public void deleteProjet(UUID uuid){
        projetRepository.deleteById(uuid);
    }
}
