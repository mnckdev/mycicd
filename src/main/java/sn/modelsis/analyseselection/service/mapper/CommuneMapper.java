package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.CommuneDTO;
import sn.modelsis.analyseselection.service.entity.Commune;

@Mapper(componentModel = "spring")
public interface CommuneMapper extends GenericMapper<Commune, CommuneDTO>{
}
