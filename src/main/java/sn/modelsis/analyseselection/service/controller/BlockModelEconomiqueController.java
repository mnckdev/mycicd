package sn.modelsis.analyseselection.service.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.DTO.BlockModelEconomiqueDTO;
import sn.modelsis.analyseselection.service.service.BlockModelEconomiqueService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/blockModelEconomique")
@Api(
        value = "BlockModelEconomique  REST Endpoint",
        tags = {"BlockModelEconomique  Controller (v1)"}
)

public class BlockModelEconomiqueController {


    private final BlockModelEconomiqueService blockModelEconomiqueService;

    public BlockModelEconomiqueController(BlockModelEconomiqueService blockModelEconomiqueService) {
        this.blockModelEconomiqueService = blockModelEconomiqueService;
    }

    @ApiOperation(value = "find All Block Model Economique", notes = "find all  Block Model Economique")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<BlockModelEconomiqueDTO>> findAll(){
        List<BlockModelEconomiqueDTO> blockModelEconomique = blockModelEconomiqueService.findAll();
        if(blockModelEconomique == null)
            return ResponseEntity.badRequest().body(null);
        return ResponseEntity.ok().body(blockModelEconomique);
    }

    @ApiOperation(value = "find By ID Block Model Economique", notes = "find By ID  Block Model Economique")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping("/{id}")
    public ResponseEntity<BlockModelEconomiqueDTO> findById(@PathVariable UUID id){
        BlockModelEconomiqueDTO blockModelEconomiqueDTO = blockModelEconomiqueService.findById(id);
        if(blockModelEconomiqueDTO == null)
            return ResponseEntity.badRequest().body(null);
        return ResponseEntity.ok().body(blockModelEconomiqueDTO);
    }

    @ApiOperation(value = "create  new  Block Model Economiques", notes = "create a new Block Model Economiques")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @PostMapping
    public ResponseEntity<BlockModelEconomiqueDTO> createblockModelEconomique(@RequestBody BlockModelEconomiqueDTO blockBlockModelEconomique) {
        blockBlockModelEconomique = blockModelEconomiqueService.newBlockModelEconomique(blockBlockModelEconomique);
        if(blockBlockModelEconomique == null){
            return ResponseEntity.badRequest().body(null);
        }
        return ResponseEntity.ok().body(blockBlockModelEconomique);
    }

    @ApiOperation(value = "delete  Block Model Economiques", notes = "delete  Block Model Economiques")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable UUID id) {
        blockModelEconomiqueService.deleteBlockModelEconomique(id);
        return new ResponseEntity<>("deleted successfully ...",HttpStatus.OK);
    }

}
