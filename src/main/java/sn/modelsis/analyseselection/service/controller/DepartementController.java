package sn.modelsis.analyseselection.service.controller;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.Departement;
import sn.modelsis.analyseselection.service.service.DepartementService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/departement")
@Api(
        value = "Departement REST Endpoint",
        tags = {"Departement Controller (v1)"}
)

public class DepartementController {


    private final DepartementService departementService;

    public DepartementController(DepartementService departementService) {
        this.departementService = departementService;
    }

    @ApiOperation(value = "find All Departement", notes = "find all  Departement")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<Departement>> findAll(){
        List<Departement> departement = departementService.findAll();
        return new ResponseEntity<List<Departement>>(departement, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID Departement", notes = "find By ID  Departement")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<Departement>> findById(@PathVariable Integer id){
        return new ResponseEntity<Optional<Departement>>(departementService.findById(id), HttpStatus.OK);
    }





}
