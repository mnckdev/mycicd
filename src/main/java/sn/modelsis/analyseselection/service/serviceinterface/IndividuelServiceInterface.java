package sn.modelsis.analyseselection.service.serviceinterface;


import sn.modelsis.analyseselection.service.DTO.*;
import sn.modelsis.analyseselection.service.entity.Cohorte;
import sn.modelsis.analyseselection.service.entity.Commune;

import java.util.List;
import java.util.UUID;

public interface IndividuelServiceInterface {

    List<IndividuelDTO> getDeveloppeurs();

    List<IndividuelDTO> getCreateurs();

    List<IndividuelDTO> getCreateursFromACommune(Commune commune);

    List<IndividuelDTO> getDeveloppeursFromACommune(Commune commune);

    List<IndividuelDTO> getCreateursByCohorte(Cohorte cohorte);

    List<IndividuelDTO> getDeveloppeursByCohorte(Cohorte cohorte);

    List<IndividuelDTO> getCreateursByCohorteAndCommune(Cohorte cohorte, Commune commune);

    List<IndividuelDTO> getDeveloppeursByCohorteAndCommune(CohorteDTO cohorte, CommuneDTO commune);


    List<IndividuelDTO> getCreateursByStatut(StatutDTO statut);

    List<IndividuelDTO> getDeveloppeursByStatut(StatutDTO statut);

    List<IdeeProjetDTO> saveIdeesProjet(UUID id, List<IdeeProjetDTO> ideeProjetDTOS);

    public List<IdeeProjetDTO> getIdeeProjetByIndividuel(UUID id);
}
