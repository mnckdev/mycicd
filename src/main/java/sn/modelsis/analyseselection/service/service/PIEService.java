package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.modelsis.analyseselection.service.DTO.*;
import sn.modelsis.analyseselection.service.DTO.externalDTO.FilterDto;
import sn.modelsis.analyseselection.service.entity.*;
import sn.modelsis.analyseselection.service.mapper.*;
import sn.modelsis.analyseselection.service.repository.*;
import sn.modelsis.analyseselection.service.serviceinterface.PIEServiceInterface;
import sn.modelsis.analyseselection.service.utils.Constantes;

import java.util.*;
import java.util.stream.Collectors;

import static sn.modelsis.analyseselection.service.utils.NullUtility.*;

@Service
public class PIEService implements PIEServiceInterface {
    private final PIERepository pieRepository;

    private final NoteQuestionRepository noteQuestionRepository;

    private final ObservationRepository observationRepository;

    private final PIEMapper pieMapper;

    private final ProjetMapper projetMapper;

    private final ModelEconomiqueMapper modelEconomiqueMapper;

    private final StatutRepository statutRepository;

    private final ProjetRepository projetRepository;

    private final SousBlocRepository sousBlocRepository;
    private final CommuneRepository communeRepository;
    private final IndividuelRepository individuelRepository;
    private final CollectifRepository collectifRepository;
    private final ObservationMapper observationMapper;
    private final StatutMapper statutMapper;

    public PIEService(PIERepository pieRepository,
                      NoteQuestionRepository noteQuestionRepository,
                      ObservationRepository observationRepository,
                      PIEMapper pieMapper,
                      ProjetMapper projetMapper,
                      ModelEconomiqueMapper modelEconomiqueMapper,
                      StatutRepository statutRepository,
                      ProjetRepository projetRepository, SousBlocRepository sousBlocRepository, CommuneRepository communeRepository, IndividuelRepository individuelRepository, CollectifRepository collectifRepository, ObservationMapper observationMapper, StatutMapper statutMapper) {
        this.pieRepository = pieRepository;
        this.noteQuestionRepository = noteQuestionRepository;
        this.observationRepository = observationRepository;
        this.pieMapper = pieMapper;
        this.projetMapper = projetMapper;
        this.modelEconomiqueMapper = modelEconomiqueMapper;
        this.statutRepository = statutRepository;
        this.projetRepository = projetRepository;
        this.sousBlocRepository = sousBlocRepository;
        this.communeRepository = communeRepository;
        this.individuelRepository = individuelRepository;
        this.collectifRepository = collectifRepository;
        this.observationMapper = observationMapper;
        this.statutMapper = statutMapper;
    }

    public PIEDTO create(PIE pie) {
        if (pie == null) {
            return null;
        }
        pie.setStatut(statutRepository.findStatutByCode(pie.getStatut().getCode()));
        return pieMapper.asDTO(pieRepository.save(pie));
    }

    public Optional<PIEDTO> read(UUID id) {
        Optional<PIE> pie = pieRepository.findById(id);
        return pie.map(pieMapper::asDTO);
    }

    public void delete(UUID id) {
        Optional<PIE> pie = pieRepository.findById(id);
        pie.ifPresent(pieRepository::delete);
    }

    @Override
    public List<PIEDTO> findAllByStatut(Integer id) {
        List<PIE> pies = new ArrayList<>();
        Optional<Statut> statut = statutRepository.findById(id);
        if (statut.isPresent()) {
            pies = pieRepository.findAllByStatut(statut.get());
        }
        return pieMapper.asDTOList(pies);
    }


    @Override
    public List<PIEDTO> getPIEsInscrits() {
        Statut statut = statutRepository.findStatutByCode(Constantes.STATUS_INITIALISE);
        return pieMapper.asDTOList(pieRepository.findAllByStatut(statut));
    }

    @Override
    public List<PIEDTO> getPIEsInscritsByFilters(FilterDto filterDto) {
        List<PIE> pies = new ArrayList<>();

        if (allFieldArePresent(filterDto)) {
            return filterWithAllFields(filterDto);
        }
        if (twoFieldArePresent(filterDto))
            return filterWithTwoFields(filterDto);

        if (!filterDto.getCodeCommune().isBlank())
            return pieMapper.asDTOList(findPieByCommune(filterDto.getCodeCommune()));

        if (!filterDto.getParcoursPie().isEmpty())
            return pieMapper.asDTOList(pieRepository.findAllByParcours(filterDto.getParcoursPie()));

        if (!filterDto.getListSoutien().isEmpty())
            return pieMapper.asDTOList(filterBySoutienList(filterDto.getListSoutien()));

        return pieMapper.asDTOList(pies);
    }

    private List<PIEDTO> filterWithTwoFields(FilterDto filterDto) {
        if (communeAndParcours(filterDto))
            return filterWithCommuneAndParcours(filterDto.getCodeCommune(), filterDto.getParcoursPie());

        if (communeAndSoutienList(filterDto))
            return filterWithCommuneAndListSoutien(filterDto.getCodeCommune(), filterDto.getListSoutien());

        if (parcoursAndSoutienList(filterDto))
            return filterWithParcoursAndListSoutien(filterDto.getParcoursPie(), filterDto.getListSoutien());
        return Collections.emptyList();
    }

    public List<PIEDTO> filterWithCommuneAndParcours(String codeCommune, String parcoursPie) {
        List<PIE> filteredPie = pieRepository.findAllByParcours(parcoursPie);
        List<PIE> piesSearchByCommune = findPieByCommune(codeCommune);
        filteredPie.retainAll(piesSearchByCommune);
        return pieMapper.asDTOList(filteredPie);
    }

    public List<PIEDTO> filterWithCommuneAndListSoutien(String codeCommune, List<String> listSoutien) {
        List<PIE> piesSearch = findPieByCommune(codeCommune);
        List<PIE> piesSearchByListSoutien = filterBySoutienList(listSoutien);
        piesSearch.retainAll(piesSearchByListSoutien);
        return pieMapper.asDTOList(piesSearch);
    }

    public List<PIEDTO> filterWithParcoursAndListSoutien(String parcoursPie, List<String> listSoutien) {
        List<PIE> filteredPie = pieRepository.findAllByParcours(parcoursPie);
        List<PIE> piesSearchByListSoutien = filterBySoutienList(listSoutien);
        filteredPie.retainAll(piesSearchByListSoutien);
        return pieMapper.asDTOList(filteredPie);
    }

    private List<PIEDTO> filterWithAllFields(FilterDto filterDto) {
        List<PIE> filteredPie = pieRepository.findAllByParcours(filterDto.getParcoursPie());
        List<PIE> piesSearchByCommune = findPieByCommune(filterDto.getCodeCommune());
        List<PIE> piesSearchByListSoutien = filterBySoutienList(filterDto.getListSoutien());
        filteredPie.retainAll(piesSearchByCommune);
        filteredPie.retainAll(piesSearchByListSoutien);
        return pieMapper.asDTOList(filteredPie);
    }


    private List<PIE> filterBySoutienList(List<String> soutienListCode) {
        List<PIE> pies = pieRepository.findAllByListSoutiensContainingCodes(soutienListCode);
        List<PIE> filteredPies = new ArrayList<>();
        pies.forEach(pie -> {
            if (soutienListCode.stream()
                    .allMatch(code -> pie.getListSoutiens().stream()
                            .anyMatch(ts -> code.equals(ts.getCode())))) {
                filteredPies.add(pie);
            }
        });
        pies = filteredPies;
        return pies;
    }

    @Override
    public List<PIEDTO> getPIEs() {
        return pieMapper.asDTOList(pieRepository.findAll());
    }

    @Override
    public PIEDTO changePIEStatut(UUID idPIE, StatutDTO statut) {
        Optional<PIE> pie = pieRepository.findById(idPIE);
        if (pie.isPresent()) {
            pie.get().setStatut(statutMapper.asEntity(statut));

            pie = Optional.of(pieRepository.save(pie.get()));
            return pieMapper.asDTO(pie.get());
        }
        return null;
    }

    public List<PIE> findAllByStatuts(Integer id) {
        Optional<Statut> statut = statutRepository.findById(id);
        if (statut.isPresent())
            return pieRepository.findAllByStatut(statut.get());
        return Collections.emptyList();
    }

    public Optional<List<PIE>> findAllByStatutCode(String code) {
        Statut statut = statutRepository.findStatutByCode(code);
        List<PIE> pies = new ArrayList<>();
        if (!isNull(statut))
            pies = pieRepository.findAllByStatut(statut);
        return Optional.of(pies);
    }


    @Override
    public List<PIEDTO> findAllByCommune(Commune commune) {
        return new ArrayList<>();
    }

    @Override
    @Transactional
    public Optional<PIEDTO> noterPIE(UUID idPie, List<NoteQuestion> notes) {
        Optional<PIE> pie = pieRepository.findById(idPie);
        if (pie.isPresent())
            noteQuestionRepository.saveAll(notes);
        return pie.map(pieMapper::asDTO);
    }

    @Override
    @Transactional
    public Optional<PIEDTO> retournerPIE(UUID idPie, ObservationDTO observation) {
        Optional<PIE> pie = pieRepository.findById(idPie);
        observationRepository.save(observationMapper.asEntity(observation));
        return pie.map(pieMapper::asDTO);
    }

    @Override
    @Transactional
    public Optional<PIEDTO> creerProjet(UUID idPie, ProjetDTO projet) {
        Optional<PIE> pie = pieRepository.findById(idPie);
        Projet projet1 = projetMapper.asEntity(projet);
        projet1 = projetRepository.save(projet1);
        if (pie.isPresent()) {
            pie.get().setProjet(projet1);
            pie = Optional.of(pieRepository.save(pie.get()));
        }
        return pie.map(pieMapper::asDTO);
    }

    @Override
    @Transactional
    public Optional<PIEDTO> concevoirModeleEconomique(UUID idPie, ModeleEconomiqueDTO modeleEconomiqueDTO) {
        Optional<PIE> pie = pieRepository.findById(idPie);
        if (pie.isPresent()) {
            Projet projet = pie.get().getProjet();
            ModelEconomique modelEconomique = modelEconomiqueMapper.asEntity(modeleEconomiqueDTO);
            for (BlockModelEconomique bme :
                    modelEconomique.getBlockModelEconomique()) {
                for (SousBloc sb :
                        bme.getSousBlocs()) {
                    sousBlocRepository.save(sb);
                }
            }
            projet.setModelEconomique(modelEconomique);
            projetRepository.save(projet);
            pie.get().setProjet(projet);
        }
        return pie.map(pieMapper::asDTO);
    }

    public List<PIE> getPieInIds(List<UUID> piesIds) {
        return pieRepository.findAllByIdPIEIsIn(piesIds);
    }

    public List<PIEDTO> findAllByStatutAndCommuneCode(String codeStatut, String codeCommune) {
        Optional<Commune> commune = communeRepository.findCommuneByCode(codeCommune);
        if (commune.isPresent()) {
            List<PIE> pies = this.findPieByCommune(commune.get().getCode());
            pies = pies.stream().filter(pie -> pie.getStatut().getCode().equals(codeStatut)).collect(Collectors.toList());
            return pieMapper.asDTOList(pies);
        }
        return new ArrayList<>();
    }

    public boolean isCniPieAlreadyExist(String cni) {
        return pieRepository.findByCni(cni).isPresent();
    }

    @Transactional
    public PIE updatePIE(PIE pie) {
        return pieRepository.save(pie);
    }

    public List<PIE> findPieByCommune(String codeCommune) {
        List<PIE> piesSearchByCommune = new ArrayList<>();
        Optional<Commune> commune = communeRepository.findCommuneByCode(codeCommune);
        if (commune.isPresent()) {
            piesSearchByCommune.addAll(individuelRepository.findAllByCommune(commune.get()));
            piesSearchByCommune.addAll(collectifRepository.findAllByCommune(commune.get()));
        }
        return piesSearchByCommune;

    }

    private boolean communeAndParcours(FilterDto filterDto) {
        return !(filterDto.getCodeCommune().isBlank() && filterDto.getParcoursPie().isBlank());
    }

    private boolean communeAndSoutienList(FilterDto filterDto) {
        return !(filterDto.getCodeCommune().isBlank() && filterDto.getListSoutien().isEmpty());
    }

    private boolean parcoursAndSoutienList(FilterDto filterDto) {
        return !(filterDto.getParcoursPie().isBlank() && filterDto.getListSoutien().isEmpty());
    }
}
