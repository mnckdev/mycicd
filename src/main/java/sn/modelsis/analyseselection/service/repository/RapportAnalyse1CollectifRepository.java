package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import sn.modelsis.analyseselection.service.entity.RapportAnalyse1Collectif;

import java.util.UUID;

public interface RapportAnalyse1CollectifRepository extends JpaRepository<RapportAnalyse1Collectif, UUID> {
}
