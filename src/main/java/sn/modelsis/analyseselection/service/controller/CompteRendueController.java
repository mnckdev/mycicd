package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.CompteRendue;
import sn.modelsis.analyseselection.service.service.CompteRendueService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/compteRendue")
@Api(
        value = "CompteRendue  REST Endpoint",
        tags = {"CompteRendue  Controller (v1)"}
)

public class CompteRendueController {


    private final CompteRendueService compteRendueService;

    public CompteRendueController(CompteRendueService compteRendueService) {
        this.compteRendueService = compteRendueService;
    }

    @ApiOperation(value = "find All Compte Rendue", notes = "find all  Compte Rendue")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<CompteRendue>> findAll(){
        List<CompteRendue> compteRendue = compteRendueService.findAll();
        return new ResponseEntity<List<CompteRendue>>(compteRendue, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID Compte Rendue", notes = "find By ID  Compte Rendue")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })


    @GetMapping("/{id}")
    public ResponseEntity<Optional<CompteRendue>> findById(@PathVariable Integer id){
        return new ResponseEntity<Optional<CompteRendue>>(compteRendueService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "create  new  Compte Rendues", notes = "create a new Compte Rendues")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @PostMapping
    public ResponseEntity<CompteRendue> newCompteRendue(@RequestBody CompteRendue CompteRendue) {
        return new ResponseEntity<CompteRendue>(compteRendueService.newCompteRendue(CompteRendue), HttpStatus.OK);
    }

    @ApiOperation(value = "delete  Compte Rendues", notes = "delete  Compte Rendues")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id) {
        compteRendueService.deleteCompteRendue(id);
        return new ResponseEntity<>("deleted successfully ...",HttpStatus.OK);
    }

}
