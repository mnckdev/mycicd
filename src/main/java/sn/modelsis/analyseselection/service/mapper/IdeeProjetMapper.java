package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.IdeeProjetDTO;
import sn.modelsis.analyseselection.service.entity.IdeeProjet;

@Mapper(componentModel = "spring")
public interface IdeeProjetMapper extends GenericMapper<IdeeProjet, IdeeProjetDTO>{
}
