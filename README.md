## Running your microservice analyseEtSelection locally

In this file are mentioned the steps to run the analyseEtSelection project with intelliJ.

---
## Requirements #

* You should have Java 11 .
* Clone the branch  that  
* Pull the latest changes with the correct branch you need to use.

---
## Setting up pgAdmin

* Download and install pgAdmin
* Server → Create Server
* Connection → Set the host name as localhost
    - Username : adel and password : Test123
    - Click Save
* Add databases:
    - Verify that under Login/Group Roles → foncier_api → right click Properties → Privileges, you
      have the right to create roles and databases. If not, change the No to a Yes and click Save.
    - right click Databases and click create → Database. Create a databases with the name :  
    - adel

---
## Deploy analyseEtSelection  locally with IntelliJ

* Download and install IntelliJ as your IDE
* Open analyseEtSelection as a project.
* Setting code styling. In Inteliji go to Preferences > Editor > Code Style > java
    - In Scheme click Show scheme actions > import scheme > Inteliji IDEA code Style XML and select
      the intellij-java-google-style.xml located in the analyseEtSelection folder
* Download the appropriate JDK 11 based on your OS and run the executable
    - In Intellij go to Project Structure →  Project Settings > Project
    - Click on New, select JDK and select the appropriate folder, e.g.
      C:\Program Files\Java\jdk 11
    - In the Project language level, select: 11. Lambda, type annotations etc
* Get Lombok
    - In Intellij go to File → Settings → Plugins → MarketPlace, search Lombok and click on Install

* To run the project in IntelliJ do right click on the  FoncierApiApplication and click debug
    - this time the debug is going to fail because you need to add the environment variables
* Add environment variables to IntelliJ (Run → Edit Configurations & look for the environment variables).


