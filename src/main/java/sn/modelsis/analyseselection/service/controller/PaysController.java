package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import sn.modelsis.analyseselection.service.entity.Pays;
import sn.modelsis.analyseselection.service.service.PaysService;

import java.util.List;
import java.util.Optional;
@RestController
@RequestMapping("/api/v1/pays")

public class PaysController {


    private final PaysService paysService;

    public PaysController(PaysService paysService) {
        this.paysService = paysService;
    }

    @ApiOperation(value = "find All Pays", notes = "find all  Pays")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<Pays>> findAll(){
        List<Pays> pays = paysService.findAll();
        return new ResponseEntity<List<Pays>>(pays, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID Pays", notes = "find By ID  Pays")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<Pays>> findById(@PathVariable Integer id){
        return new ResponseEntity<Optional<Pays>>(paysService.findById(id), HttpStatus.OK);
    }







}
