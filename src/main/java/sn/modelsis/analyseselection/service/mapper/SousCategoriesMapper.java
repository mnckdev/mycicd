package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.SousCategoriesDTO;
import sn.modelsis.analyseselection.service.entity.SousCategories;


@Mapper(componentModel = "spring")
public interface SousCategoriesMapper extends GenericMapper<SousCategories, SousCategoriesDTO> {
}
