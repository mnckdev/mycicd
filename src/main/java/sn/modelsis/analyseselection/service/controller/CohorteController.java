package sn.modelsis.analyseselection.service.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sn.modelsis.analyseselection.service.DTO.CohorteDTO;
import sn.modelsis.analyseselection.service.service.CohorteService;

@RestController
@RequestMapping("/api/v1/cohorte")

public class CohorteController {

    private final CohorteService cohorteService;

    public CohorteController(CohorteService cohorteService) {
        this.cohorteService = cohorteService;
    }

    @PostMapping("/start")
    public ResponseEntity<CohorteDTO> startCohorte(CohorteDTO cohorte){
        return ResponseEntity.ok().body(cohorteService.startNewCohorte(cohorte));
    }

    @PostMapping("/end")
    public ResponseEntity<CohorteDTO> endCohorte(CohorteDTO cohorte){
        return ResponseEntity.ok().body(cohorteService.endCohorte(cohorte));
    }

}
