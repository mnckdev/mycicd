package sn.modelsis.analyseselection.service.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.FederatedIdentityRepresentation;
import org.keycloak.representations.idm.SocialLinkRepresentation;
import org.keycloak.representations.idm.UserConsentRepresentation;

import java.util.List;
import java.util.Map;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter


public class UserRepresentationDto {
    private String self;
    private String id;
    private String origin;
    private Long createdTimestamp;
    private String username;
    private Boolean enabled;
    private Boolean totp;
    private Boolean emailVerified;
    private String firstName;
    private String lastName;
    private String email;
    private String federationLink;
    private String serviceAccountClientId;

    private Map<String, List<String>> attributes;
    private List<CredentialRepresentation> credentials;
    private Set<String> disableableCredentialTypes;
    private List<String> requiredActions;
    private List<FederatedIdentityRepresentation> federatedIdentities;
    private List<String> realmRoles;
    private Map<String, List<String>> clientRoles;
    private List<UserConsentRepresentation> clientConsents;
    private Integer notBefore;
    private Map<String, List<String>> applicationRoles;
    private List<SocialLinkRepresentation> socialLinks;
    private List<String> groups;
    private Map<String, Boolean> access;
}