package sn.modelsis.analyseselection.service.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserDTO {

    private String id;

    private String username;

    private String firstName;

    private String lastName;
    private String password;
    private String email;
    private Boolean enabled;
    private List<String> roles;
    private Map<String, List<String>> attributes;
}
