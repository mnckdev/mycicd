package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.Commune;

import java.util.Optional;

@Repository
public interface CommuneRepository extends JpaRepository<Commune, Integer> {

Optional<Commune> findCommuneByCode(String code);
}
