package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.NoteQuestionDTO;
import sn.modelsis.analyseselection.service.entity.NoteQuestion;

@Mapper(componentModel = "spring")
public interface NoteQuestionMapper extends GenericMapper<NoteQuestion, NoteQuestionDTO>{
}
