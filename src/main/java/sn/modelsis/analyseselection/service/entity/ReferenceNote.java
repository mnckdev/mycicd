package sn.modelsis.analyseselection.service.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class ReferenceNote {

    @Id
    @Column(name = "id_question")
    private Integer idReferenceNote;

    private String reponse;

    private int note;

    @ManyToOne
    @JsonIgnore
    private Questionnaire question;
}
