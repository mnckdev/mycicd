package sn.modelsis.analyseselection.service.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.TypeDocument;
import sn.modelsis.analyseselection.service.service.TypeDocumentService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/typeDocument")
@Api(
        value = "TypeDocument  REST Endpoint",
        tags = {"TypeDocument  Controller (v1)"}
)

public class TypeDocumentController {


    private final TypeDocumentService typeDocumentService;

    public TypeDocumentController(TypeDocumentService typeDocumentService) {
        this.typeDocumentService = typeDocumentService;
    }

    @ApiOperation(value = "find All Type Document", notes = "find all  Type Document")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })


    @GetMapping
    public ResponseEntity<List<TypeDocument>> findAll(){
        List<TypeDocument> typeDocument = typeDocumentService.findAll();
        return new ResponseEntity<List<TypeDocument>>(typeDocument, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID Type Document", notes = "find By ID  Type Document")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping("/{id}")
    public ResponseEntity<Optional<TypeDocument>> findById(@PathVariable Integer id){
        return new ResponseEntity<Optional<TypeDocument>>(typeDocumentService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "create  new  Type Documents", notes = "create a new Type Documents")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @PostMapping
    public ResponseEntity<TypeDocument> newTypeDocument(@RequestBody TypeDocument typeDocument) {
        return new ResponseEntity<TypeDocument>(typeDocumentService.newTypeDocument(typeDocument), HttpStatus.OK);
    }

    @ApiOperation(value = "delete  Type Documents", notes = "delete  Type Documents")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id) {
        typeDocumentService.deleteTypeDocument(id);
        return new ResponseEntity<>("deleted successfully ...",HttpStatus.OK);
    }

}
