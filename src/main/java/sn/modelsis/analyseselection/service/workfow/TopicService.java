package sn.modelsis.analyseselection.service.workfow;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.client.task.ExternalTask;
import org.camunda.bpm.client.task.ExternalTaskService;
import org.camunda.bpm.client.variable.ClientValues;
import org.passay.CharacterData;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordGenerator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import sn.modelsis.analyseselection.service.DTO.UserDTO;
import sn.modelsis.analyseselection.service.DTO.externalDTO.SmsDTO;
import sn.modelsis.analyseselection.service.DTO.externalDTO.realtimeNotification.Destination;
import sn.modelsis.analyseselection.service.DTO.externalDTO.realtimeNotification.Message;
import sn.modelsis.analyseselection.service.mapper.CohorteMapper;
import sn.modelsis.analyseselection.service.repository.CollectifRepository;
import sn.modelsis.analyseselection.service.repository.IndividuelRepository;
import sn.modelsis.analyseselection.service.service.*;
import sn.modelsis.analyseselection.service.utils.Constantes;
import sn.modelsis.analyseselection.service.entity.*;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class TopicService {

    private final PIEService pieService;
    private final IndividuelRepository individuelRepository;
    private final CollectifRepository collectifRepository;
    private final CohorteService cohorteService;
    private final CohorteMapper cohorteMapper;

    private final StatutService statutService;

    private final KeycloakService keycloakService;

    @Value("${spring.url.realtime.notification}")
    private String url;
    private final NotificationService notificationService;

    private static final ObjectMapper mapper = new ObjectMapper();

    public TopicService(PIEService pieService,
                        IndividuelRepository individuelRepository,
                        CollectifRepository collectifRepository,
                        CohorteService cohorteService,
                        CohorteMapper cohorteMapper,
                        StatutService statutService, KeycloakService keycloakService, NotificationService notificationService) {
        this.pieService = pieService;
        this.individuelRepository = individuelRepository;
        this.collectifRepository = collectifRepository;
        this.cohorteService = cohorteService;
        this.cohorteMapper = cohorteMapper;
        this.statutService = statutService;
        this.keycloakService = keycloakService;
        this.notificationService = notificationService;
    }

    @Transactional
    protected void persistance_et_mis_a_jour_pie(
            ExternalTask externalTask,
            ExternalTaskService externalTaskService) throws JsonProcessingException {
        Map<String, Object> variables = externalTask.getAllVariablesTyped();
        String pieJson = externalTask.getVariable("pie");
        Individuel individuel;
        Collectif collectif;
        if(pieJson.contains(Constantes.CREATEUR) || pieJson.contains(Constantes.DEVELOPPEUR)){
            individuel = mapper.readValue(pieJson, Individuel.class);
            individuel = individuelRepository.save(individuel);
            variables.put("pie", ClientValues.jsonValue(
                    mapper.writerWithDefaultPrettyPrinter()
                            .writeValueAsString(individuel)
            ));
            externalTaskService.complete(externalTask, variables);
        }
        else if(pieJson.contains(Constantes.COLLECTIF))
        {
            collectif = mapper.readValue(pieJson, Collectif.class);
            collectif = collectifRepository.save(collectif);
            variables.put("pie", ClientValues.jsonValue(
                    mapper.writerWithDefaultPrettyPrinter()
                            .writeValueAsString(collectif)
            ));
            externalTaskService.complete(externalTask, variables);
        }
    }

    @Transactional
    protected void definitionVariableCommune(ExternalTask externalTask,
                                             ExternalTaskService externalTaskService)
            throws JsonProcessingException {
        Map<String, Object> variables = externalTask.getAllVariablesTyped();
        String pieJson = externalTask.getVariable("pie");
        if(pieJson.contains(Constantes.CREATEUR) || pieJson.contains(Constantes.DEVELOPPEUR)){
            Individuel individuel = mapper.readValue(pieJson, Individuel.class);
            variables.put("commune", individuel.getCommune().getLibelle());
            variables.put("zone", individuel
                    .getCommune()
                    .getDepartement()
                    .getRegion()
                    .getZone()
                    .getLibelle());
            externalTaskService.complete(externalTask, variables);
        }
        else if(pieJson.contains(Constantes.COLLECTIF))
        {
            Collectif collectif = mapper.readValue(pieJson, Collectif.class);
            variables.put("commune", collectif.getCommune().getLibelle());
            variables.put("zone", collectif
                    .getCommune()
                    .getDepartement()
                    .getRegion()
                    .getZone()
                    .getLibelle());
            externalTaskService.complete(externalTask, variables);
        }
    }

    @Transactional
    protected void recuperationListePIE(ExternalTask externalTask,
                                        ExternalTaskService externalTaskService) throws JsonProcessingException {

        String piesIdsJson = externalTask.getVariable("piesIds");
        String[] uuidStrings = mapper.readValue(piesIdsJson, String[].class);
        List<UUID> piesIds = new ArrayList<>();
        for (String uuidString : uuidStrings) {
            UUID uuid = UUID.fromString(uuidString);
            piesIds.add(uuid);
        }
        List<PIE>  pies =  pieService.getPieInIds(piesIds);

        Cohorte cohorte = cohorteMapper.asEntity(cohorteService.getByStatut(Constantes.COHORTE_ENCOURS));

        Statut statut = statutService.findByCode(Constantes.STATUS_A_NOTER).get();
        Statut statutObservation = statutService.findByCode(Constantes.STATUS_INITIALISE).get();

        pies = pies.stream().map(
                pie -> {
                    pie.setCohorte(cohorte);
                    pie.setStatut(statut);
                    pie.getObservations().add(new Observation(new Date(),"PIE issues de la BADEL","RPIE",statutObservation));
                    pieService.create( pie);
                    return pie;
                }
        ).collect(Collectors.toList());
        String jsonPies;
        try {
            jsonPies = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(pies);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        Map<String, Object> variables = externalTask.getAllVariablesTyped();
        variables.put("pies",ClientValues.jsonValue(jsonPies));
        externalTaskService.complete(externalTask ,variables);
    }

    @Transactional
    protected void executerCreationComptePIE(ExternalTask externalTask,
                                             ExternalTaskService externalTaskService)
            throws JsonProcessingException {
        //recuperer le task, les infos du PIE, son mail
        Map<String, Object> variables = externalTask.getAllVariablesTyped();
        String pieJson = externalTask.getVariable("pie");
        String email = externalTask.getVariable("email");
        Individuel individuel;
        Collectif collectif;
        UserDTO userDTO = new UserDTO();
        if(email==null)
            throw new RuntimeException();
        if(pieJson.contains(Constantes.CREATEUR) || pieJson.contains(Constantes.DEVELOPPEUR)){
            individuel = mapper.readValue(pieJson, Individuel.class);
            userDTO = setProperties(individuel);
        }
        else if(pieJson.contains(Constantes.COLLECTIF))
        {
            collectif = mapper.readValue(pieJson, Collectif.class);
            userDTO = setProperties(collectif);
        }
        userDTO.setEmail(email);
        userDTO.setUsername(email);
        userDTO.setRoles(List.of("adel-pie"));
        keycloakService.createUser(userDTO);
        variables.put("username", email);
        externalTaskService.complete(externalTask, variables);
    }

    private UserDTO setProperties(PIE pie){
        UserDTO userDTO  =new UserDTO();
        userDTO.setLastName(pie.getNom());
        userDTO.setFirstName(pie.getPrenom());
        userDTO.setLastName(pie.getNom());

        Map<String, List<String>> attributes = new HashMap<>();
        List<String> codeA = new ArrayList<>();
        codeA.add(generateCode());
        List<String> rolesAtt = new ArrayList<>();
        rolesAtt.add("PIE");
        attributes.put("code-pin", codeA);
        attributes.put("adel-role", rolesAtt);
        List<String> phoneNumber = new ArrayList<>();
        phoneNumber.add("+221" + pie.getTel1());
        attributes.put("phoneNumber", phoneNumber);

        userDTO.setAttributes(attributes);
        userDTO.setRoles(rolesAtt);

        //user.setAttributes(attributes);
        return userDTO;
    }

    private String generateCode() {
        CharacterRule characterRule = new CharacterRule(new CharacterData() {
            @Override
            public String getErrorCode() {
                return "SAMPLE_ERROR_CODE";
            }

            @Override
            public String getCharacters() {
                return EnglishCharacterData.Digit.getCharacters();
            }
        });
        PasswordGenerator passwordGenerator = new PasswordGenerator();
        String code = passwordGenerator.generatePassword(6, characterRule);
        return code;
    }

    @Transactional
    protected void executerNotifierActeursRencontre(ExternalTask externalTask,
                                                    ExternalTaskService externalTaskService)
            throws JsonProcessingException {
        Map<String, Object> variables = externalTask.getAllVariablesTyped();
        //Recuperer la variable PIE
        String pieJson = externalTask.getVariable("pie");
        String zone = externalTask.getVariable("zone");
        String commune = externalTask.getVariable("commune");
        PIE pie = new PIE();
        if(pieJson.contains(Constantes.CREATEUR) || pieJson.contains(Constantes.DEVELOPPEUR)){
            pie = mapper.readValue(pieJson, Individuel.class);
        }
        else if(pieJson.contains(Constantes.COLLECTIF))
        {
            pie = mapper.readValue(pieJson, Collectif.class);
        }
        //Recuperer le numero de telephone
        String numero = pie.getTel1();
        //Recuperer la derniere rencontre
        Rencontre rencontre = pie.getRencontres()
                .get(
                        pie.getRencontres().size()-1
                );
        //Setter le dto pour l'envoie d'sms
//        String date_rencontre = rencontre.getDate().toString();
//        String heure_rencontre = rencontre.getHeure().toString();
        int moisI = rencontre.getDate().getMonth()+1;
        String mois = "";
        if(moisI<10)
            mois = "0"+moisI;
        else
            mois = mois+moisI;
        int year = rencontre.getDate().getYear()+1900;
        String message = "Bonjour, votre rencontre avec ADEL est "+
                "prevue le "+rencontre.getDate().getDate()+"/" +
                mois+"/"+year+
                " a "+ rencontre.getHeure().toString();
        numero = "+221"+numero
                .replace("-", "")
                .replace(" ", "");
        SmsDTO sms = new SmsDTO(
                numero,
                message,
                "ADEL APP"
        );
        //Envoyer sms au pie
        notificationService.envoyerSMS(sms);
            //contacter le microservice de notification
            //envoyer le message*
        log.info(sms.toString());
        //envoyer des notifications aux dets et à l'aer
            //Recuperer ces users depuis Keclock
        List<UserDTO> dets = keycloakService
                .getUserByAttributes("adel-zone:\""+zone+"\"");
        List<UserDTO> aers = keycloakService
                .getUserByAttributes("adel-commune:\""+commune+"\"");
        Set<String> users = new HashSet<>();
        String messageToAppUsers = "La rencontre avec le PIE " +
                pie.getPrenom()+" " + pie.getNom() + " est prévue le " +
                rencontre.getDate().getDate()+"/" +
                mois+"/"+year+
                " a "+ rencontre.getHeure().toString() +". Son numero de telephone est le " +
                pie.getTel1();
            //Pour chaque user, envoyer un message avec le microservice de notifications
        for (UserDTO user: dets
             ) {
            users.add(user.getUsername());
        }
        for (UserDTO user: aers
        ) {
            users.add(user.getUsername());
        }
        //Construction de message
        Message message1 = new Message();
        message1.setCodeApp("ADEL");
        message1.setContenu(messageToAppUsers);
        Destination destination = new Destination();
        destination.setUsers(users);
        Set<String> roles = new HashSet<>();
        destination.setRoles(roles);
        message1.setDestinataire(destination);
        message1.setDateCreation(new Date());
        message1.setExpediteur("ADEL APP");
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Message> request1 = new HttpEntity<>(message1);
        restTemplate
                .postForObject(
                        "http://localhost:8092/message",
                        request1,
                        String.class
                );
        externalTaskService.complete(externalTask, variables);
    }

    private PIE fromJSonToPIEObject(String jsonpie)
    {
        return null;
    }

}

