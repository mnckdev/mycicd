package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.IdeeProjet;
import sn.modelsis.analyseselection.service.repository.IdeeProjetRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class IdeeProjetService {

    private final IdeeProjetRepository ideeProjetRepository;

    public IdeeProjetService(IdeeProjetRepository ideeProjetRepository) {
        this.ideeProjetRepository = ideeProjetRepository;
    }

    public IdeeProjet create(IdeeProjet ideeProjet){
        if (ideeProjet == null)
            return null;
        return ideeProjetRepository.save(ideeProjet);
    }

    public Optional<IdeeProjet> read(UUID uuid){
        if(uuid == null)
            return null;
        return  ideeProjetRepository.findById(uuid);
    }

    public List<IdeeProjet> read(){
        return ideeProjetRepository.findAll();
    }

    public void delete(UUID uuid){
        Optional<IdeeProjet> ideeProjet = ideeProjetRepository.findById(uuid);
        if(ideeProjet.isPresent())
            ideeProjetRepository.delete(ideeProjet.get());
    }
}
