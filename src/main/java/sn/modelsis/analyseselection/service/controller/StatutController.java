package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.Statut;
import sn.modelsis.analyseselection.service.service.StatutService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/status")
@Api(
        value = "Status  REST Endpoint",
        tags = {"Status  Controller (v1)"}
)

public class StatutController {


    private final StatutService statutService;

    public StatutController(StatutService statutService) {
        this.statutService = statutService;
    }

    @ApiOperation(value = "find All Statut", notes = "find all  Statut")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<Statut>> findAll(){
        List<Statut> statut = statutService.findAll();
        return new ResponseEntity<List<Statut>>(statut, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID Statut", notes = "find By ID  Statut")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Statut>> findById(@PathVariable Integer id){
        return new ResponseEntity<Optional<Statut>>(statutService.findById(id), HttpStatus.OK);
    }
    @ApiOperation(value = "create  new  Statuts", notes = "create a new Statuts")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @PostMapping
    public ResponseEntity<Statut> newStatut(@RequestBody Statut statut) {
        return new ResponseEntity<Statut>(statutService.newStatut(statut), HttpStatus.OK);
    }

    @ApiOperation(value = "delete  Statuts", notes = "delete  Statuts")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id) {
        statutService.deleteStatut(id);
        return new ResponseEntity<>("deleted successfully ...",HttpStatus.OK);
    }


}
