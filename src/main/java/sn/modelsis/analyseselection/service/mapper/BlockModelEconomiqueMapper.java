package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.BlockModelEconomiqueDTO;
import sn.modelsis.analyseselection.service.entity.BlockModelEconomique;

@Mapper(componentModel = "spring")
public interface BlockModelEconomiqueMapper extends GenericMapper<BlockModelEconomique, BlockModelEconomiqueDTO>{
}
