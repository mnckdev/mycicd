package sn.modelsis.analyseselection.service.DTO.externalDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SmsDTO {
    private  String phoneNumber;
    private  String message;
    private  String nameService;
}
