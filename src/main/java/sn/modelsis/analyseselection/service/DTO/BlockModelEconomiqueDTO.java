package sn.modelsis.analyseselection.service.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import sn.modelsis.analyseselection.service.entity.TypeBloc;

import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BlockModelEconomiqueDTO {

    private UUID idBlocME;

    private String libelle;

    private String code;

    private List<TypeBloc> typeBloc;

    private List<SousBlocDTO> sousBlocs;

}
