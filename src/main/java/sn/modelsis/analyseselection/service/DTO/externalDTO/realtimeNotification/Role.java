package sn.modelsis.analyseselection.service.DTO.externalDTO.realtimeNotification;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Role {
    private Long id ;
    private String libelle;
    private String description;
}
