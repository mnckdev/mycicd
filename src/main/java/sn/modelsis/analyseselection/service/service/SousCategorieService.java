package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.SousCategories;
import sn.modelsis.analyseselection.service.repository.SousCategoriesRepository;

import java.util.List;
import java.util.Optional;

@Service
public class SousCategorieService {

    private SousCategoriesRepository sousCategorieRepository;

    public SousCategorieService(SousCategoriesRepository sousCategorieRepository){
        this.sousCategorieRepository=sousCategorieRepository;
    }


    public Optional<SousCategories> findById(Integer id){
        return sousCategorieRepository.findById(id);
    }
    public List<SousCategories> findAll(){
        return sousCategorieRepository.findAll();
    }

}
