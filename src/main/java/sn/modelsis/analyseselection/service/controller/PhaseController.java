package sn.modelsis.analyseselection.service.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.Phase;
import sn.modelsis.analyseselection.service.service.PhaseService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/phase")
@Api(
        value = "Phase REST endpoint",
        tags = "Phase controller(v1)"
)

public class PhaseController {

    private final PhaseService phaseService;

    public PhaseController(PhaseService phaseService) {
        this.phaseService = phaseService;
    }

    @GetMapping
    @ApiOperation(value = "Get all phases", notes = "Get all phases")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    public ResponseEntity<List<Phase>> findAll(){
        List<Phase> phases = phaseService.read();
        if(phases == null)
            return ResponseEntity.badRequest().body(phases);
        return ResponseEntity.ok().body(phases);
    }

    @ApiOperation(value = "Get one phase", notes = "Get one phase")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<Phase>> get(@PathVariable Integer id){
        Optional<Phase> phase = phaseService.read(id);
        if(phase== null)
            return  ResponseEntity.badRequest().body(phase);
        return ResponseEntity.ok().body(phase);
    }

    @PostMapping
    @ApiResponses(
            {
                    @ApiResponse(code=200, message = "created"),
            }
    )
    @ApiOperation(value = "post a phase", notes = "post a phase")
    // Devrait changer le retour de ce controller
    public ResponseEntity<Phase> create(@RequestBody Phase phase){
        phase = phaseService.create(phase);
        return ResponseEntity.ok().body(phase);
    }

    @DeleteMapping("/{id}")
    @ApiResponses(
            {
                    @ApiResponse(code=200, message = "delete"),
            }
    )
    @ApiOperation(value = "delete a phase", notes = "delete a  phase")
    public ResponseEntity<String> delete(@PathVariable Integer id){
        phaseService.delete(id);
        return ResponseEntity.ok().body("deleted");
    }
}
