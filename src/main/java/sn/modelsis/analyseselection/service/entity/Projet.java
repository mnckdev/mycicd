package sn.modelsis.analyseselection.service.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Projet {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private UUID idProjet;

    private String description;

    private String informationSupplementaire;

    private String intituleProjet;

    private String motifRetour;

    private Integer objet;
    @ManyToOne
    private SecteurActivite secteurActivite;

    @ManyToOne
    private TypeProjet typeProjet;

    @OneToMany(mappedBy = "projet")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Documents> documents;

    @OneToOne(optional = true, cascade = CascadeType.ALL)
    private ModelEconomique modelEconomique;

//    @OneToMany(mappedBy = "projet")
//    @JsonIgnore
//    private List<Action> action;

    @ManyToOne
    @JsonIgnore
    private Statut statut;
    

}
