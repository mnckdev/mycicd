package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.Documents;
import sn.modelsis.analyseselection.service.service.DocumentsService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/documents")
@Api(
        value = "Documents  REST Endpoint",
        tags = {"Documents  Controller (v1)"}
)

public class DocumentsController {

    private final DocumentsService documentsService;

    public DocumentsController(DocumentsService documentsService) {
        this.documentsService = documentsService;
    }


    @ApiOperation(value = "find All Document", notes = "find all  Document")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<Documents>> findAll(){
        List<Documents> documentsServices = documentsService.findAll();
        return new ResponseEntity<List<Documents>>(documentsServices, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID Document", notes = "find By ID  Document")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Documents>> findById(@PathVariable UUID uuid){
        return new ResponseEntity<Optional<Documents>>(documentsService.findById(uuid), HttpStatus.OK);
    }
    @ApiOperation(value = "create  new  Documents", notes = "create a new Documents")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })
    @PostMapping
    public ResponseEntity<Documents> newDocuments(@RequestBody Documents Documents) {
        return new ResponseEntity<Documents>(documentsService.newDocuments(Documents), HttpStatus.OK);
    }
    @ApiOperation(value = "delete  Documents", notes = "delete  Documents")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable UUID uuid) {
        documentsService.deleteDocuments(uuid);
        return new ResponseEntity<>("deleted successfully ...",HttpStatus.OK);
    }

}
