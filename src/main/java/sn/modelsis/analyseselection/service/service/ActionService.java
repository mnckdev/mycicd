package sn.modelsis.analyseselection.service.service;


import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.Action;
import sn.modelsis.analyseselection.service.repository.ActionRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ActionService {


    private final ActionRepository actionRepository;


    public ActionService(
            ActionRepository actionRepository){

        this.actionRepository=actionRepository;
    }

    public Action newAction(Action action){
        return  actionRepository.save(action);
    }

    public Optional<Action> findById(Integer id){
        return actionRepository.findById(id);
    }
    public List<Action> findAll(){
        return actionRepository.findAll();
    }
    public void deleteAction(Integer id){
        actionRepository.deleteById(id);
    }



}
