package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.Parcours;
import sn.modelsis.analyseselection.service.repository.ParcoursRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ParcoursService {

    private final ParcoursRepository parcoursRepository;

    public ParcoursService(ParcoursRepository parcoursRepository){
        this.parcoursRepository=parcoursRepository;
    }


    public Optional<Parcours> findById(Integer id){
        return parcoursRepository.findById(id);
    }
    public List<Parcours> findAll(){
        return parcoursRepository.findAll();
    }

}
