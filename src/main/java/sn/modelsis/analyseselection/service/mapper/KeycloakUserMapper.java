package sn.modelsis.analyseselection.service.mapper;

import org.keycloak.representations.idm.UserRepresentation;
import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.UserDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface KeycloakUserMapper{

    public UserDTO toUserDTO(UserRepresentation userRepresentation);
    public UserRepresentation toUserRepresentation (UserDTO userDTO);

    public List<UserDTO> toUserDTOList(List<UserRepresentation> userRepresentations);
}
