package sn.modelsis.analyseselection.service.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Region {

    @Id
    private Integer id;

    private String libelle;

    private String code;

    @ManyToOne
    private Pays pays;

    @ManyToOne
    private Zone zone;

    @OneToMany(
            mappedBy = "region"
    )
    @JsonIgnore
    private List<Departement> departements;
}
