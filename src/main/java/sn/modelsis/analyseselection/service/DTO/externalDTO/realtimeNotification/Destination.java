package sn.modelsis.analyseselection.service.DTO.externalDTO.realtimeNotification;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Destination {

    private Set<String> users ;
    private Set<String> roles ;
}
