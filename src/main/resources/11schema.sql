--
-- PostgreSQL database dump
--

-- Dumped from database version 14.8
-- Dumped by pg_dump version 15.3

-- Started on 2023-09-19 09:49:00

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

---DROP DATABASE "analyse-selection";
--
-- TOC entry 3762 (class 1262 OID 90740)
-- Name: analyse-selection; Type: DATABASE; Schema: -; Owner: adel
--

--CREATE DATABASE "analyse-selection" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE_PROVIDER = libc LOCALE = 'en_US.utf8';


--ALTER DATABASE "analyse-selection" OWNER TO adel;

--- connect -reuse-previous=on "dbname='analyse-selection'"

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 5 (class 2615 OID 90741)
-- Name: portal; Type: SCHEMA; Schema: -; Owner: adel
--

--CREATE SCHEMA portal;


-- ALTER SCHEMA portal OWNER TO adel;

--
-- TOC entry 6 (class 2615 OID 127366)
-- Name: public; Type: SCHEMA; Schema: -; Owner: adel
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO adel;

--
-- TOC entry 3763 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: adel
--

COMMENT ON SCHEMA public IS '';



SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 210 (class 1259 OID 90754)
-- Name: document; Type: TABLE; Schema: portal; Owner: adel
--
--
-- CREATE TABLE IF NOT EXISTS portal.document (
--                                  uuid uuid NOT NULL,
--                                  description character varying(255),
--                                  nature_action character varying(255),
--                                  projet_uuid uuid,
--                                  utilisateur_uuid uuid
-- );


-- ALTER TABLE portal.document OWNER TO adel;

--
-- TOC entry 211 (class 1259 OID 127368)
-- Name: action; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.action (
                               id_action uuid NOT NULL  PRIMARY KEY ,
                               description character varying(255),
                               m_projet character varying(255),
                               nature_action character varying(255),
                               projet_id_projet uuid,
                               utilisateurs_id_utilisateur integer
);


ALTER TABLE public.action OWNER TO adel;

--
-- TOC entry 212 (class 1259 OID 127373)
-- Name: aer; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.aer (
                            id integer NOT NULL,
                            prenom character varying(1000) NOT NULL,
                            nom character varying(1000) NOT NULL,
                            sexe character varying(500) NOT NULL,
                            tel1 character varying(1000) NOT NULL,
                            tel2 character varying(1000) NOT NULL,
                            email character varying(1000) NOT NULL,
                            commune character varying(1000) NOT NULL,
                            role integer NOT NULL,
                            structure_partenaire character varying(1000)
);


ALTER TABLE public.aer OWNER TO adel;

--
-- TOC entry 213 (class 1259 OID 127378)
-- Name: block_model_economique; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.block_model_economique (
                                               id_blocme uuid NOT NULL   PRIMARY KEY ,
                                               code character varying(255),
                                               libelle character varying(255)
);


ALTER TABLE public.block_model_economique OWNER TO adel;

--
-- TOC entry 214 (class 1259 OID 127383)
-- Name: block_model_economique_sous_blocs; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.block_model_economique_sous_blocs (
                                                          block_model_economique_id_blocme uuid NOT NULL  ,
                                                          sous_blocs_id_sous_bloc uuid NOT NULL
);


ALTER TABLE public.block_model_economique_sous_blocs OWNER TO adel;

--
-- TOC entry 215 (class 1259 OID 127386)
-- Name: categories; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.categories (
                                   id integer NOT NULL,
                                   libelle character varying(255)
);


ALTER TABLE public.categories OWNER TO adel;

--
-- TOC entry 216 (class 1259 OID 127389)
-- Name: cohorte; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.cohorte (
                                id_cohorte integer NOT NULL,
                                date_debut date,
                                date_fin date,
                                process_instance_id character varying(255),
                                statut character varying(255)
);


ALTER TABLE public.cohorte OWNER TO adel;

--
-- TOC entry 217 (class 1259 OID 127394)
-- Name: cohorte_id_cohorte_seq; Type: SEQUENCE; Schema: public; Owner: adel
--

CREATE SEQUENCE  IF NOT  EXISTS public.cohorte_id_cohorte_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cohorte_id_cohorte_seq OWNER TO adel;

--
-- TOC entry 3765 (class 0 OID 0)
-- Dependencies: 217
-- Name: cohorte_id_cohorte_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: adel
--

ALTER SEQUENCE public.cohorte_id_cohorte_seq OWNED BY public.cohorte.id_cohorte;


--
-- TOC entry 218 (class 1259 OID 127395)
-- Name: collectif; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.collectif (
                                  commune_interieur character varying(255),
                                  contact1_representant character varying(255),
                                  contact2_representant character varying(255),
                                  date_creation date,
                                  denomination character varying(255),
                                  quartier_village_interieur character varying(255),
                                  reconnaissance_juridique character varying(255),
                                  titre character varying(255),
                                  total_membre integer,
                                  total_femmes integer,
                                  total_homme integer,
                                  commune_id_commune integer,
                                  forme_juridique_id_forme_juridique integer,
                                  idpie uuid NOT NULL   PRIMARY KEY ,
                                  contact1representant character varying(255),
                                  contact2representant character varying(255),
                                  rapport_analyse1collectif_id uuid,
                                  rapport_analyse2collectif_id uuid
);


ALTER TABLE public.collectif OWNER TO adel;

--
-- TOC entry 219 (class 1259 OID 127400)
-- Name: commune; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.commune (
                                id_commune integer NOT NULL,
                                libelle character varying(255),
                                departement_id_departement integer
);


ALTER TABLE public.commune OWNER TO adel;

--
-- TOC entry 220 (class 1259 OID 127403)
-- Name: compte_rendue; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.compte_rendue (
                                      id_compte_rendue integer NOT NULL,
                                      cohorte character varying(255),
                                      commune character varying(255),
                                      id_document character varying(255),
                                      interventions_echanges character varying(2000),
                                      lieu character varying(255),
                                      nombre_idees integer NOT NULL,
                                      participants character varying(2000),
                                      periode date
);


ALTER TABLE public.compte_rendue OWNER TO adel;

--
-- TOC entry 221 (class 1259 OID 127408)
-- Name: compte_rendue_createurs_content; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.compte_rendue_createurs_content (
                                                        compte_rendue_id_compte_rendue integer NOT NULL,
                                                        createurs_content_id integer NOT NULL
);


ALTER TABLE public.compte_rendue_createurs_content OWNER TO adel;

--
-- TOC entry 222 (class 1259 OID 127411)
-- Name: compte_rendue_id_compte_rendue_seq; Type: SEQUENCE; Schema: public; Owner: adel
--

CREATE SEQUENCE  IF NOT  EXISTS public.compte_rendue_id_compte_rendue_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.compte_rendue_id_compte_rendue_seq OWNER TO adel;

--
-- TOC entry 3766 (class 0 OID 0)
-- Dependencies: 222
-- Name: compte_rendue_id_compte_rendue_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: adel
--

ALTER SEQUENCE public.compte_rendue_id_compte_rendue_seq OWNED BY public.compte_rendue.id_compte_rendue;


--
-- TOC entry 223 (class 1259 OID 127412)
-- Name: createurs_compte_rendue; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.createurs_compte_rendue (
                                                id integer NOT NULL,
                                                commentaires character varying(2000),
                                                idees_priorite_commune character varying(2000),
                                                idees_prioritepie character varying(2000),
                                                nom character varying(255),
                                                prenom character varying(255)
);


ALTER TABLE public.createurs_compte_rendue OWNER TO adel;

--
-- TOC entry 224 (class 1259 OID 127417)
-- Name: createurs_compte_rendue_id_seq; Type: SEQUENCE; Schema: public; Owner: adel
--

CREATE SEQUENCE  IF NOT  EXISTS public.createurs_compte_rendue_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.createurs_compte_rendue_id_seq OWNER TO adel;

--
-- TOC entry 3767 (class 0 OID 0)
-- Dependencies: 224
-- Name: createurs_compte_rendue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: adel
--

ALTER SEQUENCE public.createurs_compte_rendue_id_seq OWNED BY public.createurs_compte_rendue.id;


--
-- TOC entry 225 (class 1259 OID 127418)
-- Name: departement; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.departement (
                                    id_departement integer NOT NULL,
                                    libelle character varying(255),
                                    region_id_region integer
);


ALTER TABLE public.departement OWNER TO adel;

--
-- TOC entry 226 (class 1259 OID 127421)
-- Name: documents; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.documents (
                                  id_document uuid NOT NULL   PRIMARY KEY ,
                                  chemin character varying(255),
                                  date_creation timestamp without time zone,
                                  date_modification timestamp without time zone,
                                  etat_document boolean,
                                  nom_document integer,
                                  compte_rendue_id_compte_rendue integer,
                                  projet_id_projet uuid,
                                  type_document_id integer
);


ALTER TABLE public.documents OWNER TO adel;

--
-- TOC entry 227 (class 1259 OID 127424)
-- Name: forme_juridique; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.forme_juridique (
                                        id_forme_juridique integer NOT NULL,
                                        libelle character varying(255)
);


ALTER TABLE public.forme_juridique OWNER TO adel;

--
-- TOC entry 228 (class 1259 OID 127427)
-- Name: idee_projet; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.idee_projet (
                                    id_projet uuid NOT NULL   PRIMARY KEY ,
                                    description_projet character varying(255)
);


ALTER TABLE public.idee_projet OWNER TO adel;

--
-- TOC entry 229 (class 1259 OID 127430)
-- Name: individuel; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.individuel (
                                   experience_prof character varying(255),
                                   formation_prof character varying(255),
                                   niveau_etude character varying(255),
                                   idpie uuid NOT NULL   PRIMARY KEY ,
                                   categories_id integer,
                                   commune_id integer,
                                   commune_rattach_id_commune integer,
                                   soutien_immediat_id_soutien integer,
                                   tranche_age_id integer,
                                   type_categorie character varying(255),
                                   soutien_immediat_soutien integer,
                                   soutien_immediat_id integer,
                                   rapport_analyse1_id_rapport uuid,
                                   rapport_analyse2_id uuid
);


ALTER TABLE public.individuel OWNER TO adel;

--
-- TOC entry 230 (class 1259 OID 127435)
-- Name: individuel_idee_projets; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.individuel_idee_projets (
                                                individuel_idpie uuid NOT NULL,
                                                idee_projets_id_projet uuid NOT NULL
);


ALTER TABLE public.individuel_idee_projets OWNER TO adel;

--
-- TOC entry 231 (class 1259 OID 127438)
-- Name: marqueur; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.marqueur (
                                 id integer NOT NULL,
                                 libelle character varying(255),
                                 parcours_id integer
);


ALTER TABLE public.marqueur OWNER TO adel;

--
-- TOC entry 232 (class 1259 OID 127441)
-- Name: model_economique; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.model_economique (
                                         id uuid NOT NULL   PRIMARY KEY ,
                                         status character varying(255)
);


ALTER TABLE public.model_economique OWNER TO adel;

--
-- TOC entry 233 (class 1259 OID 127444)
-- Name: model_economique_block_model_economique; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.model_economique_block_model_economique (
                                                                model_economique_id uuid NOT NULL,
                                                                block_model_economique_id_blocme uuid NOT NULL
);


ALTER TABLE public.model_economique_block_model_economique OWNER TO adel;

--
-- TOC entry 234 (class 1259 OID 127447)
-- Name: note_question; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.note_question (
                                      id_note uuid NOT NULL   PRIMARY KEY ,
                                      note integer NOT NULL,
                                      reponse character varying(255),
                                      question_id_question integer
);


ALTER TABLE public.note_question OWNER TO adel;

--
-- TOC entry 235 (class 1259 OID 127450)
-- Name: observation; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.observation (
                                    id_retour integer NOT NULL,
                                    auteur character varying(255),
                                    date_retour date,
                                    observations character varying(255),
                                    statut_id integer,
                                    username_expediteur character varying(255)
);


ALTER TABLE public.observation OWNER TO adel;

--
-- TOC entry 236 (class 1259 OID 127455)
-- Name: observation_id_retour_seq; Type: SEQUENCE; Schema: public; Owner: adel
--

CREATE SEQUENCE  IF NOT  EXISTS public.observation_id_retour_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.observation_id_retour_seq OWNER TO adel;

--
-- TOC entry 3768 (class 0 OID 0)
-- Dependencies: 236
-- Name: observation_id_retour_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: adel
--

ALTER SEQUENCE public.observation_id_retour_seq OWNED BY public.observation.id_retour;


--
-- TOC entry 237 (class 1259 OID 127456)
-- Name: parcours; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.parcours (
                                 id integer NOT NULL,
                                 libelle character varying(255),
                                 sous_categories_id integer
);


ALTER TABLE public.parcours OWNER TO adel;

--
-- TOC entry 238 (class 1259 OID 127459)
-- Name: pays; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.pays (
                             id_pays integer NOT NULL,
                             libelle character varying(255)
);


ALTER TABLE public.pays OWNER TO adel;

--
-- TOC entry 239 (class 1259 OID 127462)
-- Name: phase; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.phase (
                              id_phase integer NOT NULL,
                              libelle character varying(255)
);


ALTER TABLE public.phase OWNER TO adel;

--
-- TOC entry 240 (class 1259 OID 127465)
-- Name: pie; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.pie (
                            idpie uuid NOT NULL   PRIMARY KEY ,
                            date timestamp without time zone,
                            nom character varying(255),
                            prenom character varying(255),
                            quartier character varying(255),
                            sexe character varying(255),
                            tel1 character varying(255),
                            tel2 character varying(255),
                            parcours character varying(255),
                            statut_id integer,
                            statut character varying(255),
                            typepie character varying(255),
                            inscritpar integer,
                            id_contrat_accompagnement character varying(255),
                            process_instance_id character varying(255),
                            variable_id character varying(255),
                            cohorte_id_cohorte integer,
                            projet_id_projet uuid
);


ALTER TABLE public.pie OWNER TO adel;

--
-- TOC entry 241 (class 1259 OID 127470)
-- Name: pie_note_questions; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.pie_note_questions (
                                           pie_idpie uuid NOT NULL,
                                           note_questions_id_note uuid NOT NULL
);


ALTER TABLE public.pie_note_questions OWNER TO adel;

--
-- TOC entry 242 (class 1259 OID 127473)
-- Name: pie_observations; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.pie_observations (
                                         pie_idpie uuid NOT NULL,
                                         observations_id_retour integer NOT NULL
);


ALTER TABLE public.pie_observations OWNER TO adel;

--
-- TOC entry 243 (class 1259 OID 127476)
-- Name: pie_rencontres; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.pie_rencontres (
                                       pie_idpie uuid NOT NULL,
                                       rencontres_id_rencontre uuid NOT NULL
);


ALTER TABLE public.pie_rencontres OWNER TO adel;

--
-- TOC entry 244 (class 1259 OID 127479)
-- Name: profil; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.profil (
                               id_profil integer NOT NULL,
                               libelle character varying(255),
                               utilisateurs_id_utilisateur integer
);


ALTER TABLE public.profil OWNER TO adel;

--
-- TOC entry 245 (class 1259 OID 127482)
-- Name: projet; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.projet (
                               id_projet uuid NOT NULL   PRIMARY KEY ,
                               description character varying(255),
                               information_supplementaire character varying(255),
                               intitule_projet character varying(255),
                               motif_retour character varying(255),
                               objet integer,
                               model_economique_id uuid,
                               secteur_activite_id integer,
                               statut_id integer,
                               type_projet_id integer
);


ALTER TABLE public.projet OWNER TO adel;

--
-- TOC entry 246 (class 1259 OID 127487)
-- Name: questionnaire; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.questionnaire (
                                      id_question integer NOT NULL,
                                      categorie character varying(255),
                                      code integer NOT NULL,
                                      libelle character varying(255),
                                      bloc character varying,
                                      phase_id_phase integer,
                                      id_synthese_notation integer
);


ALTER TABLE public.questionnaire OWNER TO adel;

--
-- TOC entry 247 (class 1259 OID 127492)
-- Name: rapport_analyse; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.rapport_analyse (
                                        id_rapport uuid NOT NULL   PRIMARY KEY ,
                                        activite character varying(2000),
                                        environnement character varying(2000),
                                        faiblesse character varying(2000),
                                        organisation character varying(2000),
                                        porteur character varying(2000)
);


ALTER TABLE public.rapport_analyse OWNER TO adel;

--
-- TOC entry 248 (class 1259 OID 127497)
-- Name: rapport_analyse1collectif; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.rapport_analyse1collectif (
                                                  id uuid NOT NULL   PRIMARY KEY ,
                                                  commentaire character varying(2000),
                                                  faiblesse character varying(2000),
                                                  fonctionnement character varying(2000),
                                                  gouvernance_leadership character varying(2000),
                                                  identification character varying(2000),
                                                  membership character varying(2000),
                                                  partenariat character varying(2000),
                                                  patrimoine_gestion character varying(2000)
);


ALTER TABLE public.rapport_analyse1collectif OWNER TO adel;

--
-- TOC entry 249 (class 1259 OID 127502)
-- Name: rapport_analyse2; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.rapport_analyse2 (
                                         id uuid NOT NULL   PRIMARY KEY ,
                                         faiblesse character varying(2000),
                                         fonctionnement character varying(2000),
                                         gestion character varying(2000),
                                         pratiques_manageriales character varying(2000),
                                         ressources_financieres character varying(2000)
);


ALTER TABLE public.rapport_analyse2 OWNER TO adel;

--
-- TOC entry 250 (class 1259 OID 127507)
-- Name: rapport_analyse2collectif; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.rapport_analyse2collectif (
                                                  id uuid NOT NULL   PRIMARY KEY ,
                                                  faiblesse character varying(2000),
                                                  finances_depenses character varying(2000),
                                                  finances_ressources character varying(2000),
                                                  fonctionnement character varying(2000),
                                                  modele_economique character varying(2000),
                                                  partenariat character varying(2000)
);


ALTER TABLE public.rapport_analyse2collectif OWNER TO adel;

--
-- TOC entry 251 (class 1259 OID 127512)
-- Name: reference_note; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.reference_note (
                                       id_question integer NOT NULL,
                                       note integer NOT NULL,
                                       reponse character varying(255),
                                       question_id_question integer
);


ALTER TABLE public.reference_note OWNER TO adel;

--
-- TOC entry 252 (class 1259 OID 127515)
-- Name: region; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.region (
                               id_region integer NOT NULL,
                               libelle character varying(255),
                               pays_id_pays integer,
                               zone_id_zone integer
);


ALTER TABLE public.region OWNER TO adel;

--
-- TOC entry 253 (class 1259 OID 127518)
-- Name: rencontre; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.rencontre (
                                  id_rencontre uuid NOT NULL   PRIMARY KEY ,
                                  commentaire character varying(255),
                                  date date,
                                  heure time without time zone,
                                  lieu character varying(255)
);


ALTER TABLE public.rencontre OWNER TO adel;

--
-- TOC entry 254 (class 1259 OID 127523)
-- Name: secteur_activite; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.secteur_activite (
                                         id integer NOT NULL,
                                         code character varying(255),
                                         libelle character varying(255)
);


ALTER TABLE public.secteur_activite OWNER TO adel;

--
-- TOC entry 255 (class 1259 OID 127528)
-- Name: sous_bloc; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.sous_bloc (
                                  id_sous_bloc uuid NOT NULL,
                                  valeur character varying(255)
);


ALTER TABLE public.sous_bloc OWNER TO adel;

--
-- TOC entry 256 (class 1259 OID 127531)
-- Name: sous_categories; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.sous_categories (
                                        id integer NOT NULL,
                                        libelle character varying(255),
                                        categories_id integer
);


ALTER TABLE public.sous_categories OWNER TO adel;

--
-- TOC entry 257 (class 1259 OID 127534)
-- Name: soutien; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.soutien (
                                id integer NOT NULL,
                                libelle character varying(255)
);


ALTER TABLE public.soutien OWNER TO adel;

--
-- TOC entry 258 (class 1259 OID 127537)
-- Name: statut; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.statut (
                               id integer NOT NULL,
                               code character varying(255),
                               libelle character varying(255)
);


ALTER TABLE public.statut OWNER TO adel;

--
-- TOC entry 259 (class 1259 OID 127542)
-- Name: synthese_notation; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.synthese_notation (
                                          id_synthese_notation integer NOT NULL,
                                          libelle character varying,
                                          note integer,
                                          categorie character varying
);


ALTER TABLE public.synthese_notation OWNER TO adel;

--
-- TOC entry 260 (class 1259 OID 127547)
-- Name: tranche_age; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.tranche_age (
                                    id integer NOT NULL,
                                    libelle character varying(255)
);


ALTER TABLE public.tranche_age OWNER TO adel;

--
-- TOC entry 261 (class 1259 OID 127550)
-- Name: type_bloc; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.type_bloc (
                                  id integer NOT NULL,
                                  libelle character varying(255),
                                  block_model_economique_id_blocme uuid
);


ALTER TABLE public.type_bloc OWNER TO adel;

--
-- TOC entry 262 (class 1259 OID 127553)
-- Name: type_document; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.type_document (
                                      id integer NOT NULL,
                                      code character varying(255),
                                      nom character varying(255)
);


ALTER TABLE public.type_document OWNER TO adel;

--
-- TOC entry 263 (class 1259 OID 127558)
-- Name: type_projet; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.type_projet (
                                    id integer NOT NULL,
                                    code character varying(255),
                                    libelle character varying(255)
);


ALTER TABLE public.type_projet OWNER TO adel;

--
-- TOC entry 264 (class 1259 OID 127563)
-- Name: utilisateurs; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.utilisateurs (
                                     id_utilisateur integer NOT NULL,
                                     login character varying(255),
                                     nom character varying(255),
                                     pass character varying(255),
                                     prenom character varying(255),
                                     sexe character varying(255),
                                     pie_idpie uuid
);


ALTER TABLE public.utilisateurs OWNER TO adel;

--
-- TOC entry 265 (class 1259 OID 127568)
-- Name: zone; Type: TABLE; Schema: public; Owner: adel
--

CREATE TABLE IF NOT EXISTS public.zone (
                             id_zone integer NOT NULL PRIMARY KEY ,
                             libelle character varying,
                             actif boolean
);


ALTER TABLE public.zone OWNER TO adel;

--
-- TOC entry 266 (class 1259 OID 127573)
-- Name: zone_id_zone_seq; Type: SEQUENCE; Schema: public; Owner: adel
--
--
-- ALTER TABLE public.zone ALTER COLUMN id_zone ADD GENERATED ALWAYS AS IDENTITY (
--     SEQUENCE NAME public.zone_id_zone_seq
--     START WITH 1
--     INCREMENT BY 1
--     NO MINVALUE
--     NO MAXVALUE
--     CACHE 1
--     );


--
-- TOC entry 3397 (class 2604 OID 127574)
-- Name: cohorte id_cohorte; Type: DEFAULT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.cohorte ALTER COLUMN id_cohorte SET DEFAULT nextval('public.cohorte_id_cohorte_seq'::regclass);


--
-- TOC entry 3398 (class 2604 OID 127575)
-- Name: compte_rendue id_compte_rendue; Type: DEFAULT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.compte_rendue ALTER COLUMN id_compte_rendue SET DEFAULT nextval('public.compte_rendue_id_compte_rendue_seq'::regclass);


--
-- TOC entry 3399 (class 2604 OID 127576)
-- Name: createurs_compte_rendue id; Type: DEFAULT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.createurs_compte_rendue ALTER COLUMN id SET DEFAULT nextval('public.createurs_compte_rendue_id_seq'::regclass);


--
-- TOC entry 3400 (class 2604 OID 127577)
-- Name: observation id_retour; Type: DEFAULT; Schema: public; Owner: adel
--

ALTER TABLE ONLY public.observation ALTER COLUMN id_retour SET DEFAULT nextval('public.observation_id_retour_seq'::regclass);
--
--
-- --
-- -- TOC entry 3700 (class 0 OID 90754)
-- -- Dependencies: 210
-- -- Data for Name: document; Type: TABLE DATA; Schema: portal; Owner: adel
-- --
--
--
--
-- --
-- -- TOC entry 3701 (class 0 OID 127368)
-- -- Dependencies: 211
-- -- Data for Name: action; Type: TABLE DATA; Schema: public; Owner: adel
-- --
--
--
--
-- --
-- -- TOC entry 3702 (class 0 OID 127373)
-- -- Dependencies: 212
-- -- Data for Name: aer; Type: TABLE DATA; Schema: public; Owner: adel
-- --
--
--
-- --
-- -- TOC entry 3769 (class 0 OID 0)
-- -- Dependencies: 217
-- -- Name: cohorte_id_cohorte_seq; Type: SEQUENCE SET; Schema: public; Owner: adel
-- --
--
-- SELECT pg_catalog.setval('public.cohorte_id_cohorte_seq', 17, true);
--
--
-- --
-- -- TOC entry 3770 (class 0 OID 0)
-- -- Dependencies: 222
-- -- Name: compte_rendue_id_compte_rendue_seq; Type: SEQUENCE SET; Schema: public; Owner: adel
-- --
--
-- SELECT pg_catalog.setval('public.compte_rendue_id_compte_rendue_seq', 1, false);
--
--
-- --
-- -- TOC entry 3771 (class 0 OID 0)
-- -- Dependencies: 224
-- -- Name: createurs_compte_rendue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: adel
-- --
--
-- SELECT pg_catalog.setval('public.createurs_compte_rendue_id_seq', 1, false);
--
--
-- --
-- -- TOC entry 3772 (class 0 OID 0)
-- -- Dependencies: 236
-- -- Name: observation_id_retour_seq; Type: SEQUENCE SET; Schema: public; Owner: adel
-- --
--
-- SELECT pg_catalog.setval('public.observation_id_retour_seq', 2612, true);
--
--
-- --
-- -- TOC entry 3773 (class 0 OID 0)
-- -- Dependencies: 266
-- -- Name: zone_id_zone_seq; Type: SEQUENCE SET; Schema: public; Owner: adel
-- --
--
-- SELECT pg_catalog.setval('public.zone_id_zone_seq', 52, true);
--
--
-- --
-- -- TOC entry 3402 (class 2606 OID 90974)
-- -- Name: document document_pkey; Type: CONSTRAINT; Schema: portal; Owner: adel
-- --
-- --
-- -- ALTER TABLE ONLY portal.document
-- --     ADD CONSTRAINT document_pkey PRIMARY KEY (uuid);
--
--
-- --
-- -- TOC entry 3404 (class 2606 OID 127583)
-- -- Name: action action_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- -- ALTER TABLE ONLY public.action
-- --     ADD CONSTRAINT action_pkey PRIMARY KEY (id_action);
--
--
-- --
-- -- TOC entry 3406 (class 2606 OID 127585)
-- -- Name: aer aer_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.aer
--     ADD CONSTRAINT aer_pkey PRIMARY KEY (id);
--
--
-- --
-- -- TOC entry 3408 (class 2606 OID 127587)
-- -- Name: block_model_economique block_model_economique_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.block_model_economique
--     ADD CONSTRAINT block_model_economique_pkey PRIMARY KEY (id_blocme);
--
--
-- --
-- -- TOC entry 3412 (class 2606 OID 127589)
-- -- Name: categories categories_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.categories
--     ADD CONSTRAINT categories_pkey PRIMARY KEY (id);
--
--
-- --
-- -- TOC entry 3414 (class 2606 OID 127591)
-- -- Name: cohorte cohorte_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.cohorte
--     ADD CONSTRAINT cohorte_pkey PRIMARY KEY (id_cohorte);
--
--
-- --
-- -- TOC entry 3416 (class 2606 OID 127593)
-- -- Name: collectif collectif_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.collectif
--     ADD CONSTRAINT collectif_pkey PRIMARY KEY (idpie);
--
--
-- --
-- -- TOC entry 3418 (class 2606 OID 127595)
-- -- Name: commune commune_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.commune
--     ADD CONSTRAINT commune_pkey PRIMARY KEY (id_commune);
--
--
-- --
-- -- TOC entry 3420 (class 2606 OID 127597)
-- -- Name: compte_rendue compte_rendue_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.compte_rendue
--     ADD CONSTRAINT compte_rendue_pkey PRIMARY KEY (id_compte_rendue);
--
--
-- --
-- -- TOC entry 3424 (class 2606 OID 127599)
-- -- Name: createurs_compte_rendue createurs_compte_rendue_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.createurs_compte_rendue
--     ADD CONSTRAINT createurs_compte_rendue_pkey PRIMARY KEY (id);
--
--
-- --
-- -- TOC entry 3426 (class 2606 OID 127601)
-- -- Name: departement departement_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.departement
--     ADD CONSTRAINT departement_pkey PRIMARY KEY (id_departement);
--
--
-- --
-- -- TOC entry 3428 (class 2606 OID 127603)
-- -- Name: documents documents_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.documents
--     ADD CONSTRAINT documents_pkey PRIMARY KEY (id_document);
--
--
-- --
-- -- TOC entry 3430 (class 2606 OID 127605)
-- -- Name: forme_juridique forme_juridique_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.forme_juridique
--     ADD CONSTRAINT forme_juridique_pkey PRIMARY KEY (id_forme_juridique);
--
--
-- --
-- -- TOC entry 3432 (class 2606 OID 127607)
-- -- Name: idee_projet idee_projet_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.idee_projet
--     ADD CONSTRAINT idee_projet_pkey PRIMARY KEY (id_projet);
--
--
-- --
-- -- TOC entry 3434 (class 2606 OID 127609)
-- -- Name: individuel individuel_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.individuel
--     ADD CONSTRAINT individuel_pkey PRIMARY KEY (idpie);
--
--
-- --
-- -- TOC entry 3438 (class 2606 OID 127611)
-- -- Name: marqueur marqueur_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.marqueur
--     ADD CONSTRAINT marqueur_pkey PRIMARY KEY (id);
--
--
-- --
-- -- TOC entry 3440 (class 2606 OID 127613)
-- -- Name: model_economique model_economique_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.model_economique
--     ADD CONSTRAINT model_economique_pkey PRIMARY KEY (id);
--
--
-- --
-- -- TOC entry 3444 (class 2606 OID 127615)
-- -- Name: note_question note_question_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.note_question
--     ADD CONSTRAINT note_question_pkey PRIMARY KEY (id_note);
--
--
-- --
-- -- TOC entry 3446 (class 2606 OID 127617)
-- -- Name: observation observation_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.observation
--     ADD CONSTRAINT observation_pkey PRIMARY KEY (id_retour);
--
--
-- --
-- -- TOC entry 3448 (class 2606 OID 127619)
-- -- Name: parcours parcours_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.parcours
--     ADD CONSTRAINT parcours_pkey PRIMARY KEY (id);
--
--
-- --
-- -- TOC entry 3450 (class 2606 OID 127621)
-- -- Name: pays pays_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.pays
--     ADD CONSTRAINT pays_pkey PRIMARY KEY (id_pays);
--
--
-- --
-- -- TOC entry 3452 (class 2606 OID 127623)
-- -- Name: phase phase_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.phase
--     ADD CONSTRAINT phase_pkey PRIMARY KEY (id_phase);
--
--
-- --
-- -- TOC entry 3454 (class 2606 OID 127625)
-- -- Name: pie pie_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.pie
--     ADD CONSTRAINT pie_pkey PRIMARY KEY (idpie);
--
--
-- --
-- -- TOC entry 3462 (class 2606 OID 127627)
-- -- Name: profil profil_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.profil
--     ADD CONSTRAINT profil_pkey PRIMARY KEY (id_profil);
--
--
-- --
-- -- TOC entry 3464 (class 2606 OID 127629)
-- -- Name: projet projet_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.projet
--     ADD CONSTRAINT projet_pkey PRIMARY KEY (id_projet);
--
--
-- --
-- -- TOC entry 3466 (class 2606 OID 127631)
-- -- Name: questionnaire questionnaire_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.questionnaire
--     ADD CONSTRAINT questionnaire_pkey PRIMARY KEY (id_question);
--
--
-- --
-- -- TOC entry 3470 (class 2606 OID 127633)
-- -- Name: rapport_analyse1collectif rapport_analyse1collectif_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.rapport_analyse1collectif
--     ADD CONSTRAINT rapport_analyse1collectif_pkey PRIMARY KEY (id);
--
--
-- --
-- -- TOC entry 3472 (class 2606 OID 127635)
-- -- Name: rapport_analyse2 rapport_analyse2_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.rapport_analyse2
--     ADD CONSTRAINT rapport_analyse2_pkey PRIMARY KEY (id);
--
--
-- --
-- -- TOC entry 3474 (class 2606 OID 127637)
-- -- Name: rapport_analyse2collectif rapport_analyse2collectif_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.rapport_analyse2collectif
--     ADD CONSTRAINT rapport_analyse2collectif_pkey PRIMARY KEY (id);
--
--
-- --
-- -- TOC entry 3468 (class 2606 OID 127639)
-- -- Name: rapport_analyse rapport_analyse_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.rapport_analyse
--     ADD CONSTRAINT rapport_analyse_pkey PRIMARY KEY (id_rapport);
--
--
-- --
-- -- TOC entry 3476 (class 2606 OID 127641)
-- -- Name: reference_note reference_note_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.reference_note
--     ADD CONSTRAINT reference_note_pkey PRIMARY KEY (id_question);
--
--
-- --
-- -- TOC entry 3478 (class 2606 OID 127643)
-- -- Name: region region_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.region
--     ADD CONSTRAINT region_pkey PRIMARY KEY (id_region);
--
--
-- --
-- -- TOC entry 3480 (class 2606 OID 127645)
-- -- Name: rencontre rencontre_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.rencontre
--     ADD CONSTRAINT rencontre_pkey PRIMARY KEY (id_rencontre);
--
--
-- --
-- -- TOC entry 3482 (class 2606 OID 127647)
-- -- Name: secteur_activite secteur_activite_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.secteur_activite
--     ADD CONSTRAINT secteur_activite_pkey PRIMARY KEY (id);
--
--
-- --
-- -- TOC entry 3484 (class 2606 OID 127649)
-- -- Name: sous_bloc sous_bloc_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.sous_bloc
--     ADD CONSTRAINT sous_bloc_pkey PRIMARY KEY (id_sous_bloc);
--
--
-- --
-- -- TOC entry 3486 (class 2606 OID 127651)
-- -- Name: sous_categories sous_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.sous_categories
--     ADD CONSTRAINT sous_categories_pkey PRIMARY KEY (id);
--
--
-- --
-- -- TOC entry 3488 (class 2606 OID 127653)
-- -- Name: soutien soutien_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.soutien
--     ADD CONSTRAINT soutien_pkey PRIMARY KEY (id);
--
--
-- --
-- -- TOC entry 3490 (class 2606 OID 127655)
-- -- Name: statut statut_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.statut
--     ADD CONSTRAINT statut_pkey PRIMARY KEY (id);
--
--
-- --
-- -- TOC entry 3492 (class 2606 OID 127657)
-- -- Name: synthese_notation synthese_notation_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.synthese_notation
--     ADD CONSTRAINT synthese_notation_pkey PRIMARY KEY (id_synthese_notation);
--
--
-- --
-- -- TOC entry 3494 (class 2606 OID 127659)
-- -- Name: tranche_age tranche_age_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.tranche_age
--     ADD CONSTRAINT tranche_age_pkey PRIMARY KEY (id);
--
--
-- --
-- -- TOC entry 3496 (class 2606 OID 127661)
-- -- Name: type_bloc type_bloc_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.type_bloc
--     ADD CONSTRAINT type_bloc_pkey PRIMARY KEY (id);
--
--
-- --
-- -- TOC entry 3498 (class 2606 OID 127663)
-- -- Name: type_document type_document_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.type_document
--     ADD CONSTRAINT type_document_pkey PRIMARY KEY (id);
--
--
-- --
-- -- TOC entry 3500 (class 2606 OID 127665)
-- -- Name: type_projet type_projet_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.type_projet
--     ADD CONSTRAINT type_projet_pkey PRIMARY KEY (id);
--
--
-- --
-- -- TOC entry 3456 (class 2606 OID 127667)
-- -- Name: pie_note_questions uk_4giuyd652hh1ywjl6xhdscqxr; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.pie_note_questions
--     ADD CONSTRAINT uk_4giuyd652hh1ywjl6xhdscqxr UNIQUE (note_questions_id_note);
--
--
-- --
-- -- TOC entry 3422 (class 2606 OID 127669)
-- -- Name: compte_rendue_createurs_content uk_4t7yya8mc1an2c4dg5dh0n64s; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.compte_rendue_createurs_content
--     ADD CONSTRAINT uk_4t7yya8mc1an2c4dg5dh0n64s UNIQUE (createurs_content_id);
--
--
-- --
-- -- TOC entry 3460 (class 2606 OID 127671)
-- -- Name: pie_rencontres uk_7beqcqbsfma9nr5wxnl9wvpfq; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.pie_rencontres
--     ADD CONSTRAINT uk_7beqcqbsfma9nr5wxnl9wvpfq UNIQUE (rencontres_id_rencontre);
--
--
-- --
-- -- TOC entry 3410 (class 2606 OID 127673)
-- -- Name: block_model_economique_sous_blocs uk_f6a9kw4o8tlyiwtysrja5g0u7; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.block_model_economique_sous_blocs
--     ADD CONSTRAINT uk_f6a9kw4o8tlyiwtysrja5g0u7 UNIQUE (sous_blocs_id_sous_bloc);
--
--
-- --
-- -- TOC entry 3458 (class 2606 OID 127675)
-- -- Name: pie_observations uk_lbd5fjh2klpd2b6ggtdk1efwx; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.pie_observations
--     ADD CONSTRAINT uk_lbd5fjh2klpd2b6ggtdk1efwx UNIQUE (observations_id_retour);
--
--
-- --
-- -- TOC entry 3442 (class 2606 OID 127677)
-- -- Name: model_economique_block_model_economique uk_mqixvu2ducn1xxxxs7mfm7dl2; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.model_economique_block_model_economique
--     ADD CONSTRAINT uk_mqixvu2ducn1xxxxs7mfm7dl2 UNIQUE (block_model_economique_id_blocme);
--
--
-- --
-- -- TOC entry 3436 (class 2606 OID 127679)
-- -- Name: individuel_idee_projets uk_pymv6x6u2f1aaur1w3dbpbevv; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.individuel_idee_projets
--     ADD CONSTRAINT uk_pymv6x6u2f1aaur1w3dbpbevv UNIQUE (idee_projets_id_projet);
--
--
-- --
-- -- TOC entry 3502 (class 2606 OID 127681)
-- -- Name: utilisateurs utilisateurs_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.utilisateurs
--     ADD CONSTRAINT utilisateurs_pkey PRIMARY KEY (id_utilisateur);
--
--
-- --
-- -- TOC entry 3504 (class 2606 OID 127683)
-- -- Name: zone zone_pkey; Type: CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.zone
--     ADD CONSTRAINT zone_pkey PRIMARY KEY (id_zone);
--
--
-- --
-- -- TOC entry 3509 (class 2606 OID 127684)
-- -- Name: collectif collectif_commune_id_commune_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.collectif
--     ADD CONSTRAINT collectif_commune_id_commune_fkey FOREIGN KEY (commune_id_commune) REFERENCES public.commune(id_commune);
--
--
-- --
-- -- TOC entry 3510 (class 2606 OID 127689)
-- -- Name: collectif collectif_forme_juridique_id_forme_juridique_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.collectif
--     ADD CONSTRAINT collectif_forme_juridique_id_forme_juridique_fkey FOREIGN KEY (forme_juridique_id_forme_juridique) REFERENCES public.forme_juridique(id_forme_juridique);
--
--
-- --
-- -- TOC entry 3511 (class 2606 OID 127694)
-- -- Name: collectif collectif_idpie_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.collectif
--     ADD CONSTRAINT collectif_idpie_fkey FOREIGN KEY (idpie) REFERENCES public.pie(idpie);
--
--
-- --
-- -- TOC entry 3512 (class 2606 OID 127699)
-- -- Name: collectif fk154a5b8dhepf61asp8uw1wpl1; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.collectif
--     ADD CONSTRAINT fk154a5b8dhepf61asp8uw1wpl1 FOREIGN KEY (rapport_analyse2collectif_id) REFERENCES public.rapport_analyse2collectif(id);
--
--
-- --
-- -- TOC entry 3521 (class 2606 OID 127704)
-- -- Name: individuel fk1m3ynfk4kw9me8ay2jqneubxv; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.individuel
--     ADD CONSTRAINT fk1m3ynfk4kw9me8ay2jqneubxv FOREIGN KEY (rapport_analyse1_id_rapport) REFERENCES public.rapport_analyse(id_rapport);
--
--
-- --
-- -- TOC entry 3533 (class 2606 OID 127709)
-- -- Name: model_economique_block_model_economique fk24371laycw78dfxr8wgobytxr; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.model_economique_block_model_economique
--     ADD CONSTRAINT fk24371laycw78dfxr8wgobytxr FOREIGN KEY (model_economique_id) REFERENCES public.model_economique(id);
--
--
-- --
-- -- TOC entry 3538 (class 2606 OID 127714)
-- -- Name: pie fk3xl3x1glfopq7uf9hyt81i7vv; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.pie
--     ADD CONSTRAINT fk3xl3x1glfopq7uf9hyt81i7vv FOREIGN KEY (projet_id_projet) REFERENCES public.projet(id_projet);
--
--
-- --
-- -- TOC entry 3549 (class 2606 OID 127968)
-- -- Name: projet fk45exqutagxt17mbr4sb95iqjt; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.projet
--     ADD CONSTRAINT fk45exqutagxt17mbr4sb95iqjt FOREIGN KEY (model_economique_id) REFERENCES public.model_economique(id);
--
--
-- --
-- -- TOC entry 3534 (class 2606 OID 127719)
-- -- Name: model_economique_block_model_economique fk45udhbsvah83117nr87yhqsax; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.model_economique_block_model_economique
--     ADD CONSTRAINT fk45udhbsvah83117nr87yhqsax FOREIGN KEY (block_model_economique_id_blocme) REFERENCES public.block_model_economique(id_blocme);
--
--
-- --
-- -- TOC entry 3542 (class 2606 OID 127724)
-- -- Name: pie_note_questions fk5l1ll3ewbpfy5ka9a3v9pqc3u; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.pie_note_questions
--     ADD CONSTRAINT fk5l1ll3ewbpfy5ka9a3v9pqc3u FOREIGN KEY (pie_idpie) REFERENCES public.pie(idpie);
--
--
-- --
-- -- TOC entry 3532 (class 2606 OID 127729)
-- -- Name: marqueur fk7di9unsmeio1eqacp2l3puchj; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.marqueur
--     ADD CONSTRAINT fk7di9unsmeio1eqacp2l3puchj FOREIGN KEY (parcours_id) REFERENCES public.parcours(id);
--
--
-- --
-- -- TOC entry 3505 (class 2606 OID 127734)
-- -- Name: action fk82s3vjnanvf91bxuggim7k47x; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.action
--     ADD CONSTRAINT fk82s3vjnanvf91bxuggim7k47x FOREIGN KEY (projet_id_projet) REFERENCES public.projet(id_projet);
--
--
-- --
-- -- TOC entry 3518 (class 2606 OID 127739)
-- -- Name: documents fk9x6j51mtaj0ot3hhtpprq7phy; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.documents
--     ADD CONSTRAINT fk9x6j51mtaj0ot3hhtpprq7phy FOREIGN KEY (type_document_id) REFERENCES public.type_document(id);
--
--
-- --
-- -- TOC entry 3507 (class 2606 OID 127744)
-- -- Name: block_model_economique_sous_blocs fka8u49x09xakvf7uusy5pu8xtg; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.block_model_economique_sous_blocs
--     ADD CONSTRAINT fka8u49x09xakvf7uusy5pu8xtg FOREIGN KEY (block_model_economique_id_blocme) REFERENCES public.block_model_economique(id_blocme);
--
--
-- --
-- -- TOC entry 3530 (class 2606 OID 127749)
-- -- Name: individuel_idee_projets fkbctvu3rwyarj9ag27se4xe4ta; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.individuel_idee_projets
--     ADD CONSTRAINT fkbctvu3rwyarj9ag27se4xe4ta FOREIGN KEY (individuel_idpie) REFERENCES public.individuel(idpie);
--
--
-- --
-- -- TOC entry 3522 (class 2606 OID 127754)
-- -- Name: individuel fkc624xuf8xm8ccswvp4w9fr7j7; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.individuel
--     ADD CONSTRAINT fkc624xuf8xm8ccswvp4w9fr7j7 FOREIGN KEY (rapport_analyse2_id) REFERENCES public.rapport_analyse2(id);
--
--
-- --
-- -- TOC entry 3544 (class 2606 OID 127759)
-- -- Name: pie_observations fkdpt6m96tguvqxra99blrcq1ma; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.pie_observations
--     ADD CONSTRAINT fkdpt6m96tguvqxra99blrcq1ma FOREIGN KEY (pie_idpie) REFERENCES public.pie(idpie);
--
--
-- --
-- -- TOC entry 3506 (class 2606 OID 127764)
-- -- Name: action fke1kjovkjb96pmevbft98yolv8; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.action
--     ADD CONSTRAINT fke1kjovkjb96pmevbft98yolv8 FOREIGN KEY (utilisateurs_id_utilisateur) REFERENCES public.utilisateurs(id_utilisateur);
--
--
-- --
-- -- TOC entry 3539 (class 2606 OID 127769)
-- -- Name: pie fke3sruj3twgc5dmae4fhyvki14; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.pie
--     ADD CONSTRAINT fke3sruj3twgc5dmae4fhyvki14 FOREIGN KEY (statut_id) REFERENCES public.statut(id);
--
--
-- --
-- -- TOC entry 3555 (class 2606 OID 127774)
-- -- Name: reference_note fke87pruyr23f4bgckir1astwx8; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.reference_note
--     ADD CONSTRAINT fke87pruyr23f4bgckir1astwx8 FOREIGN KEY (question_id_question) REFERENCES public.questionnaire(id_question);
--
--
-- --
-- -- TOC entry 3546 (class 2606 OID 127779)
-- -- Name: pie_rencontres fkekdaomdqcor5qgcdsj79q31gk; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.pie_rencontres
--     ADD CONSTRAINT fkekdaomdqcor5qgcdsj79q31gk FOREIGN KEY (pie_idpie) REFERENCES public.pie(idpie);
--
--
-- --
-- -- TOC entry 3547 (class 2606 OID 127784)
-- -- Name: pie_rencontres fkf3gl1g1orxub1ubtvwgc09scj; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.pie_rencontres
--     ADD CONSTRAINT fkf3gl1g1orxub1ubtvwgc09scj FOREIGN KEY (rencontres_id_rencontre) REFERENCES public.rencontre(id_rencontre);
--
--
-- --
-- -- TOC entry 3519 (class 2606 OID 127789)
-- -- Name: documents fkfydy7aum55k7mdsfdfag6qr95; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.documents
--     ADD CONSTRAINT fkfydy7aum55k7mdsfdfag6qr95 FOREIGN KEY (projet_id_projet) REFERENCES public.projet(id_projet);
--
--
-- --
-- -- TOC entry 3556 (class 2606 OID 127794)
-- -- Name: region fkgufg5f9x5yfv2ork1xtevl9ba; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.region
--     ADD CONSTRAINT fkgufg5f9x5yfv2ork1xtevl9ba FOREIGN KEY (pays_id_pays) REFERENCES public.pays(id_pays);
--
--
-- --
-- -- TOC entry 3536 (class 2606 OID 127799)
-- -- Name: observation fkgv9rkpjiwlb21i5pnf6i4parm; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.observation
--     ADD CONSTRAINT fkgv9rkpjiwlb21i5pnf6i4parm FOREIGN KEY (statut_id) REFERENCES public.statut(id);
--
--
-- --
-- -- TOC entry 3515 (class 2606 OID 127804)
-- -- Name: compte_rendue_createurs_content fkgxi1c6a6usk7c55smjgquu24l; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.compte_rendue_createurs_content
--     ADD CONSTRAINT fkgxi1c6a6usk7c55smjgquu24l FOREIGN KEY (createurs_content_id) REFERENCES public.createurs_compte_rendue(id);
--
--
-- --
-- -- TOC entry 3545 (class 2606 OID 127809)
-- -- Name: pie_observations fki2sp2q2w2v13pshyq300l7ukc; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.pie_observations
--     ADD CONSTRAINT fki2sp2q2w2v13pshyq300l7ukc FOREIGN KEY (observations_id_retour) REFERENCES public.observation(id_retour);
--
--
-- --
-- -- TOC entry 3535 (class 2606 OID 127814)
-- -- Name: note_question fkim35n2bgx1d1lgkl7lw6ety9r; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.note_question
--     ADD CONSTRAINT fkim35n2bgx1d1lgkl7lw6ety9r FOREIGN KEY (question_id_question) REFERENCES public.questionnaire(id_question);
--
--
-- --
-- -- TOC entry 3523 (class 2606 OID 127819)
-- -- Name: individuel fkiwc70uxopy21tp23tqpwfoeym; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.individuel
--     ADD CONSTRAINT fkiwc70uxopy21tp23tqpwfoeym FOREIGN KEY (soutien_immediat_id) REFERENCES public.soutien(id);
--
--
-- --
-- -- TOC entry 3514 (class 2606 OID 127824)
-- -- Name: commune fkjaakwnnew8bfjm6unjwsedmxl; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.commune
--     ADD CONSTRAINT fkjaakwnnew8bfjm6unjwsedmxl FOREIGN KEY (departement_id_departement) REFERENCES public.departement(id_departement);
--
--
-- --
-- -- TOC entry 3513 (class 2606 OID 127829)
-- -- Name: collectif fkkm3artk8k7ahu4fit4iqfu7wa; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.collectif
--     ADD CONSTRAINT fkkm3artk8k7ahu4fit4iqfu7wa FOREIGN KEY (rapport_analyse1collectif_id) REFERENCES public.rapport_analyse1collectif(id);
--
--
-- --
-- -- TOC entry 3559 (class 2606 OID 127834)
-- -- Name: type_bloc fklh84qule2qsi0m2aab2abyxg2; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.type_bloc
--     ADD CONSTRAINT fklh84qule2qsi0m2aab2abyxg2 FOREIGN KEY (block_model_economique_id_blocme) REFERENCES public.block_model_economique(id_blocme);
--
--
-- --
-- -- TOC entry 3548 (class 2606 OID 127839)
-- -- Name: profil fklo6mc52ff8b7p5jmtweenr1te; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.profil
--     ADD CONSTRAINT fklo6mc52ff8b7p5jmtweenr1te FOREIGN KEY (utilisateurs_id_utilisateur) REFERENCES public.utilisateurs(id_utilisateur);
--
--
-- --
-- -- TOC entry 3560 (class 2606 OID 127844)
-- -- Name: utilisateurs fkn563suu0ohh83ua4w3hk4jdao; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.utilisateurs
--     ADD CONSTRAINT fkn563suu0ohh83ua4w3hk4jdao FOREIGN KEY (pie_idpie) REFERENCES public.pie(idpie);
--
--
-- --
-- -- TOC entry 3550 (class 2606 OID 127849)
-- -- Name: projet fkn647w4oc8ic00gutsebel7d3s; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.projet
--     ADD CONSTRAINT fkn647w4oc8ic00gutsebel7d3s FOREIGN KEY (type_projet_id) REFERENCES public.type_projet(id);
--
--
-- --
-- -- TOC entry 3543 (class 2606 OID 127854)
-- -- Name: pie_note_questions fkoqjq8oo2m0grnanqkfyffaven; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.pie_note_questions
--     ADD CONSTRAINT fkoqjq8oo2m0grnanqkfyffaven FOREIGN KEY (note_questions_id_note) REFERENCES public.note_question(id_note);
--
--
-- --
-- -- TOC entry 3551 (class 2606 OID 127859)
-- -- Name: projet fkp3lvo2iqceniqn9hxlm85lis9; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.projet
--     ADD CONSTRAINT fkp3lvo2iqceniqn9hxlm85lis9 FOREIGN KEY (statut_id) REFERENCES public.statut(id);
--
--
-- --
-- -- TOC entry 3531 (class 2606 OID 127864)
-- -- Name: individuel_idee_projets fkp7yp1mivcv8ecfrdi086s4o1a; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.individuel_idee_projets
--     ADD CONSTRAINT fkp7yp1mivcv8ecfrdi086s4o1a FOREIGN KEY (idee_projets_id_projet) REFERENCES public.idee_projet(id_projet);
--
--
-- --
-- -- TOC entry 3508 (class 2606 OID 127869)
-- -- Name: block_model_economique_sous_blocs fkppa7bm2ft59qkexkqnyp87p1j; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.block_model_economique_sous_blocs
--     ADD CONSTRAINT fkppa7bm2ft59qkexkqnyp87p1j FOREIGN KEY (sous_blocs_id_sous_bloc) REFERENCES public.sous_bloc(id_sous_bloc);
--
--
-- --
-- -- TOC entry 3540 (class 2606 OID 127874)
-- -- Name: pie fkpw5xie6r9feajnw5w7s7sxyxw; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.pie
--     ADD CONSTRAINT fkpw5xie6r9feajnw5w7s7sxyxw FOREIGN KEY (cohorte_id_cohorte) REFERENCES public.cohorte(id_cohorte);
--
--
-- --
-- -- TOC entry 3516 (class 2606 OID 127879)
-- -- Name: compte_rendue_createurs_content fkqb6scneg9ooh9phx3vi8bq6d; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.compte_rendue_createurs_content
--     ADD CONSTRAINT fkqb6scneg9ooh9phx3vi8bq6d FOREIGN KEY (compte_rendue_id_compte_rendue) REFERENCES public.compte_rendue(id_compte_rendue);
--
--
-- --
-- -- TOC entry 3537 (class 2606 OID 127884)
-- -- Name: parcours fkqudwj4s9e8wp639s63xiisvhl; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.parcours
--     ADD CONSTRAINT fkqudwj4s9e8wp639s63xiisvhl FOREIGN KEY (sous_categories_id) REFERENCES public.sous_categories(id);
--
--
-- --
-- -- TOC entry 3517 (class 2606 OID 127889)
-- -- Name: departement fkr7r50pkc4rnxnjyvclm9bwe42; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.departement
--     ADD CONSTRAINT fkr7r50pkc4rnxnjyvclm9bwe42 FOREIGN KEY (region_id_region) REFERENCES public.region(id_region);
--
--
-- --
-- -- TOC entry 3558 (class 2606 OID 127894)
-- -- Name: sous_categories fkrkd1mssr6o6c82p33qsullg2m; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.sous_categories
--     ADD CONSTRAINT fkrkd1mssr6o6c82p33qsullg2m FOREIGN KEY (categories_id) REFERENCES public.categories(id);
--
--
-- --
-- -- TOC entry 3520 (class 2606 OID 127899)
-- -- Name: documents fkss6knax061uyr9vhrlvfubir7; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.documents
--     ADD CONSTRAINT fkss6knax061uyr9vhrlvfubir7 FOREIGN KEY (compte_rendue_id_compte_rendue) REFERENCES public.compte_rendue(id_compte_rendue);
--
--
-- --
-- -- TOC entry 3552 (class 2606 OID 127904)
-- -- Name: projet fktq8nytrbobcpwte3b0518gr0; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.projet
--     ADD CONSTRAINT fktq8nytrbobcpwte3b0518gr0 FOREIGN KEY (secteur_activite_id) REFERENCES public.secteur_activite(id);
--
--
-- --
-- -- TOC entry 3524 (class 2606 OID 127909)
-- -- Name: individuel individuel_categories_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.individuel
--     ADD CONSTRAINT individuel_categories_id_fkey FOREIGN KEY (categories_id) REFERENCES public.categories(id);
--
--
-- --
-- -- TOC entry 3525 (class 2606 OID 127914)
-- -- Name: individuel individuel_commune_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.individuel
--     ADD CONSTRAINT individuel_commune_id_fkey FOREIGN KEY (commune_id) REFERENCES public.commune(id_commune);
--
--
-- --
-- -- TOC entry 3526 (class 2606 OID 127919)
-- -- Name: individuel individuel_commune_rattach_id_commune_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.individuel
--     ADD CONSTRAINT individuel_commune_rattach_id_commune_fkey FOREIGN KEY (commune_rattach_id_commune) REFERENCES public.commune(id_commune);
--
--
-- --
-- -- TOC entry 3527 (class 2606 OID 127924)
-- -- Name: individuel individuel_idpie_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.individuel
--     ADD CONSTRAINT individuel_idpie_fkey FOREIGN KEY (idpie) REFERENCES public.pie(idpie);
--
--
-- --
-- -- TOC entry 3528 (class 2606 OID 127929)
-- -- Name: individuel individuel_soutien_immediat_id_soutien_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.individuel
--     ADD CONSTRAINT individuel_soutien_immediat_id_soutien_fkey FOREIGN KEY (soutien_immediat_id_soutien) REFERENCES public.soutien(id);
--
--
-- --
-- -- TOC entry 3529 (class 2606 OID 127934)
-- -- Name: individuel individuel_tranche_age_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.individuel
--     ADD CONSTRAINT individuel_tranche_age_id_fkey FOREIGN KEY (tranche_age_id) REFERENCES public.tranche_age(id);
--
--
-- --
-- -- TOC entry 3541 (class 2606 OID 127939)
-- -- Name: pie pie_inscritpar_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.pie
--     ADD CONSTRAINT pie_inscritpar_fkey FOREIGN KEY (inscritpar) REFERENCES public.aer(id);
--
--
-- --
-- -- TOC entry 3553 (class 2606 OID 127944)
-- -- Name: questionnaire questionnaire_id_synthese_notatoion_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.questionnaire
--     ADD CONSTRAINT questionnaire_id_synthese_notatoion_fkey FOREIGN KEY (id_synthese_notation) REFERENCES public.synthese_notation(id_synthese_notation);
--
--
-- --
-- -- TOC entry 3554 (class 2606 OID 127949)
-- -- Name: questionnaire questionnaire_phase_id_phase_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.questionnaire
--     ADD CONSTRAINT questionnaire_phase_id_phase_fkey FOREIGN KEY (phase_id_phase) REFERENCES public.phase(id_phase);
--
--
-- --
-- -- TOC entry 3557 (class 2606 OID 127954)
-- -- Name: region region_zone_id_zone_fkey; Type: FK CONSTRAINT; Schema: public; Owner: adel
-- --
--
-- ALTER TABLE ONLY public.region
--     ADD CONSTRAINT region_zone_id_zone_fkey FOREIGN KEY (zone_id_zone) REFERENCES public.zone(id_zone);


--
-- TOC entry 3764 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: adel
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;


-- Completed on 2023-09-19 09:49:00

--
-- PostgreSQL database dump complete
--

