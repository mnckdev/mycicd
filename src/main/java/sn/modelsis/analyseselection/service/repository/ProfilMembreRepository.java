package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.ProfilMembre;

import java.util.UUID;

@Repository
public interface ProfilMembreRepository extends JpaRepository<ProfilMembre, UUID> {
    ProfilMembre findProfilMembreByCode(String code);
}
