package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.StatutDTO;
import sn.modelsis.analyseselection.service.entity.Statut;

@Mapper(componentModel = "spring")
public interface StatutMapper extends GenericMapper<Statut, StatutDTO> {
}
