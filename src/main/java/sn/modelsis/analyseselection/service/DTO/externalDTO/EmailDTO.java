package sn.modelsis.analyseselection.service.DTO.externalDTO;

import lombok.*;

import java.util.Map;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
@ToString
public class EmailDTO {

    private String expediteur;

    private String destinataire;

    private String templateName;

    private String objet;

    private Map<String, Object> templateVariable;
}
