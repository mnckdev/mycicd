package sn.modelsis.analyseselection.service.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Action {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private UUID idAction;
    private String description;
    private String m_Projet;
    private String natureAction;
    @ManyToOne
    @JsonIgnore
    private Utilisateurs utilisateurs;
    @ManyToOne
    @JsonIgnore
    private Projet projet;
}
