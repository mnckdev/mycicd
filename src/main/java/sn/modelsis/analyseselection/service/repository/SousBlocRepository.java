package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.SousBloc;

@Repository
public interface SousBlocRepository extends JpaRepository<SousBloc,Integer> {
}
