package sn.modelsis.analyseselection.service.serviceinterface;

import sn.modelsis.analyseselection.service.DTO.CohorteDTO;

public interface CohorteServiceInterface {

    public CohorteDTO startNewCohorte(CohorteDTO cohorte);

    public CohorteDTO endCohorte(CohorteDTO cohorte);

    public CohorteDTO getByStatut(String statut);
}
