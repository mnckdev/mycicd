package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.TypeBloc;

@Repository
public interface TypeBlocRepository extends JpaRepository<TypeBloc,Integer> {
}
