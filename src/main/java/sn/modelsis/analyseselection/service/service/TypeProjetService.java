package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.TypeProjet;
import sn.modelsis.analyseselection.service.repository.TypeProjetRepository;

import java.util.List;
import java.util.Optional;

@Service
public class TypeProjetService {

    private TypeProjetRepository typeProjetRepository;

    public TypeProjetService(TypeProjetRepository typeProjetRepository){
        this.typeProjetRepository=typeProjetRepository;
    }

    public TypeProjet newTypeProjet(TypeProjet TypeProjet){
        return  typeProjetRepository.save(TypeProjet);
    }

    public Optional<TypeProjet> findById(Integer id){
        return typeProjetRepository.findById(id);
    }
    public List<TypeProjet> findAll(){
        return typeProjetRepository.findAll();
    }
    public void deleteTypeProjet(Integer id){
        typeProjetRepository.deleteById(id);
    }
}
