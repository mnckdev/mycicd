package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.RapportAnalyse;
import sn.modelsis.analyseselection.service.repository.RapportAnalyseRepository;

import java.util.List;
import java.util.Optional;

@Service
public class RapportAnalyseService {


    private RapportAnalyseRepository rapportAnalyseRepository;

    public RapportAnalyseService(RapportAnalyseRepository rapportAnalyseRepository){
        this.rapportAnalyseRepository=rapportAnalyseRepository;
    }

    public RapportAnalyse newRapportAnalyse(RapportAnalyse rapportAnalyse){
        return  rapportAnalyseRepository.save(rapportAnalyse);
    }

    public Optional<RapportAnalyse> findById(Integer id){
        return rapportAnalyseRepository.findById(id);
    }
    public List<RapportAnalyse> findAll(){
        return rapportAnalyseRepository.findAll();
    }
    public void deleteRapportAnalyse(Integer id){
        rapportAnalyseRepository.deleteById(id);
    }

}
