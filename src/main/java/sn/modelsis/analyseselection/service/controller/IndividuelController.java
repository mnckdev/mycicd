package sn.modelsis.analyseselection.service.controller;

import feign.Param;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.DTO.IdeeProjetDTO;
import sn.modelsis.analyseselection.service.DTO.IndividuelDTO;
import sn.modelsis.analyseselection.service.DTO.StatutDTO;
import sn.modelsis.analyseselection.service.entity.Cohorte;
import sn.modelsis.analyseselection.service.entity.Commune;
import sn.modelsis.analyseselection.service.entity.Individuel;
import sn.modelsis.analyseselection.service.service.IndividuelService;
import sn.modelsis.analyseselection.service.utils.Constantes;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/individuel")
@Api(
        value = "Individuel REST endpoint",
        tags = "Individuel controller(v1)"
)

public class IndividuelController {

    private final IndividuelService individuelService;

    public IndividuelController(IndividuelService individuelService) {
        this.individuelService = individuelService;
    }

    @GetMapping
    @ApiOperation(value = "Get all individuels", notes = "Get all individuels")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    public ResponseEntity<List<IndividuelDTO>> findAll(){
        List<IndividuelDTO> individuels = individuelService.read();
        if(individuels == null)
            return ResponseEntity.badRequest().body(individuels);
        return ResponseEntity.ok().body(individuels);
    }

    @ApiOperation(value = "Get one individuel", notes = "Get one individuel")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<IndividuelDTO> get(@PathVariable UUID id){
        IndividuelDTO individuel = individuelService.read(id);
        if(individuel== null)
            return  ResponseEntity.badRequest().body(individuel);
        return ResponseEntity.ok().body(individuel);
    }

    @PostMapping
    @ApiResponses(
            {
                    @ApiResponse(code=200, message = "created"),
            }
    )
    @ApiOperation(value = "post a individuel", notes = "post a individuel")

    public ResponseEntity<IndividuelDTO> create(@RequestBody IndividuelDTO individuelDTO){
       IndividuelDTO individuelDtoCreated = individuelService.create(individuelDTO);
        return ResponseEntity.ok().body(individuelDtoCreated);
    }
    @PutMapping("/{id}")
    @ApiResponses(
            {
                    @ApiResponse(code=200, message = "updated"),
            }
    )
    @ApiOperation(value = "put a individuel", notes = "put a individuel")

    public ResponseEntity<?> update(@PathVariable UUID id, @Param Boolean check, @RequestBody IndividuelDTO individuelDTO){
        IndividuelDTO individuelDtoUpdated;
        String verifyresult =  individuelService.verifyCheck(check,individuelDTO);
        if(!verifyresult.isEmpty()){
            return ResponseEntity.badRequest().body(verifyresult);
        }
        individuelDtoUpdated = individuelService.update(id,individuelDTO);
        if(individuelDtoUpdated == null)
            return ResponseEntity.notFound().build();
        return ResponseEntity.ok().body(individuelDtoUpdated);
    }

    @DeleteMapping("/{id}")
    @ApiResponses(
            {
                    @ApiResponse(code=200, message = "delete"),
            }
    )
    @ApiOperation(value = "delete a individuel", notes = "delete a  individuel")
    public ResponseEntity<String> delete(@PathVariable UUID id){
        individuelService.delete(id);
        return ResponseEntity.ok().body("deleted");
    }

    @GetMapping("/developpeur")
    public ResponseEntity<List<IndividuelDTO>> getDeveloppeurs() {
        return ResponseEntity.ok().body(individuelService.getDeveloppeurs());
    }

    @GetMapping("/createur")
    public ResponseEntity<List<IndividuelDTO>> getCreateurs() {
        return ResponseEntity.ok().body(individuelService.getCreateurs());
    }

    @GetMapping("/developpeur/commune")
    public ResponseEntity<List<IndividuelDTO>> getDeveloppeurs(@RequestBody Commune commune) {
        return ResponseEntity.ok().body(individuelService.getDeveloppeursFromACommune(commune));
    }

    @GetMapping("/createur/commune")
    public ResponseEntity<List<IndividuelDTO>> getCreateurs(@RequestBody Commune commune) {
        return ResponseEntity.ok().body(individuelService.getCreateursFromACommune(commune));
    }

    @GetMapping("/createur/cohorte")
    public ResponseEntity<List<IndividuelDTO>> getCreateurs(@RequestBody Cohorte cohorte) {
        return ResponseEntity.ok().body(individuelService.getCreateursByCohorte(cohorte));
    }

    @GetMapping("/developpeur/cohorte")
    public ResponseEntity<List<IndividuelDTO>> getDeveloppeurs(@RequestBody Cohorte cohorte) {
        return ResponseEntity.ok().body(individuelService.getDeveloppeursByCohorte(cohorte));
    }

    @GetMapping("/createur/cohorte/commune")
    public ResponseEntity<List<IndividuelDTO>> getCreateurs(@RequestBody Individuel individuel) {
        return ResponseEntity.ok().body(individuelService.getCreateursByCohorteAndCommune(individuel.getCohorte(), individuel.getCommune()));
    }

    @GetMapping("/developpeur/cohorte/commune")
    public ResponseEntity<List<IndividuelDTO>> getDeveloppeurs(@RequestBody IndividuelDTO individuelDTO) {
        return ResponseEntity.ok().body(individuelService.getDeveloppeursByCohorteAndCommune(individuelDTO.getCohorte(), individuelDTO.getCommune()));
    }

    @GetMapping("/createur/statut")
    public ResponseEntity<List<IndividuelDTO>> getCreateurs(@RequestBody StatutDTO statutDTO) {
        return ResponseEntity.ok().body(individuelService.getCreateursByStatut(
                statutDTO
                ));
    }

    @GetMapping("/developpeur/statut")
    public ResponseEntity<List<IndividuelDTO>> getDeveloppeurs(@RequestBody StatutDTO statutDTO) {
        return ResponseEntity.ok().body(individuelService.getDeveloppeursByStatut(
                statutDTO
        ));
    }

    @PostMapping("/createur/{id}/idees")
    public ResponseEntity<List<IdeeProjetDTO>> saveIdeeProjet(@PathVariable UUID id, @RequestBody List<IdeeProjetDTO> ideeProjetDTOS) {
        return ResponseEntity.ok().body(individuelService.saveIdeesProjet(
                id, ideeProjetDTOS
        ));
    }

    @GetMapping("/statut/{id}")
    public ResponseEntity<List<IndividuelDTO>> getDeveloppeurs(@PathVariable Integer id) {
        return ResponseEntity.ok().body(individuelService.findAllByStatut(
                id
        ));
    }

    @GetMapping("/createur/{id}/idees")
    public ResponseEntity<List<IdeeProjetDTO>> getIdeeProjetByIndividuel(@PathVariable UUID id){
        List<IdeeProjetDTO> ideeProjetDTOS = individuelService.getIdeeProjetByIndividuel(id);
        return ResponseEntity.ok().body(ideeProjetDTOS);
    }
}
