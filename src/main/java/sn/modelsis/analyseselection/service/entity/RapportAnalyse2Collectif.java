package sn.modelsis.analyseselection.service.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RapportAnalyse2Collectif {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private UUID id;

    @Column(length = 2000)
    private String fonctionnement;

    @Column(length = 2000)
    private String financesRessources;

    @Column(length = 2000)
    private String financesDepenses;

    @Column(length = 2000)
    private String modeleEconomique;

    @Column(length = 2000)
    private String partenariat;

    @Column(length = 2000)
    private String faiblesse;
}
