package sn.modelsis.analyseselection.service.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.Rencontre;
import sn.modelsis.analyseselection.service.service.RencontreService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/rencontre")
@Api(
        value = "Rencontre REST endpoint",
        tags = "Rencontre controller(v1)"
)

public class RencontreController {

    private final RencontreService rencontreService;

    public RencontreController(RencontreService rencontreService) {
        this.rencontreService = rencontreService;
    }

    @GetMapping
    @ApiOperation(value = "Get all rencontres", notes = "Get all rencontres")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    public ResponseEntity<List<Rencontre>> findAll(){
        List<Rencontre> rencontres = rencontreService.read();
        if(rencontres == null)
            return ResponseEntity.badRequest().body(rencontres);
        return ResponseEntity.ok().body(rencontres);
    }

    @ApiOperation(value = "Get one rencontre", notes = "Get one rencontre")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<Rencontre>> get(@PathVariable UUID id){
        Optional<Rencontre> rencontre = rencontreService.read(id);
        if(rencontre== null)
            return  ResponseEntity.badRequest().body(rencontre);
        return ResponseEntity.ok().body(rencontre);
    }

    @PostMapping
    @ApiResponses(
            {
                    @ApiResponse(code=200, message = "created"),
            }
    )
    @ApiOperation(value = "post a rencontre", notes = "post a rencontre")
    // Devrait changer le retour de ce controller
    public ResponseEntity<Rencontre> create(@RequestBody Rencontre rencontre){
        rencontre = rencontreService.create(rencontre);
        return ResponseEntity.ok().body(rencontre);
    }

    @DeleteMapping("/{id}")
    @ApiResponses(
            {
                    @ApiResponse(code=200, message = "delete"),
            }
    )
    @ApiOperation(value = "delete a rencontre", notes = "delete a  rencontre")
    public ResponseEntity<String> delete(@PathVariable UUID id){
        rencontreService.delete(id);
        return ResponseEntity.ok().body("deleted");
    }
}
