package sn.modelsis.analyseselection.service.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.DTO.ChangePasswordDTO;
import sn.modelsis.analyseselection.service.DTO.UserDTO;
import sn.modelsis.analyseselection.service.exception.MissingUserIdException;
import sn.modelsis.analyseselection.service.service.KeycloakService;

import java.util.List;

@RestController
@Log4j2
@RequestMapping("/api/v1/users")
public class KeycloakController {

    private final KeycloakService keycloakService;

    public KeycloakController(KeycloakService keycloakService) {
        this.keycloakService = keycloakService;
    }

    @GetMapping(params = "q")
    public ResponseEntity<List<UserDTO>> getUsers(@RequestParam("q") String parametres) {
        log.info(parametres);
        List<UserDTO> users = keycloakService.getUserByAttributes(parametres);
        return ResponseEntity.ok().body(users);
    }

    @GetMapping()
    public ResponseEntity<?> getAllUsers() {
        List<UserDTO> users = keycloakService.getAllUser();
        return ResponseEntity.ok().body(users);
    }

    @PostMapping()
    public ResponseEntity<?> createUser(@RequestBody UserDTO user) {
        if (user.getUsername() == null || user.getEmail() == null)
            throw new IllegalArgumentException("the username or the email is missing ");

        if (keycloakService.userExists(user.getUsername())) {
            log.error("The user {} already exists", user.getUsername());
            return ResponseEntity.badRequest().body("the user already exist");
        }

        UserDTO result = keycloakService.createUser(user);

        if (result != null && result.getId() != null)
            return ResponseEntity.ok().body(result);

        return ResponseEntity.internalServerError().body("Error Creating the User ");
    }

    @PutMapping()
    public ResponseEntity<?> updateUser(@RequestBody UserDTO user) {
        UserDTO result = keycloakService.updateUser(user);
        if (result.getId() != null)
            return ResponseEntity.ok().body(result);
        return ResponseEntity.ok().body("error updating the user");
    }

    @PostMapping("/updatepassword")
    public ResponseEntity<?> changePassword(@RequestBody ChangePasswordDTO user) {
        if (user.getId() == null)
            throw new MissingUserIdException();
        UserDTO result = keycloakService.changePasswordKeycloakUser(user);
        if (result.getId() != null)
            return ResponseEntity.ok().body(result);
        return ResponseEntity.badRequest().body("error updating the user");
    }

    @GetMapping("/roles")
    public ResponseEntity<?> getAllRoles() {
        List<String> result = keycloakService.getAllRoles();
        return ResponseEntity.ok().body(result);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteuser(@PathVariable String userId) {
        if (userId == null)
            throw new MissingUserIdException();
        boolean result = keycloakService.deleteUser(userId);
        if (result)
            return ResponseEntity.ok().body("user deleted successfuly");
        return ResponseEntity.badRequest().body("error deleting the user");
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUserById(@PathVariable String userId) {
        if (userId == null)
            throw new MissingUserIdException();
        UserDTO result = keycloakService.getKeycloakUserById(userId);
        if (result.getId() != null)
            return ResponseEntity.ok().body(result);
        return ResponseEntity.badRequest().body("can not get the user based on this id ");
    }


}
