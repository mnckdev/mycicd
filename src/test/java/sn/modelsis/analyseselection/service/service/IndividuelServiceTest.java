package sn.modelsis.analyseselection.service.service;

import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import sn.modelsis.analyseselection.service.DTO.IndividuelDTO;
import sn.modelsis.analyseselection.service.entity.*;
import sn.modelsis.analyseselection.service.mapper.IndividuelMapper;
import sn.modelsis.analyseselection.service.repository.*;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
class IndividuelServiceTest {
    private final EasyRandom generator = new EasyRandom();
    @Mock
    private IndividuelRepository individuelRepository;
    @Mock
    private StatutRepository statutRepository;
    @Mock
    private TrancheAgeRepository trancheAgeRepository;
    @Mock
    private CommuneRepository communeRepository;
    @Mock
    private TypeSoutienRepository typeSoutienRepository;
    @Mock
    private SousCategoriesRepository sousCategoriesRepository;
    @Mock
    private IndividuelMapper individuelMapper;

    @InjectMocks
    private IndividuelService individuelService;
    @Test
    void update() {
        //GIVEN
        IndividuelDTO individuelDto = generator.nextObject(IndividuelDTO.class);
        Individuel individuel = generator.nextObject(Individuel.class);
        Statut statut =  generator.nextObject(Statut.class);
        TypeSoutien typeSoutien = generator.nextObject(TypeSoutien.class);
        TypeSoutien soutienImmedia = generator.nextObject(TypeSoutien.class);
        List<TypeSoutien> listTypeSoutien = new ArrayList<>();
        listTypeSoutien.add(typeSoutien);
        TrancheAge trancheAge = generator.nextObject(TrancheAge.class);
        Commune commune = generator.nextObject(Commune.class);
        SousCategories sousCategories = generator.nextObject(SousCategories.class);

        Individuel expectedResult = individuel;
        expectedResult.setCommune(commune);
        expectedResult.setCommuneRattach(commune);
        expectedResult.setTrancheAge(trancheAge);
        expectedResult.setSousCategorieInd(sousCategories);
        expectedResult.setSousCategorieMig(sousCategories);
        expectedResult.setListSoutiens(listTypeSoutien);
        expectedResult.setSoutiensImmediat(soutienImmedia);
        expectedResult.setStatut(statut);

        //WHEN
        doReturn(Optional.of(individuel)).when(individuelRepository).findById(individuelDto.getIdPIE());
        doReturn(individuel).when(individuelRepository).save(individuel);
        doReturn(individuelDto).when(individuelMapper).asDTO(individuel);
        doReturn(individuel).when(individuelMapper).asEntity(individuelDto);
        doReturn(statut).when(statutRepository).findStatutByCode(individuel.getStatut().getCode());
        doReturn(Optional.of(trancheAge)).when(trancheAgeRepository).findTrancheAgeByCode(individuel.getTrancheAge().getCode());
        doReturn(Optional.of(commune)).when(communeRepository).findCommuneByCode(individuel.getCommune().getCode());
        //doReturn(Optional.of(commune)).when(communeRepository).findCommuneByCode(individuel.getCommuneRattach().getCode());
        doReturn(soutienImmedia).when(typeSoutienRepository).save(individuel.getSoutiensImmediat());
        doReturn(listTypeSoutien).when(typeSoutienRepository).saveAll(individuel.getListSoutiens());
        doReturn(Optional.of(sousCategories)).when(sousCategoriesRepository).findSousCategoriesByCode(individuel.getSousCategorieInd().getCode());
        //doReturn(Optional.of(sousCategories)).when(sousCategoriesRepository).findSousCategoriesByCode(individuel.getSousCategorieMig().getCode());
        IndividuelDTO result = individuelService.update(individuelDto.getIdPIE(),individuelDto);

        //VERIFY
        assertThat(Objects.equals(result.getCommune().getCode(), individuelDto.getCommune().getCode())).isTrue();
        assertThat(Objects.equals(result.getSoutiensImmediat().getCode(), individuelDto.getSoutiensImmediat().getCode())).isTrue();
        assertThat(Objects.equals(result.getSousCategorieInd().getCode(), individuelDto.getSousCategorieInd().getCode())).isTrue();
        assertThat(Objects.equals(result.getTrancheAge().getCode(), individuelDto.getTrancheAge().getCode())).isTrue();
        assertThat(Objects.equals(result.getStatut().getCode(), individuelDto.getStatut().getCode())).isTrue();

    }

    @Test
    void verifyIndividuel_ShouldReturnEmptyString() {
        //GIVEN
        IndividuelDTO individuelDto = generator.nextObject(IndividuelDTO.class);

        //WHEN
        String result = individuelService.verifyIndividuel(individuelDto);

        //VERIFY
        assertThat(result).isEmpty();
    }
    @Test
    void verifyIndividuel_ShouldReturnAPropertyName() {
        //GIVEN
        IndividuelDTO individuelDto = generator.nextObject(IndividuelDTO.class);
        individuelDto.setPrenom(null);

        //WHEN
        String result = individuelService.verifyIndividuel(individuelDto);

        //VERIFY
        assertThat(result).isEqualTo("Prénom");
    }

    @Test
    void verifyCheck_ShouldReturnEmptyString() {
        //GIVEN
        IndividuelDTO individuelDto = generator.nextObject(IndividuelDTO.class);

        //WHEN
        String result = individuelService.verifyCheck(false,individuelDto);

        //VERIFY
        assertThat(result).isEmpty();
    }
    @Test
    void verifyCheck_ShouldReturnAMsgWithPropertyName() {
        //GIVEN
        IndividuelDTO individuelDto = generator.nextObject(IndividuelDTO.class);

        //WHEN
        String result = individuelService.verifyCheck(true,individuelDto);

        //VERIFY
        assertThat(result).isEmpty();
    }
}