package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.PhaseDTO;
import sn.modelsis.analyseselection.service.entity.Phase;

@Mapper(componentModel = "spring")
public interface PhaseMapper extends GenericMapper<Phase, PhaseDTO>{
}
