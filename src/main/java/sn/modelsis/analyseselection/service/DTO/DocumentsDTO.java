package sn.modelsis.analyseselection.service.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import sn.modelsis.analyseselection.service.entity.TypeDocument;

import java.util.Date;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DocumentsDTO {

    private UUID idDocument;

    private Date dateCreation ;

    private Date dateModification;

    private Boolean etatDocument;

    private String chemin;

    private Integer nomDocument;

    private ProjetDTO projet;

    private TypeDocument typeDocument;
}
