package sn.modelsis.analyseselection.service.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class NoteQuestion {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private UUID idNote;

    private int note;

    private String reponse;

//    @ManyToOne(cascade = CascadeType.ALL)
//    @JsonIgnore
//    private PIE pie;

    @ManyToOne(fetch = FetchType.EAGER)
    private Questionnaire question;
}
