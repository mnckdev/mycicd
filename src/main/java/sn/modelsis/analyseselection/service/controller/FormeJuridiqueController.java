package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.FormeJuridique;
import sn.modelsis.analyseselection.service.service.FormeJuridiqueService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/forme_juridique")
@Api(
        value = "FormeJuridique  REST Endpoint",
        tags = {"FormeJuridique  Controller (v1)"}
)

public class FormeJuridiqueController {


    private final FormeJuridiqueService formeJuridiqueService;

    public FormeJuridiqueController(FormeJuridiqueService formeJuridiqueService) {
        this.formeJuridiqueService = formeJuridiqueService;
    }

    @ApiOperation(value = "find All FormeJuridique", notes = "find all  FormeJuridique")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<FormeJuridique>> findAll(){
        List<FormeJuridique> formeJuridique = formeJuridiqueService.findAll();
        return new ResponseEntity<List<FormeJuridique>>(formeJuridique, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID FormeJuridique", notes = "find By ID  FormeJuridique")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping("/{id}")
    public ResponseEntity<Optional<FormeJuridique>> findById(@PathVariable UUID id){
        return new ResponseEntity<Optional<FormeJuridique>>(formeJuridiqueService.findById(id), HttpStatus.OK);
    }
    @ApiOperation(value = "create  new  FormeJuridiques", notes = "create a new FormeJuridiques")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @PostMapping
    public ResponseEntity<FormeJuridique> newFormeJuridique(@RequestBody FormeJuridique formeJuridique) {
        return new ResponseEntity<FormeJuridique>(formeJuridiqueService.newFormeJuridique(formeJuridique), HttpStatus.OK);
    }

    @ApiOperation(value = "delete  FormeJuridiques", notes = "delete  FormeJuridiques")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable UUID id) {
        formeJuridiqueService.deleteFormeJuridique(id);
        return new ResponseEntity<>("deleted successfully ...",HttpStatus.OK);
    }


}
