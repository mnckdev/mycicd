package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.Action;
import sn.modelsis.analyseselection.service.service.ActionService;

import javax.annotation.security.RolesAllowed;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/action")
@Api(
        value = "Action REST Endpoint",
        tags = {"Action Controller (v1)"}
)

public class ActionController {


    private final ActionService actionService;

    public ActionController(ActionService actionService) {
        this.actionService = actionService;
    }

    @ApiOperation(value = "find All Action", notes = "find all  Action")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<Action>> findAll(){
        List<Action> action = actionService.findAll();
        return new ResponseEntity<>(action, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID Action", notes = "find By ID  Action")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<Action>> findById(@PathVariable Integer id){
        return new ResponseEntity<>(actionService.findById(id), HttpStatus.OK);
    }
    @ApiOperation(value = "create  new  Actions", notes = "create a new Actions")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })
    @PostMapping
    public ResponseEntity<Action> newAction(@RequestBody Action action) {
        return new ResponseEntity<>(actionService.newAction(action), HttpStatus.OK);
    }

    @ApiOperation(value = "delete  Actions", notes = "delete  Actions")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id) {
        actionService.deleteAction(id);
        return new ResponseEntity<>("deleted successfully ...",HttpStatus.OK);
    }

}
