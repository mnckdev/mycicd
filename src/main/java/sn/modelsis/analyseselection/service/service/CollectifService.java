package sn.modelsis.analyseselection.service.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.modelsis.analyseselection.service.DTO.CollectifsDTO;
import sn.modelsis.analyseselection.service.entity.Cohorte;
import sn.modelsis.analyseselection.service.entity.Collectif;
import sn.modelsis.analyseselection.service.entity.Commune;
import sn.modelsis.analyseselection.service.entity.Statut;
import sn.modelsis.analyseselection.service.mapper.CollectifMapper;
import sn.modelsis.analyseselection.service.mapper.CommuneMapper;
import sn.modelsis.analyseselection.service.repository.*;
import sn.modelsis.analyseselection.service.serviceinterface.CollectifServiceInterface;
import sn.modelsis.analyseselection.service.utils.Constantes;

import java.util.*;

@Service
@Log4j2

public class CollectifService implements CollectifServiceInterface {

    private final CollectifRepository collectifRepository;
    private final StatutRepository statutRepository;
    private final ProfilMembreRepository profilMembreRepository;
    private final CommuneRepository communeRepository;
    private final CouvertureGeographiqueRepository couvertureGeographiqueRepository;
    private final FormeJuridiqueRepository formeJuridiqueRepository;
    private final TrancheAgeRepository trancheAgeRepository;
    private final TypeSoutienRepository typeSoutienRepository;
    private final CollectifMapper collectifMapper;
    private final CommuneMapper communeMapper;

    public CollectifService(CollectifRepository collectifRepository,
                            StatutRepository statutRepository,
                            ProfilMembreRepository profilMembreRepository,
                            CommuneRepository communeRepository,
                            CouvertureGeographiqueRepository couvertureGeographiqueRepository,
                            FormeJuridiqueRepository formeJuridiqueRepository,
                            TrancheAgeRepository trancheAgeRepository,
                            TypeSoutienRepository typeSoutienRepository,
                            CollectifMapper collectifMapper, CommuneMapper communeMapper) {
        this.collectifRepository = collectifRepository;
        this.statutRepository = statutRepository;
        this.profilMembreRepository = profilMembreRepository;
        this.communeRepository = communeRepository;
        this.couvertureGeographiqueRepository = couvertureGeographiqueRepository;
        this.formeJuridiqueRepository = formeJuridiqueRepository;
        this.trancheAgeRepository = trancheAgeRepository;
        this.typeSoutienRepository = typeSoutienRepository;
        this.collectifMapper = collectifMapper;
        this.communeMapper = communeMapper;
    }

    public Collectif create(Collectif collectif) {
        if (collectif == null)
            return null;
        setInitialAttributes(collectif);
        return collectifRepository.save(collectif);
    }

    public Optional<CollectifsDTO> update(CollectifsDTO collectifsDTO) {
        Optional<Collectif> collectiveRetrieve = collectifRepository.findById(collectifsDTO.getIdPIE());
        if (collectiveRetrieve.isEmpty())
            return Optional.empty();
        Collectif collectifUpdated = updateAttributes(collectifsDTO);
        return Optional.of(collectifMapper.asDTO(collectifRepository.save(collectifUpdated)));
    }

    @Transactional
    private Collectif updateAttributes(CollectifsDTO collectifsDTO) {
        Collectif collectif = collectifMapper.asEntity(collectifsDTO);
        setUpdateAttribute(collectif);
        return collectif;
    }

    private void setUpdateAttribute(Collectif collectifRetrieve) {
        setListSoutien(collectifRetrieve);
        setUpdateStatus(collectifRetrieve);
        collectifRetrieve.setCommune(communeRepository.findCommuneByCode(collectifRetrieve.getCommune().getCode()).orElseThrow());
        collectifRetrieve.setTrancheAge(trancheAgeRepository.findTrancheAgeByCode(collectifRetrieve.getTrancheAge().getCode()).orElse(null));
        collectifRetrieve.setFormeJuridique(formeJuridiqueRepository.findFormeJuridiqueByCode(collectifRetrieve.getFormeJuridique().getCode()));
        collectifRetrieve.setCouvertureGeographique(couvertureGeographiqueRepository.findCouvertureGeographiqueByCode(collectifRetrieve.getCouvertureGeographique().getCode()));
        collectifRetrieve.setProfilMembre(profilMembreRepository.findProfilMembreByCode(collectifRetrieve.getProfilMembre().getCode()));
    }

    private void setInitialAttributes(Collectif collectif) {
        setListSoutien(collectif);
        setStatus(collectif);
        collectif.setDate(new Date());
        collectif.setCommune(communeRepository.findCommuneByCode(collectif.getCommune().getCode()).orElseThrow());
        collectif.setTrancheAge(trancheAgeRepository.findTrancheAgeByCode(collectif.getTrancheAge().getCode()).orElse(null));
        collectif.setFormeJuridique(formeJuridiqueRepository.findFormeJuridiqueByCode(collectif.getFormeJuridique().getCode()));
        collectif.setCouvertureGeographique(couvertureGeographiqueRepository.findCouvertureGeographiqueByCode(collectif.getCouvertureGeographique().getCode()));
        collectif.setProfilMembre(profilMembreRepository.findProfilMembreByCode(collectif.getProfilMembre().getCode()));
    }


    private void setStatus(Collectif collectif) {
        collectif.setStatut(statutRepository.findStatutByCode(Constantes.STATUS_INITIALISE));
    }

    private void setUpdateStatus(Collectif collectif) {
        collectif.setStatut(statutRepository.findStatutByCode(collectif.getStatut().getCode()));
    }


    private void setListSoutien(Collectif collectif) {
        collectif.setListSoutiens(typeSoutienRepository.saveAll(collectif.getListSoutiens()));
    }

    public Optional<Collectif> read(UUID uuid) {
        if (uuid == null)
            return Optional.empty();
        return collectifRepository.findById(uuid);
    }

    public List<Collectif> read() {
        return collectifRepository.findAll();
    }

    public void delete(UUID uuid) {
        Optional<Collectif> collectif = collectifRepository.findById(uuid);
        collectif.ifPresent(collectifRepository::delete);
    }

    public Optional<Collectif> findById(UUID uuid){
        return collectifRepository.findById(uuid);
    }
    @Override
    public List<CollectifsDTO> findByCohorte(Cohorte cohorte) {
        List<Collectif> collectifs = collectifRepository.findAllByCohorte(cohorte);
        return collectifMapper.asDTOList(collectifs);
    }

    @Override
    public List<CollectifsDTO> findByCohorteAndCommune(Cohorte cohorte, Commune commune) {
        List<Collectif> collectifs = collectifRepository.findAllByCohorteAndCommune(cohorte, commune);
        return collectifMapper.asDTOList(collectifs);
    }

    @Override
    public List<CollectifsDTO> findAllByCohorteAndCommuneAndStatus(Cohorte cohorte, Commune commune, Statut statut) {
        List<Collectif> collectifs = collectifRepository.findAllByCohorteAndCommuneAndStatut(cohorte, commune, statut);
        return collectifMapper.asDTOList(collectifs);
    }

    private Boolean isNull(Object object) {
        return object == null;
    }

    public Optional<String> verifyBeforeUpdate(CollectifsDTO collectifsDTO) {
        Map<String, Object> checkProperties = setInitialCheckInfo(collectifsDTO);
        for (Map.Entry<String, Object> property : checkProperties.entrySet()) {
            if (isNull(property.getValue()))

                return Optional.of("Veuillez vérifier le champ:  " + property.getKey());
        }
        return Optional.empty();
    }

    public Map<String, Object> setInitialCheckInfo(CollectifsDTO collectifsDTO) {
        Map<String, Object> properties = new HashMap<>();

        properties.put("Denomination", collectifsDTO.getDenomination());
        properties.put("LieuSiege", collectifsDTO.getLieuSiege());
        properties.put("ReconnaissanceJuridique", collectifsDTO.getReconnaissanceJuridique());
        properties.put("date-creation", collectifsDTO.getDateCreation());
        properties.put("profile_des_membres", collectifsDTO.getProfilMembre());
        properties.put("TotalMembre", collectifsDTO.getTotalMembre());
        properties.put("Total_homme", collectifsDTO.getTotal_homme());
        properties.put("Total_femmes", collectifsDTO.getTotal_femmes());
        properties.put("FormeJuridique", collectifsDTO.getFormeJuridique());
        return properties;
    }

}

