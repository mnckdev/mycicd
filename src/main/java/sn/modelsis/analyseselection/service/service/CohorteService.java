package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.modelsis.analyseselection.service.DTO.CohorteDTO;
import sn.modelsis.analyseselection.service.entity.Cohorte;
import sn.modelsis.analyseselection.service.mapper.CohorteMapper;
import sn.modelsis.analyseselection.service.repository.CohorteRepository;
import sn.modelsis.analyseselection.service.serviceinterface.CohorteServiceInterface;
import sn.modelsis.analyseselection.service.utils.Constantes;

import java.util.Date;
import java.util.List;

@Service
public class CohorteService implements CohorteServiceInterface {

    private final CohorteRepository cohorteRepository;

    private final CohorteMapper cohorteMapper;

    public CohorteService(CohorteRepository cohorteRepository, CohorteMapper cohorteMapper) {
        this.cohorteRepository = cohorteRepository;
        this.cohorteMapper = cohorteMapper;
    }


    @Override
    @Transactional
    public CohorteDTO startNewCohorte(CohorteDTO cohorteDTO) {
        List<Cohorte> cohortes = cohorteRepository.findAll();
        for (Cohorte c:
             cohortes) {
            if(c.getStatut() != Constantes.COHORTE_TERMINE){
                c.setStatut(Constantes.COHORTE_TERMINE);
                cohorteRepository.save(c);
            }

        }
        Cohorte cohorte = cohorteMapper.asEntity(cohorteDTO);
        cohorte.setDateDebut(new Date());
        cohorte.setStatut(Constantes.COHORTE_ENCOURS);

        //Affecter les PIE à cette nouvelle cohorte
        cohorte = cohorteRepository.save(cohorte);
        return cohorteMapper.asDTO(cohorte);
    }

    @Override
    public CohorteDTO endCohorte(CohorteDTO cohorteDTO) {
        Cohorte cohorte = cohorteMapper.asEntity(cohorteDTO);
        cohorte.setStatut(Constantes.COHORTE_TERMINE);
        cohorte.setDateFin(new Date());
        cohorte = cohorteRepository.save(cohorte);
        //Changer le statut des PIE d'une cohorte donnée
        return cohorteMapper.asDTO(cohorte);
    }

    @Override
    public CohorteDTO getByStatut(String statut) {
        return cohorteMapper.asDTO(cohorteRepository.findByStatut(statut));
    }
}
