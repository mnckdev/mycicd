package sn.modelsis.analyseselection.service.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Collectif extends PIE {

    @Column(name = "contact1_representant")
    private String contact1Representant;

    @Column(name = "contact2_representant")
    private String contact2Representant;

    private String titre;

    private String denomination;

    private String categories;

    private String reconnaissanceJuridique;

    @ManyToOne
    private Commune communeActivite;

    private String quartierVillageInterieur;

    @Temporal(TemporalType.DATE)
    private Date dateCreation;

    private Integer totalMembre;

    private Integer total_homme;

    private Integer total_femmes;

    @ManyToOne
    private FormeJuridique formeJuridique;

    @ManyToOne
    private Commune commune;

    @OneToOne(cascade = CascadeType.ALL)
    private RapportAnalyse1Collectif rapportAnalyse1Collectif;

    @OneToOne(cascade = CascadeType.ALL)
    private RapportAnalyse2Collectif rapportAnalyse2Collectif;
    // some added fields
    //@OneToOne(cascade = CascadeType.ALL)
    private String lieuSiege;

    @OneToOne(cascade = CascadeType.ALL)
    private CouvertureGeographique couvertureGeographique;

    @OneToOne(cascade = CascadeType.ALL)
    private ProfilMembre profilMembre;
    
    @OneToOne(cascade = CascadeType.ALL)
    private TrancheAge trancheAge;

    private String situationEconomique;

    private Double montantCapitalSocial;

    private Integer nbrEmployePermanent;

    private Integer nbrEmployeTemporaire;

    private Double montantEpargneMobilisee;

    private Double montantEndettement;

    private Double montantSubvensionRecu;

}
