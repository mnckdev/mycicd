package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.Departement;
import sn.modelsis.analyseselection.service.repository.DepartementRepository;

import java.util.List;
import java.util.Optional;

@Service
public class DepartementService {
    private DepartementRepository departementRepository;

    public DepartementService(DepartementRepository departementRepository){
        this.departementRepository=departementRepository;
    }


    public Optional<Departement> findById(Integer id){
        return departementRepository.findById(id);
    }
    public List<Departement> findAll(){
        return departementRepository.findAll();
    }


}
