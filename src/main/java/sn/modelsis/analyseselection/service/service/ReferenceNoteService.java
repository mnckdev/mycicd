package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.ReferenceNote;
import sn.modelsis.analyseselection.service.repository.ReferenceNoteRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ReferenceNoteService {

    private final ReferenceNoteRepository referenceNoteRepository;

    public ReferenceNoteService(ReferenceNoteRepository referenceNoteRepository) {
        this.referenceNoteRepository = referenceNoteRepository;
    }

    public ReferenceNote create(ReferenceNote referenceNote){
        if (referenceNote == null)
            return null;
        return referenceNoteRepository.save(referenceNote);
    }

    public Optional<ReferenceNote> read(Integer id){
        if(id == null)
            return null;
        return  referenceNoteRepository.findById(id);
    }

    public List<ReferenceNote> read(){
        return referenceNoteRepository.findAll();
    }

    public void delete(Integer id){
        Optional<ReferenceNote> referenceNote = referenceNoteRepository.findById(id);
        if(referenceNote.isPresent())
            referenceNoteRepository.delete(referenceNote.get());
    }
}
