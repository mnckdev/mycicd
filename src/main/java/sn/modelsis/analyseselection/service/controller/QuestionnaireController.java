package sn.modelsis.analyseselection.service.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.DTO.QuestionnaireDTO;
import sn.modelsis.analyseselection.service.entity.Questionnaire;
import sn.modelsis.analyseselection.service.service.QuestionnaireService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/questionnaire")
@Api(
        value = "Questionnaire REST endpoint",
        tags = "Questionnaire controller(v1)"
)

public class QuestionnaireController {

    private final QuestionnaireService questionnaireService;

    public QuestionnaireController(QuestionnaireService questionnaireService) {
        this.questionnaireService = questionnaireService;
    }

    @GetMapping
    @ApiOperation(value = "Get all questions", notes = "Get all questions")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    public ResponseEntity<List<QuestionnaireDTO>> findAll(){
        List<QuestionnaireDTO> questionnaires = questionnaireService.read();
        if(questionnaires == null)
            return ResponseEntity.badRequest().body(questionnaires);
        return ResponseEntity.ok().body(questionnaires);
    }

    @ApiOperation(value = "Get one question", notes = "Get one question")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<Questionnaire>> get(@PathVariable Integer id){
        Optional<Questionnaire> questionnaire = questionnaireService.read(id);
        if(questionnaire== null)
            return  ResponseEntity.badRequest().body(questionnaire);
        return ResponseEntity.ok().body(questionnaire);
    }

    @PostMapping
    @ApiResponses(
            {
                    @ApiResponse(code=200, message = "created"),
            }
    )
    @ApiOperation(value = "post a question", notes = "post a question")
    // Devrait changer le retour de ce controller
    public ResponseEntity<Questionnaire> create(@RequestBody Questionnaire questionnaire){
        questionnaire = questionnaireService.create(questionnaire);
        return ResponseEntity.ok().body(questionnaire);
    }

    @DeleteMapping("/{id}")
    @ApiResponses(
            {
                    @ApiResponse(code=200, message = "delete"),
            }
    )
    @ApiOperation(value = "delete a question", notes = "delete a  question")
    public ResponseEntity<String> delete(@PathVariable Integer id){
        questionnaireService.delete(id);
        return ResponseEntity.ok().body("deleted");
    }

    @GetMapping("/phase/{id}")
    @ApiOperation(value = "Find all questions by phase", notes = "Find all questions by phase")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    public ResponseEntity<List<QuestionnaireDTO>> findAllByPhase(@PathVariable Integer id){
        List<QuestionnaireDTO> questionnaires = questionnaireService.findAllByPhase(id);
        if(questionnaires == null)
            return ResponseEntity.badRequest().body(questionnaires);
        return ResponseEntity.ok().body(questionnaires);
    }
}
