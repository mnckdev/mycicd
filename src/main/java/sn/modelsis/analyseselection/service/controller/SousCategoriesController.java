package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.SousCategories;
import sn.modelsis.analyseselection.service.service.SousCategorieService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/sousCategories")
@Api(
        value = "SousCategories REST Endpoint",
        tags = {"SousCategories Controller (v1)"}
)

public class SousCategoriesController {


    private final SousCategorieService sousCategoriesService;

    public SousCategoriesController(SousCategorieService sousCategoriesService) {
        this.sousCategoriesService = sousCategoriesService;
    }

    @ApiOperation(value = "find All SousCategories", notes = "find all  SousCategories")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<SousCategories>> findAll(){
        List<SousCategories> sousCategories = sousCategoriesService.findAll();
        return new ResponseEntity<List<SousCategories>>(sousCategories, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID SousCategories", notes = "find By ID  SousCategories")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<SousCategories>> findById(@PathVariable Integer id){
        return new ResponseEntity<Optional<SousCategories>>(sousCategoriesService.findById(id), HttpStatus.OK);
    }




}
