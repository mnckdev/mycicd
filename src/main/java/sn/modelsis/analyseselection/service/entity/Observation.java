package sn.modelsis.analyseselection.service.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Observation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idRetour;

    @Temporal(TemporalType.DATE)
    private Date dateRetour;

//    @ManyToOne(cascade = CascadeType.ALL)
//    @JsonIgnore
//    private PIE pie;

    private String observations;

    private String auteur; //Agent d'Adel qui a invalidé

    private String usernameExpediteur;

    @ManyToOne
    @JoinColumn(name = "statut_id")
    private Statut statut;

    public Observation(Date dateRetour, String observations, String auteur,Statut statut) {
        this.dateRetour = dateRetour;
        this.observations = observations;
        this.auteur = auteur;
        this.statut=statut;
    }


}
