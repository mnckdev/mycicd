package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.TrancheAge;
import sn.modelsis.analyseselection.service.service.TrancheAgeService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/trancheAge")
@Api(
        value = "TrancheAge REST Endpoint",
        tags = {"TrancheAge Controller (v1)"}
)

public class TrancheAgeController {


    private final TrancheAgeService trancheAgeService;

    public TrancheAgeController(TrancheAgeService trancheAgeService) {
        this.trancheAgeService = trancheAgeService;
    }

    @ApiOperation(value = "find All TrancheAge", notes = "find all  TrancheAge")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<TrancheAge>> findAll(){
        List<TrancheAge> trancheAge = trancheAgeService.findAll();
        return new ResponseEntity<>(trancheAge, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID TrancheAge", notes = "find By ID  TrancheAge")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<TrancheAge>> findById(@PathVariable Integer id){
        return new ResponseEntity<Optional<TrancheAge>>(trancheAgeService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "create TrancheAge", notes = "TrancheAge Creation")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @PostMapping()
    public ResponseEntity<Optional<TrancheAge>> create(@RequestBody TrancheAge trancheAge){
        return new ResponseEntity<>(Optional.of(trancheAgeService.create(trancheAge)), HttpStatus.OK);
    }



}
