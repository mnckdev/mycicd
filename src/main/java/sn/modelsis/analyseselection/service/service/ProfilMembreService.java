package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.ProfilMembre;
import sn.modelsis.analyseselection.service.repository.ProfilMembreRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProfilMembreService {

    private ProfilMembreRepository profilMembreRepository;

    public ProfilMembreService(ProfilMembreRepository profilMembreRepository){
        this.profilMembreRepository=profilMembreRepository;
    }

    public ProfilMembre newProfilMembre(ProfilMembre profilMembre){
        return  profilMembreRepository.save(profilMembre);
    }

    public Optional<ProfilMembre> findById(UUID id){
        return profilMembreRepository.findById(id);
    }
    public Optional<ProfilMembre> findByCode(String code){
        return Optional.of(profilMembreRepository.findProfilMembreByCode(code));
    }
    public List<ProfilMembre> findAll(){
        return profilMembreRepository.findAll();
    }
     public void deleteProfilMembre(UUID id){
        profilMembreRepository.deleteById(id);
     }


}
