package sn.modelsis.analyseselection.service.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProjetDTO {

    private UUID idProjet;

    private String description;

    private String informationSupplementaire;

    private String intituleProjet;

    private String motifRetour;

    private Integer objet;

    private ModeleEconomiqueDTO modelEconomique;
}
