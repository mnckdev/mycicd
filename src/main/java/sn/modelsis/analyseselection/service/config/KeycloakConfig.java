package sn.modelsis.analyseselection.service.config;

import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KeycloakConfig {

    @Value("${keycloak.clientId}")
    private String clientId;

    @Value("${keycloak.host}")
    private String keycloakHost;

    @Value("${keycloak.realm}")
    private String realm;

    @Value("${keycloak.secret}")
    private String secret;

    @Bean
    public Keycloak getKeycloakInstance() {
      return   KeycloakBuilder.builder().serverUrl(keycloakHost).realm(realm).grantType(OAuth2Constants.CLIENT_CREDENTIALS)
                .clientId(clientId).clientSecret(secret).build();

 /*     return   KeycloakBuilder.builder()
                .serverUrl(keycloakHost)
                .realm("ADEL")
                .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
                .clientId("adel-api-client")
                .clientSecret("Bv00cg8zKv1I94wep8SxsgsnchyiBL3c")
                .build();*/
    }

}

