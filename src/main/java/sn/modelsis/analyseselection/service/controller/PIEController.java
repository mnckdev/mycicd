package sn.modelsis.analyseselection.service.controller;

import feign.Param;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.DTO.*;
import sn.modelsis.analyseselection.service.DTO.externalDTO.FilterDto;
import sn.modelsis.analyseselection.service.entity.NoteQuestion;
import sn.modelsis.analyseselection.service.entity.PIE;
import sn.modelsis.analyseselection.service.mapper.PIEMapper;
import sn.modelsis.analyseselection.service.service.PIEService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@Log4j2
@RequestMapping("/api/v1/pie")
@Api(
        value = "PIE REST endpoint",
        tags = "PIE controller(v1)"
)

public class PIEController {

    private final PIEService pieService;

    final private PIEMapper pieMapper;
    public PIEController(PIEService pieService, PIEMapper pieMapper) {
        this.pieService = pieService;

        this.pieMapper = pieMapper;
    }

    @GetMapping
    @ApiOperation(value = "Get all PIES nouvellement inscrit", notes = "Get all PIES nouvellement inscrit")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    public ResponseEntity<List<PIEDTO>> findAll() {
        List<PIEDTO> pies = pieService.getPIEsInscrits();
        if (pies == null)
            return ResponseEntity.badRequest().body(new ArrayList<>());
        return ResponseEntity.ok().body(pies);
    }

    @GetMapping("/getall")
    @ApiOperation(value = "Get all PIES without conditions ", notes = "Get all PIES ")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    public ResponseEntity<List<PIEDTO>> findAllPIES() {
        List<PIEDTO> pies = pieService.getPIEs();
        if (pies == null)
            return ResponseEntity.badRequest().body(new ArrayList<>());
        return ResponseEntity.ok().body(pies);
    }

    @GetMapping("/all")
    @ApiOperation(value = "Get all PIES nouvellement inscrit", notes = "Get all PIES nouvellement inscrit")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    public ResponseEntity<List<PIEDTO>> findAllPiesDto() {
        List<PIEDTO> pies = pieService.getPIEs();
        if (pies == null)
            return ResponseEntity.badRequest().body(pies);
        return ResponseEntity.ok().body(pies);
    }

    @ApiOperation(value = "Get one PIE", notes = "Get one PIE")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<PIEDTO>> get(@PathVariable UUID id) {
        Optional<PIEDTO> pie = pieService.read(id);
        if (pie.isEmpty())
            return ResponseEntity.badRequest().body(pie);
        return ResponseEntity.ok().body(pie);
    }

    @PostMapping
    @ApiResponses(
            {
                    @ApiResponse(code = 200, message = "created"),
            }
    )
    @ApiOperation(value = "post a PIE", notes = "post a PIE")
    public ResponseEntity<PIEDTO> create(@RequestBody PIEDTO pie) {
        pie = pieService.create(pieMapper.asEntity(pie));
        return ResponseEntity.ok().body(pie);
    }

    @DeleteMapping("/{id}")
    @ApiResponses(
            {
                    @ApiResponse(code = 200, message = "delete"),
            }
    )
    @ApiOperation(value = "delete a PIE", notes = "delete a  PIE")
    public ResponseEntity<String> delete(@PathVariable UUID id) {
        pieService.delete(id);
        return ResponseEntity.ok().body("deleted");
    }

    @GetMapping("/statut/{id}")
    public ResponseEntity<List<PIEDTO>> findAllByStatut(@PathVariable Integer id) {
        return ResponseEntity.ok().body(pieService.findAllByStatut(id));
    }


    @PostMapping("/{id}/statut")
    public ResponseEntity<PIEDTO> changePIEStatut(@PathVariable UUID id, @RequestBody StatutDTO statut) {
        return ResponseEntity.ok().body(pieService.changePIEStatut(id, statut));
    }

    @GetMapping("/statuts/{id}")
    public ResponseEntity<List<PIE>> findAllByStatuts(@PathVariable Integer id) {
        return ResponseEntity.ok().body(pieService.findAllByStatuts(id));
    }

    @GetMapping("/statut/code/{codeStatut}")
    public ResponseEntity<Optional<List<PIE>>> findAllByStatutCode(@PathVariable String codeStatut, HttpServletRequest request) {
        Optional<List<PIE>> pies = pieService.findAllByStatutCode(codeStatut);
        if (pies.isPresent())
            return ResponseEntity.ok().body(pies);
        return ResponseEntity.internalServerError().body(pies);
    }

    @GetMapping("/commune-statut-code")
    public ResponseEntity<List<PIEDTO>> findAllByStatutCode(@Param String codeStatut, @Param String codeCommune) {
        List<PIEDTO> pies = pieService.findAllByStatutAndCommuneCode(codeStatut, codeCommune);
        return ResponseEntity.ok().body(pies);
    }

    @GetMapping("/check_cni/{cni}")
    public ResponseEntity<Boolean> isCniAlreadyExist(@PathVariable String cni) {
        return ResponseEntity.ok().body(pieService.isCniPieAlreadyExist(cni));
    }

    @PostMapping("/{id}/noter")
    ResponseEntity<Optional<PIEDTO>> noterPIE(@PathVariable UUID id,
                                              @RequestBody List<NoteQuestion> notes) {
        Optional<PIEDTO> piedto = pieService.noterPIE(id, notes);
        if (piedto.isPresent())
            return ResponseEntity.ok().body(piedto);
        return ResponseEntity.internalServerError().body(piedto);
    }

    @PostMapping("/{id}/retour")
    ResponseEntity<Optional<PIEDTO>> retournerPIE(@PathVariable UUID id,
                                                  @RequestBody ObservationDTO observation) {
        return ResponseEntity.ok().body(pieService.retournerPIE(id, observation));
    }

    @PostMapping("/{id}/projet")
    public ResponseEntity<Optional<PIEDTO>> creerProjet(@PathVariable UUID id, @RequestBody ProjetDTO projetDTO) {
        return ResponseEntity.ok().body(pieService.creerProjet(id, projetDTO));
    }

    @PostMapping("/{id}/projet/model-economique")
    public ResponseEntity<Optional<PIEDTO>> concevoirBMC(@PathVariable UUID id,
                                                         @RequestBody ModeleEconomiqueDTO modeleEconomiqueDTO) {
        return ResponseEntity.ok().body(pieService.concevoirModeleEconomique(id, modeleEconomiqueDTO));
    }

    @PostMapping("/searchFilter")
    @ApiOperation(value = "Search inscrit by filters", notes = "Search inscrit by filters")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    public ResponseEntity<List<PIEDTO>> findPieByFilters(@RequestBody FilterDto filter) {

        List<PIEDTO> pies = pieService.getPIEsInscritsByFilters(filter);
        if (pies == null)
            return ResponseEntity.badRequest().body(pies);
        return ResponseEntity.ok().body(pies);
    }
}
