package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.TypeSoutien;
import sn.modelsis.analyseselection.service.service.TypeSoutienService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/type_soutien")
@Api(
        value = "TypeSoutien  REST Endpoint",
        tags = {"TypeSoutien  Controller (v1)"}
)

public class TypeSoutienController {

    private final TypeSoutienService typeSoutienService;

    public TypeSoutienController(TypeSoutienService typeSoutienService) {
        this.typeSoutienService = typeSoutienService;
    }

    @ApiOperation(value = "find All TypeSoutien", notes = "find all  TypeSoutien")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<TypeSoutien>> findAll() {
        List<TypeSoutien> typeSoutien = typeSoutienService.findAll();
        return new ResponseEntity<List<TypeSoutien>>(typeSoutien, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID TypeSoutien", notes = "find By ID  TypeSoutien")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping("/{id}")
    public ResponseEntity<Optional<TypeSoutien>> findById(@PathVariable Integer id) {
        return new ResponseEntity<Optional<TypeSoutien>>(typeSoutienService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "create  new  TypeSoutiens", notes = "create a new TypeSoutiens")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @PostMapping
    public ResponseEntity<TypeSoutien> newTypeSoutien(@RequestBody TypeSoutien typeSoutien) {
        return new ResponseEntity<TypeSoutien>(typeSoutienService.create(typeSoutien), HttpStatus.OK);
    }

    @ApiOperation(value = "delete  TypeSoutiens", notes = "delete  TypeSoutiens")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id) {
        typeSoutienService.delete(id);
        return new ResponseEntity<>("deleted successfully ...", HttpStatus.OK);
    }


}
