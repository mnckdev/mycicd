package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.CouvertureGeographique;
import sn.modelsis.analyseselection.service.repository.CouvertureGeographiqueRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CouvertureGeographiqueService {

    private CouvertureGeographiqueRepository couvertureGeographiqueRepository;

    public CouvertureGeographiqueService(CouvertureGeographiqueRepository couvertureGeographiqueRepository){
        this.couvertureGeographiqueRepository=couvertureGeographiqueRepository;
    }

    public CouvertureGeographique newCouvertureGeographique(CouvertureGeographique couvertureGeographique){
        return  couvertureGeographiqueRepository.save(couvertureGeographique);
    }

    public Optional<CouvertureGeographique> findById(UUID id){
        return couvertureGeographiqueRepository.findById(id);
    }
    public Optional<CouvertureGeographique> findByCode(String code){
        return Optional.of(couvertureGeographiqueRepository.findCouvertureGeographiqueByCode(code));
    }
    public List<CouvertureGeographique> findAll(){
        return couvertureGeographiqueRepository.findAll();
    }
     public void deleteCouvertureGeographique(UUID id){
        couvertureGeographiqueRepository.deleteById(id);
     }


}
