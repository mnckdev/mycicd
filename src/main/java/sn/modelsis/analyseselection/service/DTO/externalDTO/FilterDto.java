package sn.modelsis.analyseselection.service.DTO.externalDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FilterDto {
    private String codeCommune;

    private String parcoursPie;

    private List<String> listSoutien;
}
