package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.CouvertureGeographique;
import sn.modelsis.analyseselection.service.service.CouvertureGeographiqueService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/couverture_geographique")
@Api(
        value = "CouvertureGeographique  REST Endpoint",
        tags = {"CouvertureGeographique  Controller (v1)"}
)

public class CouvertureGeographiqueController {


    private final CouvertureGeographiqueService couvertureGeographiqueService;

    public CouvertureGeographiqueController(CouvertureGeographiqueService couvertureGeographiqueService) {
        this.couvertureGeographiqueService = couvertureGeographiqueService;
    }

    @ApiOperation(value = "find All CouvertureGeographique", notes = "find all  CouvertureGeographique")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<CouvertureGeographique>> findAll(){
        List<CouvertureGeographique> couvertureGeographique = couvertureGeographiqueService.findAll();
        return new ResponseEntity<List<CouvertureGeographique>>(couvertureGeographique, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID CouvertureGeographique", notes = "find By ID  CouvertureGeographique")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping("/{id}")
    public ResponseEntity<Optional<CouvertureGeographique>> findById(@PathVariable UUID id){
        return new ResponseEntity<Optional<CouvertureGeographique>>(couvertureGeographiqueService.findById(id), HttpStatus.OK);
    }
    @ApiOperation(value = "create  new  CouvertureGeographiques", notes = "create a new CouvertureGeographiques")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @PostMapping
    public ResponseEntity<CouvertureGeographique> newCouvertureGeographique(@RequestBody CouvertureGeographique couvertureGeographique) {
        return new ResponseEntity<CouvertureGeographique>(couvertureGeographiqueService.newCouvertureGeographique(couvertureGeographique), HttpStatus.OK);
    }

    @ApiOperation(value = "delete  CouvertureGeographiques", notes = "delete  CouvertureGeographiques")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable UUID id) {
        couvertureGeographiqueService.deleteCouvertureGeographique(id);
        return new ResponseEntity<>("deleted successfully ...",HttpStatus.OK);
    }


}
