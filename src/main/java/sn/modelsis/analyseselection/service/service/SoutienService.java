package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.Soutien;
import sn.modelsis.analyseselection.service.repository.SoutienRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class SoutienService {


    private SoutienRepository soutienRepository;

    public SoutienService(SoutienRepository soutienRepository){
        this.soutienRepository=soutienRepository;
    }


    public Optional<Soutien> findById(UUID id){
        return soutienRepository.findById(id);
    }
    public List<Soutien> findAll(){
        return soutienRepository.findAll();
    }

    public Soutien create(Soutien soutien){
        return soutienRepository.save(soutien);
    }

}
