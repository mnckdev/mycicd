package sn.modelsis.analyseselection.service.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.persistence.*;
import java.util.Date;
import java.util.List;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class CompteRendue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idCompteRendue;

    @Temporal(TemporalType.DATE)
    private Date periode;

    private String lieu;

    @Column(length = 2000)
    private String participants;

    @Column(length = 2000)
    private String interventionsEchanges;

    private int nombreIdees;

    @OneToMany
    private List<CreateursCompteRendue> createursContent;

    private String idDocument;

    private String cohorte;

    private String commune;
}
