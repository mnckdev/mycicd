package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.TrancheAge;
import sn.modelsis.analyseselection.service.repository.TrancheAgeRepository;

import java.util.List;
import java.util.Optional;

@Service
public class TrancheAgeService {

    private TrancheAgeRepository trancheAgeRepository;

    public TrancheAgeService(TrancheAgeRepository trancheAgeRepository){
        this.trancheAgeRepository=trancheAgeRepository;
    }


    public Optional<TrancheAge> findById(Integer id){
        return trancheAgeRepository.findById(id);
    }
    public Optional<TrancheAge> findByCode(String code){
        return trancheAgeRepository.findTrancheAgeByCode(code);
    }
    public List<TrancheAge> findAll(){
        return trancheAgeRepository.findAll();
    }
    public TrancheAge create(TrancheAge trancheAge){
        return trancheAgeRepository.save(trancheAge);
    }

}
