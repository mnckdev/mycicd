package sn.modelsis.analyseselection.service.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class Documents {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private UUID idDocument;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation ;
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateModification;
    private Boolean etatDocument;
    private String chemin;
    private Integer nomDocument;
    @ManyToOne
    private Projet projet;

    @ManyToOne
    private TypeDocument typeDocument;

    @OneToOne(optional = true)
    private CompteRendue compteRendue;
}
