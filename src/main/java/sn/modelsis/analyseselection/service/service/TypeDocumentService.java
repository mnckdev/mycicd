package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.TypeDocument;
import sn.modelsis.analyseselection.service.repository.TypeDocumentRepository;

import java.util.List;
import java.util.Optional;

@Service
public class TypeDocumentService {

    private TypeDocumentRepository typeDocumentRepository;

    public TypeDocumentService(TypeDocumentRepository typeDocumentRepository){
        this.typeDocumentRepository=typeDocumentRepository;
    }

    public TypeDocument newTypeDocument(TypeDocument typeDocument){
        return  typeDocumentRepository.save(typeDocument);
    }

    public Optional<TypeDocument> findById(Integer id){
        return typeDocumentRepository.findById(id);
    }
    public List<TypeDocument> findAll(){
        return typeDocumentRepository.findAll();
    }
    public void deleteTypeDocument(Integer id){
        typeDocumentRepository.deleteById(id);
    }
}
