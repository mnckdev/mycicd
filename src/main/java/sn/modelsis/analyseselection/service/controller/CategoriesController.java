package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.DTO.CategoriesDTO;
import sn.modelsis.analyseselection.service.service.CategoriesService;

import java.util.List;

@RestController
@RequestMapping("/api/v1/categories")
@Api(
        value = "Categories REST Endpoint",
        tags = {"Categories Controller (v1)"}
)

public class CategoriesController {


    private final CategoriesService categoriesService;

    public CategoriesController(CategoriesService categoriesService) {
        this.categoriesService = categoriesService;
    }

    @ApiOperation(value = "find All Categories", notes = "find all  Categories")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping
    public ResponseEntity<List<CategoriesDTO>> findAll(){
        List<CategoriesDTO> categories = categoriesService.getCategories() ;
        if(categories == null)
            return ResponseEntity.badRequest().body(null);
        return ResponseEntity.ok().body(categories);
    }

    @ApiOperation(value = "find By ID Categories", notes = "find By ID  Categories")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<CategoriesDTO> findById(@PathVariable Integer id){
        CategoriesDTO categoriesDTO = categoriesService.getCategorie(id);
        if(categoriesDTO == null)
            return ResponseEntity.badRequest().body(null);
        return ResponseEntity.ok().body(categoriesDTO);
    }


}
