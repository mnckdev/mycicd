package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.RegionDTO;
import sn.modelsis.analyseselection.service.entity.Region;

@Mapper(componentModel = "spring")
public interface RegionMapper extends GenericMapper<Region, RegionDTO>{
}
