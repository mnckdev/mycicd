package sn.modelsis.analyseselection.service.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class RapportAnalyse {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private UUID idRapport;

    @Column(length = 2000)
    private String porteur;

    @Column(length = 2000)
    private String activite;

    @Column(length = 2000)
    private String environnement;

    @Column(length = 2000)
    private String organisation;

    @Column(length = 2000)
    private String faiblesse;
}
