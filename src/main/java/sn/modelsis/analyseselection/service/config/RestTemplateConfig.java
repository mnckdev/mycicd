package sn.modelsis.analyseselection.service.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import sn.modelsis.analyseselection.service.filter.TokenAddingInterceptor;

@Configuration
public class RestTemplateConfig {
    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getInterceptors().add(new TokenAddingInterceptor());
        return restTemplate;
    }
}




