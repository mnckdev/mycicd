package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.Statut;

@Repository
public interface StatutRepository extends JpaRepository<Statut,Integer> {

    Statut findStatutByCode(String code);
}
