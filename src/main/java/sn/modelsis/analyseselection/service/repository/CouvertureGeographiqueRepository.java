package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.CouvertureGeographique;

import java.util.UUID;

@Repository
public interface CouvertureGeographiqueRepository extends JpaRepository<CouvertureGeographique, UUID> {
    CouvertureGeographique findCouvertureGeographiqueByCode(String code);
}
