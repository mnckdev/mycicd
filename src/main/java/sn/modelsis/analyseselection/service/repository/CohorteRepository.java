package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.Cohorte;

@Repository
public interface CohorteRepository extends JpaRepository<Cohorte, Integer> {
    Cohorte findByStatut(String statut);
}
