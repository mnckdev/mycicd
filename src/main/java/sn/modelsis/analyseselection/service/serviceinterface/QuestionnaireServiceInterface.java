package sn.modelsis.analyseselection.service.serviceinterface;

import sn.modelsis.analyseselection.service.DTO.QuestionnaireDTO;
import sn.modelsis.analyseselection.service.entity.Phase;

import java.util.List;

public interface QuestionnaireServiceInterface {

    public List<QuestionnaireDTO> getQuestionsByPhase(Phase phase);

}
