package sn.modelsis.analyseselection.service.controller;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.Marqueur;
import sn.modelsis.analyseselection.service.service.MarqueurService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/marqueur")
@Api(
        value = "Marqueur REST Endpoint",
        tags = {"Marqueur Controller (v1)"}
)

public class MarqueurController {


    private final MarqueurService marqueurService;

    public MarqueurController(MarqueurService marqueurService) {
        this.marqueurService = marqueurService;
    }

    @ApiOperation(value = "find All Marqueur", notes = "find all  Marqueur")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<Marqueur>> findAll(){
        List<Marqueur> marqueur = marqueurService.findAll();
        return new ResponseEntity<List<Marqueur>>(marqueur, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID Marqueur", notes = "find By ID  Marqueur")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<Marqueur>> findById(@PathVariable Integer id){
        return new ResponseEntity<Optional<Marqueur>>(marqueurService.findById(id), HttpStatus.OK);
    }




}
