package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.IdeeProjet;

import java.util.UUID;

@Repository
public interface IdeeProjetRepository extends JpaRepository<IdeeProjet, UUID> {

//    public List<IdeeProjet> findAllByIndividuel(Individuel individuel);
}
