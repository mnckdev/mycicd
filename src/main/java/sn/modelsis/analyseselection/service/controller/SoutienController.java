package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.Soutien;
import sn.modelsis.analyseselection.service.service.SoutienService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/soutien")
@Api(
        value = "Soutien REST Endpoint",
        tags = {"Soutien Controller (v1)"}
)

public class SoutienController {


    private final SoutienService soutienService;

    public SoutienController(SoutienService soutienService) {
        this.soutienService = soutienService;
    }

    @ApiOperation(value = "find All Soutien", notes = "find all  Soutien")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<Soutien>> findAll(){
        List<Soutien> soutien = soutienService.findAll();
        return new ResponseEntity<List<Soutien>>(soutien, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID Soutien", notes = "find By ID  Soutien")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<Soutien>> findById(@PathVariable UUID id){
        return new ResponseEntity<Optional<Soutien>>(soutienService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "Add new  Soutien", notes = "adding   Soutien")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @PostMapping()
    public ResponseEntity<Optional<Soutien>> create(@RequestBody Soutien soutien){
        return new ResponseEntity<>(Optional.of(soutienService.create(soutien)), HttpStatus.OK);
    }

}
