package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.CompteRendueDTO;
import sn.modelsis.analyseselection.service.entity.CompteRendue;

@Mapper(componentModel = "spring")
public interface CompteRendueMapper extends GenericMapper<CompteRendue, CompteRendueDTO> {
}
