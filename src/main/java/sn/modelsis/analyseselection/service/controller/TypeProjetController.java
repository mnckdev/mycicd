package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.TypeProjet;
import sn.modelsis.analyseselection.service.service.TypeProjetService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/typeProjet")
@Api(
        value = "TypeProjet  REST Endpoint",
        tags = {"TypeProjet  Controller (v1)"}
)

public class TypeProjetController {


    private final TypeProjetService typeProjetService;

    public TypeProjetController(TypeProjetService typeProjetService) {
        this.typeProjetService = typeProjetService;
    }

    @ApiOperation(value = "find All Type Projet", notes = "find all  Type Projet")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping
    public ResponseEntity<List<TypeProjet>> findAll(){
        List<TypeProjet> typeProjet = typeProjetService.findAll();
        return new ResponseEntity<List<TypeProjet>>(typeProjet, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID Type Projet", notes = "find By ID  Type Projet")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping("/{id}")
    public ResponseEntity<Optional<TypeProjet>> findById(@PathVariable Integer id){
        return new ResponseEntity<Optional<TypeProjet>>(typeProjetService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "create  new  Type Projets", notes = "create a new Type Projets")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @PostMapping
    public ResponseEntity<TypeProjet> newTypeProjet(@RequestBody TypeProjet typeProjet) {
        return new ResponseEntity<TypeProjet>(typeProjetService.newTypeProjet(typeProjet), HttpStatus.OK);
    }

    @ApiOperation(value = "delete  Type Projets", notes = "delete  Type Projets")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id) {
        typeProjetService.deleteTypeProjet(id);
        return new ResponseEntity<>("deleted successfully ...",HttpStatus.OK);
    }

}
