package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.SecteurActivite;
import sn.modelsis.analyseselection.service.service.SecteurActiviteService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/secteurActivite")
@Api(
        value = "SecteurActivite  REST Endpoint",
        tags = {"SecteurActivite  Controller (v1)"}
)

public class SecteurActiviteController {


    private final SecteurActiviteService secteurActiviteService;

    public SecteurActiviteController(SecteurActiviteService secteurActiviteService) {
        this.secteurActiviteService = secteurActiviteService;
    }

    @ApiOperation(value = "find All Secteur Activite", notes = "find all  Secteur Activite")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<SecteurActivite>> findAll(){
        List<SecteurActivite> secteurActivite = secteurActiviteService.findAll();
        return new ResponseEntity<List<SecteurActivite>>(secteurActivite, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID Secteur Activite", notes = "find By ID  Secteur Activite")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping("/{id}")
    public ResponseEntity<Optional<SecteurActivite>> findById(@PathVariable Integer id){
        return new ResponseEntity<Optional<SecteurActivite>>(secteurActiviteService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "create  new  Secteur Activites", notes = "create a new Secteur Activites")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @PostMapping
    public ResponseEntity<SecteurActivite> newSecteurActivite(@RequestBody SecteurActivite secteurActivite) {
        return new ResponseEntity<SecteurActivite>(secteurActiviteService.newSecteurActivite(secteurActivite), HttpStatus.OK);
    }


    @ApiOperation(value = "delete  Secteur Activites", notes = "delete  Secteur Activites")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id) {
        secteurActiviteService.deleteSecteurActivite(id);
        return new ResponseEntity<>("deleted successfully ...",HttpStatus.OK);
    }

}
