package sn.modelsis.analyseselection.service.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ObservationDTO {

    private Integer idRetour;

    private Date dateRetour;

    private String observations;

    private String auteur;

    private StatutDTO statut;

    private String usernameExpediteur;
}
