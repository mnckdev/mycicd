package sn.modelsis.analyseselection.service.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RapportAnalyseDTO {

    private UUID idRapport;

    private String porteur;

    private String activite;

    private String environnement;

    private String organisation;

    private String faiblesse;

}
