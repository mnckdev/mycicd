package sn.modelsis.analyseselection.service.serviceinterface;


import sn.modelsis.analyseselection.service.DTO.CollectifsDTO;
import sn.modelsis.analyseselection.service.entity.Cohorte;
import sn.modelsis.analyseselection.service.entity.Commune;
import sn.modelsis.analyseselection.service.entity.Statut;

import java.util.List;

public interface CollectifServiceInterface {

    List<CollectifsDTO> findByCohorte(Cohorte cohorte);

    List<CollectifsDTO> findByCohorteAndCommune(Cohorte cohorte, Commune commune);

    List<CollectifsDTO> findAllByCohorteAndCommuneAndStatus(Cohorte cohorte, Commune commune, Statut statut);
}
