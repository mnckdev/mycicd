package sn.modelsis.analyseselection.service.DTO;

import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.*;
import lombok.experimental.SuperBuilder;
import sn.modelsis.analyseselection.service.entity.RapportAnalyse;
import sn.modelsis.analyseselection.service.entity.RapportAnalyse2;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
@EqualsAndHashCode(callSuper=true)
@JsonTypeName("INDIVIDUEL")
public class IndividuelDTO extends PIEDTO {

    private String niveauEtude;

    private String formationProf;

    private String experienceProf;

    private String typeCategorie;

    private TrancheAgeDTO trancheAge;

    //private CommuneDTO communeActuelle;

    private CommuneDTO communeRattach;

    private CategoriesDTO categories;
    // some added fields
    private String horsSenegal;

    private String autreRegion;

    private String situationActuel;

    private String ninea;

    private String numeroRc;

    private String refProfessionnel;

    private String banqueOuSFD;

    private Boolean beneficierSoutien;

    private CommuneDTO commune;

    private RapportAnalyse rapportAnalyse1;

    private RapportAnalyse2 rapportAnalyse2;

    private List<IdeeProjetDTO> ideeProjets;

    private SousCategoriesDTO sousCategorieInd;

    private SousCategoriesDTO sousCategorieMig;

}
