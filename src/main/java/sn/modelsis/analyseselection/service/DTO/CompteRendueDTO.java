package sn.modelsis.analyseselection.service.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class CompteRendueDTO {

    private Integer idCompteRendue;

    private Date dateCompteRendue;

    private DocumentsDTO documents;
}
