package sn.modelsis.analyseselection.service.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class SecteurActivite {
    @Id
    private Integer id ;
    private String code;
    private String libelle;

    @OneToMany(mappedBy = "secteurActivite")
    @JsonIgnore
    private List<Projet> projets;
}
