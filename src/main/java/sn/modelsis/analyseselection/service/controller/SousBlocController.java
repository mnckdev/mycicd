package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.SousBloc;
import sn.modelsis.analyseselection.service.service.SousBlocService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/sousBloc")
@Api(
        value = "SousBloc  REST Endpoint",
        tags = {"SousBloc  Controller (v1)"}
)

public class SousBlocController {


    private final SousBlocService sousBlocService;

    public SousBlocController(SousBlocService sousBlocService) {
        this.sousBlocService = sousBlocService;
    }

    @ApiOperation(value = "find All Sous Bloc", notes = "find all  Sous Bloc")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<SousBloc>> findAll(){
        List<SousBloc> sousBloc = sousBlocService.findAll();
        return new ResponseEntity<List<SousBloc>>(sousBloc, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID Sous Bloc", notes = "find By ID  Sous Bloc")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping("/{id}")
    public ResponseEntity<Optional<SousBloc>> findById(@PathVariable Integer id){
        return new ResponseEntity<Optional<SousBloc>>(sousBlocService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "create  new  Sous Blocs", notes = "create a new Sous Blocs")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })
    
    @PostMapping
    public ResponseEntity<SousBloc> newSousBloc(@RequestBody SousBloc sousBloc) {
        return new ResponseEntity<SousBloc>(sousBlocService.newSousBloc(sousBloc), HttpStatus.OK);
    }

    @ApiOperation(value = "delete  Sous Blocs", notes = "delete  Sous Blocs")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id) {
        sousBlocService.deleteSousBloc(id);
        return new ResponseEntity<>("deleted successfully ...",HttpStatus.OK);
    }


}
