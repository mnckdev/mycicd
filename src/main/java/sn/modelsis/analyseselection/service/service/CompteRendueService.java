package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.CompteRendue;
import sn.modelsis.analyseselection.service.repository.CompteRendueRepository;

import java.util.List;
import java.util.Optional;

@Service
public class CompteRendueService {


    private CompteRendueRepository compteRendueRepository;

    public CompteRendueService(CompteRendueRepository compteRendueRepository){
        this.compteRendueRepository=compteRendueRepository;
    }

    public CompteRendue newCompteRendue(CompteRendue compteRendue){
        return  compteRendueRepository.save(compteRendue);
    }

    public Optional<CompteRendue> findById(Integer id){
        return compteRendueRepository.findById(id);
    }
    public List<CompteRendue> findAll(){
        return compteRendueRepository.findAll();
    }
    public void deleteCompteRendue(Integer id){
        compteRendueRepository.deleteById(id);
    }
}
