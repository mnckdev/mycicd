package sn.modelsis.analyseselection.service.utils;

import sn.modelsis.analyseselection.service.DTO.externalDTO.FilterDto;

public class NullUtility {

    private NullUtility() {
        throw new IllegalStateException("NullUtility class");
    }

    public static boolean isNull(Object object) {
        return object == null;
    }

    public static boolean allFieldArePresent(FilterDto filterDto) {
        return (!filterDto.getCodeCommune().isEmpty() && !filterDto.getListSoutien().isEmpty() && !filterDto.getParcoursPie().isEmpty());
    }

    public static boolean not(boolean b) {
        return !b;
    }

    public static boolean twoFieldArePresent(FilterDto filterDto) {
        return !((filterDto.getParcoursPie().isBlank() && filterDto.getCodeCommune().isBlank())
                || (filterDto.getParcoursPie().isEmpty() && filterDto.getListSoutien().isEmpty())
                || (filterDto.getCodeCommune().isEmpty() && filterDto.getListSoutien().isEmpty())
                );
    }
}
