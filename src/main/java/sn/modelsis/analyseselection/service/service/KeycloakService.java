package sn.modelsis.analyseselection.service.service;

import lombok.extern.slf4j.Slf4j;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.RoleMappingResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import sn.modelsis.analyseselection.service.DTO.ChangePasswordDTO;
import sn.modelsis.analyseselection.service.DTO.UserDTO;
import sn.modelsis.analyseselection.service.DTO.externalDTO.EmailDTO;
import sn.modelsis.analyseselection.service.mapper.KeycloakUserMapper;
import sn.modelsis.analyseselection.service.utils.Constantes;

import javax.ws.rs.core.Response;
import java.security.SecureRandom;
import java.util.*;

@Slf4j
@Service
public class KeycloakService {
    @Value("${notification.host}")
    private String notificationHost;
    @Value("${keycloak.realm}")
    private String realm;
    private final KeycloakUserMapper userMapper;
    private RestTemplate restTemplate = new RestTemplate();
    private final Keycloak keycloak;

    private   String PASSWORD = "";

    public KeycloakService(KeycloakUserMapper userMapper, Keycloak keycloak) {
        this.keycloak = keycloak;
        this.userMapper = userMapper;
    }

    public List<UserDTO> getUserByAttributes(String attributes) {

        List<UserRepresentation> userRepresentations = keycloak
                .realm(realm)
                .users()
                .searchByAttributes(attributes);
        List<UserDTO> userDTOS = userMapper.toUserDTOList(userRepresentations);
        return userDTOS;
    }

    public List<UserDTO> getAllUser() {
        List<UserDTO> userDTOS = new ArrayList<>();
        List<UserRepresentation> userRepresentations = keycloak.realm(realm).users().list();
        try {
            userDTOS = userMapper.toUserDTOList(userRepresentations);
            userDTOS.forEach(userDTO -> userDTO.setRoles(getAllUserRealmRoles(userDTO.getUsername())));
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return userDTOS;
    }

    @Transactional
    public UserDTO createUser(
            UserDTO userRepresentationDto) {
        UserDTO userDTO = new UserDTO();
        if(userRepresentationDto.getUsername() ==null || userRepresentationDto.getEmail()==null)
            throw new IllegalArgumentException("the Email and the Username are Mandatory .");
        try {
            if (userRepresentationDto.getAttributes() != null) {
                userRepresentationDto.getAttributes().put("update_password", List.of("true"));
                userRepresentationDto.getAttributes().put("code-pin", List.of(generateCodePin()));
            }            UserRepresentation userRepresentation = userMapper.toUserRepresentation(userRepresentationDto);
            this.PASSWORD = generateRandomPassword(8);
            userRepresentation.setCredentials(List.of(preparePassword(PASSWORD)));
            userRepresentation.setEnabled(true);
            Response response = keycloak.realm(realm).users().create(userRepresentation);
            if (response.getStatus() == 201 && isNotNull(userRepresentationDto.getRoles())) {
                userRepresentation = getUserByUsername(userRepresentation.getUsername());
                addKeycloakRole(userRepresentation, userRepresentationDto.getRoles());
                try {
                    sendCredentialsByMail(userRepresentation);
                } catch (RestClientException e) {
                    log.error("Rest Client Exception: " + e.getMessage(), e);
                    deleteUser(getUserByUsername(userRepresentation.getUsername()).getId());
                    return null;
                } catch (Exception e) {
                    log.error("An unexpected error occurred: " + e.getMessage(), e);
                }

                userDTO = userMapper.toUserDTO(userRepresentation);
                userDTO.setRoles(getAllUserRealmRoles(userDTO.getUsername()));
                log.info("the user {} is created successfullly ",userRepresentation.getUsername());
            }
            return userDTO;
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return userDTO;
    }

    private String generateCodePin() {
        Random random = new Random();
        return  String.valueOf (random.nextInt(8000)+1000);
    }

    public boolean userExists(String username){
        return ! keycloak.realm(realm).users().search(username, true).isEmpty();
}

    @Transactional
    public UserDTO updateUser(
            UserDTO userRepresentationDto) {
        UserDTO userDTO = new UserDTO();
        try {
            UserRepresentation user = getUserByUsername(userRepresentationDto.getUsername());
            user = updateUserRepresentation(user, userRepresentationDto);
            user = addKeycloakRole(user, userRepresentationDto.getRoles());
            keycloak.realm(realm).users().get(user.getId()).update(user);
            userDTO = userMapper.toUserDTO(user);
            userDTO.setRoles(getAllUserRealmRoles(userDTO.getUsername()));
            return userDTO;
        } catch (Exception e) {
            log.info("error creation the user");
        }
        return userDTO;
    }

    public UserDTO changePasswordKeycloakUser(
            ChangePasswordDTO user) {
        UserDTO userDTO = new UserDTO();
        try {
            UserRepresentation userRepresentation = keycloak.realm(realm).users().get(user.getId()).toRepresentation();
            userRepresentation.getAttributes().put("update_password", List.of("false"));
            keycloak.realm(realm).users().get(userRepresentation.getId()).resetPassword(preparePassword(user.getNewPassword()));
            keycloak.realm(realm).users().get(userRepresentation.getId()).update(userRepresentation);
            return userMapper.toUserDTO(userRepresentation);
        } catch (Exception e) {
            log.info("error  Changing the User Password");
        }
        return userDTO;
    }

    public boolean deleteUser(String id) {
        boolean isDeleted = false;
        try {
            keycloak.realm(realm).users().delete(id);
            isDeleted = true;
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return isDeleted;
    }

    public UserDTO getKeycloakUserById(String id) {
        UserDTO userDTO = new UserDTO();
        try {
            userDTO = userMapper.toUserDTO(keycloak.realm(realm).users().get(id).toRepresentation());
            userDTO.setRoles(getAllUserRealmRoles(userDTO.getUsername()));
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return userDTO;
    }

    public List<String> getAllRoles() {
        List<String> list = new ArrayList<>();
        try {
            List<RoleRepresentation> roleRepresentations = keycloak.realm(realm).roles().list();
            if (!roleRepresentations.isEmpty())
                roleRepresentations.forEach(role -> list.add(role.getName()));
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return list;
    }
    @Transactional
    public void sendCredentialsByMail(
            UserRepresentation user
    ) {

            Map<String, Object> templateVariable = new HashMap<>();
            templateVariable.put("password", PASSWORD);
            templateVariable.put("username", user.getUsername());
            templateVariable.put("firstName", user.getFirstName());
            templateVariable.put("lastName", user.getLastName());
            templateVariable.put("hasCodepin",false);
            templateVariable.put("codePin", user.getAttributes().get("code-pin").get(0));
            if(isSigner(user.getUsername())) {
                templateVariable.put("hasCodepin", true);
            }
            // creating the Email Request
            HttpEntity<EmailDTO> request = new HttpEntity<>(
                    new EmailDTO(
                            Constantes.ADEL_EMAIL,
                            user.getEmail(),
                            Constantes.ADEL_TEMPLATE_CREATE_ACCOUNT,
                            Constantes.ADEL_EMAIL_OBJECT,
                            templateVariable
                    )
            );

            // Sending the Email Requect
            EmailDTO emailDTO = restTemplate.postForObject(
                    notificationHost +Constantes.ADEL_EMAIL_PATH,
                    request,
                    EmailDTO.class
            );
            log.info("Email Send to User  " + emailDTO.getDestinataire());

    }

    private boolean isSigner(String username) {
        return getAllUserRealmRoles(username).stream().anyMatch(userRole-> Constantes.CAN_SIGN_LIST.contains(userRole));
    }


    private CredentialRepresentation preparePassword(String password) {
        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        credentialRepresentation.setValue(password);
        credentialRepresentation.setTemporary(false);
        return credentialRepresentation;
    }

    private CredentialRepresentation preparePassword() {
        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setType(CredentialRepresentation.PASSWORD);
        credentialRepresentation.setValue(PASSWORD);
        credentialRepresentation.setTemporary(false);
        return credentialRepresentation;
    }


    private UserRepresentation getUserByUsername(String username) {
        UserRepresentation user = new UserRepresentation();
        try {
            List<UserRepresentation> users = keycloak.realm(realm).users().search(username, true);
            user = users.get(0);
            return user;
        } catch (Exception e) {
            log.error("User with this username not found !");
        }
        return user;
    }

    private boolean isNotNull(Object object) {
        return object != null;
    }

    private UserRepresentation updateUserRepresentation(UserRepresentation userRepresentation, UserDTO userDTO) {
        if (isNotNull(userDTO.getLastName())) userRepresentation.setLastName(userDTO.getLastName());
        if (isNotNull(userDTO.getFirstName())) userRepresentation.setFirstName(userDTO.getFirstName());
        if (isNotNull(userDTO.getEnabled())) userRepresentation.setEnabled(userDTO.getEnabled());
        if (isNotNull(userDTO.getAttributes())) userRepresentation.setAttributes(userDTO.getAttributes());
        if (isNotNull(userDTO.getEmail())) userRepresentation.setEmail(userDTO.getEmail());
        if (isNotNull(userDTO.getPassword())) {
            userRepresentation.setCredentials(List.of(preparePassword(userDTO.getPassword())));
        }
        userRepresentation.getAttributes().put("update_password", List.of("false"));
        return userRepresentation;
    }


    private UserRepresentation addKeycloakRole(UserRepresentation user, List<String> roles) {
        try {
            UsersResource usersResource = keycloak.realm(realm).users();
            List<RoleRepresentation> existingRoles = usersResource.get(user.getId()).roles().realmLevel().listAvailable();
            if (!roles.isEmpty())
                roles.forEach(roleN -> {
                    RoleRepresentation role = existingRoles.stream()
                            .filter(r -> r.getName().equals(roleN))
                            .findFirst()
                            .orElseThrow(() -> new RuntimeException("Role not found: " + roleN));

                    RoleMappingResource roleMappingResource = usersResource.get(user.getId()).roles();
                    roleMappingResource.realmLevel().add(Collections.singletonList(role));
                    List<String> roleList = getAllRoles();
                    user.setRealmRoles(roleList);
                    usersResource.get(user.getId()).update(user);
                });
        } catch (Exception e) {
            log.info(e.getMessage());
        }

        return user;
    }

    public List<String> getAllUserRealmRoles(String username) {
        RealmResource realmResource = keycloak.realm(realm);
        UsersResource usersResource = realmResource.users();
        List<UserRepresentation> users = usersResource.search(username);
        if (users.isEmpty()) {
            return null;
        }
        UserRepresentation user = users.get(0);
        UserResource userResource = usersResource.get(user.getId());
        List<RoleRepresentation> roles = userResource.roles().realmLevel().listAll();
        List<String> roleNames = new ArrayList<>();
        for (RoleRepresentation role : roles) {
            roleNames.add(role.getName());
        }
        return roleNames;
    }

    public static String generateRandomPassword(int length) {
        final String charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        SecureRandom random = new SecureRandom();
        StringBuilder password = new StringBuilder();

        for (int i = 0; i < length; i++) {
            int randomIndex = random.nextInt(charset.length());
            password.append(charset.charAt(randomIndex));
        }

        return password.toString();
    }
}
