package sn.modelsis.analyseselection.service.DTO.externalDTO.realtimeNotification;

import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class Message {
    private Long id;

    private String codeApp;

    private Date dateCreation;

    private boolean lu;

    private String objet;

    private String contenu;

    private Destination destinataire;

    private String mailDestinataire;

    private String telephoneDestinataire;

    private String typeMessage;

    private String expediteur;

    private String priorite;
}
