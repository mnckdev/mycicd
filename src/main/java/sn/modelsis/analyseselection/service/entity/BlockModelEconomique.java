package sn.modelsis.analyseselection.service.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
public class BlockModelEconomique {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private UUID idBlocME;

    private String libelle;

    private String code;


//    @JsonIgnore
//    @ManyToOne
//    private ModelEconomique modelEconomique;
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(mappedBy = "blockModelEconomique")
    private List<TypeBloc> typeBloc;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    private List<SousBloc> sousBlocs;
}

