package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.ModeleEconomiqueDTO;
import sn.modelsis.analyseselection.service.entity.ModelEconomique;

@Mapper(componentModel = "spring")
public interface ModelEconomiqueMapper extends GenericMapper<ModelEconomique, ModeleEconomiqueDTO>{
}
