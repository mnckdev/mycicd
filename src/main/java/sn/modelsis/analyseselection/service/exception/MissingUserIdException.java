package sn.modelsis.analyseselection.service.exception;

    public class MissingUserIdException extends IllegalArgumentException {

    public MissingUserIdException() {
        super("The user Id is mandatory.");
    }

    public MissingUserIdException(String message) {
        super(message);
    }

    public MissingUserIdException(String message, Throwable cause) {
        super(message, cause);
    }
}
