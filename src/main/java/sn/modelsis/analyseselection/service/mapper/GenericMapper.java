package sn.modelsis.analyseselection.service.mapper;

import java.util.List;

public interface GenericMapper<Entite, DTO>{

    public Entite asEntity(DTO d);

    public DTO asDTO(Entite e);

    public List<Entite> asEntiteList(List<DTO> dtos);

    public List<DTO> asDTOList(List<Entite> entites);

}
