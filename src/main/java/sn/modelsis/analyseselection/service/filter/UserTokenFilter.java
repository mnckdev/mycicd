package sn.modelsis.analyseselection.service.filter;

import org.springframework.core.annotation.Order;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import java.io.IOException;

@Component
@Order(1) // Define the order of the filter in the filter chain
public class UserTokenFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // Initialization code if needed

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        Jwt jwtToken = (Jwt) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userToken = jwtToken.getTokenValue();
        TokenHolder.setAuthToken(userToken);

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        // Cleanup code if needed
    }
}
