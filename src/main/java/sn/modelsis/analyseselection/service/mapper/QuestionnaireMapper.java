package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.QuestionnaireDTO;
import sn.modelsis.analyseselection.service.entity.Questionnaire;

@Mapper(componentModel = "spring")
public interface QuestionnaireMapper extends GenericMapper<Questionnaire, QuestionnaireDTO>{
}
