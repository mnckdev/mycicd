package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.FormeJuridique;

import java.util.UUID;

@Repository
public interface FormeJuridiqueRepository extends JpaRepository<FormeJuridique, UUID> {

    FormeJuridique findFormeJuridiqueByCode(String code);
}
