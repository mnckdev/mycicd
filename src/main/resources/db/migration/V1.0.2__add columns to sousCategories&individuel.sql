-- Add columns marqueur et code to sous_categories
ALTER TABLE public.sous_categories
    ADD COLUMN marqueur INTEGER;
ALTER TABLE public.sous_categories
    ADD COLUMN code character varying(255);

-- Update sous_categories table to set value to column code for existing data
UPDATE public.sous_categories
SET code = 'sous_categories_' || CAST((RANDOM() * 100) + 101 AS VARCHAR(3));

-- Set property not-null to column code
ALTER TABLE public.sous_categories
ALTER COLUMN code TYPE character varying(255) USING code::character varying(255),
ALTER COLUMN code SET NOT NULL;


-- Add columns sous_categorie_ind_id and sous_categorie_mig_id to sous_categories
ALTER TABLE public.individuel
    ADD COLUMN sous_categorie_ind_id INTEGER;
ALTER TABLE public.individuel
    ADD COLUMN sous_categorie_mig_id INTEGER;

-- Add a foreign key constraint to sous_categories
ALTER TABLE public.sous_categories
    ADD CONSTRAINT fk_sous_categorie_marqueur
        FOREIGN KEY (marqueur) REFERENCES public.marqueur(id);

-- Add a foreign key constraint to individuel
ALTER TABLE public.individuel
    ADD CONSTRAINT fk_individuel_sous_categorie_ind
        FOREIGN KEY (sous_categorie_ind_id) REFERENCES public.sous_categories(id);
-- Add a foreign key constraint to individuel
ALTER TABLE public.individuel
    ADD CONSTRAINT fk_individuel_sous_categorie_mig
        FOREIGN KEY (sous_categorie_mig_id) REFERENCES public.sous_categories(id);

-- Add columns CNI to pie table
ALTER TABLE public.pie
    ADD COLUMN cni character varying(255);

INSERT INTO public.statut (id, code, libelle) VALUES (54, 'Récupéré de BADEL', 'Pie enregistré à partir des anciens formulaires de badel');
