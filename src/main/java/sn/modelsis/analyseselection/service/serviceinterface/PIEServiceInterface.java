package sn.modelsis.analyseselection.service.serviceinterface;

import sn.modelsis.analyseselection.service.DTO.*;
import sn.modelsis.analyseselection.service.DTO.externalDTO.FilterDto;
import sn.modelsis.analyseselection.service.entity.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PIEServiceInterface {

    List<PIEDTO> findAllByStatut(Integer id);

    List<PIEDTO> getPIEsInscrits();

    List<PIEDTO> getPIEsInscritsByFilters(FilterDto filterDto);

    List<PIEDTO> getPIEs();

    PIEDTO changePIEStatut(UUID idPIE, StatutDTO statut);

    List<PIEDTO> findAllByCommune(Commune commune);

    Optional<PIEDTO> noterPIE(UUID idPie, List<NoteQuestion> notes);

    Optional<PIEDTO> retournerPIE(UUID idPie, ObservationDTO observation);

    Optional<PIEDTO> creerProjet(UUID idPie, ProjetDTO projet);

    Optional<PIEDTO> concevoirModeleEconomique(UUID idPie, ModeleEconomiqueDTO modeleEconomiqueDTO);

}
