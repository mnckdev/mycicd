package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.Utilisateurs;
import sn.modelsis.analyseselection.service.service.UtilisateursService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/utilisateurs")
@Api(
        value = "Utilisateurs REST Endpoint",
        tags = {"Utilisateurs Controller (v1)"}
)

public class UtilisateursController {


    private final UtilisateursService utilisateursService;

    public UtilisateursController(UtilisateursService utilisateursService) {
        this.utilisateursService = utilisateursService;
    }

    @ApiOperation(value = "find All Utilisateurs", notes = "find all  Utilisateurs")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<Utilisateurs>> findAll(){
        List<Utilisateurs> utilisateurs = utilisateursService.findAll();
        return new ResponseEntity<List<Utilisateurs>>(utilisateurs, HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID Utilisateurs", notes = "find By ID  Utilisateurs")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Utilisateurs>> findById(@PathVariable Integer id){
        return new ResponseEntity<Optional<Utilisateurs>>(utilisateursService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "create  new  Utilisateurss", notes = "create a new Utilisateurss")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })
    @PostMapping
    public ResponseEntity<Utilisateurs> newUtilisateurs(@RequestBody Utilisateurs Utilisateurs) {
        return new ResponseEntity<Utilisateurs>(utilisateursService.newUtilisateurs(Utilisateurs), HttpStatus.OK);
    }

    @ApiOperation(value = "delete  Utilisateurss", notes = "delete  Utilisateurss")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id) {
        utilisateursService.deleteUtilisateurs(id);
        return new ResponseEntity<>("deleted successfully ...", HttpStatus.OK);
    }

}
