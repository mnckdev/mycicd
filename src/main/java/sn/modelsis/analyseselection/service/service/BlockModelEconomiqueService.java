package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.DTO.BlockModelEconomiqueDTO;
import sn.modelsis.analyseselection.service.entity.BlockModelEconomique;
import sn.modelsis.analyseselection.service.mapper.BlockModelEconomiqueMapper;
import sn.modelsis.analyseselection.service.repository.BlockModelEconomiqueRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class BlockModelEconomiqueService {


    private final BlockModelEconomiqueRepository blockModelEconomiqueRepository;

    private final BlockModelEconomiqueMapper blockModelEconomiqueMapper;
    public BlockModelEconomiqueService(
            BlockModelEconomiqueRepository blockModelEconomiqueRepository, BlockModelEconomiqueMapper blockModelEconomiqueMapper){
        this.blockModelEconomiqueRepository=blockModelEconomiqueRepository;
        this.blockModelEconomiqueMapper = blockModelEconomiqueMapper;
    }

    public BlockModelEconomiqueDTO newBlockModelEconomique(BlockModelEconomiqueDTO blockModelEconomiqueDTO){
        BlockModelEconomique blockModelEconomique =
                blockModelEconomiqueMapper.asEntity(blockModelEconomiqueDTO);

        blockModelEconomique = blockModelEconomiqueRepository.save(blockModelEconomique);
        return blockModelEconomiqueMapper.asDTO(blockModelEconomique);
    }

    public BlockModelEconomiqueDTO findById(UUID uuid){
        Optional<BlockModelEconomique> blockModelEconomique = blockModelEconomiqueRepository.findById(uuid);
        if(blockModelEconomique.isPresent())
            return blockModelEconomiqueMapper.asDTO(blockModelEconomique.get());
        return null;
    }
    public List<BlockModelEconomiqueDTO> findAll(){
        List<BlockModelEconomique> blockModelEconomiques = blockModelEconomiqueRepository.findAll();
        return blockModelEconomiqueMapper.asDTOList(blockModelEconomiques);
    }
    public void deleteBlockModelEconomique(UUID uuid){
        blockModelEconomiqueRepository.deleteById(uuid);
    }
}
