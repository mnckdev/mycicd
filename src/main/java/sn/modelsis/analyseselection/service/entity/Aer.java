//package sn.modelsis.analyseETSelection.entity;
//
//import lombok.AllArgsConstructor;
//import lombok.Getter;
//import lombok.NoArgsConstructor;
//import lombok.Setter;
//
//import javax.persistence.Entity;
//import javax.persistence.Id;
//
//@Entity
//@NoArgsConstructor
//@AllArgsConstructor
//@Getter
//@Setter
//public class Aer {
//
//    @Id
//    private Integer id;
//
//    private String prenom;
//
//    private String nom;
//
//    private String sexe;
//
//    private String tel1;
//
//    private String tel2;
//
//    private String email;
//
//    private String commune;
//
//    private int role;
//
//    private String structurePartenaire;
//}
