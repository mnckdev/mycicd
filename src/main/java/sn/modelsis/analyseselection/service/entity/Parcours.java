package sn.modelsis.analyseselection.service.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Parcours {

    @Id
    private Integer id;

    private String libelle;

    @OneToMany(mappedBy = "parcours")
    private List<Marqueur> marqueurs;

    @ManyToOne
    @JsonIgnore
    private SousCategories sousCategories;

}
