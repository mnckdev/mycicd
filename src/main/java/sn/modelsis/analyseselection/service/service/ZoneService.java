package sn.modelsis.analyseselection.service.service;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.DTO.ZoneDTO;
import sn.modelsis.analyseselection.service.entity.Zone;
import sn.modelsis.analyseselection.service.mapper.RegionMapper;
import sn.modelsis.analyseselection.service.mapper.ZoneMapper;
import sn.modelsis.analyseselection.service.repository.RegionRepository;
import sn.modelsis.analyseselection.service.repository.ZoneRepository;

@Service
public class ZoneService {

    private ZoneRepository zoneRepository;
    private final RegionRepository regionRepository;
    private final ZoneMapper zoneMapper;
    private final RegionMapper regionMapper;

    public ZoneService(ZoneRepository zoneRepository, ZoneMapper zoneMapper, 
            RegionRepository regionRepository, RegionMapper regionMapper) {
        this.zoneRepository = zoneRepository;
        this.zoneMapper = zoneMapper;
        this.regionRepository = regionRepository;
        this.regionMapper = regionMapper;
    }

    public Optional<Zone> findById(Integer id) {
        return zoneRepository.findById(id);
    }

    public List<Zone> findAll() {
        return zoneRepository.findAll();
    }

    public Zone create(Zone zone) {
        if (zone == null) {
            return null;
        }
        return zoneRepository.save(zone);
    }

    public Optional<Zone> read(Integer id) {
        if (id == null) {
            return null;
        }
        return zoneRepository.findById(id);
    }

    public List<ZoneDTO> read() {
        List<Zone> zones = zoneRepository.findAll();
        return zoneMapper.asDTOList(zones);
    }

    public void delete(Integer id) {
        Optional<Zone> zone = zoneRepository.findById(id);
        if (zone.isPresent()) {
            zoneRepository.delete(zone.get());
        }
    }

}
