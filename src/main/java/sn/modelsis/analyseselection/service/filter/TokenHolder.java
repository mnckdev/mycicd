package sn.modelsis.analyseselection.service.filter;

public class TokenHolder {
    private static ThreadLocal<String> authTokenThreadLocal = new ThreadLocal<>();

    public static void setAuthToken(String token) {
        authTokenThreadLocal.set(token);
    }

    public static String getAuthToken() {
        return authTokenThreadLocal.get();
    }

    public static void clearAuthToken() {

        authTokenThreadLocal.remove();
    }
}
