package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.DepartementDTO;
import sn.modelsis.analyseselection.service.entity.Departement;

@Mapper(componentModel = "spring")
public interface DepartementMapper extends GenericMapper<Departement, DepartementDTO>{
}
