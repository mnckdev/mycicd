package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.Projet;

import java.util.UUID;

@Repository
public interface ProjetRepository  extends JpaRepository<Projet, UUID> {
}
