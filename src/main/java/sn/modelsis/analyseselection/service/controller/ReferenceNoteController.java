package sn.modelsis.analyseselection.service.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.ReferenceNote;
import sn.modelsis.analyseselection.service.service.ReferenceNoteService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/refernce-note")
@Api(
        value = "ReferenceNote REST endpoint",
        tags = "ReferenceNote controller(v1)"
)

public class ReferenceNoteController {

    private final ReferenceNoteService referenceNoteService;

    public ReferenceNoteController(ReferenceNoteService referenceNoteService) {
        this.referenceNoteService = referenceNoteService;
    }

    @GetMapping
    @ApiOperation(value = "Get all reference note", notes = "Get all reference note")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    public ResponseEntity<List<ReferenceNote>> findAll(){
        List<ReferenceNote> referenceNotes = referenceNoteService.read();
        if(referenceNotes == null)
            return ResponseEntity.badRequest().body(referenceNotes);
        return ResponseEntity.ok().body(referenceNotes);
    }

    @ApiOperation(value = "Get one reference note", notes = "Get one reference note")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<ReferenceNote>> get(@PathVariable Integer id){
        Optional<ReferenceNote> referenceNote = referenceNoteService.read(id);
        if(referenceNote== null)
            return  ResponseEntity.badRequest().body(referenceNote);
        return ResponseEntity.ok().body(referenceNote);
    }

    @PostMapping
    @ApiResponses(
            {
                    @ApiResponse(code=200, message = "created"),
            }
    )
    @ApiOperation(value = "post a reference note", notes = "post a reference note")
    public ResponseEntity<ReferenceNote> create(@RequestBody ReferenceNote referenceNote){
        referenceNote = referenceNoteService.create(referenceNote);
        return ResponseEntity.ok().body(referenceNote);
    }

    @DeleteMapping("/{id}")
    @ApiResponses(
            {
                    @ApiResponse(code=200, message = "delete"),
            }
    )
    @ApiOperation(value = "delete a reference note", notes = "delete a reference note")
    public ResponseEntity<String> delete(@PathVariable Integer id){
        referenceNoteService.delete(id);
        return ResponseEntity.ok().body("deleted");
    }
}
