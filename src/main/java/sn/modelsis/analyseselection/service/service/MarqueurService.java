package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.Marqueur;
import sn.modelsis.analyseselection.service.repository.MarqueurRepository;

import java.util.List;
import java.util.Optional;

@Service
public class MarqueurService {

    private MarqueurRepository marqueurRepository;

    public MarqueurService(MarqueurRepository marqueurRepository){
        this.marqueurRepository=marqueurRepository;
    }


    public Optional<Marqueur> findById(Integer id){
        return marqueurRepository.findById(id);
    }
    public List<Marqueur> findAll(){
        return marqueurRepository.findAll();
    }

}
