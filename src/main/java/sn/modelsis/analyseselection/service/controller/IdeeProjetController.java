package sn.modelsis.analyseselection.service.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.IdeeProjet;
import sn.modelsis.analyseselection.service.service.IdeeProjetService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/idee-projet")
@Api(
        value = "Idee de projet REST endpoint",
        tags = {"IdeeProjet controller(v1)"}
)

public class IdeeProjetController {

    private final IdeeProjetService ideeProjetService;

    public IdeeProjetController(IdeeProjetService ideeProjetService) {
        this.ideeProjetService = ideeProjetService;
    }

    @GetMapping
    @ApiOperation(value = "Get all idees de projet", notes = "Get all idees de projet")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    public ResponseEntity<List<IdeeProjet>> findAll(){
        List<IdeeProjet> ideeProjets = ideeProjetService.read();
        if(ideeProjets == null)
            return ResponseEntity.badRequest().body(ideeProjets);
        return ResponseEntity.ok().body(ideeProjets);
    }

    @ApiOperation(value = "Get one idee de projet", notes = "Get one idee de projet")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<IdeeProjet>> get(@PathVariable UUID id){
        Optional<IdeeProjet> ideeProjet = ideeProjetService.read(id);
        if(ideeProjet== null)
            return  ResponseEntity.badRequest().body(ideeProjet);
        return ResponseEntity.ok().body(ideeProjet);
    }

    @PostMapping
    @ApiResponses(
            {
                    @ApiResponse(code=200, message = "created"),
            }
    )
    @ApiOperation(value = "post a idee de projet", notes = "post a idee de projet")
    // Devrait changer le retour de ce controller
    public ResponseEntity<IdeeProjet> create(@RequestBody IdeeProjet ideeProjet){
        ideeProjet = ideeProjetService.create(ideeProjet);
        return ResponseEntity.ok().body(ideeProjet);
    }

    @DeleteMapping("/{id}")
    @ApiResponses(
            {
                    @ApiResponse(code=200, message = "delete"),
            }
    )
    @ApiOperation(value = "delete a idee de projet", notes = "delete a  idee de projet")
    public ResponseEntity<String> delete(@PathVariable UUID id){
        ideeProjetService.delete(id);
        return ResponseEntity.ok().body("deleted");
    }


}
