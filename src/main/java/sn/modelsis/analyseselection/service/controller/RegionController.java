package sn.modelsis.analyseselection.service.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.Region;
import sn.modelsis.analyseselection.service.mapper.RegionMapper;
import sn.modelsis.analyseselection.service.service.RegionService;

import java.util.List;
import java.util.Optional;
import sn.modelsis.analyseselection.service.DTO.RegionDTO;

@RestController
@RequestMapping("/api/v1/region")
@Api(
        value = "Region REST Endpoint",
        tags = {"Region Controller (v1)"}
)

public class RegionController {

    private final RegionService regionService;
    private final RegionMapper regionMapper;

    public RegionController(RegionService regionService, RegionMapper regionMapper) {
        this.regionService = regionService;
        this.regionMapper = regionMapper;
    }

    @ApiOperation(value = "find All Region", notes = "find all  Region")
    @ApiResponses({
        @ApiResponse(code = 200, message = "success"),
        @ApiResponse(code = 400, message = "Bad request"),
        @ApiResponse(code = 500, message = "Server Error")
    })

    @GetMapping
    public ResponseEntity<List<RegionDTO>> findAll() {
        List<Region> region = regionService.findAll();

        return new ResponseEntity<>(regionMapper.asDTOList(region), HttpStatus.OK);
    }

    @ApiOperation(value = "find By ID Region", notes = "find By ID  Region")
    @ApiResponses({
        @ApiResponse(code = 200, message = "success"),
        @ApiResponse(code = 400, message = "Bad request"),
        @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping("/{id}")
    public ResponseEntity<Optional<Region>> findById(@PathVariable Integer id) {
        return new ResponseEntity<Optional<Region>>(regionService.findById(id), HttpStatus.OK);
    }

    @PostMapping
    @ApiResponses(
            {
                @ApiResponse(code = 200, message = "created"),}
    )
    @ApiOperation(value = "post a Region", notes = "post a Region")
    public ResponseEntity<Region> create(@RequestBody Region region) {
        region = regionService.create(region);
        return ResponseEntity.ok().body(region);
    }

    @DeleteMapping("/{id}")
    @ApiResponses(
            {
                @ApiResponse(code = 200, message = "delete"),}
    )
    @ApiOperation(value = "delete a Region", notes = "delete a  Region")
    public ResponseEntity<String> delete(@PathVariable Integer id) {
        regionService.delete(id);
        return ResponseEntity.ok().body("deleted");
    }

    @GetMapping("/zone/{idZone}")
    public ResponseEntity<List<RegionDTO>> findAllRegionsByZone(@PathVariable Integer idZone) {
        return ResponseEntity.ok().body(regionService.findAllRegionsByZone(idZone));
    }

    @GetMapping("/zone/{idZone}/{idRegion}")
    public ResponseEntity<List<RegionDTO>> findAllRerrgionsByZone(@PathVariable Integer idZone,
            @PathVariable Integer idRegion) {
        return ResponseEntity.ok().body(regionService.findRemainListRegionsByZone(idZone, idRegion));
    }

    @PostMapping("/zone/{idZone}/{idRegion}")
    public ResponseEntity<List<RegionDTO>> addListRegionsByZone(@PathVariable Integer idZone,
            @PathVariable Integer idRegion) {
        return ResponseEntity.ok().body(regionService.addListRegionsByZone(idZone, idRegion));
    }

}
