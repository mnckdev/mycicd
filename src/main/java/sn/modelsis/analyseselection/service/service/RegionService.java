package sn.modelsis.analyseselection.service.service;

import java.util.ArrayList;
import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.Region;
import sn.modelsis.analyseselection.service.repository.RegionRepository;

import java.util.List;
import java.util.Optional;

import sn.modelsis.analyseselection.service.DTO.RegionDTO;
import sn.modelsis.analyseselection.service.entity.Zone;
import sn.modelsis.analyseselection.service.mapper.RegionMapper;
import sn.modelsis.analyseselection.service.repository.ZoneRepository;

@Service
public class RegionService {

    private final RegionRepository regionRepository;
    private final ZoneRepository zoneRepository;
    private final RegionMapper regionMapper;

    public RegionService(RegionRepository regionRepository, ZoneRepository zoneRepository, RegionMapper regionMapper) {
        this.regionRepository = regionRepository;
        this.zoneRepository = zoneRepository;
        this.regionMapper = regionMapper;
    }

    public Optional<Region> findById(Integer id) {
        return regionRepository.findById(id);
    }

    public Region create(Region region) {
        if (region == null) {
            return null;
        }
        return regionRepository.save(region);
    }

    public Optional<Region> read(Integer id) {
        if (id == null) {
            return null;
        }
        return regionRepository.findById(id);
    }

    public List<RegionDTO> read() {
        List<Region> pies = regionRepository.findAll();
        return regionMapper.asDTOList(pies);
    }

    public void delete(Integer id) {
        Optional<Region> region = regionRepository.findById(id);
        if (region.isPresent()) {
            regionRepository.delete(region.get());
        }
    }

    public List<Region> findAll() {
       var returns =  regionRepository.findAll();
        return regionRepository.findAll();
    }

//    @Override
    public List<RegionDTO> findAllRegionsByZone(Integer id) {
        Zone zone = zoneRepository.findById(id).get();
        List<Region> regions = regionRepository.findAllRegionsByZone(zone);
        List<RegionDTO> regiondtos = regionMapper.asDTOList(regions);
        return regiondtos;
    }

//    @Override
    public List<RegionDTO> findRemainListRegionsByZone(Integer idzone, Integer idRegion) {
        Zone zone = zoneRepository.findById(idzone).get();
        Region regionSelect = regionRepository.findById(idRegion).get();
        List<Region> listRegions = regionRepository.findAllRegionsByZone(zone);

        for (Region region : listRegions) {
            if (region != null) {
                regionSelect.setZone(null);
            }
        }

        regionRepository.saveAll(listRegions);

        return regionMapper.asDTOList(listRegions);
    }

    public List<RegionDTO> addListRegionsByZone(Integer idZone, Integer idRegion) {
        Zone zone = zoneRepository.findById(idZone).get();
        List<Region> listRegionsZone = new ArrayList();
        Region regionSelect = regionRepository.findById(idRegion).get();

        List<Region> listRegions = regionRepository.findAll();

        for (Region region : listRegions) {
            if (region != null) {
                regionSelect.setZone(zone);
            }

        }

        listRegionsZone.add(regionSelect);

        regionRepository.saveAll(listRegionsZone);

        return regionMapper.asDTOList(listRegionsZone);
    }

}
