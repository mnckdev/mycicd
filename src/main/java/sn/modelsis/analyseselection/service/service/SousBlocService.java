package sn.modelsis.analyseselection.service.service;

import org.springframework.stereotype.Service;
import sn.modelsis.analyseselection.service.entity.SousBloc;
import sn.modelsis.analyseselection.service.repository.SousBlocRepository;

import java.util.List;
import java.util.Optional;

@Service
public class SousBlocService {

    private SousBlocRepository sousBlocRepository;

    public SousBlocService(SousBlocRepository sousBlocRepository){
        this.sousBlocRepository=sousBlocRepository;
    }

    public SousBloc newSousBloc(SousBloc sousBloc){
        return  sousBlocRepository.save(sousBloc);
    }

    public Optional<SousBloc> findById(Integer id){
        return sousBlocRepository.findById(id);
    }
    public List<SousBloc> findAll(){
        return sousBlocRepository.findAll();
    }
    public void deleteSousBloc(Integer id){
        sousBlocRepository.deleteById(id);
    }
}
