package sn.modelsis.analyseselection.service.mapper;

import org.mapstruct.Mapper;
import sn.modelsis.analyseselection.service.DTO.SousBlocDTO;
import sn.modelsis.analyseselection.service.entity.SousBloc;

@Mapper(componentModel = "spring")
public interface SousBlocMapper extends GenericMapper<SousBloc, SousBlocDTO> {
}
