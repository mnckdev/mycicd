package sn.modelsis.analyseselection.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.SousCategories;

import java.util.Optional;

@Repository
public interface SousCategoriesRepository extends JpaRepository<SousCategories, Integer> {
    Optional<SousCategories> findSousCategoriesByCode(String code);
}
