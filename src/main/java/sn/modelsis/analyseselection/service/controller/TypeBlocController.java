package sn.modelsis.analyseselection.service.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.modelsis.analyseselection.service.entity.TypeBloc;
import sn.modelsis.analyseselection.service.service.TypeBlocService;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/typeBloc")
@Api(
        value = "TypeBloc  REST Endpoint",
        tags = {"TypeBloc  Controller (v1)"}
)

public class TypeBlocController {


    private final TypeBlocService typeBlocService;

    public TypeBlocController(TypeBlocService typeBlocService) {
        this.typeBlocService = typeBlocService;
    }

    @ApiOperation(value = "find All Type Bloc", notes = "find all  Type Bloc")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })
    @GetMapping
    public ResponseEntity<List<TypeBloc>> findAll(){
        List<TypeBloc> typeBloc = typeBlocService.findAll();
        return new ResponseEntity<List<TypeBloc>>(typeBloc, HttpStatus.OK);
    }
    @ApiOperation(value = "find By ID Type Bloc", notes = "find By ID  Type Bloc")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
            @ApiResponse(code = 500, message = "Server Error")
    })


    @GetMapping("/{id}")
    public ResponseEntity<Optional<TypeBloc>> findById(@PathVariable Integer id){
        return new ResponseEntity<Optional<TypeBloc>>(typeBlocService.findById(id), HttpStatus.OK);
    }

    @ApiOperation(value = "create  new  Type Blocs", notes = "create a new Type Blocs")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @PostMapping
    public ResponseEntity<TypeBloc> newTypeBloc(@RequestBody TypeBloc typeBloc) {
        return new ResponseEntity<TypeBloc>(typeBlocService.newTypeBloc(typeBloc), HttpStatus.OK);
    }



    @ApiOperation(value = "delete  Type Blocs", notes = "delete  Type Blocs")
    @ApiResponses({
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 400, message = "Bad request"),
    })

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Integer id) {
        typeBlocService.deleteTypeBloc(id);
        return new ResponseEntity<>("deleted successfully ...",HttpStatus.OK);
    }

}
