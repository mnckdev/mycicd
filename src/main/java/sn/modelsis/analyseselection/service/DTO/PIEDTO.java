package sn.modelsis.analyseselection.service.DTO;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@SuperBuilder
/*@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "typePIE"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = IndividuelDTO.class, name = "INDIVIDUEL"),
        @JsonSubTypes.Type(value = CollectifsDTO.class, name = "COLLECTIF")
})*/
public class PIEDTO {

    private UUID idPIE;

    private String prenom;

    private String typePIE;

    private String sexe;

    private String nom;

    private String tel1;

    private String tel2;

    private String quartier;

    private Date date;

    private StatutDTO statut;

    private List<NoteQuestionDTO> noteQuestions;

    private List<RencontreDTO> rencontres;

    private ProjetDTO projet;

    private CohorteDTO cohorte;

    private List<ObservationDTO> retours;

    private String parcours;

    private String inscritpar;

    private List<TypeSoutienDTO> listSoutiens;

    private TypeSoutienDTO soutiensImmediat;

    private String domainActivite;

    private String secteurActivite;

    private String cni;

}
