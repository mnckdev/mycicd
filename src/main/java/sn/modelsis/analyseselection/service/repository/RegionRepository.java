package sn.modelsis.analyseselection.service.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sn.modelsis.analyseselection.service.entity.Region;
import sn.modelsis.analyseselection.service.entity.Zone;

@Repository
public interface RegionRepository extends JpaRepository<Region, Integer> {
        public List<Region> findAllRegionsByZone(Zone zone);

    Optional<Region> findRegionAgeByCode(String code);
}


