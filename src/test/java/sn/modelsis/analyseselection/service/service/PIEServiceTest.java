package sn.modelsis.analyseselection.service.service;

import org.jeasy.random.EasyRandom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import sn.modelsis.analyseselection.service.entity.PIE;
import sn.modelsis.analyseselection.service.entity.Statut;
import sn.modelsis.analyseselection.service.repository.PIERepository;
import sn.modelsis.analyseselection.service.repository.StatutRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;


@ExtendWith(MockitoExtension.class)
class PIEServiceTest {
    private final EasyRandom generator = new EasyRandom();
    @Mock
    private PIERepository pieRepository;
    @Mock
    private StatutRepository statutRepository;

    @InjectMocks
    private PIEService pieService;
    @BeforeEach
    void setUp() {
    }

    @Test
    void findAllByStatutCode() {
        //GIVEN
        PIE pie1 =  generator.nextObject(PIE.class);
        PIE pie2 =  generator.nextObject(PIE.class);
        Statut statut =  generator.nextObject(Statut.class);
        List<PIE> pies = Arrays.asList(pie1, pie2);

        //WHEN
        doReturn(statut).when(statutRepository).findStatutByCode("code");
        doReturn(pies).when(pieRepository).findAllByStatut(statut);
        Optional<List<PIE>> result = pieService.findAllByStatutCode("code");

        //VERIFY
        assertThat(result).isNotEmpty();
        UUID pieId1 = pie1.getIdPIE();
        UUID pieId2 = pie2.getIdPIE();
        assertThat(result.get().stream().anyMatch(p -> p.getIdPIE().equals(pieId1))).isTrue();
        assertThat(result.get().stream().anyMatch(p -> p.getIdPIE().equals(pieId2))).isTrue();
    }

    @Test
    void getPieInIds() {
        //GIVEN
        PIE pie1 =  generator.nextObject(PIE.class);
        PIE pie2 =  generator.nextObject(PIE.class);
        List<PIE> pies = Arrays.asList(pie1, pie2);
        List<UUID> piesIds = List.of(pie1.getIdPIE(),pie2.getIdPIE());

        //WHEN
        doReturn(pies).when(pieRepository).findAllByIdPIEIsIn(piesIds);
        List<PIE> result = pieService.getPieInIds(piesIds);

        //VERIFY
        assertThat(result).isNotEmpty();
        UUID pieId1 = pie1.getIdPIE();
        UUID pieId2 = pie2.getIdPIE();
        assertThat(result.stream().anyMatch(p -> p.getIdPIE().equals(pieId1))).isTrue();
        assertThat(result.stream().anyMatch(p -> p.getIdPIE().equals(pieId2))).isTrue();
    }

    @Test
    void isCniPieAlreadyExist() {
        //GIVEN
        PIE pie =  generator.nextObject(PIE.class);
        Optional<PIE> foundPie = Optional.of(pie);

        //WHEN
        doReturn(foundPie).when(pieRepository).findByCni("cni");
        Boolean result = pieService.isCniPieAlreadyExist("cni");
        //VERIFY
        assertThat(result).isTrue();
    }
}