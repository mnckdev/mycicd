package sn.modelsis.analyseselection.service.workfow;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.client.ExternalTaskClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class TopicPollingService {

  private static final String RECUPERATION_PIES_INSCRITS = "recuperation-pies_inscrits";

  @Value("${camunda.client.base-url}")
  private String camundaEngineRestURL;

  @Value("${camunda.client.lock-duration}")
  private int lockDuration;

  private TopicService workflowService;

  public TopicPollingService(TopicService workflowService) {
    this.workflowService = workflowService;
  }

  @EventListener(ContextRefreshedEvent.class)


  public void executeExternalTasks() {

    if (camundaEngineRestURL == null || camundaEngineRestURL.isEmpty()) {
      log.warn("POLLING_NOT_SET_UP");
      return;
    }
    ExternalTaskClient client =
            ExternalTaskClient.create().baseUrl(camundaEngineRestURL).maxTasks(1).build();


    log.info("POLLING_INITIATED");

    client
            .subscribe(RECUPERATION_PIES_INSCRITS)
            .lockDuration(lockDuration)
            .handler(
                    (externalTask, externalTaskService) -> {
                        try {
                            workflowService.recuperationListePIE(externalTask, externalTaskService);
                        } catch (JsonProcessingException e) {
                            throw new RuntimeException(e);
                        }
                    }).open();


    client
            .subscribe("persistance-et-mis-a-jour-pie")
            .lockDuration(lockDuration)
            .handler(
                    ((externalTask, externalTaskService) -> {
                      try {
                        workflowService.persistance_et_mis_a_jour_pie(externalTask, externalTaskService);
                      }
                      catch (Exception e) {
                        e.printStackTrace();
                      }
                    })
            ).open();

    client
            .subscribe("embarquer-la-commune-variable")
            .lockDuration(lockDuration)
            .handler((
                    (((externalTask, externalTaskService) -> {
                        try {
                            workflowService.definitionVariableCommune(
                                    externalTask, externalTaskService
                            );
                        } catch (JsonProcessingException e) {
                            throw new RuntimeException(e);
                        }
                    }))
                    )).open();

      client
              .subscribe("creation-compte-pie")
              .lockDuration(lockDuration)
              .handler((
                      (((externalTask, externalTaskService) -> {
                          try {
                              workflowService.executerCreationComptePIE(
                                      externalTask, externalTaskService
                              );
                          } catch (JsonProcessingException e) {
                              throw new RuntimeException(e);
                          }
                      }))
              )).open();

      client
              .subscribe("notifier_acteurs_de_la_rencontre")
              .lockDuration(lockDuration)
              .handler((
                      (((externalTask, externalTaskService) -> {
                          try {
                              workflowService.executerNotifierActeursRencontre(
                                      externalTask, externalTaskService
                              );
                          } catch (JsonProcessingException e) {
                              throw new RuntimeException(e);
                          }
                      }))
              )).open();
  }
}
